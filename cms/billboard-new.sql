-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 26, 2017 at 04:53 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `billboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `billboards`
--

CREATE TABLE `billboards` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_billboard` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_provinsi` int(10) UNSIGNED NOT NULL,
  `id_kabupaten` int(10) UNSIGNED NOT NULL,
  `id_kecamatan` int(10) UNSIGNED DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `panjang` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lebar` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lighting` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `side` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'v: vertical, h: horizontal',
  `bahan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL COMMENT 'vendor',
  `harga` int(10) UNSIGNED NOT NULL,
  `view_count` int(10) UNSIGNED NOT NULL,
  `verify` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0: unverified, 1: verified, 2: rejected',
  `note` text COLLATE utf8mb4_unicode_ci,
  `reject_note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `billboards`
--

INSERT INTO `billboards` (`id`, `kode_billboard`, `id_provinsi`, `id_kabupaten`, `id_kecamatan`, `address`, `longitude`, `latitude`, `panjang`, `lebar`, `lighting`, `side`, `format`, `bahan`, `id_user`, `harga`, `view_count`, `verify`, `note`, `reject_note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(24, 'ACH001', 11, 1171, 117105, 'Jl. Mr. Dr. Muhammad Hasan No. 8', '95.33033449999994', '5.541884700000001', '12', '13', 'Frontlight', '2', 'v', 'Kayu', 78, 135000000, 0, '1', NULL, NULL, '2017-09-15 06:54:52', '2017-09-15 13:42:25', NULL),
(25, 'ACH002', 11, 1171, 117101, 'JL T.ISKANDAR SIMPANG ILIE ULEE', '95.34737489999998', '5.5521493', '10', '22', 'Frontlight', '3', 'h', 'Kayu', 78, 130000000, 0, '1', NULL, NULL, '2017-09-15 07:07:18', '2017-09-15 13:42:14', NULL),
(26, 'MDN001', 12, 1271, 127103, 'Jl.Gagak Hitam, Ringroad, Setia Budi', '98.62396999999999', '3.568821', '1', '3', 'Backlight', '3', 'h', 'Kayu', 78, 200000000, 0, '1', NULL, NULL, '2017-09-15 07:09:01', '2017-09-15 13:35:53', NULL),
(27, 'MDN002', 12, 1271, 127114, 'JLN. LETDA SUJONO NO 131', '98.70730200000003', '3.598056999999999', '4', '3', 'Backlight', '2', 'v', 'Kayu', 78, 135000000, 0, '1', NULL, NULL, '2017-09-15 07:10:25', '2017-09-15 13:41:56', NULL),
(28, 'SIA001', 12, 1272, 127207, 'Jl. Kartini Bawah No. 2', '99.05829889999995', '2.9580367', '2', '3', 'Frontlight', '4', 'h', 'Kayu', 78, 130000000, 0, '1', NULL, NULL, '2017-09-15 07:13:17', '2017-09-15 13:35:51', NULL),
(29, 'SIA002', 12, 1272, 127206, 'Jalan medan km.4,5 no.45,', '99.07967400000007', '2.977788', '7', '8', 'Backlight', '4', 'h', 'Stainless', 79, 489000000, 0, '1', NULL, NULL, '2017-09-15 07:15:38', '2017-09-15 13:46:03', NULL),
(30, 'PKU001', 14, 1471, 147101, 'Jl. Nuri No. 28 Sukajadi Pekanbaru', '101.43538339999998', '0.5157508', '5', '7', 'Frontlight', '2', 'h', 'Kayu', 79, 133000000, 0, '1', NULL, NULL, '2017-09-15 07:17:26', '2017-09-15 13:35:50', NULL),
(31, 'PKU002', 14, 1471, 147102, 'Jl. Aur Kuning No.1 Pekanbaru', '101.45708850000005', '0.4555682', '3', '5', 'Frontlight', '3', 'h', 'Besi', 79, 200000000, 0, '1', NULL, NULL, '2017-09-15 07:19:21', '2017-09-15 13:35:49', NULL),
(32, 'BTM001', 21, 2171, 217103, 'Tiban Bukit Asri Blok P23', '103.96020320000002', '1.1064524', '3', '4', 'Frontlight', '5', 'h', 'Kayu', 79, 133000000, 0, '1', NULL, NULL, '2017-09-15 07:21:05', '2017-09-15 13:35:48', NULL),
(33, 'BTM002', 21, 2171, 217103, 'Jl Laks L RE Martadinata Batam', '103.95395499999995', '1.105881', '5', '5', 'Frontlight', '3', 'h', 'Kayu', 79, 512000000, 0, '1', NULL, NULL, '2017-09-15 07:23:54', '2017-09-15 13:35:47', NULL),
(34, 'JMB001', 15, 1571, 157103, 'JL. RADEN WIJAYA RT 27', '103.62493380000001', '-1.637068', '12', '12', 'Frontlight', '3', 'h', 'Kayu', 80, 200000000, 0, '1', NULL, NULL, '2017-09-15 07:26:17', '2017-09-15 13:47:26', NULL),
(35, 'JMB002', 15, 1571, 157102, 'Jln. Jendral Sudirman', '103.62066370000002', '-1.6057763', '12', '7', 'Backlight', '1', 'h', 'Aluminium', 80, 132000000, 0, '1', NULL, NULL, '2017-09-15 07:28:28', '2017-09-15 13:47:13', NULL),
(36, 'LMP001', 18, 1871, 187110, 'Jl. Karya Bakti, Sinar Harapan', '105.25854900000002', '-5.347303999999999', '12', '7', 'Backlight', '2', 'h', 'Kayu', 80, 122000000, 0, '1', NULL, NULL, '2017-09-15 07:31:11', '2017-09-15 13:47:02', NULL),
(37, 'LMP002', 18, 1871, 187103, 'Jl. Panglima Polim No. 7 Segala Mider', '105.249417', '-5.387705', '5', '7', 'Frontlight', '3', 'h', 'Kayu', 80, 135000000, 0, '1', NULL, NULL, '2017-09-15 07:33:28', '2017-09-15 13:46:49', NULL),
(38, 'PLM001', 16, 1671, 167115, 'Jl Soekarno Hatta Palembang', '104.70533769999997', '-2.959524', '5', '3', 'Frontlight', '3', 'h', 'Kayu', 80, 135000000, 0, '1', NULL, NULL, '2017-09-15 07:36:10', '2017-09-15 13:23:17', NULL),
(39, 'PLM002', 16, 1671, 167113, 'Jl Tembus Patal Pusri', '104.754145', '-2.940298', '3', '4', 'Backlight', '3', 'h', 'Kayu', 81, 135000000, 0, '1', NULL, NULL, '2017-09-15 07:38:42', '2017-09-15 13:23:22', NULL),
(40, 'JKT001', 31, 3174, 317401, 'Jl. Taman Margasatwa No. 12,', '106.82445299999995', '-6.289766999999999', '3', '6', 'Frontlight', '4', 'v', 'Aluminium', 81, 215000000, 0, '1', NULL, NULL, '2017-09-15 07:41:07', '2017-09-15 13:50:36', NULL),
(41, 'JKT002', 31, 3174, 317401, 'JL. Tebet Raya No. 84, Tebet', '106.85697989999994', '-6.2277855', '12', '3', 'Frontlight', '4', 'h', 'Besi', 81, 135000000, 0, '1', NULL, NULL, '2017-09-15 12:09:53', '2017-09-15 13:52:48', NULL),
(42, 'BGR001', 32, 3271, 327105, 'Jl. Karadenan Kaumpandak No.', '106.81646699999999', '-6.523929', '21', '33', 'Backlight', '3', 'h', 'Stainless', 81, 455000000, 0, '1', NULL, NULL, '2017-09-15 12:12:20', '2017-09-15 13:52:37', NULL),
(43, 'BGR002', 32, 3271, 327105, 'JL RAYA BOGOR,KEDUNG HALANG', '106.82144300000004', '-6.5513014', '2', '3', 'Frontlight', '3', 'v', 'Kayu', 81, 212000000, 0, '1', NULL, NULL, '2017-09-15 12:13:30', '2017-09-15 13:50:24', NULL),
(44, 'BKS001', 32, 3275, 327501, 'Dewi Kunti II no.2 Bekasi', '106.97859560000006', '-6.2427983', '12', '3', 'Frontlight', '2', 'h', 'Besi Ringan', 82, 223000000, 0, '1', NULL, NULL, '2017-09-15 12:15:27', '2017-09-15 13:09:47', NULL),
(45, 'BKS002', 32, 3271, 327101, 'Jl. Raya Hankam Pondok Gede', '106.91502660000003', '-6.288352199999999', '3', '4', 'Frontlight', '4', 'v', 'Kayu', 82, 313000000, 0, '1', NULL, NULL, '2017-09-15 12:16:56', '2017-09-15 13:53:27', NULL),
(46, 'BDG001', 32, 3273, 327323, 'jl.moch toha no.306 Bandung', '107.61110480000002', '-6.9535676', '13', '4', 'Backlight', '2', 'h', 'Besi', 82, 135000000, 0, '1', NULL, NULL, '2017-09-15 12:18:31', '2017-09-15 13:09:30', NULL),
(47, 'BDG002', 32, 3273, 327301, 'Jl. Merkuri Utara XV No. 11 Bandung', '107.66957560000003', '-6.944842', '3', '4', 'Backlight', '4', 'h', 'Baja', 82, 112000000, 0, '1', NULL, NULL, '2017-09-15 12:19:40', '2017-09-15 13:09:29', NULL),
(48, 'CRB001', 32, 3209, 320902, 'Jalan Pinus 2 No.31 Cirebon', '108.51419399999997', '-6.761755', '2', '5', 'Frontlight', '3', 'h', 'Kayu', 82, 135000000, 0, '1', NULL, NULL, '2017-09-15 12:21:34', '2017-09-15 13:09:26', NULL),
(49, 'CRB002', 32, 3274, 327401, 'Jalan Ampera IV No. 2 Kelurahan', '108.55647169999997', '-6.7168139', '3', '7', 'Frontlight', '3', 'v', 'Kayu', 83, 314000000, 0, '1', NULL, NULL, '2017-09-15 12:23:25', '2017-09-15 13:55:58', NULL),
(50, 'TSK001', 32, 3206, 320636, 'JL. RAYA TIMUR 157 CIAWI', '108.15139699999997', '-7.159552', '3', '4', 'Frontlight', '4', 'h', 'Stainless', 83, 342000000, 0, '1', NULL, NULL, '2017-09-15 12:25:21', '2017-09-15 13:55:44', NULL),
(51, 'TSK002', 32, 3278, 327806, 'JL. KHOER AFFANDI - CIBEREUM', '108.25335560000008', '-7.3549583', '4', '5', 'Frontlight', '2', 'h', 'Aluminium', 83, 315000000, 0, '1', NULL, NULL, '2017-09-15 12:27:28', '2017-09-15 13:55:31', NULL),
(52, 'PUR001', 33, 3302, 330226, 'Jl. Gatot Subroto No.107,', '109.23369130000003', '-7.420729799999999', '13', '21', 'Frontlight', '4', 'v', 'Baja', 83, 515000000, 0, '1', NULL, NULL, '2017-09-15 12:30:07', '2017-09-15 13:55:18', NULL),
(53, 'PUR002', 33, 3302, 330226, 'Jl. Jend. Soedirman No.299', '109.23028299999999', '-7.425087199999998', '3', '2', 'Frontlight', '2', 'h', 'Kayu', 83, 221000000, 0, '1', NULL, NULL, '2017-09-15 12:31:23', '2017-09-15 13:55:03', NULL),
(54, 'SMR001', 33, 3374, 337408, 'Jalan Sultan Agung No.144,', '110.41949629999999', '-7.018782099999999', '10', '11', 'Frontlight', '3', 'h', 'Besi', 84, 513200000, 0, '1', NULL, NULL, '2017-09-15 12:33:44', '2017-09-15 13:58:32', NULL),
(55, 'SMR002', 33, 3374, 337401, 'Jl. Pemuda No.148, Sekayu,', '110.41263320000007', '-6.9811881', '2', '3', 'Frontlight', '3', 'h', 'Kayu', 84, 215000000, 0, '1', NULL, NULL, '2017-09-15 12:34:50', '2017-09-15 13:58:21', NULL),
(56, 'SBY001', 35, 3578, 357818, 'Gg. X No.19, Lidah Wetan', '112.67119539999999', '-7.3071518', '3', '2', 'Frontlight', '1', 'h', 'Kayu', 84, 215000000, 0, '1', NULL, NULL, '2017-09-15 12:36:52', '2017-09-15 13:58:10', NULL),
(57, 'SBY002', 35, 3578, 357801, 'Jl. Tambak Asri No. 63 Surabaya', '112.71158600000001', '-7.233127000000001', '3', '5', 'Frontlight', '3', 'h', 'Baja Ringan', 84, 135000000, 0, '1', NULL, NULL, '2017-09-15 12:38:12', '2017-09-15 13:09:11', NULL),
(58, 'MLG001', 35, 3573, 357302, 'Jl. Jaksa Agung Suprapto No.19', '112.63153239999997', '-7.9663802', '13', '12', 'Frontlight', '4', 'h', 'Stainless', 84, 555000000, 0, '1', NULL, NULL, '2017-09-15 12:39:36', '2017-09-15 13:57:58', NULL),
(59, 'MLG002', 35, 3573, 357301, 'Jalan Simpang Laksda Adi Sucipto', '112.65674030000002', '-7.957225200000001', '2', '3', 'Frontlight', '3', 'h', 'Aluminium', 85, 742000000, 0, '1', NULL, NULL, '2017-09-15 12:41:02', '2017-09-15 14:00:27', NULL),
(60, 'PTK001', 61, 6171, 617101, 'Jln. Jendral Ahmad Yani', '109.35566169999993', '-0.06293100000000001', '7', '9', 'Backlight', '4', 'h', 'Kayu', 85, 150000000, 0, '1', NULL, NULL, '2017-09-15 12:43:45', '2017-09-15 14:00:39', NULL),
(61, 'PTK002', 61, 6171, 617103, 'Jl. Gajah Mada No.89', '109.34372269999994', '-0.039115', '12', '22', 'Frontlight', '1', 'h', 'Kayu', 85, 342516000, 0, '1', NULL, NULL, '2017-09-15 12:45:13', '2017-09-15 14:00:50', NULL),
(62, 'BKP001', 64, 6471, 647105, 'Jl. Ruhui Rahayu E-1 N0.4', '116.90008740000007', '-1.2497729', '12', '31', 'Backlight', '3', 'h', 'Kayu', 85, 212000000, 0, '1', NULL, NULL, '2017-09-15 12:46:54', '2017-09-15 13:09:05', NULL),
(63, 'BKP002', 64, 6471, 647101, 'Jl. Mulawarman teritip Rt.05 No. ra 79', '117.00212340000007', '-1.1710891', '4', '7', 'Frontlight', '4', 'v', 'Stainless', 85, 221000000, 0, '1', NULL, NULL, '2017-09-15 12:48:31', '2017-09-15 13:09:02', NULL),
(64, 'SMD001', 64, 6472, 647208, 'jalan Kesuma Bangsa No. 82,', '117.14592930000003', '-0.492125', '2', '3', 'Backlight', '4', 'h', 'Stainless', 86, 212000000, 0, '1', NULL, NULL, '2017-09-15 12:51:04', '2017-09-15 14:03:32', NULL),
(65, 'SMD002', 64, 6472, 647208, 'Jalan Basuki Rahmat No.55,', '117.15274799999997', '-0.4958289999999999', '4', '9', 'Backlight', '3', 'h', 'Kayu', 86, 215000000, 0, '1', NULL, NULL, '2017-09-15 12:52:51', '2017-09-15 14:03:22', NULL),
(66, 'MKS001', 73, 7371, 737103, 'Jl. A. P. Pettarani No.30, Balla Parang', '119.43687499999999', '-5.1536719', '4', '5', 'Frontlight', '2', 'h', 'Stainless', 86, 615000000, 0, '1', NULL, NULL, '2017-09-15 12:54:40', '2017-09-15 14:03:10', NULL),
(67, 'MKS002', 73, 7371, 737114, 'Jln.Perintis Kemerdekaan', '119.50612720000004', '-5.1317301', '8', '4', 'Frontlight', '4', 'h', 'Kayu', 86, 150000000, 0, '1', NULL, NULL, '2017-09-15 12:56:19', '2017-09-15 14:02:58', NULL),
(68, 'MLK001', 82, 8203, 820320, 'Jl. Pattimura No.1,', '128.18263479999996', '-3.6945456', '21', '12', 'Frontlight', '4', 'h', 'Stainless', 86, 433000000, 0, '1', NULL, NULL, '2017-09-15 12:58:47', '2017-09-15 14:02:42', NULL),
(69, 'MLK002', 81, 8102, 810201, 'Jl. Pahlawan Revolusi No. 17', '132.74013230000003', '-5.6489518', '12', '13', 'Frontlight', '4', 'h', 'Kayu', 87, 874000000, 0, '1', NULL, NULL, '2017-09-15 13:00:59', '2017-09-15 14:06:06', NULL),
(70, 'MNK001', 92, 9202, 920212, 'Jl. Siliwangi No. 28 Manokwari', '134.07577249999997', '-0.8666372999999999', '3', '4', 'Frontlight', '4', 'v', 'Baja Ringan', 87, 445000000, 0, '1', NULL, NULL, '2017-09-15 13:03:22', '2017-09-15 13:08:52', NULL),
(71, 'MNK002', 92, 9202, 920212, 'Jl. Trikora Wosi Gaya Baru', '134.05249090000007', '-0.8628367', '3', '8', 'Frontlight', '4', 'h', 'Stainless Steel', 87, 404000000, 0, '1', NULL, NULL, '2017-09-15 13:05:07', '2017-09-15 13:08:48', NULL),
(72, 'SOR001', 92, 9271, 927106, 'Jl. A. Yani No. 19, Klaligi', '131.2585699', '-0.876969', '4', '2', 'Backlight', '4', 'h', 'Stainless', 87, 455000000, 0, '1', NULL, NULL, '2017-09-15 13:07:03', '2017-09-15 14:39:18', NULL),
(73, 'SOR002', 92, 9271, 927101, 'JL. Masjid Raya HBM, Remu Utara,', '131.28200590000006', '-0.8820914', '4', '5', 'Frontlight', '4', 'h', 'Stainless', 87, 135000000, 0, '1', NULL, NULL, '2017-09-15 13:08:25', '2017-09-15 14:05:37', NULL),
(74, 'KGD001', 31, 3172, 317206, 'jl.kelapa gading', '106.90546180000001', '-6.1604549', '12', '33', 'Frontlight', '3', 'h', 'Stainless', 85, 200000, 0, '1', NULL, NULL, '2017-09-18 21:34:01', '2017-09-18 21:34:24', NULL),
(75, 'MKK001', 31, 3172, 317201, 'Jl.Muara karang', '106.7834583', '-6.125459999999999', '12', '12', 'Frontlight', '3', 'v', 'Stainless', 85, 200000, 0, '0', NULL, NULL, '2017-09-18 23:07:09', '2017-09-19 03:34:14', '2017-09-19 03:34:14'),
(76, 'PCSUKASUKA', 31, 3172, 317206, 'Jl. Puyuh Raya', '106.9038749', '-6.1725812', '100', '100', 'Frontlight', '1', 'v', 'Kertas', 85, 1, 0, '0', 'GPL', NULL, '2017-09-19 00:45:09', '2017-09-19 03:34:10', '2017-09-19 03:34:10'),
(77, 'KDG001', 12, 1203, 120304, 'dkfnskfnksdjnfkdsjnf', '106.85516200000006', '-6.148351799999999', '100', '100', 'Backlight', '4', 'h', 'Kayu', 85, 20000, 0, '0', NULL, NULL, '2017-09-19 01:20:09', '2017-09-19 03:34:07', '2017-09-19 03:34:07'),
(78, 'sad', 12, 1205, 120503, 'sdfsdfsdf', '106.85516200000006', '-6.148351799999999', '12', '12', 'Frontlight', '1', 'h', 'dsf', 78, 3242, 0, '1', 'dsf', NULL, '2017-09-19 02:21:54', '2017-09-19 02:22:05', '2017-09-19 02:22:05'),
(79, 'KODING123', 12, 1206, 120603, 'Jl.JALAN', '106.85516200000006', '-6.148351799999999', 'dsa', 'sad', 'Backlight', '3', 'v', 'IJDSAAD', 85, 213000, 0, '0', 'kdsajl', NULL, '2017-09-19 03:33:56', '2017-09-19 03:34:03', '2017-09-19 03:34:03'),
(80, 'test', 14, 1405, 140505, 'iojfe', '106.85516200000006', '-6.148351799999999', '12', '21', 'Frontlight', '3', 'h', 'das', 85, 123231, 0, '0', 'asdsdasad', NULL, '2017-09-19 03:52:45', '2017-09-19 03:59:59', '2017-09-19 03:59:59'),
(81, 'asd', 12, 1203, 120303, 'asdasd', '106.85516200000006', '-6.148351799999999', '122', '23123', 'Frontlight', '1', 'v', 'kayu', 85, 324444, 0, '1', NULL, NULL, '2017-09-19 04:00:46', '2017-09-28 03:25:33', '2017-09-28 03:25:33'),
(82, 'SDF00233', 31, 3172, 317206, 'jl.putih salju', '106.90807963599855', '-6.164636300448754', '20', '12', 'Backlight', '3', 'h', 'Aluminium', 85, 200000000, 0, '0', 'this is note', NULL, '2017-09-25 06:33:25', '2017-09-25 06:33:25', NULL),
(83, 'RWD001', 31, 3175, 317508, 'Jl.Rawa Domba Raya', '106.91396250000003', '-6.2457615', '12', '12', 'Frontlight', '3', 'v', 'Aluminium', 85, 115000000, 0, '1', NULL, NULL, '2017-09-28 03:23:12', '2017-09-28 04:05:05', NULL),
(100, 'KODE_100', 32, 3205, 320504, 'Jalan Otista', '107.89068520000001', '-7.1764849', '3', '3', 'Frontlight', '1', 'v', 'bahan', 100, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-23 09:04:33', NULL),
(101, 'KODE_101', 32, 3204, 320405, 'Tol cileunyi, Cangkuang, Bandung, Jawa Barat, Indonesia', '107.79198999999994', '-6.977120999999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 101, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:56:36', NULL),
(102, 'KODE_102', 32, 3213, 321307, 'Jalan Jendral Ahmad Yani', '107.78437510000003', '-6.437747099999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 101, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:55:08', NULL),
(103, 'KODE_103', 32, 3274, 327404, 'Jalan Lawang Gada', '108.56219970000006', '-6.724869', '3', '3', 'Frontlight', '1', 'v', 'bahan', 102, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:58:21', NULL),
(104, 'KODE_104', 31, 3172, 317201, 'JL.RY PEREMPATAN TUPAREV ( GUNUNG SARI ) CIREBON', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 103, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(105, 'KODE_105', 32, 3214, 321404, 'Jalan Raya Plered,Purwakarta', '107.38694459999999', '-6.6430712', '3', '3', 'Frontlight', '1', 'v', 'bahan', 104, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 05:01:52', NULL),
(106, 'KODE_106', 32, 3209, 320919, 'Jalan Otto Iskandardinata', '108.5097045', '-6.706554', '3', '3', 'Frontlight', '1', 'v', 'bahan', 103, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:59:55', NULL),
(107, 'KODE_107', 32, 3274, 327404, 'Jalan Lawang Gada', '108.56219970000006', '-6.724869', '3', '3', 'Frontlight', '1', 'v', 'bahan', 102, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:57:49', NULL),
(108, 'KODE_108', 32, 3213, 321319, 'Jalan Jend. Achmad Yani', '107.73719119999998', '-6.5901811', '3', '3', 'Frontlight', '1', 'v', 'bahan', 101, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:54:22', NULL),
(109, 'KODE_109', 31, 3172, 317201, 'PANTAI TADDYS DEPAN TERMINAL LAMA ( JL SILIWANGI )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 105, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(110, 'KODE_110', 52, 5206, 520604, 'Jalan Sultan Muhamad Salahuddin', '118.69637209999996', '-8.5172434', '3', '3', 'Frontlight', '1', 'v', 'bahan', 107, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 05:25:36', NULL),
(111, 'KODE_111', 31, 3172, 317201, 'JL. RAYA NARMADA, LOBAR', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 107, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(112, 'KODE_112', 31, 3175, 317508, 'Jalan Pondok Gede', '106.88603260000002', '-6.2909589', '3', '3', 'Frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 08:20:20', NULL),
(113, 'KODE_113', 32, 3216, 321620, 'Jalan Raya Kali Malang', '107.1603973', '-6.3187479', '3', '3', 'Frontlight', '1', 'v', 'bahan', 109, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 08:33:41', NULL),
(114, 'KODE_114', 36, 3673, 367305, 'Jalan Akses Tol Serang Timur', '106.18148369999994', '-6.118926099999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 110, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 08:34:57', NULL),
(115, 'KODE_115', 12, 1271, 127107, 'Jalan Jamin Ginting', '98.62969670000007', '3.5252885', '3', '3', 'Frontlight', '1', 'v', 'bahan', 111, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:43:12', NULL),
(116, 'KODE_116', 31, 3172, 317201, 'JL ADAM MALIK SIMPANG JL AMIR HAMZAH', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(117, 'KODE_117', 13, 1306, 130607, 'Jalan Simpang Taluak', '100.4027049', '-0.2998785', '3', '3', 'Frontlight', '1', 'v', 'bahan', 113, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:03:34', NULL),
(118, 'KODE_118', 13, 1371, 137103, 'Jalan Mr. Sutan M. Rasyid, Padang Pariaman, Katapiang, Batang Anai, Kota Padang, Sumatera Barat 25586', '100.28318009999998', '-0.7891585', '3', '3', 'Frontlight', '1', 'v', 'bahan', 114, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:05:54', NULL),
(119, 'KODE_119', 13, 1371, 137103, 'Jalan Samudera', '100.35064020000004', '-0.9334378999999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 113, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:02:36', NULL),
(120, 'KODE_120', 13, 1371, 137103, 'Jalan Tepi Pasang', '100.36118340000007', '-0.9582142', '3', '3', 'Frontlight', '1', 'v', 'bahan', 115, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:08:18', NULL),
(121, 'KODE_121', 14, 1471, 147102, 'Jalan Pangeran Hidayat', '101.44503570000006', '0.5243419', '3', '3', 'Frontlight', '1', 'v', 'bahan', 116, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:09:25', NULL),
(122, 'KODE_122', 15, 1571, 157101, 'Jalan Kol Abunjani', '103.5932229', '-1.6137336', '3', '3', 'Frontlight', '1', 'v', 'bahan', 117, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:12:51', NULL),
(123, 'KODE_123', 34, 3471, 347103, 'Jalan Laksda Adisucipto', '110.40702420000002', '-7.783228599999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 106, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 05:07:27', NULL),
(124, 'KODE_124', 31, 3172, 317201, 'JL. ARIEF RACHMAN VIEW KERTABUMI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(125, 'KODE_125', 32, 3214, 321401, 'Jalan Veteran', '107.44736149999994', '-6.526873699999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 09:13:03', NULL),
(126, 'KODE_126', 32, 3274, 327401, 'Jalan Tol Cipali', '108.41725510000003', '-6.6898371', '3', '3', 'Frontlight', '1', 'v', 'bahan', 119, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 09:51:25', NULL),
(127, 'KODE_127', 31, 3172, 317201, 'JL. INTERCHANGE KARAWANG BARAT', '107.27456830000006', '-6.3090486', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:55:50', NULL),
(128, 'KODE_128', 31, 3172, 317201, 'JL. LETNAN JIDUN - PEREMPATAN BRIMOB SERANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(129, 'KODE_129', 36, 3674, 367406, 'Jalan Siliwangi', '106.72511110000005', '-6.342773299999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:55:16', NULL),
(130, 'KODE_130', 36, 3674, 367402, 'Jalan Raya Serpong', '106.65102309999997', '-6.254085700000001', '3', '3', 'Frontlight', '1', 'v', 'bahan', 101, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:53:03', NULL),
(131, 'KODE_131', 31, 3172, 317201, 'JL. PEMUDA BUNDARAN MALL PARAGON', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 121, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(132, 'KODE_132', 31, 3172, 317201, 'JL. SIMPANG LIMA (DEPAN MALL CIPUTRA)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 122, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(133, 'KODE_133', 31, 3172, 317201, 'Jl. A Yani KM 7 Hotel Amaris', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 123, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(134, 'KODE_134', 31, 3172, 317201, 'JALAN BRIGJEN M JOENOES BY PASS (JEMBATAN TAMAN KALI KADIA)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 124, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(135, 'KODE_135', 31, 3172, 317201, 'JALAN VETERAN - M YAMIN PALU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 125, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(136, 'KODE_136', 31, 3172, 317201, 'JL. COKROMINOTO KISARAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(137, 'KODE_137', 31, 3172, 317201, 'JL. WR SUPRATMAN RANTAU PRAPAT', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(138, 'KODE_138', 31, 3172, 317201, 'JL. PERINTIS KEMERDEKAAN SIMPANG JALAN THAMRIN', '98.6868101', '3.5988236', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:50:56', NULL),
(139, 'KODE_139', 31, 3172, 317201, 'JL. JAMIN GINTING SIMPANG JL. SETIABUDI  (SELAYANG)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(140, 'KODE_140', 31, 3172, 317201, 'JL. SM RAJA - DEPAN JL DIPONOROGO, SIBOLGA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(141, 'KODE_141', 31, 3172, 317201, 'JL. WR SUPRATMAN DEPAN BANK SUMUT SYARIAH', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(142, 'KODE_142', 31, 3172, 317201, 'JALAN GAJAH MADA DEKAT POM BENSIN TAMAN KOTA MAS', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 127, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(143, 'KODE_143', 31, 3172, 317201, 'JL. BUNGA RAYA DEKAT BCS MALL', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 127, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(144, 'KODE_144', 13, 1371, 137104, 'Jalan Parman', '100.35012440000003', '-0.9144679000000001', '3', '3', 'Frontlight', '1', 'v', 'bahan', 113, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:02:20', NULL),
(145, 'KODE_145', 31, 3172, 317201, 'JL. HR SUBRANTAS', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 128, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(146, 'KODE_146', 31, 3172, 317201, 'JL. MAHAKAN SIMPANG 4 KM 8 KOTA BENGKULU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 129, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(147, 'KODE_147', 31, 3172, 317201, 'JL. RAYA JEND SUDIRMAN  - PEREMPATAN CAFE KINGKONG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 130, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(148, 'KODE_148', 31, 3172, 317201, 'Jl. A Yani KM 32 simpang 3 Jl. RO Ulin Banjarbaru', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(149, 'KODE_149', 31, 3172, 317201, 'JL. RAYA SIMPANG 4 TANJUNG PATI LIMA PULUH KOTA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 132, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(150, 'KODE_150', 31, 3172, 317201, 'JL. JEND. SUDIRMAN FLYOVER IMAM MUNANDAR (KOTA-AIRPORT) RELOKASI KE JL T TAMBUSAI / JL NANGKA SAMP JALAN AMAL MULIA BANK BNI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 133, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(151, 'KODE_151', 12, 1207, 120705, 'Jalan Jamin Ginting', '98.60385900000006', '3.447533', '3', '3', 'Frontlight', '1', 'v', 'bahan', 111, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:41:48', NULL),
(152, 'KODE_152', 31, 3172, 317201, 'JL. NASIONAL MEUREBO, MEULABOH', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 134, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(153, 'KODE_153', 31, 3172, 317201, 'JL RAYA PARUNG LEBAK WANGI SEBRANG POM BENSIN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(154, 'KODE_154', 31, 3172, 317201, 'JL RAYA PALUR TIMUR JEMBATAN JURUG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 135, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(155, 'KODE_155', 31, 3172, 317201, 'JL A YANI PEREMPATAN PASAR KARTASURA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 135, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(156, 'KODE_156', 31, 3172, 317201, 'JL. SIMPANG 7 NO 1 ALUN ALUN PATI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 136, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(157, 'KODE_157', 12, 1207, 120702, 'JL. MEDAN TANJUNGMORAWA (SIMPANG KAYU BESAR)', '98.78014170000006', '3.5227833', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:50:33', NULL),
(158, 'KODE_158', 31, 3172, 317201, 'JL. MEDAN TEBING TINGGI SIMP PASAR BENGKEL', '99.0072252', '3.5632909', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:49:57', NULL),
(159, 'KODE_159', 31, 3172, 317201, 'JL. SUDIRMAN PINTU MASUK PERUMAHAN SUKAJADI BATAM', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 137, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(160, 'KODE_160', 31, 3172, 317201, 'JL. PEMUDA BUNDARAN MALL PARAGON', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 121, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(161, 'KODE_161', 31, 3172, 317201, 'JL. SIMPANG LIMA (DEPAN MALL CIPUTRA)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 122, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(162, 'KODE_162', 31, 3172, 317201, 'JL. JEND. SUDIRMAN - HALAMAN MALL RAMAYANA SALATIGA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 138, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(163, 'KODE_163', 31, 3172, 317201, 'JL. SAM RATULANGI PRASKO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 139, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(164, 'KODE_164', 31, 3172, 317201, 'JL. KLAMONO PERTIGAAN ALUN ALUN AIMAS', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 140, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(165, 'KODE_165', 31, 3172, 317201, 'JL. MAYOR SUJUDI, TULANGAGUNG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 141, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(166, 'KODE_166', 31, 3172, 317201, 'JL. VETERAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 142, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(167, 'KODE_167', 31, 3172, 317201, 'PERTIGAAN MONDOAN TUBAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(168, 'KODE_168', 31, 3172, 317201, 'JL. SIMPANG 7 NO 1 ALUN ALUN PATI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 136, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(169, 'KODE_169', 31, 3172, 317201, 'JL. SUNSET ROAD (CARREFOUR)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(170, 'KODE_170', 31, 3172, 317201, 'PEREMPATAN DAMALANG JL. S PARMAN - CILACAP', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 143, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(171, 'KODE_171', 33, 3326, 332613, 'Jalan Raya Kedungwuni', '109.64627910000002', '-6.966569000000001', '3', '3', 'Frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 07:30:46', NULL),
(172, 'KODE_172', 31, 3172, 317201, 'JL. AHMAD YANI SIMP JL SOEPRAPTO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(173, 'KODE_173', 31, 3172, 317201, 'JL. AHMAD YANI SIMP JL RUNDING ( SIMPSUBUSALAM)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 144, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(174, 'KODE_174', 31, 3172, 317201, 'JL. T HAMZAH BENDAHARA SIMPANG BANK MANDIRI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 145, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(175, 'KODE_175', 31, 3172, 317201, 'JL. MEDAN - BANDA ACEH SIMPANG 4 TUGU BIREUN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 146, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(176, 'KODE_176', 31, 3172, 317201, 'JL. ULAKMA SINAGA SIMPANG JALAN MEDAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(177, 'KODE_177', 31, 3172, 317201, 'JL. JENDRAL SUDIRMAN BREBES', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 147, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(178, 'KODE_178', 31, 3172, 317201, 'JL. KOMISARIS BAMBANG SUPRAPTO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 148, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(179, 'KODE_179', 31, 3172, 317201, 'JL. RAYA MALANGBONG (DEKAT PASAR/TERMINAL)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 149, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(180, 'KODE_180', 31, 3172, 317201, 'JL. RAYA MALANGBONG (DEKAT PASAR/TERMINAL)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 149, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(181, 'KODE_181', 31, 3172, 317201, 'JL. TENTARA PELAJAR, KOTA TASIKMALAYA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 150, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(182, 'KODE_182', 31, 3172, 317201, 'JL. PELABUHAN DUA PEREMPATAN JALUR LINGKAR SELATAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 151, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(183, 'KODE_183', 31, 3172, 317201, 'JL. SUKARAJA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 151, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(184, 'KODE_184', 31, 3172, 317201, 'JL RAYA CIBADAK ( TERMINAL BIS CIBADAK )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 100, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(185, 'KODE_185', 12, 1271, 127101, 'Jalan Madong Lubis', '98.69435869629524', '3.590570681168615', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:49:28', NULL),
(186, 'KODE_186', 12, 1271, 127120, 'Jalan Jawa', '98.68017510000004', '3.591947', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:47:06', NULL),
(187, 'KODE_187', 31, 3172, 317201, 'PERTIGAAN JL. JEND SUDIRMAN - JL. ANDI KAMBO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 152, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(188, 'KODE_188', 31, 3172, 317201, 'JL. NANI WARTABONE ( JL PANJAITAN )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 153, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(189, 'KODE_189', 31, 3172, 317201, 'JL. SANGNAWALUH', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 126, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(190, 'KODE_190', 31, 3172, 317201, 'JALINSUM TEBING TINGGI INDRAPURA SIMPANG BODREK, AIR PUTIH, BATU BARA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(191, 'KODE_191', 12, 1271, 127115, 'Jalan Imam Bonjol', '98.675807858197', '3.58258437410522', '3', '3', 'Frontlight', '1', 'v', 'bahan', 111, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:39:34', NULL),
(192, 'KODE_192', 31, 3172, 317201, 'JL. M BOYA TEMBILAHAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 154, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(193, 'KODE_193', 31, 3172, 317201, 'JL. DIPONOGORO SIMPANG AGUS SALIM, RENGAT', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 154, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(194, 'KODE_194', 31, 3172, 317201, 'JL. ALAMSYAH RPN TUGU PAYAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 155, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(195, 'KODE_195', 31, 3172, 317201, 'JL. TEUKU UMAR DEPAN MALL BOEMI KEDATON (MBK)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 155, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(196, 'KODE_196', 31, 3172, 317201, 'JL. DIPONOGORO TUGU ADIPURA, ENGGAL,  BANDAR LAMPUNG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 155, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(197, 'KODE_197', 31, 3172, 317201, 'JL. ALAMSYAH RATU PERWIRA NEGARA SIMPANG MACAN LINDUNGAN, PALEMBANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 156, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(198, 'KODE_198', 31, 3172, 317201, 'JL. KOL. BARLIAN SIMPANG GOR LAHAT', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 157, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(199, 'KODE_199', 31, 3172, 317201, 'JALAN RAJAWALI SIMPANG VETERAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 158, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(200, 'KODE_200', 32, 3217, 321708, 'Jalan Raya Padalarang', '107.49380880000001', '-6.8491334', '3', '3', 'Frontlight', '1', 'v', 'bahan', 101, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:52:34', NULL),
(201, 'KODE_201', 31, 3172, 317201, 'JL. RAYA PANDEGLANG PASAR PANDEGLANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 159, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(202, 'KODE_202', 31, 3172, 317201, 'JL AW SYAHRANIE', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 160, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(203, 'KODE_203', 36, 3603, 360318, 'Jalan Raya Serang', '106.54342880000002', '-6.223559', '3', '3', 'Frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 07:28:00', NULL),
(204, 'KODE_204', 36, 3603, 360318, 'Jalan Raya Serang', '106.49725220000005', '-6.2182507', '3', '3', 'Frontlight', '1', 'v', 'bahan', 112, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 03:45:08', NULL),
(205, 'KODE_205', 31, 3172, 317201, 'JL. RAYA SALABENDA KEMANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(206, 'KODE_206', 32, 3216, 321608, 'Jalan Teuku Umar', '107.08878159999995', '-6.270092099999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 101, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:51:38', NULL),
(207, 'KODE_207', 31, 3172, 317201, 'JL RAYA INDUSTRI, LAMPU MERAH LIPPO CIKARANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 159, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(208, 'KODE_208', 32, 3275, 327505, 'Jalan Siliwangi', '106.99429329999998', '-6.270052', '3', '3', 'Frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 07:22:08', NULL),
(209, 'KODE_209', 31, 3172, 317201, 'JALAN RAYA BOGOR  SUKABUMI ( PASAR CARINGIN BOGOR)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 161, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(210, 'KODE_210', 31, 3172, 317201, 'JL. AP PETTARANI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 125, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(211, 'KODE_211', 31, 3172, 317201, 'PEREMPATAN JL KERTAJAYA INDAH - JL IR SUKARNO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 141, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(212, 'KODE_212', 31, 3172, 317201, 'JL RAYA INDUSTRI, LAMPU MERAH LIPPO CIKARANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 159, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(213, 'KODE_213', 31, 3172, 317201, 'JL. RAYA KASRI KABUPATEN PASURUAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 162, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(214, 'KODE_214', 31, 3172, 317201, 'JL. DR. WAHIDIN (PERTIGAAN MASJID AGUNG), GRESIK', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 163, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(215, 'KODE_215', 31, 3172, 317201, 'PEREMPATAN JL. PEMUDA SIMPANG JL. GAJAH MADA MOJOSARI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 164, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(216, 'KODE_216', 31, 3172, 317201, 'JL. SULTAN KHAIRUDIN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 165, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(217, 'KODE_217', 31, 3172, 317201, 'JL. RAYA DARMO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 141, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(218, 'KODE_218', 32, 3207, 320701, 'Jalan Stasiun', '108.35600220000003', '-7.329144899999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 102, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 04:57:27', NULL),
(219, 'KODE_219', 32, 3273, 327309, 'Jalan R.E. Martadinata', '107.61981739999999', '-6.9058959', '3', '3', 'Frontlight', '1', 'v', 'bahan', 100, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-23 09:02:29', NULL),
(220, 'KODE_220', 31, 3172, 317201, 'JL. PASAR WETAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 150, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(221, 'KODE_221', 31, 3172, 317201, 'JALAN MEDAN-BANDA ACEH DEKAT JEMBATAN KUALA SIMPANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 146, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(222, 'KODE_222', 31, 3172, 317201, 'JL. A. YANI (DEPAN KANTOR POS), LANGSA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 145, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(223, 'KODE_223', 31, 3172, 317201, 'JL. RE MARTADINATA DEPAN SPBU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 166, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(224, 'KODE_224', 31, 3172, 317201, 'JL. SULTAN SYARIEF KASIM SIMPANG JALAN SUKAJADI DUMAI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 154, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(225, 'KODE_225', 31, 3172, 317201, 'JL.MERDEKA DEKAT SIMPANG JL.TEMON BUKIT KECIL', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 167, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(226, 'KODE_226', 31, 3172, 317201, 'JL KS TUBUN FLAY OVER GAJAH MADA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 155, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(227, 'KODE_227', 31, 3172, 317201, 'JL. T. NYAK ARIEF SIMPANG RUKOH DARUSSALAM', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 168, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(228, 'KODE_228', 31, 3172, 317201, 'JL BANDA ACEH MEDAN SIMPANG ANEUK GALONG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 134, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(229, 'KODE_229', 31, 3172, 317201, 'JL. HANGTUAH SIMPANG DESA HARAPAN DURI, BENGKALIS', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 154, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(230, 'KODE_230', 31, 3172, 317201, 'JL. JEND. SUDIRMAN (PASAR), PRINGSEWU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 169, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(231, 'KODE_231', 31, 3172, 317201, 'JL. BAU MASSEPE', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 125, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(232, 'KODE_232', 31, 3172, 317201, 'JL AHMAD DAHLAN - A YANI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 170, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(233, 'KODE_233', 31, 3172, 317201, 'JL. PAHLAWAN REVOLUSI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 171, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(234, 'KODE_234', 31, 3172, 317201, 'JL. PERCETAKAN JAYAPURA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 152, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(235, 'KODE_235', 31, 3172, 317201, 'JL. A YANI - SORONG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 152, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(236, 'KODE_236', 31, 3172, 317201, 'JL. DI PANJAITAN, LEPO-LEPO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 124, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(237, 'KODE_237', 31, 3172, 317201, 'JL. MGR SOEGIOPRANOTO ( HOTEL SILIWANGI )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 122, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(238, 'KODE_238', 31, 3172, 317201, 'JL. GARUDA PEREMPATAN SIMPANG BINGUNG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 106, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(239, 'KODE_239', 31, 3172, 317201, 'JL. LANGKO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 172, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(240, 'KODE_240', 31, 3172, 317201, 'JL. RAYA KAJEN TALANG PASAR PESAYANGAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 173, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(241, 'KODE_241', 31, 3172, 317201, 'JL. PR MONJALI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 174, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(242, 'KODE_242', 31, 3172, 317201, 'JL. PEMBANGUNAN SAMPING RM LA COSTA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 127, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(243, 'KODE_243', 31, 3172, 317201, 'Jl Teuku Umar Simpang 3 The Hill Hotel', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 175, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(244, 'KODE_244', 31, 3172, 317201, 'JL. BY PASS NGURAH RAI EXIT TOL ( NUSA DUA ) KAB. BADUNG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(245, 'KODE_245', 31, 3172, 317201, 'JL. RAYA ULUWATU JIMBARAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 176, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(246, 'KODE_246', 31, 3172, 317201, 'JL. KAPTEN SUDIBYO (TIKUNGAN TIRUS) - KOTA TEGAL', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 147, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(247, 'KODE_247', 31, 3172, 317201, 'JL. AA MARIMIS PERTIGAAN RING ROAD 2', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 166, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(248, 'KODE_248', 31, 3172, 317201, 'JL. PANTAK KUTA KAB. BADUNG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(249, 'KODE_249', 31, 3172, 317201, 'JL. KAYU AYA (OBROI)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(250, 'KODE_250', 31, 3172, 317201, 'JL. BY PASS NGURAH RAI SIMPANG SIUR DEWA RUCI KAB. BADUNG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(251, 'KODE_251', 31, 3172, 317201, 'JL. ERLANGGA, SINGARAJA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 176, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(252, 'KODE_252', 31, 3172, 317201, 'JL. RAYA KUTA (JOGER)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(253, 'KODE_253', 31, 3172, 317201, 'JL. DR. WAHIDIN JEMBATAN LAYANG - UMUM KALIPAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 177, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(254, 'KODE_254', 31, 3172, 317201, 'JL. IR SOEKARNO, PEREMPATAN THE PARK SOLO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 138, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(255, 'KODE_255', 31, 3172, 317201, 'JL MARTADINATA JEMBATAN MIANGAS', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 166, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(256, 'KODE_256', 31, 3172, 317201, 'JALAN PERINTIS KEMERDEKAAN DEPAN UIM', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 125, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(257, 'KODE_257', 31, 3172, 317201, 'JALAN  SUNGAI SADDANG MAKASSAR', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 178, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(258, 'KODE_258', 31, 3172, 317201, 'JL. BOULEVARD', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 125, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(259, 'KODE_259', 31, 3172, 317201, 'JL. BASUKI RAHMAT (TEMPEL TOKO)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 179, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(260, 'KODE_260', 31, 3172, 317201, 'JL. SOEKARNO HATTA - KOTA MALANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 180, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(261, 'KODE_261', 32, 3211, 321115, 'Jalan Raya Jatinangor', '107.77345479999997', '-6.933273', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 09:11:00', NULL),
(262, 'KODE_262', 32, 3273, 327303, 'Jalan Raya Kopo', '107.58008386242682', '-6.9598434044739275', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 08:08:55', NULL),
(263, 'KODE_263', 32, 3273, 327303, 'Jalan Raya Kopo', '107.58004631150061', '-6.959859379141737', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 07:47:34', NULL),
(264, 'KODE_264', 31, 3172, 317201, 'JL. LINGKAR SELATAN - PEREMPATAN PCI CILEGON', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 181, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(265, 'KODE_265', 31, 3172, 317201, 'PERTIGAAN JL. GATOT SUBROTO - JL. AM. SANGAJI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 182, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(266, 'KODE_266', 31, 3172, 317201, 'JL. LINGKAR SELATAN - PEREMPATAN KEBON JAHE SERANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 109, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(267, 'KODE_267', 32, 3215, 321501, 'Jalan Jomin', '107.48826269999995', '-6.4150835', '3', '3', 'Frontlight', '1', 'v', 'bahan', 109, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 08:29:59', NULL),
(268, 'KODE_268', 31, 3172, 317201, 'JL. BOULEVARD BINTARO JAYA / FLAY OVER SEKTOR 9', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(269, 'KODE_269', 31, 3172, 317201, 'JL.SILIWANGI SENTUL CITY', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(270, 'KODE_270', 31, 3172, 317201, 'JL. SOEKARNO HATTA KM 4 BALUT', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 153, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(271, 'KODE_271', 31, 3172, 317201, 'JL. KALIMANTAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 178, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(272, 'KODE_272', 31, 3172, 317201, 'JL SIRKUIT SENTUL (GERBANG SIRKUIT)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(273, 'KODE_273', 31, 3172, 317201, 'JL. BRIGJEN KATAMSO NO. 68 KEC WARU SEDOARJO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 183, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(274, 'KODE_274', 31, 3172, 317201, 'JL. PAHLAWAN ( PEREMPATAN PASAR SIDOHARJO )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 184, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(275, 'KODE_275', 31, 3172, 317201, 'PEREMPATAN JALAN BY PASS MOJOKERTO SIMPANG JALAN JAYANEGARA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 164, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(276, 'KODE_276', 31, 3172, 317201, 'JL. LAKSDA LEO WATTIMENA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 152, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(277, 'KODE_277', 31, 3172, 317201, 'JL PUNCAK LONTAR / BUNDARAN PAKUWON MALL', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 179, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(278, 'KODE_278', 31, 3172, 317201, 'JL. RUNGKUT INDUSTRI ( SIER )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 142, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(279, 'KODE_279', 31, 3172, 317201, 'JL. SALAK SIMPANG 3 PASAR PANORAMA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 129, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(280, 'KODE_280', 31, 3172, 317201, 'JL. TRANS KALIMANTAN HANDIL', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 131, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(281, 'KODE_281', 31, 3172, 317201, 'JL. NANI WARTABONE ( JL PANJAITAN )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 153, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(282, 'KODE_282', 31, 3172, 317201, 'JL GERILYA-PAHLAWAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(283, 'KODE_283', 15, 1508, 150803, 'DEPAN PASAR ATAS MUARA BUNGO', '102.11198790000003', '-1.490061', '3', '3', 'Frontlight', '1', 'v', 'bahan', 117, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:11:49', NULL),
(284, 'KODE_284', 31, 3172, 317201, 'JALAN KYAI MOJO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 185, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL);
INSERT INTO `billboards` (`id`, `kode_billboard`, `id_provinsi`, `id_kabupaten`, `id_kecamatan`, `address`, `longitude`, `latitude`, `panjang`, `lebar`, `lighting`, `side`, `format`, `bahan`, `id_user`, `harga`, `view_count`, `verify`, `note`, `reject_note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(285, 'KODE_285', 31, 3172, 317201, 'JL. BASUKI RAHMAT ( UTOMODECK )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 153, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(286, 'KODE_286', 31, 3172, 317201, 'JL. HR BUNYAMIN PASAR GLEMPANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 148, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(287, 'KODE_287', 18, 1871, 187101, 'Jalan Sultan Hasanudin', '105.26826019999999', '-5.4434457', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 07:46:45', NULL),
(288, 'KODE_288', 31, 3172, 317201, 'JL. RAYA LINTAS TIMUR - PASAR UNIT II', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 155, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(289, 'KODE_289', 31, 3172, 317201, 'JL A YANI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 153, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(290, 'KODE_290', 31, 3172, 317201, 'JL. DIPENOGORO (PERTIGAAN PELABUHAN TANGLOK), SAMPANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 186, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(291, 'KODE_291', 31, 3172, 317201, 'JL. PB SUDIRMAN KRAKSAAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 141, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(292, 'KODE_292', 31, 3172, 317201, 'JL. BRIGJEN KATAMSO NO. 68 KEC WARU SEDOARJO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 183, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(293, 'KODE_293', 31, 3172, 317201, 'JL. VETERAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 142, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(294, 'KODE_294', 31, 3172, 317201, 'Jl. A Yani KM 14 Pasar Gambut', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 123, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(295, 'KODE_295', 31, 3172, 317201, 'JL. NANI WARTABONE ( JL PANJAITAN )', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 153, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(296, 'KODE_296', 31, 3172, 317201, 'JL. JATINOM PUNGGUNG TERMINAL PENGGUNG KLATEN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 187, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(297, 'KODE_297', 31, 3172, 317201, 'JL. AHMAD YANI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 130, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(298, 'KODE_298', 31, 3172, 317201, 'JL. NEGARA PROKLAMATOR, BANDAR JAYA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 169, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(299, 'KODE_299', 31, 3172, 317201, 'JL. SULTAN AGUNG SP PULAI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 188, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(300, 'KODE_300', 31, 3172, 317201, 'JL. RAYA LINTAS TIMUR - PASAR UNIT II', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 155, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(301, 'KODE_301', 31, 3172, 317201, 'JL. TJILIK RIWUT KM 1.5 PASAR KHAYAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 189, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(302, 'KODE_302', 31, 3172, 317201, 'JL. MASJID RAYA - JALAN SUNU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 125, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(303, 'KODE_303', 31, 3172, 317201, 'JL. KH AHMAD MUNIF ( PERTIGAAN TANGKEL ) BANGKALAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(304, 'KODE_304', 31, 3172, 317201, 'JL JAWA JEMBER', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 190, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(305, 'KODE_305', 31, 3172, 317201, 'JL. RANUGRATI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 190, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(306, 'KODE_306', 31, 3172, 317201, 'JL.TRUNOJOYO - WAHID HASYIM', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 191, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(307, 'KODE_307', 31, 3172, 317201, 'JALAN A. YANI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 190, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(308, 'KODE_308', 31, 3172, 317201, 'BUNDARAN WARU II', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 192, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(309, 'KODE_309', 31, 3172, 317201, 'JL. AHMAD YANI (ROYAL PLAZA)', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 193, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(310, 'KODE_310', 33, 3371, 337102, 'Jalan Pemuda', '109.69693610000002', '-7.396856799999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 108, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-24 05:31:22', NULL),
(311, 'KODE_311', 31, 3172, 317201, 'JL. PANGLIMA SUDIRMAN', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 194, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(312, 'KODE_312', 31, 3172, 317201, 'JL.MAYOR DASUKI  ( PERTIGAAN LAMPU MERAH JATIBARANG ) INDRAMAYU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 147, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(313, 'KODE_313', 33, 3374, 337413, 'Jalan Siliwangi', '110.37231509999992', '-6.987269599999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 07:45:27', NULL),
(314, 'KODE_314', 33, 3374, 337413, 'Jalan Siliwangi', '110.37231509999992', '-6.987269599999999', '3', '3', 'Frontlight', '1', 'v', 'bahan', 118, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 07:45:01', NULL),
(315, 'KODE_315', 31, 3172, 317201, 'PANGKALAN KERINCI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 128, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(316, 'KODE_316', 31, 3172, 317201, 'JL. M YAMIN BANGKINANG', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 128, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(317, 'KODE_317', 31, 3172, 317201, 'JLN SUDIRMAN PASAR UJUNG BATU RELOKASI KE  KAWASAN LKA UJUNG BATU', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 128, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(318, 'KODE_318', 31, 3172, 317201, 'JL. JUANDA - JUANDA 3 SAMARINDA', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 160, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(319, 'KODE_319', 31, 3172, 317201, 'SIMPANG 3 JL. P HIDAYATULLAH - P . FLORES', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 160, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(320, 'KODE_320', 31, 3172, 317201, 'JL. A YANI KM 6.5, DEPAN GIANT', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 123, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(321, 'KODE_321', 31, 3172, 317201, 'JL. MOCHTAR TABRANI', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 195, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(322, 'KODE_322', 32, 3201, 320101, 'Jalan Raya Puncak', '106.86702479999997', '-6.6498218', '3', '3', 'Frontlight', '1', 'v', 'bahan', 120, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 09:54:19', NULL),
(323, 'KODE_323', 31, 3172, 317201, 'BUNDARAN BESAR SIMPANG JL. YOS SUDARSO', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 189, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL),
(324, 'KODE_324', 14, 1471, 147108, 'Jalan Putri Tujuh', '101.40538289999995', '0.4604659', '3', '3', 'Frontlight', '1', 'v', 'bahan', 115, 500000, 0, '1', 'note', 'NULL\r', NULL, '2017-10-26 04:07:45', NULL),
(325, 'KODE_325', 31, 3172, 317201, 'PEREMPATAN DAMALANG JL. S PARMAN - CILACAP', '0', '0', '3', '3', 'frontlight', '1', 'v', 'bahan', 143, 500000, 0, '1', 'note', 'NULL\r', NULL, NULL, NULL);

--
-- Triggers `billboards`
--
DELIMITER $$
CREATE TRIGGER `add_notification_billboard` AFTER INSERT ON `billboards` FOR EACH ROW BEGIN
    INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `created_at`, `updated_at`)
    VALUES (NEW.id, 'billboard', NEW.id_user, NOW(), NOW());
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `approval_billboard` AFTER UPDATE ON `billboards` FOR EACH ROW BEGIN
	CALL billboard_approval_notification(NEW.id, OLD.verify, NEW.verify, NEW.id_user);          
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billboards`
--
ALTER TABLE `billboards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `billboards_kode_billboard_unique` (`kode_billboard`),
  ADD KEY `billboards_id_provinsi_foreign` (`id_provinsi`),
  ADD KEY `billboards_id_kabupaten_foreign` (`id_kabupaten`),
  ADD KEY `billboards_id_kecamatan_foreign` (`id_kecamatan`),
  ADD KEY `billboards_id_user_foreign` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billboards`
--
ALTER TABLE `billboards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `billboards`
--
ALTER TABLE `billboards`
  ADD CONSTRAINT `billboards_id_kabupaten_foreign` FOREIGN KEY (`id_kabupaten`) REFERENCES `kabupatens` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `billboards_id_kecamatan_foreign` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatans` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `billboards_id_provinsi_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `billboards_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
