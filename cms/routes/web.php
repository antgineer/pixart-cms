<?php
// Start Routes Page Login
Route::get('/', 'AuthController@login');
Route::post('/login', 'AuthController@authenticate')->name('login'); // {{route('login')}}
// End Routes Page Login

//Need Authentication
Route::group(['middleware' => 'authentication'], function () {

    /*
    |--------------------------------------------------------------------------
    | Start Admin Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:admin'], function () {
        //Dashboard Admin
        Route::get('/dashboard-admin', 'DashboardController@dashboardAdmin')->name('dashboard-admin');

        // Start Routes Page Master
        Route::prefix('master')->group(function () {
            // Matches The "/master/paket" URL
            Route::get('paket', function () {
                return view('pages.admin.master.paket.index_paket');
            })->name('master-paket'); // {{route('master-paket')}}
            //Route Role
            Route::get('role', 'RoleController@getAllRole')->name('getAll-master-paket'); // {{route('getAll-master-paket')}}
            Route::get('role/{id}', 'RoleController@show')->name('getByID-master-paket'); // {{route('getByID-master-paket', '')}}
            Route::post('role', 'RoleController@store')->name('save-master-paket'); // {{route('save-master-paket')}}
            Route::put('role/{id}', 'RoleController@update')->name('update-master-paket'); // {{route('update-master-paket', '')}}
            Route::delete('role/{id}', 'RoleController@destroy')->name('delete-master-paket'); // {{route('delete-master-paket', '')}}
            //End Route Role

            //Route Users
            // Matches The "/master/admin" URL
            Route::get('admin', function () {
                return view('pages.admin.master.user_admin.index_admin');
            })->name('master-admin'); // {{route('master-admin')}}

            // Matches The "/master/vendor" URL
            Route::get('vendor', function () {
                return view('pages.admin.master.user_vendor.index_vendor');
            })->name('master-vendor'); // {{route('master-vendor')}}

            Route::get('vendor-detail', function(){
                return view('pages.admin.master.user_vendor.detail_vendor');
            })->name('master-vendor-detail'); // {{route('master-vendor-detail')}}

            // Matches The "/master/customer" URL
            Route::get('customer', function () {
                return view('pages.admin.master.user_customer.index_customer');
            })->name('master-customer'); // {{route('master-customer')}}

            //Route Provinsi
            // Matches The "/master/provinsi" URL
            Route::get('provinsi', function () {
                return view('pages.admin.master.provinsi.index_provinsi');
            })->name('master-provinsi'); // {{route('master-provinsi')}}

            Route::get('provinsi/show/{id}', 'ProvinsiController@show')->name('getByIDProvinsi'); // {{route('getByIDProvinsi', '')}}
            Route::post('provinsi', 'ProvinsiController@store')->name('save-provinsi'); // {{route('save-provinsi')}}
            Route::put('provinsi/{id}', 'ProvinsiController@update')->name('update-provinsi'); // {{route('update-provinsi', '')}}
            Route::delete('provinsi/{id}', 'ProvinsiController@destroy')->name('delete-provinsi'); // {{route('delete-provinsi', '')}}
            //End Route Provinsi

            //Route Kabupaten
            // Matches The "/master/kabupaten" URL
            Route::get('kabupaten', function () {
                return view('pages.admin.master.kabupaten.index_kabupaten');
            })->name('master-kabupaten'); // {{route('master-kabupaten')}}

            Route::get('kabupaten/show/{id}', 'KabupatenController@show')->name('getByIDKabupaten'); // {{route('getByIDKabupaten', '')}}
            Route::post('kabupaten', 'KabupatenController@store')->name('save-kabupaten'); // {{route('save-kabupaten')}}
            Route::put('kabupaten/{id}', 'KabupatenController@update')->name('update-kabupaten'); // {{route('update-kabupaten', '')}}
            Route::delete('kabupaten/{id}', 'KabupatenController@destroy')->name('delete-kabupaten'); // {{route('delete-kabupaten', '')}}
            //End Route Kabupaten

            //Route Kecamatan
            // Matches The "/master/kecamatan" URL
            Route::get('kecamatan', function () {
                return view('pages.admin.master.kecamatan.index_kecamatan');
            })->name('master-kecamatan'); // {{route('master-kecamatan')}}

            Route::get('kecamatan/show/{id}', 'KecamatanController@show')->name('getByIDKecamatan'); // {{route('getByIDKecamatan', '')}}
            Route::post('kecamatan', 'KecamatanController@store')->name('save-kecamatan'); // {{route('save-kecamatan')}}
            Route::put('kecamatan/{id}', 'KecamatanController@update')->name('update-kecamatan'); // {{route('update-kecamatan', '')}}
            Route::delete('kecamatan/{id}', 'KecamatanController@destroy')->name('delete-kecamatan'); // {{route('delete-kecamatan', '')}}
            //End Route Kecamatan

            //Route Billboard
            // Matches The "/master/billboard" URL
            Route::get('billboard', function () {
                return view('pages.admin.master.billboard.index_billboard');
            })->name('master-billboard'); // {{route('master-billboard')}}

            // Matches The "/master/add-billboard" URL
            Route::get('add-billboard', function () {
                return view('pages.admin.master.billboard.add_billboard');
            })->name('add-billboard'); // {{route('add-billboard')}}

            // Matches The "/master/edit-billboard" URL
            Route::get('edit-billboard/{id}', 'BillboardController@edit')->name('edit-billboard'); // {{route('edit-billboard', '')}}

            // Matches The "/master/detail-billboard" URL
            // Route::get('detail-billboard/{id}', 'BillboardController@show');
            Route::get('detail-billboard/{id}', 'BillboardController@show')->name('detail-billboard'); // {{route('detail-billboard', '')}}

            Route::get('billboard/all', 'BillboardController@getAllBillboard')->name('all-billboard'); // {{route('all-billboard')}}
            Route::get('billboard/all/latest/{verify?}', 'BillboardController@getLatestBillboard')->name('getLatestBillboard'); // {{route('getLatestBillboard')}}
        });
        // End Routes Page Master

        // Start Routes Page Verify Billboard
        Route::get('/verify-billboard', function () {
            return view('pages.admin.verify_billboard.index_verify_billboard');
        })->name('verify-billboard'); // {{route('verify-billboard')}}

        Route::put('/billboard/verify/{id}', 'BillboardController@verify')->name('billboard-verify'); // {{route('billboard-verify', '')}}

        // Start Routes Page Report
        Route::get('/report', function () {
            return view('pages.admin.report.index_report');
        })->name('report'); // {{route('report')}}
    });
    /*
    |--------------------------------------------------------------------------
    | End Admin Routes
    |--------------------------------------------------------------------------
    */

     /*
    |--------------------------------------------------------------------------
    | Start Admin & Sampoerna Shared Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:admin,sampoerna'], function () {
        Route::prefix('master')->group(function () {
            Route::post('user/{role}', 'UserController@store')->name('save-user'); // {{route('save-user')}}
            Route::delete('user/{id}', 'UserController@destroy')->name('delete-user'); // {{route('delete-user', '')}}
        });
    });
     /*
    |--------------------------------------------------------------------------
    | End Admin & Sampoerna Shared Routes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Start Vendor Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:vendor'], function () {
        Route::get('/report-vendor/billboard-list/pdf','ReportController@vendorReportBillboardDetail');
        Route::get('/report-vendor/billboard-list/excel','ReportController@vendorReportBillboardDetailExcel');
        //Dashboard
        Route::get('/dashboard-vendor', 'DashboardController@dashboardVendor')->name('dashboard-vendor');

        Route::get('/vendor-unoccupied',function() {
            return view('pages.vendor.billboard.unoccupied');
        })->name('vendor-unoccupied');

        Route::get('/vendor-willexpire',function() {
            return view('pages.vendor.billboard.willexpire');
        })->name('vendor-willexpire');

        Route::get('/vendor-occupied',function() {
            return view('pages.vendor.billboard.occupied');
        })->name('vendor-occupied');

        Route::get('/vendor-contract/all/{id}', 'BillboardContractController@getContractByUser')->name('getContractByUser');
        Route::get('/vendor-contract/show/{id}', 'BillboardContractController@show')->name('contractShowByID');
        Route::post('/vendor-contract', 'BillboardContractController@store')->name('save-contract');
        Route::put('/vendor-contract/{id}', 'BillboardContractController@update')->name('update-contract');
        Route::delete('/vendor-contract/{id}', 'BillboardContractController@destroy')->name('delete-contract');
        // End Route Menu Master -> Contract

        //Billboard by vendor
        Route::get('billboard/all/detail/{id}', 'BillboardController@getAllBillboardDetailByVendor')->name('getAllBillboardDetailByVendor');

        // Start Route Menu Master -> Billboard
        Route::get('vendor-billboard', function () {
            return view('pages.vendor.master.billboard.index_billboard');
        })->name('vendor-billboard');
        Route::get('/vendor-billboard/{role}/{id}/{status?}', 'BillboardContractController@getBillboardContract')->name('vendorbillboard-role');

        Route::get('/vendor-addbillboard',function() {
            return view('pages.vendor.master.billboard.addbillboard');
        })->name('vendoraddbillboard');

        Route::get('/vendor-editbillboard/{id}', 'BillboardController@editVendor')->name('vendoreditbillboard');

        Route::get('/vendor-detailbillboard/{id}', 'BillboardController@showVendor')->name('vendordetailbillboard');
        // End Route Menu Master -> Billboard

        Route::get('/vendor-report',function() {
            return view('pages.vendor.report');
        });

        Route::get('/vendor-cetak',function() {
            return view('pages.vendor.cetak');
        });
    });
    /*
    |--------------------------------------------------------------------------
    | End Vendor Routes
    |--------------------------------------------------------------------------
    */

     /*
    |--------------------------------------------------------------------------
    | Start Admin & Vendor Shared Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:admin,vendor'], function () {
        Route::prefix('master')->group(function () {
            Route::post('billboard', 'BillboardController@store')->name('save-billboard'); // {{route('save-billboard')}}
            Route::get('billboard/show/{id}', 'BillboardController@show')->name('getBillboardByID'); // {{route('getBillboardByID', '')}}
            Route::put('billboard/{id}', 'BillboardController@update')->name('update-billboard'); // {{route('update-billboard', '')}}
            Route::delete('billboard/{id}', 'BillboardController@destroy')->name('delete-billboard'); // {{route('delete-billboard', '')}}
            //End Route Billboard
        });

        //Route Report
        Route::group(['prefix' => '/report'], function(){
            Route::get('/billboard/preview', 'BillboardController@reportBillboardDetail')->name('report-billboard');
            Route::get('/billboard/pdf','ReportController@billboardPdf')->name('report-billboard-list-pdf');
            Route::get('/billboard/excel','ReportController@billboardExcel')->name('report-billboard-list-excel');
        });

        Route::get('/vendor-preview/report-billboard', 'BillboardController@vendorReportBillboardDetail')->name('vendor-report-billboard');
        Route::get('/vendor-onepreview/{id}', 'BillboardController@reportBillboardOne')->name('billboardReportPreviewOne');
        Route::get('/vendor-onepreview', function() {
            return view('pages.vendor.onepreview-contoh');
        });
        Route::get('/vendor-onepreview-fix', function() {
            return view('pages.vendor.onepreview-fix');
        })->name('onepreviewfix');
    });
     /*
    |--------------------------------------------------------------------------
    | End Admin & Vendor Shared Routes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Start Customer Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:customer,sampoerna'], function () {
        // Start Routes Page Dashboard Customer
        Route::get('/dashboard-customer', 'DashboardController@dashboardCustomer')->name('dashboard-customer');
        // End Routes Page Dashboard Customer

        // Start Route Page Billboard
        Route::get('/customer-will-expire', function () {
            return view('pages.customer.will_expire');
        })->name('customer-will-expire');

        Route::get('/customer-occupied', function () {
            return view('pages.customer.occupied');
        })->name('customer-occupied');
        Route::get('/customer-occupied/{role}/{id}/{status?}', 'BillboardContractController@getBillboardContract')->name('customeroccupiedstatus');
        // End Route Page Billboard

        // Start Route Page Report
        Route::get('/customer-report', function () {
            return view('pages.customer.report');
        })->name('customer-report');

        Route::get('/customer-preview/{verify}', 'BillboardContractController@reportContract')->name('customer-preview');

        //Route::get('/customer-previewone/{id}/pdf','ReportController@reportContractPDF');
        // End Route Page Report
        Route::get('/customer-detail-billboard', function () {
            return view('pages.customer.detail_billboard');
        });

        Route::get('/customer-map', function () {
            return view('pages.customer.customer-map');
        })->name('customer-map');

        Route::get('/contract/search-map', 'BillboardContractController@showContractByMap')->name('contract-search-map');
    });
    /*
    |--------------------------------------------------------------------------
    | End Customer Routes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Start Shared Routes
    |--------------------------------------------------------------------------
    */
    //statistic
    Route::group(['prefix' => '/statistic'], function(){
        Route::get('/topuser/{role}/{id}', 'DashboardController@topUser')->name('statistic.top.user');
        Route::get('/provinsi/{role}/{id}', 'DashboardController@contractByProvinsi')->name('statistic.contract.provinsi');
        Route::get('/willexpired/{role}/{id}', 'DashboardController@contractWillExpired')->name('statistic.contract.willexpired');
    });

    Route::get('/contract-details/{id}', 'BillboardContractController@showDetail')->name('contract-details'); // {{route('vendor-contract-details')}}

    //Route Detail Contract
    Route::group(['prefix' => '/contract-detail'], function(){
        Route::post('/', 'BillboardContractDetailController@store')->name('save-contract-detail');
        Route::get('/{id}', 'BillboardContractDetailController@show')->name('contractDetailShowByID');
        Route::put('/{id}', 'BillboardContractDetailController@update')->name('update-contract-detail');
        Route::delete('/{id}', 'BillboardContractDetailController@destroy')->name('delete-contract-detail');
    });

    Route::group(['prefix' => '/master'], function(){
        //Provinsi
        Route::get('provinsi/all', 'ProvinsiController@getAllProvinsi')->name('getAllProvinsi'); // {{route('getAllProvinsi')}}

        //Kabupaten
        Route::get('kabupaten/all', 'KabupatenController@getAllKabupaten')->name('getAllKabupaten'); // {{route('getAllKabupaten')}}
        Route::get('kabupaten/byprovinsi/{id}', 'KabupatenController@getKabupatenByProvinsi')->name('getKabupatenByProvinsi'); // {{route('getKabupatenByProvinsi', '')}}

        //Kecamatan
        Route::get('kecamatan/all', 'KecamatanController@getAllKecamatan')->name('getAllKecamatan'); // {{route('getAllKecamatan')}}
        Route::get('kecamatan/bykabupaten/{id}', 'KecamatanController@getKecamatanByKabupaten')->name('getKecamatanByKabupaten'); // {{route('getKecamatanByKabupaten', '')}}

        Route::get('user/all/{role?}', 'UserController@getAllUser')->name('getAllUser'); // {{route('getAllUser')}}
        Route::get('user/{id}', 'UserController@show')->name('getUserByID'); // {{route('getUserByID', '')}}
        Route::put('user/{id}', 'UserController@update')->name('update-user'); // {{route('update-user', '')}}
    });

    //profile
    Route::get('/profile', 'UserController@profile')->name('profile'); // {{route('profile')}}

    //change password
    Route::put('/changepassword', 'AuthController@changePassword')->name('changepassword'); // {{route('changepassword')}}

    //get billboard by vendor
    Route::get('/billboard/{id}/{verify?}', 'BillboardController@getBillboardByVendor')->name('getBillboardByVendor');

    //Ticket
    Route::put('/tiket/keluhan/{id_billboard}', 'TicketController@store');
    Route::put('/tiket/balas-keluhan/{id_ticket}', 'TicketController@store_reply');
    Route::get('/tiket/tutup-keluhan/{id_ticket}','TicketController@close_tiket')->name('close.feedback');

    //Ticket Event
    Route::post('/tiket/event/keluhan/{id_event}', 'TicketController@storeEvent')->name('open.feedback.event');
    Route::post('/tiket/event/balas-keluhan/{id_ticket}', 'TicketController@storeReplyEvent')->name('reply.feedback.event');
    Route::get('/tiket/event/tutup-keluhan/{id_ticket}','TicketController@closeTicketEvent')->name('close.feedback.event');

    //notification
    Route::get('/notification/{header?}', 'NotificationController@getNotification')->name('notification');
    Route::get('/notification/show/all', 'NotificationController@getNotificationAll')->name('notification.all');

    Route::get('/logout', 'AuthController@logout')->name('logout'); // {{route('logout')}}

    //Route Report
    Route::group(['prefix' => '/report'], function(){
        //billboard
        Route::get('/billboard/pdf/data/{verify}','ReportController@billboardPdf')->name('customerpdf');
        Route::get('/billboard/pdf/{id}','ReportController@billboardPdfOne');
        Route::get('/billboard/excel/{id}', 'ReportController@billboardExcelOne');

        //billboard contract
        Route::get('/billboard/contract/pdf/data/{status}','ReportController@billboardContractPdf')->name('reportcontractpdf');
        //Route::get('/billboard/contract/pdf/{id}','ReportController@billboardContractPdfOne')->name('reportpdfbyid');
        Route::get('/billboard/contract/excel/data/{status}','ReportController@billboardContractExcel')->name('reportcontractexcel');
        Route::get('/billboard/contract/excel/{id}','ReportController@billboardContractExcelOne');
        Route::get('/billboard/contract/pdf/{id}','ReportController@billboardContractPdfOne')->name('report.contract.pdf.single');

        //preview contract
        Route::group(['prefix' => '/preview'], function(){
            Route::get('/contract/single/{id}', 'BillboardContractController@reportContractOne')->name('contract.preview.one');
        });
    });

    Route::get('/contract', 'BillboardContractController@showContract')->name('contract');
    Route::get('/contract/excel','BillboardContractController@getExcel')->name('contract.excel');
    Route::get('/contract-details/report/{id}', 'BillboardContractController@showDetailReport');

    //Route File
    Route::group(['prefix' => '/file'], function(){
        Route::get('/{path}', 'FileController@download')->name('download');
        Route::delete('/photo/{id}', 'FileController@deletePhoto')->name('file-delete-photo');
    });
    /*
    |--------------------------------------------------------------------------
    | End Shared Routes
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Start EO Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:eo'], function(){
        Route::get('/dashboard-eo', 'DashboardController@dashboardEo')->name('dashboard-eo'); // {{route('dashboard-eo')}}

        Route::get('/detail-event/show/{id}', 'EventController@show')->name('detail-event-single'); // {{route('detail-event', '')}}
        Route::put('/detail-event/{id}', 'EventController@update')->name('update-event'); // {{route('update-event', '')}}
        Route::put('/detail-event/eo/{id}', 'EventController@updateEo')->name('update-event-eo'); // {{route('update-event', '')}}
    });
    /*
    |--------------------------------------------------------------------------
    | End EO Routes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Start Sampoerna Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:sampoerna'], function () {
        // Route::get('/dashboard-sampoerna', function(){
        //     return view('pages.sampoerna.dashboard');
        // })->name('dashboard-sampoerna');

        Route::get('/sampoerna-master-user', function(){
            return view('pages.sampoerna.master.index_master_user');
          })->name('sampoerna-master-user'); // {{route('sampoerna-master-user')}}

        Route::get('/sampoerna-master-wilayah', function(){
            return view('pages.sampoerna.master.index_master_wilayah');
        })->name('sampoerna-master-wilayah'); // {{route('sampoerna-master-wilayah')}}

        Route::get('/sampoerna-map', function(){
            return view('pages.sampoerna.sampoerna-map');
        })->name('sampoerna-map'); // {{route('sampoerna-master-wilayah')}}     
    });
    /*
    |--------------------------------------------------------------------------
    | End Sampoerna Routes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Start Sampoerna & EO Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['middleware' => 'role:sampoerna,eo,customer'], function () {
        Route::get('/detail-event/{id}', 'EventController@showDetailEvent')->name('detail-event'); // {{route('detail-event', '')}}
        
        Route::get('/detail-event/show/{id}', 'EventController@show')->name('detail-event-single'); // {{route('detail-event', '')}}

        Route::get('event', 'EventController@showEvent')->name('event'); // {{route('event')}}
        Route::get('event-report/{id}', 'EventController@reportEventOne')->name('report.event.single');
        Route::get('report/event/excel/{id}', 'ReportController@eventExcelOne')->name('report.event.single.excel');
        Route::get('report/event/pdf/{id}', 'ReportController@eventPdfOne')->name('report.event.single.pdf');
        
        Route::get('/sampoerna-report-event', function(){
            return view('pages.sampoerna.report-sampoerna');
        })->name('report-sampoerna');

        Route::post('/event', 'EventController@store')->name('save-event'); // {{route('save-event')}}
        Route::delete('/event/{id}', 'EventController@destroy')->name('delete-event'); // {{route('delete-event', '')}}
        Route::put('/detail-event/{id}', 'EventController@update')->name('update-event'); // {{route('update-event', '')}}
    });
    /*
    |--------------------------------------------------------------------------
    | End Sampoerna & EO Routes
    |--------------------------------------------------------------------------
    */

});
