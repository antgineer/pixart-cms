$(document).ready(function(){
    'use strict';

    $('#notification-center').click(function(e){
        e.preventDefault();
        
        if( $('#notification-body').is(':empty') ){
            $.get(route('notification', 'header'), result => {
                let elem = '';
                
                if(result.length > 0){
                    $.each(result, (key, val) => {
                        elem += `<div class="notification-item  clearfix">
                            <div class="heading">
                                <a href="${val.url}?ref=notification&notif=${val.id}" target="blank" class="${val.class} pull-left">
                                    <span class="bold">${val.message}</span>
                                </a>
                                <span class="pull-right time">${val.created_at}</span>
                            </div>
                        </div>`;
                    });
                }
                else{
                    elem = `<div class="notification-item  clearfix">
                        <div class="heading">
                            <div href="#" class="pull-left">
                                <span class="">Tidak ada notifikasi</span>
                            </div>
                        </div>
                    </div>`;
                }

                $('#notification-body').append(elem);
            });
        }
    });
});