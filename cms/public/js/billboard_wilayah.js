$('document').ready(function(){
    'use strict';

    //ajax kabupaten
    function kabupaten(){
        let prov = $('#prov').val();

        return $.get(route('getKabupatenByProvinsi', prov), function(data){
            $.each(data.data, function(key, val){
                $('#kab').append(`<option data-name="${val.nama_kabupaten_gmap.toLowerCase()}" value="${val.id}">${val.nama_kabupaten}</option>`);
            });
        });
    }

    //ajax kecamatan
    function kecamatan(){
        let kab = $('#kab').val();

        return $.get(route('getKecamatanByKabupaten', kab), function(data){
            $.each(data.data, function(key, val){
                $('#kec').append(`<option data-name="${val.nama_kecamatan_gmap.toLowerCase()}" value="${val.id}">${val.nama_kecamatan}</option>`);
            });
        });
    }

    //select option provinsi sesuai keluaran gmap
    function selectProvinsi(prov){
        let selectedProv = $('#prov').find(`option[data-name="${prov.toLowerCase()}"]`);
        if(selectedProv.length > 0){
            $('#prov').select2('val', selectedProv[0].value);
        }
        else{
            $('#prov').select2('val', '');
        }
    }

    //select option kabupaten sesuai keluaran gmap
    function selectKabupaten(kab){
        let selectedKab = $('#kab').find(`option[data-name="${kab.toLowerCase()}"]`);
        if(selectedKab.length > 0){
            $('#kab').select2('val', selectedKab[0].value);
        }
        else{
            $('#kab').select2('val', '');
            $('#kec').select2('val', '');
        }
    }

    //select option kecamatan sesuai keluaran gmap
    function selectKecamatan(kec){
        let selectedKec = $('#kec').find(`option[data-name="${kec.toLowerCase()}"]`);
        if(selectedKec.length > 0){
            $('#kec').select2('val', selectedKec[0].value);
        }
        else{
            $('#kec').select2('val', '');
        }
    }

    function setWilayah(data, latlng){
        console.log(data);
        //provinsi select
        selectProvinsi(data.administrative_area_level_1);

        $.when( kabupaten() ).then(() => {
            //kabupaten select
            if(data.administrative_area_level_2){
                selectKabupaten(data.administrative_area_level_2);
                
                let idKab = $('#kab').val();
                
                if(idKab) return kecamatan(idKab);
            }
        })
        .then(() => {
            //kecamatan select
            if(data.administrative_area_level_3) selectKecamatan(data.administrative_area_level_3);
        });

        $('#address').val(data.route || '');
        $('#latitude').val(latlng.lat());
        $('#longitude').val(latlng.lng());
    }

    let mapObj = new GMaps({
        el: '#map',
        lat: -6.214213,
        lng: 106.840674,
        zoom: 10
    });

    let input = document.getElementById('cari_alamat');
    let autocomplete = new google.maps.places.Autocomplete(input, {
        componentRestrictions: {country: "id"}
    });
    //autocomplete.bindTo('bounds', mapObj);

    //event saat isi textbox autocomplete berubah
    autocomplete.addListener('place_changed', function() {
        let address = $('#cari_alamat').val();
        $('#kab').empty();
        $('#kec').empty();

        GMaps.geocode({
            address: address,
            callback: (results, status) => {
                if(status == 'OK') {
                    //console.log(results);
                    let data = {};
                    let latlng = results[0].geometry.location;

                    //susun ulang data address component
                    $.each(results[0].address_components, (key, val) => {
                        data[val.types[0]] = val.long_name;
                    });
                    
                    mapObj.removeMarkers(mapObj.markers);
                    
                    mapObj.setCenter(latlng.lat(), latlng.lng());
                    mapObj.setZoom(16);
                    mapObj.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng(),
                        draggable: true,
                        dragend: event => {
                            //var lat = event.latLng.lat();
                            //var lng = event.latLng.lng();

                            GMaps.geocode({
                                lat: event.latLng.lat(),
                                lng: event.latLng.lng(),
                                callback: (results, status) => {
                                    if(status == 'OK') {
                                        console.log(results);

                                        let dataDrag = {};
                                        let latlngDrag = results[0].geometry.location;

                                        //susun ulang data address component
                                        $.each(results[0].address_components, (key, val) => {
                                            dataDrag[val.types[0]] = val.long_name;
                                        });

                                        setWilayah(dataDrag, latlngDrag);
                                    }
                                }
                            });

                            //$('#latitude').val(lat);
                            //$('#longitude').val(lng);
                        }
                    });

                    setWilayah(data, latlng);

                } else if (status == 'ZERO_RESULTS') {
                    alert('Sorry, no location named ' + address);
                }
            }
        });
    });

    //combobox provinsi
    $.get(route('getAllProvinsi'), function(data){
        $.each(data.data, function(key, val){
            $('#prov').append(`<option data-name="${val.nama_provinsi_gmap.toLowerCase()}" value="${val.id}">${val.nama_provinsi}</option>`);
        });
    });

    $('#prov').change(function(){
        $('#kab').empty();
        $('#kab').select2();

        $('#kec').empty();
        $('#kec').select2();

        kabupaten();
    });

    $('#kab').change(function(){
        $('#kec').empty();
        $('#kec').select2();

        kecamatan();
    });
});