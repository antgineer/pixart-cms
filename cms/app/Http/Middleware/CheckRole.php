<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $userRole = session('role_name');

        foreach($roles as $role){
            if($userRole == $role){
                return $next($request);
            }
        }

        return redirect('/');
    }
}
