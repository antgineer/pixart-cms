<?php

namespace App\Http\Middleware;

use Closure;
// use GuzzleHttp\Client;
// use GuzzleHttp\Exception\RequestException;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( session('auth_token') ){
            //try {
                // $token = session('auth_token');
                // JWTAuth::parseToken()->authenticate();
                // return $next($request);
            
            //} catch (JWTException $e) {
                // $session = session('auth_token');
                
                // $client = new Client([
                //     'base_uri'=> env('API_URL'),
                // ]);
        
                // $res = $client->get("refreshtoken", [
                //     'http_errors' => false,
                //     'headers' => [
                //         'Accept' => 'application/json',
                //         'Authorization' => "Bearer $session"
                //     ]
                // ]);
        
                // $status = $res->getStatusCode();
                // $body = json_decode($res->getBody(), true);
                
                // if($status == 200){
                //     $token = $body['token'];
                //     session(['auth_token' => $token]);

                //     return $next($request);
                // }
            //}

            return $next($request);
        }
            
        return redirect('/');
    }
}
