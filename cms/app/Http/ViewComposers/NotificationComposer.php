<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class NotificationComposer
{
    public function compose(View $view)
    {
        $client = new Client([
            'base_uri'=> env('API_URL'),
        ]);

        $session = session('auth_token');
        $idUser = session('role_name') != 'admin' ? session('id') : null;
        
        $res = $client->get("notification/total/$idUser", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        $data = $status == 200 ? $body : array();

        $view->with('notification', $data);
    }
}