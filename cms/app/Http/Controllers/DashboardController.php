<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class DashboardController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function dashboardAdmin(){
        $session = session('auth_token');
        $idUser = session('id');
        
        $res = $this->client->get("statistic/administrator", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status == 200){
            return view('pages.admin.dashboard', $body['data']);
        }
    }

    public function dashboardVendor(){
        $session = session('auth_token');
        $idUser = session('id');
        
        $res = $this->client->get("statistic/vendor/$idUser", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status == 200){
            return view('pages.vendor.dashboard', $body['data']);
        }
    }

    public function dashboardCustomer(){
        $session = session('auth_token');
        $idUser = session('id');
        
        $res = $this->client->get("statistic/customer/$idUser", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.customer.dashboard', $body['data']);
        }
    }

    public function dashboardEo(){
        $session = session('auth_token');
        $idUser = session('id');

        $query = 'page=1&limit=2';
        
        $res = $this->client->get("event?$query", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.eo.dashboard', $body);
        }
    }

    public function topUser($role, $id){
        $session = session('auth_token');
        $idUser = session('id');
        
        $res = $this->client->get("statistic/contract/topuser/$role/$idUser", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return $body;
        }
    }

    public function contractByProvinsi($role, $id){
        $session = session('auth_token');
        $idUser = session('id');
        
        $res = $this->client->get("statistic/contract/provinsi/$role/$idUser", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $data = [];

        if($status == 200){
            foreach($body['data'] as $val){
                if($val['total_kontrak'] > 0){
                    $data[] = [
                        'kode_provinsi' => $val['kode_provinsi'],
                        'total_kontrak' => $val['total_kontrak']  
                    ];
                }
            }
            return $data;
        }

        return $data;
    }

    public function contractWillExpired($role, $id){
        $session = session('auth_token');
        $idUser = session('id');
        
        $res = $this->client->get("statistic/contract/willexpired/$role/$idUser", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        $data = [];
        if($status == 200){
            if($body['data']){
                $arrBulan = [null, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                
                foreach($body['data'] as $val){
                    $data[] = [
                        $arrBulan[$val['bulan']].' '.$val['tahun'],
                        $val['total_kontrak']
                    ];
                }
            }
            else{
                $data[] = [
                    'Tidak ada data', 0
                ];
            }
            
            return $data;
        }

        return $data;
    }
}
