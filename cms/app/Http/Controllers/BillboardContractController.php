<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Excel;

class BillboardContractController extends Controller
{
    private $client;

    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function showContract(Request $request){
        $session = session('auth_token');
        $role = session('role_name');
        $idUser = session('id');

        $parameters = 'page=1&limit=12';
        $currentPage = $request->page ?: 1;

        if($request){
            if(!$request->page){
                $page = $request->page ?: 1;
                $tempArray[] = "page=$page";
            }

            if(!$request->limit){
                $limit = $request->limit ?: 12;
                $tempArray[] = "limit=$limit";
            }

            foreach($request->all() as $key => $val){
                $tempArray[] = "$key=$val";
                $parameters = implode('&', $tempArray);
            }
        }

        $queryString = $_GET;

        if($queryString){
            unset($queryString['page'], $queryString['limit']);
            $queryString = http_build_query($queryString);
        }

        if(!$queryString || empty($queryString)){
            $queryString = null;
        }

        $res = $this->client->get("contract/$role/$idUser?$parameters", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $body['status_search'] = $request->status ?: 'all';
        $body['current_page'] = $currentPage;
        $body['query'] = $queryString;

        $request->flash();

        return view('pages.contract', $body);
    }

    public function showContractByMap(Request $request){
        $session = session('auth_token');
        $role = session('role_name');
        $idUser = session('id');

        $parameters = 'page=1&limit=12';
        $currentPage = $request->page ?: 1;

        if($request){
            if(!$request->show_all){
                if(!$request->page){
                    $page = $request->page ?: 1;
                    $tempArray[] = "page=$page";
                }

                if(!$request->limit){
                    $limit = $request->limit ?: 12;
                    $tempArray[] = "limit=$limit";
                }
            }

            foreach($request->all() as $key => $val){
                $tempArray[] = "$key=$val";
                $parameters = implode('&', $tempArray);
            }
        }

        $queryString = $_GET;

        if($queryString){
            unset($queryString['page'], $queryString['limit']);
            $queryString = http_build_query($queryString);
        }

        if(!$queryString || empty($queryString)){
            $queryString = null;
        }

        $res = $this->client->get("contract/$role/$idUser?$parameters", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $body['status_search'] = $request->status ?: 'all';
        $body['current_page'] = $currentPage;
        $body['query'] = $queryString;

        return response()->json($body, 200);
    }

    public function store(Request $request){
        $session = session('auth_token');

        $idCustomer = ($request->id_customer != 'other') ? $request->id_customer : null;

        $postData = [
            ['name' => 'id_vendor', 'contents' => session('id')],
            ['name' => 'id_customer', 'contents' => $idCustomer],
            ['name' => 'id_billboard', 'contents' => $request->id_billboard],
            ['name' => 'customer_name', 'contents' => $request->customer_name],
            ['name' => 'contract_date', 'contents' => $request->contract_date],
            ['name' => 'expired_date', 'contents' => $request->expired_date],
        ];

        $res = $this->client->post('contract', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        if($request->contract_date || $request->expired_date){
            $contractDateArr = explode('/', $request->contract_date);
            $contractDate = $contractDateArr['2'].'-'.$contractDateArr['0'].'-'.$contractDateArr['1'];

            $expiredDateArr = explode('/', $request->expired_date);
            $expiredDate = $expiredDateArr['2'].'-'.$expiredDateArr['0'].'-'.$expiredDateArr['1'];
        }
        else{
            $contractDate = null;
            $expiredDate = null;
        }

        $idCustomer = ($request->id_customer != 'other') ? $request->id_customer : null;

        $postData = [
            ['name' => '_method', 'contents' => $request->_method],
            ['name' => 'id_vendor', 'contents' => session('id')],
            ['name' => 'id_customer', 'contents' => $idCustomer],
            ['name' => 'id_billboard', 'contents' => $request->id_billboard],
            ['name' => 'customer_name', 'contents' => $request->customer_name],
            ['name' => 'contract_date', 'contents' => $contractDate],
            ['name' => 'expired_date', 'contents' => $expiredDate],
        ];

        $res = $this->client->post("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);


        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }

    public function showDetail(Request $request, $id){
        $session = session('auth_token');
        $reference = $request->ref ?: null;

        $res = $this->client->get("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        if($reference == 'notification'){
            $idNotif = $request->notif;
            $admin = session('role_name') == 'admin' ? true : false;

            $notif = $this->client->put("notification/$idNotif/$admin", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);
        }

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        // return $body;

        return view('pages.details_contract', $body['data']);
    }

    public function getContractByUser($id){
        $session = session('auth_token');
        $role = session('role_name');

        $res = $this->client->get("contract/$role/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $contractDate = explode(' ', $value['contract_date']);
                    $expiredDate = explode(' ', $value['expired_date']);
                    $customer = ($value['customer'] != null) ? $value['customer']['profile']['fullname'] : null;

                    $data[] = [
                        'index'         => $num++,
                        'id'            => $value['id'],
                        'contract_date' => $contractDate[0],
                        'expired_date'  => $expiredDate[0],
                        'kode'          => $value['billboard']['kode_billboard'],
                        'customer'      => $customer,
                        'guest'         => $value['customer_name']
                    ];
                }

                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function getBillboardContract($role, $id, $status = 'all'){
        $session = session('auth_token');

        $res = $this->client->get("billboard/contract/$role/$id?status=$status", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $contractDate = explode(' ', $value['contract_date']);
                    $expiredDate = explode(' ', $value['expired_date']);

                    $data[] = [
                        'index'         => $num++,
                        'id'            => $value['id'],
                        'id_contract'   => $value['id_contract'],
                        'contract_date' => $contractDate[0],
                        'expired_date'  => $expiredDate[0],
                        'kode'          => $value['kode_billboard'],
                        'address'       => $value['address'],
                        'panjang'       => $value['panjang'],
                        'lebar'         => $value['lebar'],
                        'bahan'         => $value['bahan'],
                        'harga'         => number_format($value['harga'], '0', ',', '.'),
                        'latitude'      => $value['latitude'],
                        'longitude'     => $value['longitude'],
                        'sisa_hari'     => $value['days_remaining'],
                        'customer'      => $value['customer_fullname'],
                        'guest'         => $value['other_customer'],
                    ];
                }

                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function getExcel(Request $request){
        $session = session('auth_token');
        $role = session('role_name');
        $idUser = session('id');

        $parameters = null;

        if($request){

            foreach($request->all() as $key => $val){
                $tempArray[] = "$key=$val";
                $parameters = implode('&', $tempArray);
            }
        }

        $queryString = $_GET;

        if($queryString){
            unset($queryString['page'], $queryString['limit']);
            $queryString = http_build_query($queryString);
        }

        if(!$queryString || empty($queryString)){
            $queryString = null;
        }

        $res = $this->client->get("contract/$role/$idUser?$parameters", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $body['status_search'] = $request->status ?: 'all';
        $body['query'] = $queryString;
        $data = $body['data'];
        $request->flash();
        return Excel::create('Billboard-Report-'.date('d-m-Y'), function($excel) use($data){
            $excel->sheet('Data Billboard', function($sheet)  use($data){
                $sheet->loadView('report.contract.customer.excel')->with('data',$data);
            });
        })->export('xlsx');

    }

    public function reportContract($status){
        $session = session('auth_token');

        $id = session('id');
        $role = session('role_name');

        $res = $this->client->get("billboard/contract/$role/$id?status=$status", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        //$status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $data = [
            'status' => $status,
            'data'   => $body['data']
        ];

        return view('pages.customer.preview', $data);
    }

    public function reportContractOne($id){
        $session = session('auth_token');

        $res = $this->client->get("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        //redirect jika bukan pemilik vendor

        //return $body['data'];

        if($status == 200 && ($body['data']['id_customer'] == session('id') || $body['data']['id_vendor'] == session('id'))){
            return view('report.contract.contract_preview_single', $body['data']);
        }
        else{
            return redirect('/contract');
        }
    }

    public function showDetailReport($id){
        $session = session('auth_token');

        $res = $this->client->get("contract/".session('role_name')."/$id/", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        // return $body;
        return view("pages.customer.contract.report_contract", $body);
    }

}
