<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class BillboardContractDetailController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function store(Request $request){
        $session = session('auth_token');

        if($request->expired_date){
            $expiredDateArr = explode('/', $request->expired_date);
            $expiredDate = $expiredDateArr['2'].'-'.$expiredDateArr['0'].'-'.$expiredDateArr['1'];
        }
        else{
            $expiredDate = null;
        }

        $postData = [
            ['name' => 'tema_iklan', 'contents' => $request->tema_iklan],
            ['name' => 'brand', 'contents' => $request->brand],
            ['name' => 'expired_date', 'contents' => $expiredDate],
            ['name' => 'id_contract', 'contents' => $request->id_contract],
            ['name' => 'id_user', 'contents' => session('id')],
        ];

        if($request->file){
            foreach($request->file as $file){
                $postData[] = ['name' => 'file[]', 'contents' => fopen($file, 'r')];
            }
        }

        $res = $this->client->post('contract-detail', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("contract-detail/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        //return $request->all();

        if($request->expired_date){
            $expiredDateArr = explode('/', $request->expired_date);
            $expiredDate = $expiredDateArr['2'].'-'.$expiredDateArr['0'].'-'.$expiredDateArr['1'];
        }
        else{
            $expiredDate = null;
        }

        $postData = [
            ['name' => '_method', 'contents' => $request->_method],
            ['name' => 'tema_iklan', 'contents' => $request->tema_iklan],
            ['name' => 'brand', 'contents' => $request->brand],
            ['name' => 'expired_date', 'contents' => $expiredDate],
            ['name' => 'id_contract', 'contents' => $request->id_contract],
            ['name' => 'id_user', 'contents' => session('id')],
        ];

        if($request->file){
            foreach($request->file as $file){
                $postData[] = ['name' => 'file[]', 'contents' => fopen($file, 'r')];
            }
        }

        $res = $this->client->post("contract-detail/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("contract-detail/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }
}
