<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Excel;
use PDF;
use PDFS;

class ReportController extends Controller
{
    private $client;

    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function billboardPdf(Request $request){
        $startDate = $request->start_date;
        $endDate = $request->end_date;
           
        if(!empty($startDate) || !empty($endDate)){
            $session = session('auth_token');
            $id = session('id');

            $url = session('role_name') == 'admin' ? 'billboard' : "billboard/vendor/$id";

            $res = $this->client->get("$url?verify=verified&startdate=$startDate&enddate=$endDate", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);

            $status = $res->getStatusCode();
            $body = json_decode($res->getBody(), true);

            $pdf = PDFS::loadView('report.billboard.billboard-pdf', $body);
            return $pdf->download('Billboard-Report-list.pdf');
            //return $pdf->stream('Billboard-Report-list.pdf', ['Attachment' => false]);
        }
    }

    public function billboardPdfOne($id){
        $session = session('auth_token');

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        $data = $body['data'];

        //return $data;
        $pdf = PDFS::loadView('report.billboard.billboard-pdf-single', $data);
        return $pdf->download('Billboard-Report-Single.pdf');
        //return $pdf->stream('Billboard-Report-Single.pdf', ['Attachment' => false]);
    }

    public function billboardExcel(Request $request){
        $startDate = $request->start_date;
        $endDate = $request->end_date;
           
        if(!empty($startDate) || !empty($endDate)){
            $session = session('auth_token');
            $id = session('id');

            $url = session('role_name') == 'admin' ? 'billboard' : "billboard/vendor/$id";

            $res = $this->client->get("$url?verify=verified&startdate=$startDate&enddate=$endDate", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);

            $status = $res->getStatusCode();
            $body = json_decode($res->getBody(), true);

            $data=$body['data'];

            return Excel::create('Billboard-Report-list', function($excel) use($data){
                $excel->sheet('Data Billboard', function($sheet)  use($data){
                    $sheet->loadView('report.billboard.billboard-excel')->with('data',$data);
                });
            })->export('xlsx');
        }
    }

    public function billboardExcelOne($id){
        $session = session('auth_token');

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        $data = $body['data'];
        return Excel::create('Billboard-Report-Single', function($excel) use($data){
            $excel->sheet('Data Billboard', function($sheet)  use($data){
                $sheet->loadView('report.billboard.excelsingle')->with('data',$data);
            });
        })->export('xlsx');
    }

    public function billboardContractPdf($status){
        $session = session('auth_token');
        $id = session('id');
        $role = session('role_name');

        $res = $this->client->get("billboard/contract/$role/$id?status=$status", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $pdf = PDF::loadView('includes.customer-report-preview', $body);
        return $pdf->download('Billboard-Contract-Report.pdf');
    }

    public function billboardContractExcel($status){
        $session = session('auth_token');
        $id = session('id');
        $role = session('role_name');

        $res = $this->client->get("billboard/contract/$role/$id?status=$status", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        $data = $body['data'];

        return Excel::create('Billboard-Contract-Report', function($excel) use($data){
            $excel->sheet('Data Contract', function($sheet)  use($data){
                $sheet->loadView('report.contract.excel')->with('data',$data);
            });
        })->export('xlsx');
    }

    public function billboardContractExcelOne($id){
        $session = session('auth_token');

        $res = $this->client->get("contract/billboard/detail/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        $data = $body['data'];
        return Excel::create('Billboard-Contract-Report-Single', function($excel) use($data){
            $excel->sheet('Data Contract', function($sheet)  use($data){
                $sheet->loadView('report.contract.excelsingle')->with('data',$data);
            });
        })->export('xlsx');
    }

    public function billboardContractPdfOne($id){
        $session = session('auth_token');

        $res = $this->client->get("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        //return $data;
        $pdf = PDFS::loadView('report.contract.contract-pdf-single', $body['data']);
        return $pdf->download('Contract-Report-Single.pdf');
        //return $pdf->stream('Contract-Report-Single.pdf', ['Attachment' => false]);
    }

    public function reportContractPDF($id){
        $session = session('auth_token');
    
        $res = $this->client->get("contract/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        //redirect jika bukan pemilik vendor
        
        // return $body['data'];

        if($status == 200 && $body['data']['id_customer'] == session('id')){
            // return view('pages.customer.preview-one', $body['data']);
            $pdf = PDFS::loadView('includes.report_contract_pdf', $body['data']);
            return $pdf->download('Report.pdf');
        }
        else{
            return redirect('/customer-report');
        }
    }

    public function vendorReportBillboardDetail(Request $request){
        
        if((!empty($request['start'])) || (!empty($request['end']))){
            $session = session('auth_token');

            $res = $this->client->get("billboard/detail/".session('id')."?verify=verified&stardate=$request[start]&enddate=$request[end]", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);

            $status = $res->getStatusCode();
            $body = json_decode($res->getBody(), true);
            
            $pdf = PDFS::loadView('includes.billboard-pdf-vendor-list', $body);
            return $pdf->download('Billboard-Report-list.pdf');
            // return view('includes.billboard-pdf-single-list', $body);

        }
    }

    public function vendorReportBillboardDetailExcel(Request $request){
        if((!empty($request['start'])) || (!empty($request['end']))){
            $session = session('auth_token');
   
            $res = $this->client->get("billboard/detail/".session('id')."?verify=verified&stardate=$request[start]&enddate=$request[end]", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);
   
            $status = $res->getStatusCode();
            $body = json_decode($res->getBody(), true);
            
            $data=$body['data'];
            return Excel::create('Billboard-Report-list', function($excel) use($data){
                $excel->sheet('Data Billboard', function($sheet)  use($data){
                    $sheet->loadView('report.billboard.billboard-excel-vendor-list')->with('data',$data);
                });
            })->export('xlsx');
        }
    }

    public function eventExcelOne($id){
        $session = session('auth_token');
        
        $res = $this->client->get("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        $data = $body['data'];

        return Excel::create('Event-Report-Single', function($excel) use($data){
            $excel->sheet('Data Event', function($sheet)  use($data){
                $sheet->loadView('report.event.event-excel')->with('data',$data);
            });
        })->export('xlsx');
    }

    public function eventPdfOne($id){
        $session = session('auth_token');

        $res = $this->client->get("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        $data = $body['data'];

        //return $data;
        $pdf = PDFS::loadView('report.event.event-pdf', $data);
        return $pdf->download('Event-Report.pdf');
        //return $pdf->stream('Event-Report.pdf', ['Attachment' => false]);
    }
}
