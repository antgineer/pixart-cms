<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class UserController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function getAllUser($role = null){
        // $page = ($request->start / $request->length) + 1;
        // $limit = $request->length;
        // $order = $request->order['0']['column'];
        // $orderDir = $request->order['0']['dir'];
        // $keyword = $request->search['value'];
        $session = session('auth_token');

        if($role == 'customer') $role = 'customer,sampoerna';
        if($role == 'sampoerna') $role = 'sampoerna,eo';

        $column = ['id', 'username', 'fullname', 'address', 'phone', 'email'];
        //$url = "user?page=$page&limit=$limit&orderby=$column[$order]&orderdir=$orderDir&keyword=$keyword";
        $res = $this->client->get("user?role=$role&limit=20", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $data[] = [
                        'index'     => $num++,
                        'id'        => $value['id'],
                        'username'  => $value['username'],
                        'id_role'   => $value['id_role'],
                        'parent'    => $value['parent_fullname'],
                        'fullname'  => $value['fullname'],
                        'address'   => $value['address'],
                        'phone'     => $value['phone'],
                        'email'     => $value['email'],
                        'badge'     => $value['badge'],
                        'role_name' => $value['role_name']
                    ];
                }

                // $dataTables = [
                //     'draw'            => intval($req->draw),
                //     "recordsTotal"    => intval( $res->totalData ),
                //     "recordsFiltered" => intval( $res->totalFilter ),
                //     "data"            => $data
                // ];

                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function store(Request $request, $role){
        $session = session('auth_token');
        
        //ambil role
        $roleRes = $this->client->get("role/name/$role", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $roleBody = json_decode($roleRes->getBody(), true);
        $roleId = $roleBody['data']['id'];

        //susun data
        foreach($request->all() as $key => $value){
            if($key != 'file'){
                $postData[] = ['name' => $key, 'contents' => $value];
            }else{
                if($value != 'undefined'){
                    $postData[] = ['name' => 'file', 'contents' => fopen($value, 'r')];
                }
            }
        }

        $postData[] = ['name' => 'role', 'contents' => $roleId];

        if( session('role_name') != 'admin' ){
            $postData[] = ['name' => 'parent', 'contents' => session('id')];
        }

        $res = $this->client->post('user', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("user/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function profile(){
        $session = session('auth_token');
        $id = session('id');

        $res = $this->client->get("user/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        return view('pages.profile', $body['data']);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        //susun data
        foreach($request->all() as $key => $value){
            if($key != 'file'){
                $postData[] = ['name' => $key, 'contents' => $value];
            }else{
                if($value != 'undefined'){
                    $postData[] = ['name' => 'file', 'contents' => fopen($value, 'r')];
                }
            }
        }

        if($request->role){
            //ambil role
            $role = $request->role;
            $roleRes = $this->client->get("role/name/$role", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);

            $roleBody = json_decode($roleRes->getBody(), true);
            $roleId = $roleBody['data']['id'];

            $postData[] = ['name' => 'id_role', 'contents' => $roleId];
        }
        
        $res = $this->client->post("user/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        if( session('id') == $id ){
            session([
                'fullname' => $body['data']['profile']['fullname'],
                'photo'    => $body['data']['photo']
            ]);
        }

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("user/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }
}
