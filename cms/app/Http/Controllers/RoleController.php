<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class RoleController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function getAllRole(){
        $session = session('auth_token');

        $res = $this->client->get("role", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        $num = 1;
        foreach($body['data'] as $key => $value){
            $data[] = [
                'index'     => $num++,
                'id'        => $value['id'],
                'role_name' => $value['role_name'],
                'harga'     => $value['harga']
            ];
        }
        if($status == 200){
            return response()->json(['data' => $data], 200);
        }
    }

    public function store(Request $request){
        $session = session('auth_token');

        $postData = [
            'role_name' => $request->role_name,
            'harga' => $request->harga
        ];

        $res = $this->client->post('role', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("role/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        $postData = [
            'role_name' => $request->role_name,
            'harga'   => $request->harga
        ];
        
        $res = $this->client->put("role/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("role/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }
}