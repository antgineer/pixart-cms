<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Excel;
use PDF;

class BillboardController extends Controller
{
    private $client;

    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function getAllBillboard(Request $request){
        $session = session('auth_token');

        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';
        $keyword   = $request->keyword ?: null;
        $verify    = $request->verify ?: 'all';

        $res = $this->client->get("billboard?verify=$verify&page=$page&limit=$limit", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $data[] = [
                        'index'     => $num++,
                        'id'        => $value['id'],
                        'kode'      => $value['kode_billboard'],
                        'vendor'    => $value['user_fullname'],
                        // 'provinsi'  => $value['provinsi']['nama_provinsi'],
                        // 'kabupaten' => $value['kabupaten']['nama_kabupaten'],
                        // 'kecamatan' => $value['kecamatan']['nama_kecamatan'],
                        'address'   => $value['address'],
                        'photo'     => $value['photo']
                    ];
                }
                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function getAllBillboardDetailByVendor(Request $request, $id){
        $session = session('auth_token');

        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';
        $keyword   = $request->keyword ?: null;
        $verify    = $request->verify ?: 'all';

        $res = $this->client->get("billboard/detail/$id?verify=$verify&page=$page&limit=$limit", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $data[] = [
                        'index'     => $num++,
                        'id'        => $value['id'],
                        'kode'      => $value['kode_billboard'],
                        'vendor'    => $value['vendor'],
                        'address'   => $value['address'],
                        'latitude'  => $value['latitude'],
                        'longitude' => $value['longitude'],
                        'verify'    => $value['verify'],
                        'remaining' => $value['days_remaining'],
                        'photo'     => $value['photo']
                    ];
                }
                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function getLatestBillboard($verify = 'all'){
        $session = session('auth_token');

        $res = $this->client->get("billboard?verify=$verify&page=1&limit=20", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $data[] = [
                        'index'     => $num++,
                        'id'        => $value['id'],
                        'kode'      => $value['kode_billboard'],
                        'vendor'    => $value['user_fullname'],
                        'address'   => $value['address'],
                        'latitude'  => $value['latitude'],
                        'longitude' => $value['longitude'],
                        'verify'    => $value['verify'],
                        'photo'     => $value['photo']
                    ];
                }
                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function store(Request $request){
        $session = session('auth_token');

        //susun data
        foreach($request->all() as $key => $value){
            if($key != 'file'){
                $postData[] = ['name' => $key, 'contents' => $value];
            }else{
                if($value != 'undefined'){
                    foreach($value as $file){
                        $postData[] = ['name' => 'file[]', 'contents' => fopen($file, 'r')];
                    }
                }
            }
        }

        if(session('role_name') == 'vendor'){
            $postData[] = ['name' => 'id_user', 'contents' => session('id')];
        }

        $verify = (session('role_name') == 'admin') ? '1' : '0';

        $postData[] = ['name' => 'verify', 'contents' => $verify];


        $res = $this->client->post('billboard', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show(Request $request, $id){
        $session = session('auth_token');
        $reference = $request->ref ?: null;

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        if($reference == 'notification'){
            $idNotif = $request->notif;
            $admin = session('role_name') == 'admin' ? true : false;

            $notif = $this->client->put("notification/$idNotif/$admin", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);
        }

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.admin.master.billboard.detail_billboard', $body['data']);
        }
    }

    public function edit($id){
        $session = session('auth_token');

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.admin.master.billboard.edit_billboard', $body['data']);
        }
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        if(!$request->id_user) $postData[] = ['name' => 'id_user', 'contents' => session('id')];

        //susun data
        foreach($request->all() as $key => $value){
            if($key != 'file'){
                $postData[] = ['name' => $key, 'contents' => $value];
            }else{
                //if($value != 'undefined'){
                    foreach($value as $file){
                        $postData[] = ['name' => 'file[]', 'contents' => fopen($file, 'r')];
                    }
                //}
            }
        }

        $res = $this->client->post("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }

    public function getBillboardByVendor($id, $verify = 'all'){
        $session = session('auth_token');

        $res = $this->client->get("billboard/vendor/$id?verify=$verify", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $data[] = [
                        'index'     => $num++,
                        'id'        => $value['id'],
                        'kode'      => $value['kode_billboard'],
                        'vendor'    => $value['user_fullname'],
                        'address'   => $value['address'],
                        'latitude'  => $value['latitude'],
                        'longitude' => $value['longitude'],
                        'verify'    => $value['verify'],
                        'photo'     => $value['photo']
                    ];
                }
                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    public function showVendor(Request $request, $id){
        $session = session('auth_token');
        $reference = $request->ref ?: null;

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        if($reference == 'notification'){
            $idNotif = $request->notif;
            $admin = session('role_name') == 'admin' ? true : false;

            $notif = $this->client->put("notification/$idNotif/$admin", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);
        }

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.vendor.master.billboard.detailbillboard', $body['data']);
        }
    }

    public function editVendor($id){
        $session = session('auth_token');

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.vendor.master.billboard.editbillboard', $body['data']);
        }
    }

    public function verify(Request $request, $id){
        $session = session('auth_token');

        $postData = [
            'type' => $request->type,
            'note' => $request->note
        ];

        $res = $this->client->put("billboard/verify/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'message' => $body['message']
        ], 200);
    }

    // public function reportBillboard($verify){
    //     $session = session('auth_token');
    //     $id = session('id');

    //     $res = $this->client->get("billboard/vendor/$id?verify=$verify", [
    //         'http_errors' => false,
    //         'headers' => [
    //             'Accept' => 'application/json',
    //             'Authorization' => "Bearer $session"
    //         ]
    //     ]);

    //     $status = $res->getStatusCode();
    //     $body = json_decode($res->getBody(), true);

    //     $data = [
    //         'verify' => $verify,
    //         'data'   => $body['data']
    //     ];
        
    //     if($status == 200){
    //         return view('pages.vendor.preview', $data);
    //     }
    // }

    public function reportBillboardOne($id){
        $session = session('auth_token');

        $res = $this->client->get("billboard/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        //redirect jika bukan pemilik vendor
        if(($status == 200 && $body['data']['id_user'] == session('id')) || session('role_name')=="admin"){
            return view('report.billboard.billboard-preview-single', $body['data']);
        }
        else{
            return redirect('/vendor-report');
        }
    }

    public function reportBillboardDetail(Request $request){
        $startDate = $request->start_date;
        $endDate = $request->end_date;
           
        if(!empty($startDate) || !empty($endDate)){
            $session = session('auth_token');
            $id = session('id');
            
            $url = session('role_name') == 'admin' ? 'billboard' : "billboard/vendor/$id";

            $res = $this->client->get("$url?verify=verified&startdate=$startDate&enddate=$endDate", [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);

            $status = $res->getStatusCode();
            $body = json_decode($res->getBody(), true);

            return view('report.billboard.billboard-preview', $body);
        }
    }
}
