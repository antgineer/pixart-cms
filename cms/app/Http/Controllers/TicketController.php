<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class TicketController extends Controller
{
    //
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function store(Request $request, $idbillboard){
    	$session = session('auth_token');

    	$postData = [
            'title' => $request->keluhan,
            'id_detail_contract' => $idbillboard,
            'id_customer' => session('id')
        ];

        $res = $this->client->request('POST','ticket', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) 
            return redirect()->back();
        else
            return redirect()->back();
    }

    public function store_reply(Request $request, $idtiket){
        $session = session('auth_token');

    	$postData = [
            'content' => $request->reply_keluhan,
            'id_ticket' => $idtiket,
            'id_user' => session('id')
        ];

        $res = $this->client->request('POST','ticket/reply', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        return redirect()->back();
    }

    public function close_tiket(Request $request, $idtiket){
        $session = session('auth_token');

    	$postData = [
            'id_ticket' => $idtiket
        ];

        $res = $this->client->request('POST','ticket/close', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        return redirect()->back();
    }

    public function storeEvent(Request $request, $id){
    	$session = session('auth_token');

    	$postData = [
            'title'       => $request->keluhan,
            'id_event'    => $id,
            'id_customer' => session('id')
        ];

        $res = $this->client->request('POST','ticket-event', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        return redirect()->back();
    }

    public function storeReplyEvent(Request $request, $id){
        $session = session('auth_token');

    	$postData = [
            'content'   => $request->reply_keluhan,
            'id_ticket' => $id,
            'id_user'   => session('id')
        ];

        $res = $this->client->request('POST','ticket-event/reply', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        return redirect()->back();
    }

    public function closeTicketEvent(Request $request, $id){
        $session = session('auth_token');

    	$postData = [
            'id_ticket' => $id
        ];

        $res = $this->client->request('PUT','ticket-event/close', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        return redirect()->back();
    }
}
