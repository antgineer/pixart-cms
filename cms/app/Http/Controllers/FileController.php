<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class FileController extends Controller
{
    public function download($path){
        $client = new Client([
            'base_uri'=> env('API_URL'),
        ]);

        $res = $client->get("download/$path", [
            'http_errors' => false,
            'headers'   => [
                'Accept' => 'image/jpeg'
            ]
        ]);

        $status = $res->getStatusCode();

        //jika sukses
        if($status == 200){
            return $res;
            //return $res->getBody();

            // $body = $res->getBody();
            // header('Content-Type: image/jpeg');
            // $im = imagecreatefromstring($body);

            // if ($im !== false) {
            //     imagejpeg($im);
            //     imagedestroy($im);
            // }
        }
    }

    public function deletePhoto($id){
        $client = new Client([
            'base_uri'=> env('API_URL'),
        ]);

        $session = session('auth_token');

        $res = $client->delete("media/photo/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }

    // public function deleteVideo($id){
    //     $client = new Client([
    //         'base_uri'=> env('API_URL'),
    //     ]);
    //
    //     $session = session('auth_token');
    //
    //     $res = $client->delete("media/video/$id", [
    //         'http_errors' => false,
    //         'headers' => [
    //             'Accept' => 'application/json',
    //             'Authorization' => "Bearer $session"
    //         ]
    //     ]);
    //
    //     return $res->getBody();
    // }
}
