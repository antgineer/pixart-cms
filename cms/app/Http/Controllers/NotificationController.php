<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class NotificationController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    protected function notification($request, $header = null){
        $session = session('auth_token');
        $idUser = session('role_name') != 'admin' ? session('id') : '';

        $page = $request->page ?: 1;
        
        $res = $this->client->get("notification/all/$idUser?page=$page", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status == 200){
            if($body['data']){
                foreach($body['data'] as $notif){
                    if($notif['notification_type'] == 'contract'){
                        $notifSender = $notif['sender']['profile']['fullname'];
                        $notifMessage = "$notifSender membuat kontrak baru";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    elseif($notif['notification_type'] == 'billboard_contract'){
                        $notifSender = $notif['sender']['profile']['fullname'];
                        $notifMessage = "$notifSender menginput pemasangan baru";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    elseif($notif['notification_type'] == 'billboard_contract_expired'){
                        $notifMessage = "Pemasangan terakhir telah expired";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    elseif($notif['notification_type'] == 'billboard'){
                        $notifMessage = "Billboard baru telah dibuat";
                        if( session('role_name') == 'admin' ){
                            $notifLink = route('detail-billboard', $notif['notification_id']);
                        }
                        else{
                            $notifLink = route('vendordetailbillboard', $notif['notification_id']);
                        }
                    }
                    elseif($notif['notification_type'] == 'billboard_approved'){
                        $notifMessage = "Billboard telah di verifikasi";
                        if( session('role_name') == 'admin' ){
                            $notifLink = route('detail-billboard', $notif['notification_id']);
                        }
                        else{
                            $notifLink = route('vendordetailbillboard', $notif['notification_id']);
                        }
                    }
                    elseif($notif['notification_type'] == 'billboard_declined'){
                        $notifMessage = "Billboard telah di tolak";
                        if( session('role_name') == 'admin' ){
                            $notifLink = route('detail-billboard', $notif['notification_id']);
                        }
                        else{
                            $notifLink = route('vendordetailbillboard', $notif['notification_id']);
                        }
                    }
                    elseif($notif['notification_type'] == 'contract_will_expired'){
                        $notifMessage = "Kontrak akan berakhir dalam 30 hari";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    elseif($notif['notification_type'] == 'contract_expired'){
                        $notifMessage = "Kontrak telah expired";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    elseif($notif['notification_type'] == 'ticket'){
                        $notifSender = $notif['sender']['profile']['fullname'];
                        $notifMessage = "$notifSender Memberi feedback baru";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    elseif($notif['notification_type'] == 'ticket_content'){
                        $notifSender = $notif['sender']['profile']['fullname'];
                        $notifMessage = "$notifSender Membalas feedback";
                        $notifLink = route('contract-details', $notif['notification_id']);
                    }
                    else{
                        $notifMessage = 'Error';
                        $notifLink = '#';
                    }

                    if($header == 'header'){
                        if(session('role_name') == 'admin'){
                            $notifClass = $notif['admin_read_status'] == 0 ? 'text-complete' : 'text-default';
                        }
                        else{
                            $notifClass = $notif['user_read_status'] == 0 ? 'text-complete' : 'text-default';
                        }
                    }
                    else{
                        if(session('role_name') == 'admin'){
                            $notifClass = $notif['admin_read_status'] == 0 ? 'alert-info' : 'alert-default';
                        }
                        else{
                            $notifClass = $notif['user_read_status'] == 0 ? 'alert-info' : 'alert-default';
                        }
                    }

                    $data[] = [
                        'id'         => $notif['id'],
                        'message'    => $notifMessage,
                        'url'        => $notifLink,
                        'class'      => $notifClass,
                        'created_at' => date("d-m-Y",strtotime($notif['created_at']))
                    ];
                }

                return $data;
            }

            return array();
        }
        else{
            return array();
        }
    }
    
    public function getNotification(Request $request, $header = null){
        $data = $this->notification($request, $header);
        return $data;
    }

    public function getNotificationAll(Request $request){
        $data = $this->notification($request);
        return view('pages.notification', ['data' => $data]);
    }
}
