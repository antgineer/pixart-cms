<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ProvinsiController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function getAllProvinsi(){
        $session = session('auth_token');

        $res = $this->client->get("provinsi", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        $num = 1;
        foreach($body['data'] as $key => $value){
            $data[] = [
                'index'              => $num++,
                'id'                 => $value['id'],
                'nama_provinsi'      => $value['nama_provinsi'],
                'nama_provinsi_gmap' => $value['nama_provinsi_gmap']
            ];
        }
        if($status == 200){
            return response()->json(['data' => $data], 200);
        }
    }

    public function store(Request $request){
        $session = session('auth_token');

        $postData = [
            'nama_provinsi'      => $request->nama_provinsi,
            'nama_provinsi_gmap' => $request->nama_provinsi,
        ];

        $res = $this->client->post('provinsi', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("provinsi/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        $postData = [
            'nama_provinsi'      => $request->nama_provinsi,
            'nama_provinsi_gmap' => $request->nama_provinsi
        ];
        
        $res = $this->client->put("provinsi/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("provinsi/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }
}
