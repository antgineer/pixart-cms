<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class KecamatanController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function getAllKecamatan(){
        $session = session('auth_token');

        $res = $this->client->get("kecamatan", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        $num = 1;
        foreach($body['data'] as $key => $value){
            $data[] = [
                'index'               => $num++,
                'id'                  => $value['id'],
                'nama_kecamatan'      => $value['nama_kecamatan'],
                'nama_kecamatan_gmap' => $value['nama_kecamatan_gmap']
            ];
        }
        if($status == 200){
            return response()->json(['data' => $data], 200);
        }
    }

    public function store(Request $request){
        $session = session('auth_token');

        $postData = [
            'id_kabupaten'        => $request->id_kabupaten,
            'nama_kecamatan'      => $request->nama_kecamatan,
            'nama_kecamatan_gmap' => $request->nama_kecamatan
        ];

        $res = $this->client->post('kecamatan', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("kecamatan/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        $postData = [
            'id_kabupaten'        => $request->id_kabupaten,
            'nama_kecamatan'      => $request->nama_kecamatan,
            'nama_kecamatan_gmap' => $request->nama_kecamatan
        ];
        
        $res = $this->client->put("kecamatan/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("kecamatan/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }

    public function getKecamatanByKabupaten($id){
        $session = session('auth_token');
        
        $res = $this->client->get("kecamatan/bykabupaten/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }
}
