<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Validator;

class AuthController extends Controller
{
    private $client;

    public function __construct(){
        $this->client = new Client([
          'base_uri'=> env('API_URL'),
        ]);
    }

    public function login(){
        if(!session('auth_token')){
            return view('pages.login');
        }

        if(session('role_name') == 'admin') return redirect('/dashboard-admin');
        elseif(session('role_name') == 'vendor') return redirect('/dashboard-vendor');
        elseif(session('role_name') == 'eo') return redirect('/dashboard-eo');
        else return redirect('/dashboard-customer');
    }

    public function authenticate(Request $request){
        $postData = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $res = $this->client->post('login', [
            'http_errors' => false,
            'json'        => $postData
        ]);

        $status = $res->getStatusCode();
        $data = json_decode($res->getBody(), true);
        
        if($status == 200){
            session([
                'auth_token' => $data['token'],
                'id'         => $data['data']['id'],
                'fullname'   => $data['data']['fullname'],
                'id_role'    => $data['data']['id_role'],
                'role_name'  => strtolower($data['data']['role_name']),
                'photo'      => $data['data']['photo'],
                'event'      => $data['data']['event']
            ]);
            
            $role = session('role_name');

            if($role == 'admin')
                return redirect('/dashboard-admin');
            elseif($role == 'vendor')
                return redirect('/dashboard-vendor');
            elseif($role == 'eo')
                return redirect('/dashboard-eo');
            else
                return redirect('/dashboard-customer');
        }
        else{
            return redirect('/')->withInput()->with('message_notif', $data['message']);
        }
    }

    public function changePassword(Request $request){
        $this->validate($request, [
            'current_password' => 'bail|required',
            'new_password'     => 'bail|required|confirmed'
        ]);

        $postData = [
            'current_password' => $request->current_password,
            'new_password' => $request->new_password
        ];

        $session = session('auth_token');

        $res = $this->client->put('changepassword', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['message']
        ], 200);
    }

    public function logout(){
        $session = session('auth_token');

        $res = $this->client->get("logout", [
            'http_errors' => false,
            'headers' => [
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();

        if($status == 200){
            session()->flush();
            return redirect('/');
        }
    }
}
