<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class KabupatenController extends Controller
{
    private $client;
    
    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function getAllKabupaten(){
        $session = session('auth_token');

        $res = $this->client->get("kabupaten", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        $num = 1;
        foreach($body['data'] as $key => $value){
            $data[] = [
                'index'               => $num++,
                'id'                  => $value['id'],
                'nama_kabupaten'      => $value['nama_kabupaten'],
                'nama_kabupaten_gmap' => $value['nama_kabupaten_gmap']
            ];
        }
        if($status == 200){
            return response()->json(['data' => $data], 200);
        }
    }

    public function store(Request $request){
        $session = session('auth_token');

        $postData = [
            'id_provinsi'         => $request->id_provinsi,
            'nama_kabupaten'      => $request->nama_kabupaten,
            'nama_kabupaten_gmap' => $request->nama_kabupaten
        ];

        $res = $this->client->post('kabupaten', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("kabupaten/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');

        $postData = [
            'id_provinsi'         => $request->id_provinsi,
            'nama_kabupaten'      => $request->nama_kabupaten,
            'nama_kabupaten_gmap' => $request->nama_kabupaten
        ];
        
        $res = $this->client->put("kabupaten/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("kabupaten/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }

    public function getKabupatenByProvinsi($id){
        $session = session('auth_token');
        
        $res = $this->client->get("kabupaten/byprovinsi/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);
        
        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        
        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }
}
