<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class EventController extends Controller
{
    private $client;

    public function __construct(){
        $this->client = new Client([
            'base_uri'=> env('API_URL'),
        ]);
    }

    public function showEvent(Request $request){
        $session = session('auth_token');
        $role = session('role_name');
        $idUser = session('id');

        $limit = 12;
        $parameters = "page=1&limit=$limit";
        $currentPage = $request->page ?: 1;

        if($request){
            if(!$request->page){
                $page = $request->page ?: 1;
                $tempArray[] = "page=$page";
            }

            if(!$request->limit){
                $limit = $request->limit ?: $limit;
                $tempArray[] = "limit=$limit";
            }

            foreach($request->all() as $key => $val){
                $tempArray[] = "$key=$val";
                $parameters = implode('&', $tempArray);
            }
        }

        $url = session('role_name') == 'sampoerna' ? 'event' : "event/user/$idUser/$idUser";

        if(session('role_name') == 'sampoerna') {
            $res = $this->client->get($url, [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);
        } else {
            $res = $this->client->get($url.$parameters, [
                'http_errors' => false,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Bearer $session"
                ]
            ]);
        }

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            // dd($body);
            $pages = ceil($body['total_data']/$limit);

            $body['total_page'] = $pages;
            $body['current_page'] = $currentPage;

            //return $body;
            return view('pages.event', $body);
        }

        return abort(500);
    }

    public function showDetailEvent($id){
        $session = session('auth_token');

        $res = $this->client->get("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.details_event', $body['data']);
        }
    }

    public function showDetailEvent2($id){
        $session = session('auth_token');

        $res = $this->client->get("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            return view('pages.details_event', $body['data']);
        }
    }

    public function getEventByUser($id){
        $session  = session('auth_token');
        // $idUser   = session('id');

        $res = $this->client->get("event/user/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status == 200){
            if(count($body['data']) > 0){
                $num = 1;
                foreach($body['data'] as $key => $value){
                    $eventDate = explode(' ', $value['event_date']);
                    $expiredDate = explode(' ', $value['expired_date']);
                    // $customer = ($value['customer'] != null) ? $value['customer']['profile']['fullname'] : null;

                    $data[] = [
                        'index'         => $num++,
                        'id'            => $value['id'],
                        'nama_event'    => $value['nama_event'],
                        'address'       => $value['address'],
                        'event_date'    => $eventDate[0],
                        'expired_date'  => $expiredDate[0],
                        'jumlah_pengunjung' => $value['jumlah_pengunjung'],
                        'status'        => $value['status']
                    ];
                }

                return response()->json(['data' => $data], 200);
            }
            else{
                return response()->json(['data' => []], 200);
            }
        }
    }

    

    public function store(Request $request){
        $session = session('auth_token');

        // $postData = [
        //     ['name' => 'id_user', 'contents' => session('id')],
        //     ['name' => 'nama_event', 'contents' => $request->nama_event],
        //     ['name' => 'address', 'contents' => $request->address],
        //     ['name' => 'event_date', 'contents' => $request->event_date],
        //     ['name' => 'expired_date', 'contents' => $request->expired_date]
        // ];

        $postData = [
            'id_user'      => $request->id_eo,
            'id_create_event' => $request->id_create_event,
            'nama_event'   => $request->nama_event,
            'address'      => $request->address,
            'event_date'   => $request->event_date,
            'expired_date' => $request->expired_date
        ];

        $res = $this->client->post('event', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'json' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function update(Request $request, $id){
        $session = session('auth_token');
        $postData = [
            ['name' => '_method', 'contents' => $request->_method],
            ['name' => 'id_user', 'contents' => session('id')],
            ['name' => 'nama_event', 'contents' => $request->nama_event],
            ['name' => 'address', 'contents' => $request->address],
            ['name' => 'event_date', 'contents' => $request->event_date],
            ['name' => 'expired_date', 'contents' => $request->expired_date],
            ['name' => 'status', 'contents' => $request->status]
        ];

        if($request->file){
            foreach($request->file as $file){
                $postData[] = ['name' => 'file[]', 'contents' => fopen($file, 'r')];
            }
        }

        $res = $this->client->post("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        return $body;

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function updateEo(Request $request, $id){
        $session = session('auth_token');
        //return $request->video;
        $postData = [
            ['name' => '_method', 'contents' => $request->_method],
            ['name' => 'jumlah_pengunjung', 'contents' => $request->jumlah_pengunjung],
            ['name' => 'note', 'contents' => $request->note]
        ];

        if($request->file){
            foreach($request->file as $file){
                $postData[] = ['name' => 'file[]', 'contents' => fopen($file, 'r')];
            }
        }

        if($request->video && $request->video != 'undefined'){
            $postData[] = ['name' => 'video', 'contents' => fopen($request->video, 'r')];
        }

        $res = $this->client->post("event/eo/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ],
            'multipart' => $postData
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);
        return $body;

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function show($id){
        $session = session('auth_token');

        $res = $this->client->get("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        if($status != 200) return response()->json([
            'status'  => 'Error',
            'message' => $body['message']
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data'   => $body['data']
        ], 200);
    }

    public function destroy($id){
        $session = session('auth_token');

        $res = $this->client->delete("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        return $res->getBody();
    }

    public function reportEventOne($id){
        $session = session('auth_token');

        $res = $this->client->get("event/$id", [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $session"
            ]
        ]);

        $status = $res->getStatusCode();
        $body = json_decode($res->getBody(), true);

        //redirect jika bukan pemilik vendor
        if($status == 200){
            if($body['data']['id_user'] == session('id') || session('role_name')=="sampoerna"){
                return view('report.event.event-preview-single', $body['data']);
            }
            else{
                return redirect('/event');
            }
        }
    }

}
