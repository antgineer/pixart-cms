<html>
<tr>
    <td colspan="8"><h1>Event {{$data['nama_event']}}</h1></td>
</tr>
<tr>
    <td><b>No</b></td>
    <td><b>Nama Event</b></td>
    <td><b>Lokasi</b></td>
    <td><b>Event Organizer</b></td>
    <td><b>Jumlah Pengunjung</b></td>
    <td><b>Tanggal Event</b></td>
    <td><b>Tanggal Berakhir</b></td>
    <td><b>Status</b></td>
</tr>

<tr>
    <td>1</td>
    <td>{{ $data['nama_event'] }}</td>
    <td>{{ $data['address'] }}</td>
    <td>{{ $data['user']['profile']['fullname'] }}</td>
    <td>{{ $data['jumlah_pengunjung'] }}</td>
    <td>{{date("d-m-Y", strtotime($data['event_date']))}}</td>
    <td>{{date("d-m-Y", strtotime($data['expired_date']))}}</td>
    <td>{{ $data['status'] == '1' ? 'Sedang Berjalan' : 'Telah Berakhir' }}</td>
</tr>

</html>