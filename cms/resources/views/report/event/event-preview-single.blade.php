@extends('layouts.cms')

@section('title', 'Report')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@section('content')
  <!-- START PANEL -->
  <div class="container-fluid">
  
    <div class="panel panel-default">
    <div class="panel-body">
    <a class="btn btn-success btn-cons" href="{{route('report.event.single.excel', $id)}}">Download Report to Excel</a>
  <a class="btn btn-success btn-cons" href="{{route('report.event.single.pdf', $id)}}">Download Report to PDF</a>
    </div>
  </div>
  </div>
  
  
<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
    <!-- START PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="invoice padding-50 sm-padding-10">
                <div>
                <div class="pull-left">
                    <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" data-src="{{asset('assets/img/logopixartdark.png')}}" src="{{ asset('assets/img/logopixartdark.png')}}">
                    <address class="m-t-10">
                        <strong style="font-size:20px"> {{$user['profile']['fullname']}} </strong>
                        <br><strong>{{$user['profile']['address']}}</strong>
                        <br>{{$user['profile']['phone']}}.
                        <br>
                    </address>
                </div>
                <div class="pull-right sm-m-t-20">
                    <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
                </div>
                <br>
                <br>
                <div>
                <table class="table m-t-50">
                <tbody>
                <tr>
                    <td>
                    <strong>Nama Event</strong>
                    </td>
                    <td>:</td>
                    <td >{{$nama_event}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Lokasi</strong>
                    </td>
                    <td>:</td>
                    <td >{{$address}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Event Organizer</strong>
                    </td>
                    <td>:</td>
                    <td >{{$user['profile']['fullname']}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Jumlah Pengunjung</strong>
                    </td>
                    <td>:</td>
                    <td >{{ number_format($jumlah_pengunjung, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Tanggal Event</strong>
                    </td>
                    <td>:</td>
                    <td >{{date("d-m-Y", strtotime($event_date))}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Tanggal Berakhir</strong>
                    </td>
                    <td>:</td>
                    <td >{{date("d-m-Y", strtotime($expired_date))}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Status</strong>
                    </td>
                    <td>:</td>
                    <td >{{$status=='1'?'Sedang Berjalan':'Telah Berakhir'}}</td>
                </tr>
                </tbody>
                </table>
                <div class="row">
                    @foreach($photo as $foto)
                    <div class="col-sm-3"><img src="{{ route('download', $foto['photo_name']) }}" class="img-thumbnail"></div>
                    @endforeach
                </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END CONTAINER FLUID -->

  <!-- END PANEL -->
@endsection
