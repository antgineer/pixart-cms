<table>
    <tr>
        <th>Vendor</th>
        <th>Billboard Code</th>
        <th>Address</th>
        <th>Province</th>
        <th>City</th>
        <th>District</th>
        <th>Length</th>
        <th>Width</th>
        <th>Lighting</th>
        <th>Side</th>
        <th>Format</th>
        <th>Material</th>
        <th>Cost</th>
    </tr>
    @foreach($data as $dta)
    <tr>
    <td>{{$dta['user']['profile']['fullname']}}</td>
    <td>{{$dta['kode_billboard']}}</td>
    <td>{{$dta['address']}}</td>
    <td>{{$dta['provinsi']['nama_provinsi']}}</td>
    <td>{{$dta['kabupaten']['nama_kabupaten']}}</td>
    <td>{{$dta['kecamatan']['nama_kecamatan']}}</td>
    <td>{{$dta['panjang']}} meters</td>
    <td>{{$dta['lebar']}} meters</td>
    <td>{{$dta['lighting']}}</td>
    <td>{{$dta['side']}} sides</td>
    <td>{{$dta['format']=='h'?'Horizontal':'Vertical'}}</td>
    <td>{{$dta['bahan']}}</td>
    <td>Rp {{$dta['harga']}}</td>
    </tr>
    @endforeach
</table>