@extends('layouts.cms')

@section('title', 'Report')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START PANEL -->
  <div class="container-fluid">
  
    <div class="panel panel-default">
    <div class="panel-body">
    <a class="btn btn-success btn-cons" href="/report/billboard/excel/{{$id}}">Download Report to Excel</a>
  <a class="btn btn-success btn-cons" href="/report/billboard/pdf/{{$id}}">Download Report to PDF</a>
    </div>
  </div>
  </div>
  
  
<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
    <!-- START PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="invoice padding-50 sm-padding-10">
                <div>
                <div class="pull-left">
                    <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" data-src="{{asset('assets/img/logopixartdark.png')}}" src="{{ asset('assets/img/logopixartdark.png')}}">
                    <address class="m-t-10">
                        <strong style="font-size:20px"> {{$user['profile']['fullname']}} </strong>
                        <br><strong>{{$user['profile']['address']}}</strong>
                        <br>{{$user['profile']['phone']}}.
                        <br>
                    </address>
                </div>
                <div class="pull-right sm-m-t-20">
                    <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
                </div>
                <br>
                <br>
                <div>
                <table class="table m-t-50">
                <tbody>
                <tr>
                    <td>
                    <strong>Billboard Code</strong>
                    </td>
                    <td>:</td>
                    <td >{{$kode_billboard}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Address</strong>
                    </td>
                    <td>:</td>
                    <td >{{$address}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Province</strong>
                    </td>
                    <td>:</td>
                    <td >{{$provinsi['nama_provinsi']}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>City</strong>
                    </td>
                    <td>:</td>
                    <td >{{$kabupaten['nama_kabupaten']}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>District</strong>
                    </td>
                    <td>:</td>
                    <td >{{$kecamatan['nama_kecamatan']}}</td>
                </tr>
                <tr>
                    <td class="">
                    
                    <strong>Length</strong>
                    </td>
                    <td>:</td>
                    <td >{{$panjang}} meters</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Width</strong>
                    </td>
                    <td>:</td>
                    <td >{{$lebar}} meters</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Lighting</strong>
                    </td>
                    <td>:</td>
                    <td >{{$lighting}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Side</strong>
                    </td>
                    <td>:</td>
                    <td >{{$side}} sides</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Format</strong>
                    </td>
                    <td>:</td>
                    <td >{{$format=='h'?'Horizontal':'Vertical'}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Material</strong>
                    </td>
                    <td>:</td>
                    <td >{{$bahan}}</td>
                </tr>
                <tr>
                    <td class="">
                    <strong>Price</strong>
                    </td>
                    <td>:</td>
                    <td >Rp {{ number_format($harga, 0, ',', '.') }}</td>
                </tr>
                </tbody>
                </table>
                <div class="row">
                    @foreach($photo as $foto)
                    <div class="col-sm-3"><img src="{{ route('download', $foto['photo_name']) }}" class="img-thumbnail"></div>
                    @endforeach
                </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END CONTAINER FLUID -->

  <!-- END PANEL -->
@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
@endpush
