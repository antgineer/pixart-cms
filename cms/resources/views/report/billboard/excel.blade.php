<html>
<tr>
    <td colspan="7"><h1>Daftar Billboard</h1></td>
</tr>
<tr>
    <td><b>No</b></td>
    <td><b>Kode</b></td>
    {{--  <td><b>Vendor</b></td>  --}}
    <td><b>Alamat</b></td>
    <td><b>Kecamatan</b></td>
    <td><b>Kabupaten</b></td>
    <td><b>Provinsi</b></td>
    <td><b>Size</b></td>
    <td><b>Lighting</b></td>
    <td><b>Arah Pandang</b></td>
    <td><b>Format</b></td>
    <td><b>Bahan</b></td>
    <td><b>Harga</b></td>
</tr>
@foreach($data as $key=>$row)
<tr>
    <td>{{ ++$key }}</td>
    <td>{{ $row['kode_billboard'] }}</td>
    {{--  <td>{{ $row['user_fullname'] }}</td>  --}}
    <td>{{ $row['address'] }}</td>
    <td>{{ $row['kecamatan']['nama_kecamatan'] }}</td>
    <td>{{ $row['kabupaten']['nama_kabupaten'] }}</td>
    <td>{{ $row['provinsi']['nama_provinsi'] }}</td>
    <td>{{ $row['panjang'] . ' X ' . $row['lebar'] }}</td>
    <td>{{ $row['lighting'] }}</td>
    <td>{{ $row['format'] == 'h' ? 'Horizontal' : 'Vertikal' }}</td>
    <td>{{ $row['bahan'] }}</td>
    <td>Rp.{{ number_format($row['harga'], '0', ',', '.') }}</td>
</tr>
@endforeach
</html>