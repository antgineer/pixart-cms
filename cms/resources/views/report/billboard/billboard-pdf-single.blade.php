<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
    <!-- START PANEL -->
        <div class="invoice padding-50 sm-padding-10">
            <div>
                <div class="pull-left">
                    <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{public_path('assets/img/logopixartdark.png')}}" data-src="{{public_path('assets/img/logopixartdark.png')}}" src="{{ public_path('assets/img/logopixartdark.png')}}">
                    <address class="m-t-10">
                        <strong style="font-size:20px"> {{$user['profile']['fullname']}} </strong>
                        <br><strong>{{$user['profile']['address']}}</strong>
                        <br>{{$user['profile']['phone']}}.
                        <br>
                    </address>
                </div>
                <div class="pull-right sm-m-t-20">
                    <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
            </div>
            <br>
            <br>
            <div class="table table-striped">
                <table class="table m-t-50">
                    <tbody>
                        <tr>
                        <td>
                            <strong>Billboard Code</strong>
                        </td>
                        <td>:</td>
                        <td >{{$kode_billboard}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Address</strong>
                        </td>
                        <td>:</td>
                        <td >{{$address}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Province</strong>
                        </td>
                        <td>:</td>
                        <td >{{$provinsi['nama_provinsi']}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>City</strong>
                        </td>
                        <td>:</td>
                        <td >{{$kabupaten['nama_kabupaten']}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>District</strong>
                        </td>
                        <td>:</td>
                        <td >{{$kecamatan['nama_kecamatan']}}</td>
                        </tr>
                        <tr>
                        <td class="">
                        
                            <strong>Length</strong>
                        </td>
                        <td>:</td>
                        <td >{{$panjang}} meters</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Width</strong>
                        </td>
                        <td>:</td>
                        <td >{{$lebar}} meters</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Lighting</strong>
                        </td>
                        <td>:</td>
                        <td >{{$lighting}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Side</strong>
                        </td>
                        <td>:</td>
                        <td >{{$side}} sides</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Format</strong>
                        </td>
                        <td>:</td>
                        <td >{{$format=='h'?'Horizontal':'Vertical'}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Material</strong>
                        </td>
                        <td>:</td>
                        <td >{{$bahan}}</td>
                        </tr>
                        <tr>
                        <td class="">
                            <strong>Price</strong>
                        </td>
                        <td>:</td>
                        <td >Rp {{ number_format($harga, 0, ',', '.') }}</td>
                        </tr>
                    </tbody>
                </table>
                
                <div class="row">
                    @foreach($photo as $foto)
                    @php
                        $url = env('API_URL').'download/'.$foto['photo_name'];
                    @endphp
                    <div class="col-xs-3"><img src="{{$url}}" class="img-thumbnail"></div>
                    @endforeach
                </div>
            </div>

        </div>
    <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->

</body>
</html>
    
    