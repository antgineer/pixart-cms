@extends('layouts.cms')

@section('title', 'Master - Billboard')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">

@endpush

@section('content')
<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
            <a class="btn btn-success" href="{{ route('report-billboard-list-excel') }}?start_date={{Request::input('start_date')}}&end_date={{Request::input('end_date')}}" data-toggle="tooltip" data-placement="top" title="Download All Report to Excel"><i class="fa fa-file-excel-o"></i> Download All Report to Excel</a>
            <a class="btn btn-success" href="{{ route('report-billboard-list-pdf') }}?start_date={{Request::input('start_date')}}&end_date={{Request::input('end_date')}}" data-toggle="tooltip" data-placement="top" title="Download All Report to PDF"><i class="fa fa-file-pdf-o"></i> Download All Report to PDF</a>
        </div>
      </div>
    </div>
  </div>
@foreach($data as $dta)
    <!-- START PANEL -->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div>
            </div>
            <div>
            <div class="pull-left">
              <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" data-src="{{asset('assets/img/logopixartdark.png')}}" src="{{ asset('assets/img/logopixartdark.png')}}">
              <address class="m-t-10">
                  <strong style="font-size:20px"> {{ $dta['user']['profile']['fullname'] }} </strong>
              </address>
            </div>
            <div class="pull-right sm-m-t-20">
                  <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
            <table class="table">
              <tbody>
                <tr>
                  <td><strong>Billboard Code</strong></td>
                  <td>:</td>
                  <td>{{$dta['kode_billboard']}}</td>
                </tr>
                <tr>
                  <td><strong>Address</strong></td>
                  <td>:</td>
                  <td>{{$dta['address']}}</td>
                </tr>
                <tr>
                  <td><strong>Province</strong></td>
                  <td>:</td>
                  <td>{{$dta['provinsi']['nama_provinsi']}}</td>
                </tr>
                <tr>
                  <td><strong>City</strong></td>
                  <td>:</td>
                  <td>{{$dta['kabupaten']['nama_kabupaten']}}</td>
                </tr>
                <tr>
                  <td><strong>District</strong></td>
                  <td>:</td>
                  <td>{{$dta['kecamatan']['nama_kecamatan']}}</td>
                </tr>
                <tr>
                  <td><strong>Length</strong></td>
                  <td>:</td>
                  <td>{{$dta['panjang']}} meters</td>
                </tr>
                <tr>
                  <td><strong>Width</strong></td>
                  <td>:</td>
                  <td>{{$dta['lebar']}} meters</td>
                </tr>
                <tr>
                  <td><strong>Lighting</strong></td>
                  <td>:</td>
                  <td>{{$dta['lighting']}}</td>
                </tr>
                <tr>
                  <td><strong>Side</strong></td>
                  <td>:</td>
                  <td>{{$dta['side']}} sides</td>
                </tr>
                <tr>
                  <td><strong>Format</strong></td>
                  <td>:</td>
                  <td>{{$dta['format']=='h'?'Horizontal':'Vertical'}}</td>
                </tr>
                <tr>
                  <td><strong>Material</strong></td>
                  <td>:</td>
                  <td>{{$dta['bahan']}}</td>
                </tr>
                <tr>
                  <td><strong>Cost</strong></td>
                  <td>:</td>
                  <td>Rp {{ number_format($dta['harga'], 0, ',', '.') }}</td>
                </tr>
              </tbody>
            </table>
            @if(!empty($dta['photo']))
              <div class="row">
                @foreach($dta['photo'] as $foto)
                  <div class="col-sm-3"><img src="{{url('file/'.$foto['photo_name'])}}" class="img-thumbnail"></div>
                @endforeach
              </div>
            @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END PANEL -->
@endforeach
</div>
<!-- END CONTAINER FLUID -->

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    {{-- BOOTSTRAP FILE INPUT JS --}}
    <script src="{{ asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
@endpush
    