<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
    .page-break {
        page-break-after: always;
    }
    </style>
  </head>
  <body>
    @foreach($data as $key=>$row)
    <table>
        <tr>
            <td>Kode</td>
            <td>{{ $row['kode_billboard'] }}</td>
        </tr>
        <tr>
            <td>Vendor</td>
            <td>{{ $row['user_fullname'] }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>{{ $row['address'] }}</td>
        </tr>
        <tr>
            <td>Kecamatan</td>
            <td>{{ $row['kecamatan']['nama_kecamatan'] }}</td>
        </tr>
        <tr>
            <td>Kabupaten</td>
            <td>{{ $row['kabupaten']['nama_kabupaten'] }}</td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>{{ $row['provinsi']['nama_provinsi'] }}</td>
        </tr>
        <tr>
            <td>Foto</td>
            <td><img src="tes" alt="tes" /></td>
        </tr>
    </table>
    <div class="page-break"></div>
    @endforeach
    
  </body>
</html>