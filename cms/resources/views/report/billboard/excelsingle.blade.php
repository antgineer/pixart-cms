<html>
<tr>
    <td colspan="7"><h1>Billboard {{$data['kode_billboard']}}</h1></td>
</tr>
<tr>
    <td><b>No</b></td>
    <td><b>Kode</b></td>
    <td><b>Vendor</b></td>
    <td><b>Alamat</b></td>
    <td><b>Kecamatan</b></td>
    <td><b>Kabupaten</b></td>
    <td><b>Propinsi</b></td>
</tr>

<tr>
    <td>1</td>
    <td>{{ $data['kode_billboard'] }}</td>
    <td>{{ $data['user_fullname'] }}</td>
    <td>{{ $data['address'] }}</td>
    <td>{{ $data['kecamatan']['nama_kecamatan'] }}</td>
    <td>{{ $data['kabupaten']['nama_kabupaten'] }}</td>
    <td>{{ $data['provinsi']['nama_provinsi'] }}</td>
</tr>

</html>