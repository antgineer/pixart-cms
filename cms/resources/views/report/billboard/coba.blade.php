<html>
<tr>
    <td colspan="7"><h1>Daftar Billboard Terverifikasi</h1></td>
</tr>
<tr>
    <td><b>No</b></td>
    <td><b>Kode Billboard</b></td>
    <td><b>Nama Vendor</b></td>
    <td><b>Provinsi</b></td>
    <td><b>Kabupaten</b></td>
    <td><b>Kecamatan</b></td>
    <td><b>Alamat</b></td>
    <td><b>Panjang</b></td>
    <td><b>Lebar</b></td>
    <td><b>Format</b></td>
    <td><b>Bahan</b></td>
    <td><b>Harga</b></td>
</tr>
@foreach($data as $key=>$row)
<tr>
    <td>{{ ++$key }}</td>
    <td>{{ $row['kode_billboard'] }}</td>
    <td>{{ $row['user_fullname'] }}</td>
    <td>{{ $row['provinsi']['nama_provinsi'] }}</td>
    <td>{{ $row['kabupaten']['nama_kabupaten'] }}</td>
    <td>{{ $row['kecamatan']['nama_kecamatan'] }}</td>
    <td>{{ $row['address'] }}</td>
    <td>{{ $row['panjang'] }}</td>
    <td>{{ $row['lebar'] }}</td>
    <td>{{ $row['format'] }}</td>
    <td>{{ $row['bahan'] }}</td>
    <td>{{ $row['harga'] }}</td>
</tr>
@endforeach
</html>
