<html>
<tr>
    <td colspan="7"><h1>Daftar Kontrak {{ date('d-m-Y') }}</h1></td>
</tr>
<tr>
    <th><b>No</b></th>
    <th><b>Kode Billboard</b></th>
    <th><b>Address</b></th>
    <th><b>Kecamatan</b></th>
    <th><b>Kabupaten/Kota</b></th>
    <th><b>Provinsi</b></th>
    <th><b>Dimensi</b></th>
    <th><b>Lightning</b></th>
    <th><b>Side</b></th>
    <th><b>Arah Pandang</b></th>
    <th><b>Format</b></th>
    <th><b>Bahan</b></th>
    <th><b>Harga</b></th>
    <th><b>Tanggal Mulai Kontrak</b></th>
    <th><b>Tanggal Akhir Kontrak</b></th>
    <th><b>Vendor</b></th>
</tr>
    @foreach($data as $key=>$row)
    <tr>
        <td>{{ ++$key }}</td>
        <td>{{ $row['billboard']['kode_billboard'] }}</td>
        <td>{{ $row['billboard']['address'] }}</td>
        <td>{{ $row['billboard']['kecamatan']['nama_kecamatan'] }}</td>
        <td>{{ $row['billboard']['kabupaten']['nama_kabupaten'] }}</td>
        <td>{{ $row['billboard']['provinsi']['nama_provinsi'] }}</td>
        <td>{{ $row['billboard']['panjang'].'M X '.$row['billboard']['lebar'].'M' }}</td>
        <td>{{ $row['billboard']['lighting'] }}</td>
        <td>{{ $row['billboard']['side'] }} Sisi</td>
        <td>{{ $row['billboard']['format']=="v"?"Vertical":"Horizontal" }}</td>
        <td>{{ $row['billboard']['bahan'] }}</td>
        <td>Rp.{{ number_format($row['billboard']['harga'], '0', ',', '.') }}</td>
        <td>{{ date("d-m-Y",strtotime($row['contract_date'])) }}</td>
        <td>{{ date("d-m-Y",strtotime($row['expired_date'])) }}</td>
        <td>{{ $row['vendor']['profile']['fullname'] }}</td>
    </tr>
    @endforeach
</html>