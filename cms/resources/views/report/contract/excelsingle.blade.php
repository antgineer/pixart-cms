<html>
<tr>
    <td colspan="7"><h1>Kontrak Billboard {{$data['billboard']['kode_billboard']}}</h1></td>
</tr>
<tr>
    <td><b>No</b></td>
    <td><b>Kode</b></td>
    <td><b>Vendor</b></td>
    <td><b>Alamat</b></td>
    <td><b>Kecamatan</b></td>
    <td><b>Kabupaten</b></td>
    <td><b>Provinsi</b></td>
    <td><b>Tgl. Mulai</b></td>
    <td><b>Tgl. Habis</b></td>
</tr>

<tr>
    <td>1</td>
    <td>{{ $data['billboard']['kode_billboard'] }}</td>
    <td>{{ $data['vendor_fullname'] }}</td>
    <td>{{ $data['billboard']['address'] }}</td>
    <td>{{ $data['billboard']['kecamatan']['nama_kecamatan'] }}</td>
    <td>{{ $data['billboard']['kabupaten']['nama_kabupaten'] }}</td>
    <td>{{ $data['billboard']['provinsi']['nama_provinsi'] }}</td>
    <td>{{ $data['contract_date'] }}</td>
    <td>{{ $data['expired_date'] }}</td>
</tr>

</html>