<html>
<tr>
    <td colspan="7"><h1>Daftar Kontrak</h1></td>
</tr>
<tr>
    <th><b>No</b></th>
    <th><b>Kode Billboard</b></th>
    <th><b>Address</b></th>
    <th><b>Tanggal Mulai</b></th>
    <th><b>Valid Thru</b></th>
    <th><b>Harga</b></th>
</tr>
    @foreach($data as $key=>$row)
    <tr>
        <td>{{ ++$key }}</td>
        <td>{{ $row['kode_billboard'] }}</td>
        {{--  <td>{{ $row['user_fullname'] }}</td>  --}}
        <td>{{ $row['address'] }}</td>
        <td>{{ $row['contract_date'] }}</td>
        <td>{{ $row['expired_date'] }}</td>
        <td>Rp.{{ number_format($row['harga'], '0', ',', '.') }}</td>
    </tr>
    @endforeach
</html>