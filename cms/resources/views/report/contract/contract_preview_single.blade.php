@extends('layouts.cms')

@section('title', 'Master - Billboard')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">

@endpush

@section('content')
<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
  <!-- <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
                <a class="btn btn-success" href="?start={{Request::input('contract_date')}}&end={{Request::input('end')}}" data-toggle="tooltip" data-placement="top" title="Download All Report to Excel"><i class="fa fa-file-excel-o"></i> Download All Report to Excel</a>
                <a class="btn btn-success" href="?start={{Request::input('contract_date')}}&end={{Request::input('end')}}" data-toggle="tooltip" data-placement="top" title="Download All Report to PDF"><i class="fa fa-file-pdf-o"></i> Download All Report to PDF</a>
              
        </div>
      </div>
    </div>
  </div> -->
    <!-- START PANEL -->
    
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="text-right">
                {{--  <a class="btn btn-success" href="/report/contract/excel/{{$id}}" data-toggle="tooltip" data-placement="top" title="Download Report to Excel"><i class="fa fa-file-excel-o"></i> Download Report to Excel</a>  --}}
                <a class="btn btn-success" href="{{route('report.contract.pdf.single', $id)}}" data-toggle="tooltip" data-placement="top" title="Download Report to PDF"><i class="fa fa-file-pdf-o"></i> Download Report to PDF</a>
            </div>

            <div>
                <div class="pull-left">
                <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" data-src="{{asset('assets/img/logopixartdark.png')}}" src="{{ asset('assets/img/logopixartdark.png')}}">
                <address class="m-t-10">
                    <strong style="font-size:20px">  </strong>
                </address>
                </div>
                <div class="pull-right sm-m-t-20">
                  <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
            </div>

            <h2>Data Kontrak</h2>
            <table class="table">
              <tbody>
                <tr>
                  <td><strong>Kode Billboard</strong></td>
                  <td>:</td>
                  <td>{{$billboard['kode_billboard']}}</td>
                </tr>
                <tr>
                    <td><strong>Alamat Billboard</strong></td>
                    <td>:</td>
                    <td>{{$billboard['address']}}</td>
                </tr>
                <tr>
                    <td><strong>Provinsi</strong></td>
                    <td>:</td>
                    <td>{{$billboard['provinsi']['nama_provinsi']}}</td>
                </tr>
                <tr>
                    <td><strong>Kabupaten</strong></td>
                    <td>:</td>
                    <td>{{$billboard['kabupaten']['nama_kabupaten']}}</td>
                </tr>
                <tr>
                    <td><strong>Kecamatan</strong></td>
                    <td>:</td>
                    <td>{{$billboard['kecamatan']['nama_kecamatan']}}</td>
                </tr>
                <tr>
                    <td><strong>Harga</strong></td>
                    <td>:</td>
                    <td>{{$billboard['harga']}}</td>
                </tr>
                <tr>
                    <td><strong>Tanggal Contract</strong></td>
                    <td>:</td>
                    <td>{{$contract_date}}</td>
                </tr>
                <tr>
                    <td><strong>Tanggal Expired</strong></td>
                    <td>:</td>
                    <td>{{$expired_date}}</td>
                </tr>
                <tr>
                    <td><strong>Nama Vendor</strong></td>
                    <td>:</td>
                    <td>{{$vendor['profile']['fullname']}}</td>
                </tr>
                <tr>
                    <td><strong>Nama Customer</strong></td>
                    <td>:</td>
                    <td>{{ ($id_customer != null) ? $customer['profile']['fullname'] : $customer_name}}</td>
                </tr>
              </tbody>
            </table>
            @if($detail_contract)
                @if($detail_contract[0]['status'] == '1')
                    <h2>Data Installasi Aktif</h2>
                    <table class="table">
                        <tbody>
                                <tr>
                                    <td><strong>Tema Iklan</strong></td>
                                    <td>:</td>
                                    <td>{{$detail_contract[0]['tema_iklan']}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Brand</strong></td>
                                    <td>:</td>
                                    <td>{{$detail_contract[0]['brand']}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Tanggal Pasang</strong></td>
                                    <td>:</td>
                                    <td>{{$detail_contract[0]['created_date']}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Tanggal Expired</strong></td>
                                    <td>:</td>
                                    <td>{{$detail_contract[0]['expired_date']}}</td>
                                </tr>
                        </tbody>
                    </table>

                    @if($detail_contract[0]['photo'])
                        <div class="row">
                            @foreach($detail_contract[0]['photo'] as $foto)
                            <div class="col-sm-3"><img src="{{route('download', $foto['photo_name'])}}" class="img-thumbnail"></div>
                            @endforeach
                        </div>
                    @endif
                @endif
            @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END CONTAINER FLUID -->

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    {{-- BOOTSTRAP FILE INPUT JS --}}
    <script src="{{ asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <script>
    $(document).ready(function(){
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true
        });
    });
    </script>
@endpush
    