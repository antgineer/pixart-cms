<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link href="{{public_path('assets/plugins/boostrapv3/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{public_path('assets/img/logopixartdark.png')}}" data-src="{{public_path('assets/img/logopixartdark.png')}}" src="{{ public_path('assets/img/logopixartdark.png')}}">
                      
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Report</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <h2>Data Kontrak</h2>
                  <div class="table table-striped">
                  
                    <table class="table m-t-50">
                        <tbody>
                            <tr>
                                <td><strong>Kode Billboard</strong></td>
                                <td>:</td>
                                <td>{{$billboard['kode_billboard']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Alamat Billboard</strong></td>
                                <td>:</td>
                                <td>{{$billboard['address']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Provinsi</strong></td>
                                <td>:</td>
                                <td>{{$billboard['provinsi']['nama_provinsi']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Kabupaten</strong></td>
                                <td>:</td>
                                <td>{{$billboard['kabupaten']['nama_kabupaten']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Kecamatan</strong></td>
                                <td>:</td>
                                <td>{{$billboard['kecamatan']['nama_kecamatan']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Harga</strong></td>
                                <td>:</td>
                                <td>{{$billboard['harga']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Tanggal Contract</strong></td>
                                <td>:</td>
                                <td>{{$contract_date}}</td>
                            </tr>
                            <tr>
                                <td><strong>Tanggal Expired</strong></td>
                                <td>:</td>
                                <td>{{$expired_date}}</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Vendor</strong></td>
                                <td>:</td>
                                <td>{{$vendor['profile']['fullname']}}</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Customer</strong></td>
                                <td>:</td>
                                <td>{{ ($id_customer != null) ? $customer['profile']['fullname'] : $customer_name}}</td>
                            </tr>
                        </tbody>
                    </table>
                    @if($detail_contract)
                        @if($detail_contract[0]['status'] == '1')
                            <h2>Data Installasi Aktif</h2>
                            <table class="table">
                                <tbody>
                                        <tr>
                                            <td><strong>Tema Iklan</strong></td>
                                            <td>:</td>
                                            <td>{{$detail_contract[0]['tema_iklan']}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Brand</strong></td>
                                            <td>:</td>
                                            <td>{{$detail_contract[0]['brand']}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Tanggal Pasang</strong></td>
                                            <td>:</td>
                                            <td>{{$detail_contract[0]['created_date']}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Tanggal Expired</strong></td>
                                            <td>:</td>
                                            <td>{{$detail_contract[0]['expired_date']}}</td>
                                        </tr>
                                </tbody>
                            </table>

                            @if($detail_contract[0]['photo'])
                                <div class="row">
                                    @foreach($detail_contract[0]['photo'] as $foto)
                                    {{--  sementara  --}}
                                    @php
                                    $url = env('API_URL').'download/'.$foto['photo_name'];
                                    @endphp
                                    {{--  <div class="col-xs-3"><img src="{{route('download', $foto['photo_name'])}}" class="img-thumbnail"></div>  --}}
                                    <div class="col-xs-3"><img src="{{$url}}" class="img-thumbnail"></div>
                                    @endforeach
                                </div>
                            @endif
                        @endif
                    @endif
                  </div>

                </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->

</body>
</html>
    
    