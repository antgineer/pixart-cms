@extends('layouts.cms')

@section('title', 'Unoccupied')

@section('sidebar')
    @include('sidebar.vendor') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker3.min.css')}}">
  <link href="{{asset('assets/css/default.css')}}"rel="stylesheet" type="text/css" media="screen" />

  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="row">
              <div class="panel-title" style="padding: 10px 10px;">Unoccupied</div>
              <div class="clearfix"></div>
            </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->
  {{--start modal create contract--}}
  <div class="modal fade" id="modal-add-contract">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Buat Kontrak</h2><br>
            </div>
            <div class="modal-body" style="padding-bottom: 0px;">
              <form id="form-save" role="form" autocomplete="off">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="id_billboard" name="id_billboard" required>
                <div class="form-group-attached">
                  <div class="row clearfix">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default required">
                        <label>Nama Customer</label>
                        <select class="full-width" data-init-plugin="select2" name="id_customer" id="id_customer" required>
                          <option value="">Pilih Customer</option>
                          <option value="other">Customer Lain-lain</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="customer_name">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Nama Customer Lain</label>
                        <input type="text" class="form-control" id="customer_name" name="customer_name" required style="padding-top: 5px; height: 40px;">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Mulai</label>
                            <input id="datepicker" value="{{old('start_date')}}" class="datepicker-awal form-control" type="text" name="contract_date">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Akhir</label>
                            <input id="datepicker datepicker2" value="{{old('end_date')}}" class="datepicker-akhir form-control" type="text" name="expired_date">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
            </div><br>
            <div class="modal-footer" style="padding-right: 20px;">
              <button type="button" class="btn btn-success" id="button-save">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>
  {{--end modal create contract--}}
@endsection

@push('script')
    <script src="{{asset('assets/plugins/new-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/new-datepicker/locales/bootstrap-datepicker.id.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>



    <script>
    $('document').ready(function(){
        'use strict';

        // Fungsi untuk show and hidden jika memilih user lain pada saat membuat kontrak
        $("#id_customer").change(function()
          {
                if($(this).val() == "other")
            {
                $("#customer_name").show();
            } else {
                $("#customer_name").hide();
            }
          });
          $("#customer_name").hide();


        let id_vendor = "{{ session('id') }}";

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type : "GET",
                //url : `/vendor-billboard/vendor/${id_vendor}/unoccupied`,
                url : `{{route('vendorbillboard-role', array(
                  'role'    =>'vendor',
                  'id'      =>session('id'),
                  'status'  =>'unoccupied'
                ))}}`,
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                  title : "Kode Billboard",
                  data : "kode"
                },
                {
                  title : "Lokasi",
                  data : "address"
                },
                {
                    title: "Ukuran",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        actions = `${data.panjang} x ${data.lebar}`;
                        return actions.replace();
                    }
                },
                {
                    title: "Bahan",
                    data: "bahan"
                },
                {
                    title: "Harga",
                    data: "harga"
                },
                {
                    title: "Action",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        actions = `<a href="{{route('vendordetailbillboard','')}}/${data.id}" class="btn btn-success btn-complete btn-animated from-top fa fa-info" id="btn-detail">
                            <span class="fa fa-info"></span>
                        </a>
                        <button class="btn btn-success btn-animated from-top fa fa-book btn-add" data-id="${data.id}" type="button" data-toggle="modal" title="Tambah" href='#modal-add-contract'>
                            <span>Contract</span>
                        </button>`;
                        return actions.replace();
                    }
                }
            ]
        });

        //combobox user parent
        $.get(`{{route('getAllUser', 'customer')}}`, function(data){
            $.each(data.data, function(key, val){
                $('#id_customer').append(`<option value="${val.id}">${val.fullname}</option>`);
            });
        });

        $('#tableWithSearch').on('click', '.btn-add', function(){
            let id = $(this).data('id');
            $('#id_billboard').val(id);
        });

        $('#button-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Kontrak Sudah Terisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = $('#form-save').serializeArray();

                    $.ajax({
                        url         : "{{route('save-contract')}}",
                        type        : "POST",
                        data        : data,
                        success     : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kontrak Berhasil Disimpan!",
                                        type: "success"
                                    }, function(){
                                        window.location.reload();
                                    });
                                }, 1000);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kontrak Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kontrak Batal Disimpan :)", "error");
                }
            });
        });


        var datepicker_awal=$('.datepicker-awal').datepicker({
              startDate: "now",
              format: "yyyy-mm-dd",
              clearBtn: true,
              language: "id",
              autoclose: true,
              todayHighlight: true
          });



        $('.datepicker-awal').change(function() {
          var start_date_end = $(this).val();

          $('.datepicker-akhir').datepicker({
              startDate: start_date_end,
              format: "yyyy-mm-dd",
              clearBtn: true,
              language: "id",
              autoclose: true
          });
        });

    });
  </script>
@endpush
