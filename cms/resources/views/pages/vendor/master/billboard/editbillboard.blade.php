@extends('layouts.cms')

@section('title', 'Vendor - Edit Billboard')

@section('sidebar')
    @include('sidebar.vendor') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/remove-image/remove-image.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg bg-white">
        <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
            <li>
              <a href="{{route('vendor-billboard')}}">Billboard</a>
            </li>
            <li><a href="#" class="active">Edit Billboard</a>
            </li>
          </ul>
        <!-- END BREADCRUMB -->

        <div class="row">
          <div class="col-sm-5">
            <!-- START MAP -->
            <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
              <div class="panel-heading" style="padding-left: 0px; padding-right: 0px;">
                <div class="row">
                  <div class="col-md-12">
                    <p style="font-size: 20px;">Search Location</p>
                    <div class="form-group form-group-default">
                      <label>Enter Location</label>
                      <input type="text" class="form-control" name="cari_alamat" id="cari_alamat" style="padding-top: 5px; height: 40px;">
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-body" style="padding-left: 0px;
              padding-right: 0px; padding-bottom: 0px;">
                <div id="map" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
            <!-- END MAP -->

            <!-- START UPLOADER -->
            <div class="panel panel-default">
              <div class="form-group form-group-default" style="border-bottom-width: 0px; margin-bottom: 0px;">
                <input id="edit_foto" name="image_file_arr[]" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg", "jpeg"]' style="padding-top: 5px; height: 40px;">
                <hr style="margin-bottom: 0px; margin-top: 10px;">
                @if($photo)
                <output id="list">
                  <h4 style="margin-top: 0px; margin-bottom: 0px;">Current Photos</h4>
                    @foreach($photo as $foto)
                        <span class="file-preview-wrapper">
                            <div class="file-preview-frame krajee-default kv-preview-thumb" style="padding-right: 10px; padding-left: 10px; margin-left: 0px;">
                              <div class="kv-file-content">
                                <img class="thumb file-preview-image kv-preview-data" src="{{ route('download', $foto['photo_name']) }}">
                                <span class="remove_img_preview" data-id="{{$foto['id']}}"></span>
                              </div>
                            </div>
                        </span>
                    @endforeach
                </output>
                @else
                <div>
                  <h4>Billboard belum memiliki foto</h4>
                </div>
                @endif
              </div>
            </div>
            <!-- END UPLOADER -->
          </div> {{-- END COL-SM-5 --}}

          <div class="col-sm-7">
            <!-- START FORM INPUT -->
            <div class="panel panel-transparent">
              <div class="panel-body">
                <div class="alert alert-danger" role="alert">
                  <strong>Declined Information : </strong>The daily report has failed
                </div>
                <form id="form-project" method="POST" action="#" role="form" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                  <p style="font-size: 20px;">Basic Information</p>
                  <div class="form-group-attached">
                    <div class="row clearfix">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Kode Billboard</label>
                          <input type="hidden" class="form-control" value="{{ $id }}" id="id_billboard" name="id" required>
                          <input type="text" class="form-control" value="{{ $kode_billboard }}" name="kode_billboard" required style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Provinsi</label>
                          <select class="full-width" data-init-plugin="select2" name="id_provinsi" id="prov" required>

                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Kabupaten</label>
                          <select class="full-width" data-init-plugin="select2" name="id_kabupaten" id="kab" required>

                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Kecamatan</label>
                          <select class="full-width" data-init-plugin="select2" name="id_kecamatan" id="kec" required>

                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Alamat</label>
                          <textarea class="form-control" id="address" name="address" style="height: 70px;" required>{{$address}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Latitude</label>
                          <input type="text" class="form-control" value={{$latitude}} id="latitude" name="latitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Longtitude</label>
                          <input type="text" class="form-control" value={{$longitude}} id="longitude" name="longitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <p class="m-t-10" style="font-size: 20px;">Advanced Information</p>
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Panjang</label>
                          <input type="text" class="form-control" value={{$panjang}} name="panjang" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Lebar</label>
                          <input type="text" class="form-control" value={{$lebar}} name="lebar" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Lighting</label>
                          <select class="full-width" data-init-plugin="select2" name="lighting" id="lighting">
                            <option value="Frontlight" {{ $lighting == 'Frontlight' ? 'selected' : '' }}>Frontlight</option>
                            <option value="Backlight" {{ $lighting == 'Backlight' ? 'selected' : '' }}>Backlight</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Side</label>
                          <select class="full-width" data-init-plugin="select2" name="side" id="side">
                            <option value="1" {{ $side == '1' ? 'selected' : '' }}>1 Sisi</option>
                            <option value="2" {{ $side == '2' ? 'selected' : '' }}>2 Sisi</option>
                            <option value="3" {{ $side == '3' ? 'selected' : '' }}>3 Sisi</option>
                            <option value="4" {{ $side == '4' ? 'selected' : '' }}>4 Sisi</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      {{-- <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Arah Pandang</label>
                          <input type="text" class="form-control" value="" name="arah_pandang" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div> --}}
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Bahan</label>
                          <input type="text" class="form-control" value={{$bahan}} name="bahan" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Format</label>
                          <select class="full-width" data-init-plugin="select2" name="format" id="format">
                            <option value="v" {{ $format == 'v' ? 'selected' : '' }}>Vertical</option>
                            <option value="h" {{ $format == 'h' ? 'selected' : '' }}>Horizontal</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Harga</label>
                          <input type="text" class="form-control" value={{$harga}} name="harga" id="harga" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Keterangan</label>
                          <textarea class="form-control" id="note" name="note" style="height: 70px;">{{$note}}</textarea>
                        </div>
                      </div>
                    </div>
                  </div><br>
                  <button class="btn btn-success" type="button" id="button-update">Update</button>
                </form>
              </div>
            </div>
            <!-- END FORM INPUT -->
          </div>
        </div> {{-- END ROW --}}

      </div>
  <!-- END CONTAINER FLUID -->
@endsection

@push('script')
  {{-- FILE UPLOAD --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>

  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>

  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>

  {{--  Sweet Alert  --}}
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

  {{--  <script src="{{asset('assets/plugins/remove-image/remove-image.js')}}" type="text/javascript"></script>  --}}
  
  {{-- Billboard Page JS --}}
  <script src="{{ asset('js/billboard_wilayah.js') }}"></script>

  {{-- Cleave JS --}}
  <script src="{{ asset('assets/plugins/cleave.js/cleave.min.js') }}"></script>

  <script>
  $(document).ready(function() {

      //cleave.js number format
      var harga = new Cleave('#harga', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        numeralDecimalMark: ',',
        delimiter: '.'
      });

      //combobox provinsi
      $.get(`{{route('getAllProvinsi')}}`, function(data){
          let id_provinsi = {{ $id_provinsi }};
          $.each(data.data, function(key, val){
              $('#prov').append(`<option value="${val.id}">${val.nama_provinsi}</option>`);
          });

          $('#prov').select2('val', id_provinsi);
      });

      //combobox kabupaten
      $.get(`{{route('getKabupatenByProvinsi', '')}}/{{$id_provinsi}}`, function(data){
          let id_kabupaten = {{ $id_kabupaten }};
          $.each(data.data, function(key, val){
              $('#kab').append(`<option value="${val.id}">${val.nama_kabupaten}</option>`);
          });

          $('#kab').select2('val', id_kabupaten);
      });

      //combobox kecamatan
      $.get(`{{route('getKecamatanByKabupaten', '')}}/{{$id_kabupaten}}`, function(data){
          let id_kecamatan = {{ $id_kecamatan }};
          $.each(data.data, function(key, val){
              $('#kec').append(`<option value="${val.id}">${val.nama_kecamatan}</option>`);
          });

          $('#kec').select2('val', id_kecamatan);
      });

      $('#prov').change(function(){
          let prov = $(this).val();
          $('#kab').empty();
          $('#kab').select2();

          $('#kec').empty();
          $('#kec').select2();

          $.get(`{{route('getKabupatenByProvinsi', '')}}/${prov}`, function(data){
              $.each(data.data, function(key, val){
                  $('#kab').append(`<option value="${val.id}">${val.nama_kabupaten}</option>`);
              });
          });
      });

      $('#kab').change(function(){
          let kab = $(this).val();
          $('#kec').empty();
          $('#kec').select2();

          $.get(`{{route('getKecamatanByKabupaten', '')}}/${kab}`, function(data){
              $.each(data.data, function(key, val){
                  $('#kec').append(`<option value="${val.id}">${val.nama_kecamatan}</option>`);
              });
          });
      });

      $("#input-6").fileinput({
          showUpload: false,
          maxFileCount: 10,
          mainClass: "input-group-lg"
      });

      /* Map Object */
      var mapObj = new GMaps({
          el: '#map',
          lat: {{ $latitude }},
          lng: {{ $longitude }},
      });

      var m = mapObj.addMarker({
          lat: {{ $latitude }},
          lng: {{ $longitude }},
          draggable: true,
          dragend: event => {
              var lat = event.latLng.lat();
              var lng = event.latLng.lng();
              $('#latitude').val(lat);
              $('#longitude').val(lng);
          }
      });

      $('#cari_alamat').keypress(function(e){
          if (e.which == 13) {
              var address = $('#cari_alamat').val();
              mapObj.removeMarkers(mapObj.markers);

              GMaps.geocode({
                  address: address,
                  callback: function(results, status) {
                      if (status == 'OK') {
                          let latlng = results[0].geometry.location;
                          mapObj.setCenter(latlng.lat(), latlng.lng());
                          mapObj.setZoom(16);
                          mapObj.addMarker({
                              lat: latlng.lat(),
                              lng: latlng.lng(),
                              draggable: true,
                              dragend: event => {
                                  var lat = event.latLng.lat();
                                  var lng = event.latLng.lng();
                                  $('#latitude').val(lat);
                                  $('#longitude').val(lng);
                              }
                          });

                          $('#latitude').val(latlng.lat());
                          $('#longitude').val(latlng.lng());
                      } else if (status == 'ZERO_RESULTS') {
                          alert('Sorry, no location named ' + address);
                      }
                  }
              });
          }
      });

      $(".remove_img_preview").click(function(){
          let id = $(this).data('id');
          let boxPhoto = $(this).parents('.file-preview-wrapper');

          swal({
            title: "Apakah Anda Yakin ?",
            text: "Foto Akan Dihapus Permanen !",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#00C853",
              confirmButtonText: "Ya, Yakin !",
              cancelButtonText: "Tidak, Batalkan !",
              closeOnConfirm: false,
              closeOnCancel: true
              },
              function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url  : `{{route('file-delete-photo', '')}}/${id}`,
                        type : "DELETE",
                        data : {
                            "_token": "{{ csrf_token() }}"
                        },
                        success : function(data, status){
                            boxPhoto.remove();

                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Foto Billboard Berhasil Dihapus!",
                                        type: "success"
                                    });
                                }, 1000);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Billboard Gagal Diubah", "error");
                            }, 1000);
                        }
                    });
                }
          });
      });

      $("#button-update").click(function(){
          let id = $('#id_billboard').val();
          swal({
            title: "Apakah Anda Yakin ?",
            text: "Pastikan Data Billboard Sudah Terisi Dengan Benar",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#00C853",
              confirmButtonText: "Ya, Yakin !",
              cancelButtonText: "Tidak, Batalkan !",
              closeOnConfirm: false,
              closeOnCancel: false
              },
              function(isConfirm){
              if (isConfirm) {
                  let data = new FormData();
                  let formData = $('#form-project').serializeArray();
                  var files = $('#edit_foto')[0].files;

                  $.each(formData, function(key, value){
                      data.append(value.name, value.value);
                  });

                  $.each(files, function(key, value){
                      data.append('file[]', value);
                  });

                  $.ajax({
                      url         : `{{route('update-billboard', '')}}/${id}`,
                      type        : "POST",
                      data        : data,
                      cache 		  : false,
                      contentType : false,
                      processData : false,
                      success : function(data, status){
                          if(status=="success"){
                              setTimeout(function(){
                                  swal({
                                      title: "Sukses",
                                      text: "Data Billboard Berhasil Diubah!",
                                      type: "success"
                                  }, function(){
                                      window.location.href = `{{route('vendor-billboard')}}`;
                                  });
                              }, 1000);
                          }
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                          setTimeout(function(){
                              swal("Gagal", "Data Billboard Gagal Diubah", "error");
                          }, 1000);
                      }
                  });
              } else {
                  swal("Dibatalkan", "Data Billboard Batal Disimpan :)", "error");
              }
          });
      });
  });
  </script>
@endpush
