@extends('layouts.cms')

@section('title', 'Master - Billboard')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">

@endpush

@section('content')
<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
                <a class="btn btn-success" href="/report-vendor/billboard-list/excel?start={{Request::input('contract_date')}}&end={{Request::input('end')}}" data-toggle="tooltip" data-placement="top" title="Download All Report to Excel"><i class="fa fa-file-excel-o"></i> Download All Report to Excel</a>
                <a class="btn btn-success" href="/report-vendor/billboard-list/pdf?start={{Request::input('contract_date')}}&end={{Request::input('end')}}" data-toggle="tooltip" data-placement="top" title="Download All Report to PDF"><i class="fa fa-file-pdf-o"></i> Download All Report to PDF</a>
              
        </div>
      </div>
    </div>
  </div>
@foreach($data as $dta)
    <!-- START PANEL -->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div>
                
            </div>
            <div>
            <div class="pull-left">
              <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" data-src="{{asset('assets/img/logopixartdark.png')}}" src="{{ asset('assets/img/logopixartdark.png')}}">
              
            </div>
            <div class="pull-right sm-m-t-20">
                  <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
            <table class="table">
              <tbody>
                <tr>
                  <td><strong>Billboard Code</strong></td>
                  <td>:</td>
                  <td>{{$dta['kode_billboard']}}</td>
                </tr>
                <tr>
                  <td><strong>Address</strong></td>
                  <td>:</td>
                  <td>{{$dta['address']}}</td>
                </tr>
                <tr>
                  <td><strong>Province</strong></td>
                  <td>:</td>
                  <td>{{$dta['provinsi']['nama_provinsi']}}</td>
                </tr>
                <tr>
                  <td><strong>City</strong></td>
                  <td>:</td>
                  <td>{{$dta['kabupaten']['nama_kabupaten']}}</td>
                </tr>
                <tr>
                  <td><strong>District</strong></td>
                  <td>:</td>
                  <td>{{$dta['kecamatan']['nama_kecamatan']}}</td>
                </tr>
                <tr>
                  <td><strong>Length</strong></td>
                  <td>:</td>
                  <td>{{$dta['panjang']}} meters</td>
                </tr>
                <tr>
                  <td><strong>Width</strong></td>
                  <td>:</td>
                  <td>{{$dta['lebar']}} meters</td>
                </tr>
                <tr>
                  <td><strong>Lighting</strong></td>
                  <td>:</td>
                  <td>{{$dta['lighting']}}</td>
                </tr>
                <tr>
                  <td><strong>Side</strong></td>
                  <td>:</td>
                  <td>{{$dta['side']}} sides</td>
                </tr>
                <tr>
                  <td><strong>Format</strong></td>
                  <td>:</td>
                  <td>{{$dta['format']=='h'?'Horizontal':'Vertical'}}</td>
                </tr>
                <tr>
                  <td><strong>Material</strong></td>
                  <td>:</td>
                  <td>{{$dta['bahan']}}</td>
                </tr>
                <tr>
                  <td><strong>Cost</strong></td>
                  <td>:</td>
                  <td>Rp {{$dta['harga']}}</td>
                </tr>
              </tbody>
            </table>
            @if(!empty($dta['photo']))
              <div class="row">
                @foreach($dta['photo'] as $foto)
                  <div class="col-sm-3"><img src="{{url('file/'.$foto['photo_name'])}}" class="img-thumbnail"></div>
                @endforeach
              </div>
            @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END PANEL -->
@endforeach
</div>
<!-- END CONTAINER FLUID -->

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    {{-- BOOTSTRAP FILE INPUT JS --}}
    <script src="{{ asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <script>
    $(document).ready(function(){
      lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true
      });

      var table = $('#tableWithSearch').DataTable({
          "ajax":{
              type  : "GET",
              // url : "/master/billboard/all?verify=verified",
              url   : "{{route('all-billboard')}}?verify=verified"
          },
          "columns": [
              {
                  title : '#',
                  data : "index"
              },
              {
                title : "Kode Billboard",
                data : "kode"
              },
              {
                  title: "Nama Vendor",
                  data: "vendor"
              },
              {
                  title: "Alamat",
                  data: "address"
              },
              {
                  title: "Foto",
                  data: null,
                  render: function (data) {
                      let photo = data.photo;
                      let foto = '';

                      if(photo.length > 0){
                          $.each(photo, function(key, value){
                              let link = `/file/${value.photo_name}`;

                              if(key == 0){
                                  foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}">
                                      <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                          <span>Lihat</span>
                                      </button>
                                  </a>`;
                              } else {
                                  foto += `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}"></a>`;
                              }
                          });
                      } else {
                          foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="{{asset('assets/img/gallery/no-pic.png')}}">
                              <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                  <span>Lihat</span>
                              </button>
                          </a>`;
                      }
                      return foto.replace();
                  },
                  searchable: false,
                  orderable: false
              },
              {
                  title: "Actions",
                  data: null,
                  render: function (data) {
                      var actions = '';
                      actions = `<a href="{{route('detail-billboard', '')}}/${data.id}" class="btn btn-default btn-animated from-top pg pg-search" id="btn-detail">
                          <span>Details</span>
                      </a>
                      <a href="{{route('edit-billboard', '')}}/${data.id}" class="btn btn-primary btn-animated from-top pg pg-form" id="btn-edit">
                          <span>Edit</span>
                      </a>
                      <button class="btn btn-danger btn-complete btn-animated from-top pg pg-trash_line button-delete" type="button" data-id="${data.id}">
                          <span>Delete</span>
                      </button>`;
                      return actions.replace();
                  },
                  searchable: false,
                  orderable: false
              }
          ]
      });

      //delete user
      $('#tableWithSearch').on('click', '.button-delete', function(){
          let id = $(this).data('id');
          swal({
              title: "Apakah Anda Yakin ?",
              text: "Data Billboard Akan Dihapus",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ya, Yakin !",
              cancelButtonText: "Tidak, Batalkan !",
              closeOnConfirm: false,
              closeOnCancel: false
              },
              function(isConfirm){
              if (isConfirm) {
                  $.ajax({
                      url : `{{route('delete-billboard', '')}}/${id}`,
                      type : "DELETE",
                      data : {
                          "_token": "{{ csrf_token() }}"
                      },
                      success : function(data, status){
                          if(status=="success"){
                              swal({
                                  title: "Sukses",
                                  text: "Data Billboard Berhasil Dihapus!",
                                  type: "success"
                              }, function(){
                                  table.ajax.reload();
                              });
                          }
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                          swal("Gagal", "Data Billboard Gagal Dihapus", "error");
                      }
                  });
              } else {
                  swal("Dibatalkan", "Data Billboard Batal Dihapus :)", "error");
              }
          });
      });
    });
    </script>
@endpush
    