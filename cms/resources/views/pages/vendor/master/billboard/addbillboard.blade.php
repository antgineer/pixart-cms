@extends('layouts.cms')

@section('title', 'Master - Add Billboard')

@section('sidebar')
    @include('sidebar.vendor') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg bg-white">
        <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
            <li>
              <a href="{{route('vendor-billboard')}}">Billboard</a>
            </li>
            <li><a href="#" class="active">Add Billboard</a>
            </li>
          </ul>
        <!-- END BREADCRUMB -->

        <div class="row">
          <div class="col-sm-5">
            <!-- START MAP -->
            <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
              <div class="panel-heading" style="padding-left: 0px; padding-right: 0px;">
                <div class="row">
                  <div class="col-md-12">
                    <p style="font-size: 20px;">Search Location</p>
                    <div class="form-group form-group-default">
                      <label>Enter Location</label>
                      <input type="text" class="form-control" name="cari_alamat" id="cari_alamat" style="padding-top: 5px; height: 40px;">
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-body" style="padding-left: 0px;
              padding-right: 0px; padding-bottom: 0px;">
                <div id="map" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
            <!-- END MAP -->

            <!-- START UPLOADER -->
            <div class="panel panel-default">
              <div class="form-group form-group-default required" style="border-bottom-width: 0px; margin-bottom: 0px; padding-bottom: 8px;">
                <label>Select Photos</label>
                <input id="input-7" name="input7[]" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg", "jpeg"]' style="padding-top: 5px; height: 40px;">
              </div>
            </div>
            <!-- END UPLOADER -->
          </div> {{-- END COL-SM-5 --}}

          <div class="col-sm-7">
            <!-- START FORM INPUT -->
            <div class="panel panel-transparent">
              <div class="panel-body">
                <form id="form-project" role="form" autocomplete="off">
                  <p style="font-size: 20px;">Basic Information</p>
                  <div class="form-group-attached">
                    <div class="row clearfix">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Kode Billboard</label>
                          <input type="text" class="form-control" name="kode_billboard" required style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Provinsi</label>
                          <select class="full-width" data-init-plugin="select2" name="id_provinsi" id="prov" required>
                            <option value="">Pilih Provinsi</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Kabupaten/Kota</label>
                          <select class="full-width" data-init-plugin="select2" name="id_kabupaten" id="kab" required>
                            <option value="">Pilih kabupaten/Kota</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Kecamatan</label>
                          <select class="full-width" data-init-plugin="select2" name="id_kecamatan" id="kec" required>
                            <option value="">Pilih Kecamatan</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Alamat</label>
                          <textarea class="form-control" id="address" name="address" style="height: 70px;" required></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Latitude</label>
                          <input type="text" class="form-control" name="latitude" id="latitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Longtitude</label>
                          <input type="text" class="form-control" name="longitude" id="longitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>

                  </div>

                  <p class="m-t-10" style="font-size: 20px;">Advanced Information</p>
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Panjang</label>
                          <input type="text" class="form-control" name="panjang" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Lebar</label>
                          <input type="text" class="form-control" name="lebar" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Lighting</label>
                          <select class="full-width" data-init-plugin="select2" name="lighting" id="lighting" required>
                            <option value="Frontlight">Frontlight</option>
                            <option value="Backlight">Backlight</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Side</label>
                          <select class="full-width" data-init-plugin="select2" name="side" id="side" required>
                            <option value="1 Sisi">1 Sisi</option>
                            <option value="2 Sisi">2 Sisi</option>
                            <option value="3 Sisi">3 Sisi</option>
                            <option value="4 Sisi">4 Sisi</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      {{-- <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Arah Pandang</label>
                          <input type="text" class="form-control" name="arah_pandang" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div> --}}
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Bahan</label>
                          <input type="text" class="form-control" name="bahan" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Format</label>
                          <select class="full-width" data-init-plugin="select2" name="format" id="format">
                            <option value="v">Vertical</option>
                            <option value="h">Horizontal</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default input-group required">
                          <label>Harga</label>
                          <input type="text" class="form-control" name="harga" id="harga" style="padding-top: 5px; height: 40px;" required>
                          <span class="input-group-addon">
                             Rp
                          </span>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Keterangan</label>
                          <textarea class="form-control" id="note" name="note" style="height: 70px;"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <button class="btn btn-success" type="button" id="btn-save">Save</button>
                </form>
              </div>
            </div>
            <!-- END FORM INPUT -->
          </div>
        </div> {{-- END ROW --}}

      </div>
  <!-- END CONTAINER FLUID -->
@endsection

@push('script')
  {{-- FILE UPLOAD --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>

  <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/js/form_layouts.js')}}" type="text/javascript"></script>

  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>

  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>

  {{--  Sweet Alert  --}}
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
  {{-- Cleave JS --}}
  <script src="{{ asset('assets/plugins/cleave.js/cleave.min.js') }}"></script>

  {{-- Billboard Page JS --}}
  <script src="{{ asset('js/billboard_wilayah.js') }}"></script>

  <script>
    $('document').ready(function(){
        'use strict';

        //cleave.js number format
        var harga = new Cleave('#harga', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalMark: ',',
            delimiter: '.'
        });

        $('#btn-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Billboard Sudah Terisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-project').serializeArray();
                    var files = $('#input-7')[0].files;

                    data.append("_token", "{{ csrf_token() }}");

                    $.each(formData, function(key, value){
                        data.append(value.name, value.value);
                    });

                    $.each(files, function(key, value){
                        data.append('file[]', value);
                    });

                    $.ajax({
                        //url         : "{{route('save-billboard')}}",
                        url         : route('save-billboard'),
                        type        : "POST",
                        data        : data,
                        cache 		  : false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Billboard Berhasil Disimpan!",
                                        type: "success"
                                    }, function(){
                                        //window.location.href = `{{route('vendor-billboard')}}`;
                                        window.location.href = route('vendor-billboard');
                                    });
                                }, 1000);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Billboard Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Billboard Batal Disimpan :)", "error");
                }
            });
        });
    });
  </script>
@endpush
