@extends('layouts.cms')

@section('title', 'Vendor - Report')

@section('sidebar')
    @include('sidebar.vendor') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="panel-title" style="padding: 10px 10px;">Vendor - Report</div>
              <div class="row">
                <div class="">
                  <div class="col-xs-6" style="padding-right: 0px;">
                    <a class="btn btn-success btn-animated from-top pg pg-printer" id="btn-add" data-target="#modalSlideUp" data-toggle="modal">
                      <span>Cetak</span>
                    </a>
                    <a class="btn btn-success btn-animated from-top pg pg-printer" href="/vendor-preview/billboard/all">
                      <span>Cetak Semua</span>
                    </a>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
          </div> {{-- END PANEL BODY --}}

        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

  {{-- START MODAL CETAK --}}
  <!-- Modal -->
  <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
      <div class="modal-content-wrapper">
        <div class="modal-content">
          <div class="modal-header clearfix text-left">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <h5>Pilih Tanggal Report</h5>
          </div>
          <form action="{{url('/vendor-preview')}}" method="GET"  role="form" id="form-cetak">
            <div class="modal-body">
              <div class="form-group-attached">
                <div class="row input-daterange" id="datepicker-range">
                    <div class="col-sm-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Mulai</label>
                            <input type="text" class="form-control" name="start" placeholder="Pick a date" id="datepicker-component2">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group form-group-default input-group col-sm-10">
                          <label>Tanggal Akhir</label>
                          <input type="text" class="form-control" name="end" placeholder="Pick a date" id="datepicker-component2">
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="modal-footer" style="padding-right: 20px;">
              <button type="submit" class="btn btn-primary btn-animated from-top pg pg-printer" id="btn-cetak"><span>Print</span></button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>
  <!-- /.modal-dialog -->
    {{-- END MODAL CETAK --}}

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    {{-- BOOTSTRAP FILE INPUT JS --}}
    <script src="{{ asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>

    <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script>
    $(document).ready(function(){
        let id_vendor = "{{ session('id') }}";

        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true
        });

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type : "GET",
                url : `/billboard/${id_vendor}/all`,
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                  title : "Kode Billboard",
                  data : "kode"
                },
                {
                    title: "Nama Vendor",
                    data: "vendor"
                },
                {
                    title: "Alamat",
                    data: "address"
                },
                {
                    title: "Status",
                    data: "verify",
                    render: function (data) {
                        let actions = '';
                        if(data == '0') actions = '<span class="badge badge-important">Unverified</span>';
                        else if(data == '1') actions = '<span class="badge badge-inverse">Verified</span>';
                        else actions = 'Declined';

                        return actions.replace();
                    },
                },
                {
                    title: "Foto",
                    data: null,
                    render: function (data) {
                        let photo = data.photo;
                        let foto = '';

                        if(photo.length > 0){
                            $.each(photo, function(key, value){
                                let link = `/file/${value.photo_name}`;

                                if(key == 0){
                                    foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}">
                                        <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                            <span>Lihat</span>
                                        </button>
                                    </a>`;
                                } else {
                                    foto += `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}"></a>`;
                                }
                            });
                        } else {
                            foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="{{asset('assets/img/gallery/no-pic.png')}}">
                                <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                    <span>Lihat</span>
                                </button>
                            </a>`;
                        }
                        return foto.replace();
                    },
                    searchable: false,
                    orderable: false
                },
                {
                    title: "Actions",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        actions = `
                        <a href="/vendor-onepreview/${data.id}" class="btn btn-complete btn-animated from-top fa fa-print" id="btn-print">
                            <span>Cetak</span>
                        </a>`;
                        return actions.replace();
                    },
                }
            ]
        });
    });
    </script>
@endpush
