@extends('layouts.cms')

@section('title', 'Details Contract')

@push('style')
  <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/remove-image/remove-image.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('css/next-prev.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">

  {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.10/sweetalert2.min.css"> --}}
@endpush

@section('sidebar')
    @include('sidebar.vendor') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@section('content')
  <!-- START BREADCRUMB -->
    <ul class="breadcrumb" style="padding-bottom: 0px;">
      <li>
        <a href="{{route('vendor-contract')}}" style="margin-left: 0px; padding-left: 0px; padding-right: 0px;">Contract</a>
      </li>
      <li><a href="#" class="active" style="padding-left: 0px;">Details Contract</a>
      </li>
    </ul>
  <!-- END BREADCRUMB -->

  <div class="row">
    <div class="col-lg-8 hidden-xlg m-b-10">
     <!-- START MAP -->
     <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
       <div class="panel-body" style="padding-left: 0px;
       padding-right: 0px; padding-bottom: 0px; padding-top:1px">
         <div id="map" style="width: 100%; height: 455px;"></div>
       </div>
     </div>
   <!-- END MAP -->
    </div>

    <div class="panel-body col-md-12 col-lg-4" style="padding-top: 0px; padding-bottom: 0px;">
      <div class="row">
        <div class="col-xs-6 col-md-6 col-lg-12 m-b-10" style="margin-bottom: 15px;">
          <div class="panel panel-default" style="margin-bottom: 10px;">
            <div class="panel-heading separator">
              {{-- <div class="panel-title">Kode Billboard : <span class="semi-bold">B001DPK</span></div> --}}
              <h3>Kode Billboard : <span class="semi-bold">{{ $billboard['kode_billboard'] }}</span></h3>
            </div>
            <div class="panel-body" style="padding-bottom: 0px;">
              <h4>Nama Customer : <span class="semi-bold">{{ ($customer != null) ? $customer['profile']['fullname'] : $customer_name }}</span></h4>
              <h4>Tanggal Mulai : <span class="semi-bold">{{ $contract_date }}</span></h4>
              <h4>Tanggal Akhir : <span class="semi-bold">{{ $expired_date }}</span></h4>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-md-6 col-lg-12 m-b-10" style="margin-bottom: 0px;">
          <div class="panel panel-default">
            <div class="panel-heading separator" style="padding-bottom: 3px;">
              <div class="panel-title"><span class="semi-bold"><h4>Info Billboard</h4></span></div>
            </div>
            <div class="panel-body" style="padding-bottom: 0px;">
              <h4>Panjang x Lebar : <span class="semi-bold"> {{ $billboard['panjang'] }}M x {{ $billboard['lebar'] }}M</span></h4>
              <h4>Bahan Billboard : <span class="semi-bold"> {{ $billboard['bahan'] }}</span></h4>
              <h4>Harga Kontrak : <span class="semi-bold"> Rp. {{ number_format($billboard['harga'], 0, ',', '.') }}</span></h4>
            </div>
          </div>
        </div>
      </div>
    </div>

    <br><hr style="margin-top: 0px;">

    <div class="row detail-contract-box" style="margin-left: 0px; padding-left: 5px;">
      <div class="pull-left">
        <div class="col-xs-4" style="padding-left: 5px;">
          <a class="btn btn-success btn-animated from-top pg pg-plus" id="btn-add" data-target="#add-details" data-toggle="modal" href="#add-details">
            <span>Add</span>
          </a>
        </div>
      </div>
      <div class="clearfix"></div><br>

      {{-- AWAL FOTO DETAILS CONTRACT --}}
      @if($detail_contract != null)
      <div class="col-sm-5">
        <div class="panel panel-default">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
            @foreach($detail_contract[0]['photo'] as $key => $photo)
              <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active' : '' }}"></li>
            @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
            @foreach($detail_contract[0]['photo'] as $key => $photo)
              <div class="item {{ ($key == 0) ? 'active' : '' }}" style="height: 263px;">
                <img src="{{ route('download', $photo['photo_name']) }}" alt="Foto Detail Pemasangan Billboard">
              </div>
            @endforeach
            </div>
          </div>
        </div>
      </div>
      {{-- AKHIR FOTO DETAILS CONTRACT --}}

      {{-- AWAL DETAILS CONTRACT --}}
      <div class="col-sm-7">
        <div class="panel panel-default">
          <div class="panel-heading separator" style="padding-right: 0px;">
            <div class="panel-title"><span class="semi-bold"><h4>Details Contract</h4></span></div>
            <div class="pull-right edit-delete-box" style="margin-top: 10px;">
              <div class="col-xs-6" >
                <a class="btn btn-warning btn-animated from-top fa fa-pencil" id="btn-edit" data-target="#edit-details" data-toggle="modal" data-id="{{$detail_contract[0]['id']}}" href="#edit-details">
                  <span>Edit</span>
                </a>
              </div>
              <div class="col-xs-6" style="padding-left: 5px;">
                <a class="btn btn-danger btn-animated from-top pg pg-trash_line" id="btn-delete" data-id="{{$detail_contract[0]['id']}}" href="#">
                  <span>Delete</span>
                </a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="panel-body" style="padding-bottom: 0px;">
            <h4>Tema Kontrak : <span class="semi-bold" id="detail-tema"> {{ $detail_contract[0]['tema_iklan'] }}</span></h4>
            <h4>Brand Kontrak : <span class="semi-bold" id="detail-brand"> {{ $detail_contract[0]['brand'] }}</span></h4>
            <h4>Tanggal Pemasangan : <span class="semi-bold" id="detail-tglpasang"> {{ $detail_contract[0]['created_date'] }}</span></h4>
            <h4>Tanggal Expired : <span class="semi-bold" id="detail-tglexpired"> {{ $detail_contract[0]['expired_date'] }}</span></h4>
          </div>
        </div>
      </div>
      {{-- AKHIR DETAILS CONTRACT --}}

      <div class="col-sm-12">
        <div class="col-sm-6 text-right paginate-left" style="padding-right: 5px;">
          {{--  <a href="#" class="paginate mundur">&#8249;&#8249; Newer</a>  --}}
        </div>
        <div class="col-sm-6 text-left paginate-right" style="padding-left: 5px;">
        @if($detail_contract[0]['previous_id'] != null)
          <a href="#" class="paginate maju" data-id="{{ $detail_contract[0]['previous_id'] }}">Older &#8250;&#8250;</a>
        @endif
        </div>
      </div>
    </div>
    @else
        <div class="col-sm-8">
            <div class="alert alert-info" role="alert">
              <h4 class="alert-heading">Belum Ada Pemasangan</h4>
              <p>Jika Anda mau menambahkan detail kontrak, silahkan klik tombol <b>"Add"</b> yang ada diatas kemudian isi data secara lengkap dan benar.</p>
              <hr>
              <h4 class="alert-heading">No Installation</h4>
              <p class="mb-0">If you want to add contract details, please click the <b> "Add" </b> button above then complete and correct data.</p>
            </div>
        </div>
    @endif
  </div>

  {{--start modal add detail contract--}}
    <div class="modal fade" id="add-details">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Tambah Detail Kontrak</h2><br>
              </div>
              <div class="modal-body" style="padding-bottom: 20px;">
                <form action="{{url('/#')}}" id="form-add-detail" method="POST" role="form" autocomplete="off">
                  {{ csrf_field() }}
                  <input type="hidden" value="{{ $id }}" class="form-control text-black" name="id_contract" id="id_contract">
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Tema</label>
                          <input type="text" class="form-control text-black" name="tema_iklan" id="tema_iklan" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Brand</label>
                          <input type="text" class="form-control text-black" name="brand" id="brand" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div id="datepicker-component" class="form-group form-group-default input-group date col-sm-10">
                            <label>Tanggal Expired</label>
                            <input type="text" class="form-control" placeholder="Pick a date" name="expired_date" id="expired_date" style="padding-top: 5px; height: 40px;">

                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- START UPLOADER -->
                        <div class="panel panel-default" style="margin-bottom: 0px;">
                          <div class="form-group form-group-default required" style="border-bottom-width: 0px; margin-bottom: 0px;">
                            <label>Select Photos</label>
                            <input id="file-add" name="file-add[]" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg"]' style="padding-top: 5px; height: 40px;">
                          </div>
                        </div>
                        <!-- END UPLOADER -->
                      </div>
                    </div>
                  </div>
                </form><br>
                <div class="modal-footer" style="padding-right: 0px;">
                  <button type="button" class="btn btn-success" id="button-save">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  {{--end modal add detail contract--}}

  {{--start modal edit detail contract--}}
    <div class="modal fade" id="edit-details">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Ubah Detail Kontrak</h2><br>
              </div>
              <div class="modal-body" style="padding-bottom: 20px;">
                <form action="{{url('/#')}}" id="form-edit-detail" method="POST" role="form" autocomplete="off">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                  <input type="hidden" class="form-control text-black" name="id_edit" id="id_edit">
                  <input type="hidden" class="form-control text-black" name="id_contract_edit" id="id_contract_edit">
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Tema</label>
                          <input type="text" class="form-control text-black" name="tema_iklan_edit" id="tema_iklan_edit" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Brand</label>
                          <input type="text" class="form-control text-black" name="brand_edit" id="brand_edit" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div id="datepicker-component" class="form-group form-group-default input-group date col-sm-10">
                            <label>Tanggal Expired</label>
                            <input type="text" class="form-control" placeholder="Pick a date" name="expired_date_edit" id="expired_date_edit" style="padding-top: 5px; height: 40px;">

                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- START UPLOADER -->
                        <div class="panel panel-default" style="margin-bottom: 0px;">
                          <div class="form-group form-group-default">
                            <label for="photo" class="col-sm-3 control-label">Photo</label>
                            <input type="file" class="form-control" id="file_edit" name="image_file_arr[]" multiple>
                            <br>
                            <output id="edit_list"></output>
                          </div>
                        </div>
                        <!-- END UPLOADER -->
                      </div>
                    </div>
                  </div>
                </form><br>
                <div class="modal-footer" style="padding-right: 0px;">
                  <button type="button" class="btn btn-success" id="button-update">Update</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  {{--end modal edit detail contract--}}
@endsection


@push('script')
  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>

  <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>

  {{-- FILE UPLOAD --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>
  <script src="{{asset('assets/plugins/remove-image/remove-image.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.10/sweetalert2.min.js"></script> --}}

  <script>
    $(document).ready(function(){
        var mapObj = new GMaps({
            el: '#map',
            lat: "{{ $billboard['latitude'] }}",
            lng: "{{ $billboard['longitude'] }}",
        });

        var m = mapObj.addMarker({
            lat: "{{ $billboard['latitude'] }}",
            lng: "{{ $billboard['longitude'] }}",
        });

        $('#button-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Detail Kontrak Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-add-detail').serializeArray();
                    var files = $('#file-add')[0].files;

                    $.each(formData, function(key, value){
                        data.append(value.name, value.value);
                    });

                    $.each(files, function(key, value){
                        data.append('file[]', value);
                    });

                    $.ajax({
                        url : `{{route('save-contract-detail')}}`,
                        type : "POST",
                        data : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kontrak Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kontrak Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kontrak Batal Disimpan :)", "error");
                }
            });
        });

        $('.detail-contract-box').on('click', '.paginate', function(e){
            e.preventDefault();
            let id = $(this).data('id');

            $.get(`{{ route('contractDetailShowByID', '') }}/${id}`, function(result){
                $('.carousel-indicators, .carousel-inner').empty();
                $('.paginate-left, .paginate-right').empty();
                $('.edit-delete-box').empty();

                let data = result.data;

                $('#detail-tema').text(data.tema_iklan);
                $('#detail-brand').text(data.brand);
                $('#detail-tglpasang').text(data.created_date);
                $('#detail-tglexpired').text(data.expired_date);

                if(data.next_id != null){
                    let newerElem = `<a href="#" class="paginate mundur" data-id="${data.next_id}">&#8249;&#8249; Newer</a>`;
                    $('.paginate-left').append(newerElem);
                }
                else{
                    let editDeleteElem = `<div class="col-xs-6" >
                        <a class="btn btn-warning btn-animated from-top fa fa-pencil" id="btn-edit" data-target="#edit-details" data-toggle="modal" data-id="${data.id}" href="#edit-details">
                        <span>Edit</span>
                        </a>
                    </div>
                    <div class="col-xs-6" style="padding-left: 5px;">
                        <a class="btn btn-danger btn-animated from-top pg pg-trash_line" id="btn-delete" data-id="${data.id}" href="#">
                        <span>Delete</span>
                        </a>
                    </div>`;

                    $('.edit-delete-box').append(editDeleteElem);
                }

                if(data.previous_id != null){
                    let olderElem = `<a href="#" class="paginate maju" data-id="${data.previous_id}">Older &#8250;&#8250;</a>`;
                    $('.paginate-right').append(olderElem);
                }

                if(data.photo.length > 0){
                    $.each(data.photo, function(key, val){
                        let active = (key == 0) ? 'active' : '';

                        let radioElem = `<li data-target="#myCarousel" data-slide-to="${key}" class="${active}"></li>`;
                        let fotoElem = `<div class="item ${active}" style="height: 263px;">
                            <img src="{{ route('download', '') }}/${val.photo_name}" alt="Foto Detail Pemasangan Billboard">
                        </div>`;

                        $('.carousel-inner').append(fotoElem);
                        $('.carousel-indicators').append(radioElem);
                    });
                }
                else{
                    let radioElem = `<li data-target="#myCarousel" data-slide-to="0" class="active"></li>`;
                    let fotoElem = `<div class="item active" style="height: 263px;">
                        <img src="{{asset('assets/img/gallery/no-pic.png')}}" alt="Foto Detail Pemasangan Billboard">
                    </div>`;

                    $('.carousel-inner').append(fotoElem);
                    $('.carousel-indicators').append(radioElem);
                }
            });
        });

        $('.edit-delete-box').on('click', '#btn-edit', function(){
            let id = $(this).data('id');
            $('.form-control').val('');
            $('#edit_list').empty();

            $.get(`{{ route('contractDetailShowByID', '') }}/${id}`, function(result){
                let data = result.data;

                let date2 = data.expired_date.split('-');
                let expiredDate = `${date2['1']}/${date2['2']}/${date2['0']}`;

                $('#id_edit').val(data.id);
                $('#tema_iklan_edit').val(data.tema_iklan);
                $('#brand_edit').val(data.brand);
                $('#expired_date_edit').val(expiredDate);
                $('#id_contract_edit').val(data.id_contract);

                //show foto
                if(data.photo.length > 0){
                    $.each(data.photo, (key, foto) => {
                        let elem = `<span>
                            <img class="thumb" src="/file/${foto.photo_name}">
                            <span class="remove_img_preview"></span>
                        </span>`;

                        $('#edit_list').append(elem);
                    });
                }
            });
        });

        $('#button-update').click(function(){
            let id = $('#id_edit').val();

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Detail Kontrak Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-edit-detail').serializeArray();
                    let files = $('#file_edit')[0].files;

                    $.each(formData, function(key, value){
                        let keyname = value.name;
                        let name = keyname.replace('_edit', '');
                        data.append(name, value.value);
                    });

                    $.each(files, function(key, value){
                        data.append('file[]', value);
                    });

                    $.ajax({
                        url : `{{route('update-contract-detail','')}}/${id}`,
                        type : "POST",
                        data : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kontrak Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kontrak Gagal Diubah", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kontrak Batal Diubah :)", "error");
                }
            });
        });

        $('.edit-delete-box').on('click', '#btn-delete', function(e){
            e.preventDefault();
            let id = $(this).data('id');
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Data Detail Kontrak Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : `{{route('delete-contract-detail','')}}/${id}`,
                        type : "DELETE",
                        data : {
                            "_token": "{{ csrf_token() }}"
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Detail Kontrak Berhasil Dihapus!",
                                    type: "success"
                                }, function(){
                                    location.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Detail Kontrak Gagal Dihapus", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Detail Kontrak Batal Dihapus :)", "error");
                }
            });
        });
    });
  </script>

  <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>
@endpush
