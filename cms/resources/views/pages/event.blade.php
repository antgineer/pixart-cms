@extends('layouts.cms')

@section('title', 'Event')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
    <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link href="{{asset('assets/css/default.css')}}"rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
    <style>
        .w3l {
            background-color:#2b82ad;
        }

        .w3l h1 {
            color:#ffffff;
            font-size:70px;
            font-weight:700;
            text-align:center;
            padding-top:50px;
        }

        .actived{
            background-color: blue;
            text-color: white
        }

        .w3l p{
            color:#ffffff;
            font-size:20px;
            font-weight:300;
            text-align:center;
            padding-top:50px;
        }

        #gallery{
            width: 100% !important;
        }

        .panel-heading:hover{
            cursor: pointer;
        }

        .panel-body{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        .panel-body ul {
            list-style: none;
        }

        .panel-body ul a {
            text-decoration: none;
            color: black;
        }

        .panel-body ul a:hover {
            text-decoration: underline;
            cursor: pointer;
        }

        .panel-default {
            margin-bottom: 0px;
        }

        .form-group-default{
            margin-left: 10px;
            margin-right: 10px;
        }

        .pull-right{
            margin-right: 10px;
        }

        .thumbnail a>img, .thumbnail>img {
            min-height:235px;
            max-height: 236px;
            width: 100%;
        }

       .item {
            position:relative;
            padding-top:20px;
            display:inline-block;
            border: none !important;
        }

        .notify-badge{
            position: absolute;
            right:15px;
            top:10px;
            text-align: center;
            border-radius: 30px 30px 30px 30px;
            color:white;
            padding:5px 10px;
            font-size:12px;
        }

        .thumbnail{
          display:block;padding:4px;margin-bottom:20px;line-height:1.42857143;border-radius:4px;-webkit-transition:border .2s ease-in-out;-o-transition:border .2s ease-in-out;transition:border .2s ease-in-out
        }

        .badge {
            width: 100%;
            border: 1px #bdc3c7 solid;
            border-radius:15px !important;
            text-align:left !important;
            line-height:20px;
            font-size: 15px !important;
            background: #ecf0f1;
        }

        .info-badge {
            width: 40%;
            border : 1px #BDC3C7 dotted;
            border-radius:25px !important;
            text-align: center !important;
            line-height: 30px;
            font-size:13px !important;
            background-color:#BDC3C7;
            color:#2C3E50;
        }

        .text li {
            font-size:16px;
        }

        .text li a {
            text-decoration: none;
            color: black
        }

        .haempat {
            font-family: 'Montserrat';
            text-transform: uppercase;
        }
    </style>
@endpush

@section('content')
<div class="content sm-gutter" style="padding-top: 0px;">
    <div class="container-fluid">
        <div class="col-md-12" style="padding-left: 0px;">
          <div class="pull-left">
          @if( session('role_name') == 'sampoerna' || session('event') == 1 )
            <button class="btn btn-complete btn-animated from-top pg pg-plus btn-add" type="button" data-toggle="modal" title="Add Event" href='#modal-add-event'>
                <span>Add Event</span>
            </button>
          @endif
          </div>
        </div>

        <div id="products" class="row list-group">
          @if($data)
              @foreach($data as $key => $event)
              @php
                    if($event['photo']){
                        $foto = route('download', $event['photo'][0]['photo_name']);
                    }
                    else{
                        $foto = asset('assets/img/gallery/no-pic.png');
                    }
              @endphp
            <div class="item col-xs-3 col-lg-3">
                <div class="thumbnail" id="list-event">
                    <div class="thumbnail item">
                    @php
                        if($event['status'] == '1'){
                            $eventStatus = 'Sedang Berjalan';
                            $classBadge = 'bg-success';
                        }
                        elseif($event['status'] == '0'){
                            $eventStatus = 'Telah Berakhir';
                            $classBadge = 'bg-danger';
                        }
                        else{
                            $classBadge = 'bg-danger';
                        }
                    @endphp
                        <span class="notify-badge {{$classBadge}}">{{$eventStatus}}</span>
                        <img class="group list-group-image" src="{{$foto}}" alt="" />
                    </div>
                    <div class="caption">
                        <p class="group inner list-group-item-heading" style="font-size: 25px;">
                            {{str_limit($event['nama_event'],35)}}</p>
                        <p class="group inner list-group-item-text">
                            {{str_limit($event['address'],30)}}</p>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <p class="lead">Jumlah Pengunjung :
                                    <b>{{number_format($event['jumlah_pengunjung'], 0, ',', '.')}}</b></p>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <a class="btn btn-primary pull-right btn-animated from-top pg pg-search" href="{{route('detail-event', $event['id'])}}" style="margin-right: 0px;">
                                  <span>Detail</span>
                                </a>

                                @if( session('role_name') == 'sampoerna' )
                                <button class="btn btn-danger btn-animated from-top pg pg-trash_line button-delete" title="Delete Event" type="button" data-id="{{$event['id']}}">
                                    <span class="pg pg-trash_line"></span>
                                </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            {{-- Pagination --}}
            <div class="col-xs-12">
                <nav aria-label="...">
                    <ul class="pagination">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span> </a>
                        </li>
                        @for($a = 1; $a <= $total_page; $a++)
                        <li class="page-item {{ ($a == $current_page) ? 'active' : '' }}">
                          <a class="page-link" href="{{route('event')}}?page={{$a}}">{{$a}}</a>
                        </li>
                        @endfor
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                              <span aria-hidden="true">&raquo;</span>
                              <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            @else

            {{-- kalau masih belum ada event yang dibuat --}}
            <div class="col-md-12 w3l">
              <div class="row">
                  <div class="col-md-8">
                      <h1>Not Found</h1>
                      <p>We're sorry the item that you search didn't match please check again</p>
                  </div>
                  <div class="col-md-4">
                      <div class="image">
                          <img class="img-responsive" src="{{asset('assets/images/smile.png')}}">
                      </div>
                  </div>
              </div>
            </div>
          @endif
        </div>
    </div>
</div>

{{--start modal tambah event--}}
<div class="modal fade" id="modal-add-event">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h2 class="modal-title">Tambah Event</h2><br>
          </div>
          <div class="modal-body" style="padding-bottom: 0px; padding-right: 40px;">
            <form id="form-save" role="form" autocomplete="off">
              {{ csrf_field() }}
              <div class="form-group-attached">
                <div class="row clearfix">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default required">
                      <label>Nama Event</label>
                      <input type="text" class="form-control" id="nama_event" name="nama_event" required style="padding-top: 5px; height: 40px;">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default required">
                      <label>Lokasi Event</label>
                      <input type="text" class="form-control" id="address" name="address" required style="padding-top: 5px; height: 40px;">
                    </div>
                  </div>
                </div>
                {{--  <input type="text" class="form-control" name="id_create_event" id="id_create_event" value="{{ session('id') }}" required>  --}}
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default required">
                      <label>Nama EO</label>
                      <select class="full-width" data-init-plugin="select2" name="id_eo" id="status" required>
                        <option>Pilih EO</option>
                        @foreach($data_eo as $data)
                            <option value="{{ $data['id'] }}">{{ $data['username'] }}</option>
                        @endforeach                         
                      </select>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group form-group-default input-group col-sm-10">
                          <label>Tanggal Mulai Event</label>
                          <input id="datepicker" class="datepicker-awal form-control" type="text" name="event_date" style="padding-top: 5px; height: 40px;">
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group form-group-default input-group col-sm-10">
                          <label>Tanggal Selesai Event</label>
                          <input id="datepicker" class="datepicker-akhir form-control" type="text" name="expired_date" style="padding-top: 5px; height: 40px;">
                          <span class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </span>
                      </div>
                  </div>
                </div>
                {{-- <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default required">
                      <label>Status Event</label>
                      <select class="full-width" data-init-plugin="select2" name="status" id="status" required>
                        <option value="">Pilih Status</option>
                        <option value="1">Sedang Berjalan</option>
                        <option value="0">Telah Berkahir</option>
                      </select>
                    </div>
                  </div>
                </div> --}}
              </div>
            </form>
          </div><br>
          <div class="modal-footer" style="padding-right: 25px;">
            <button type="button" class="btn btn-success" id="button-save">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
      </div>
  </div>
</div>
{{--end modal tambah event--}}
@endsection

@push('script')
  {{-- START FORM VALIDATOR --}}
  <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/js/form_layouts.js')}}" type="text/javascript"></script>
  {{-- END FORM VALIDATOR --}}
  <script src="{{asset('assets/plugins/new-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('assets/plugins/new-datepicker/locales/bootstrap-datepicker.id.min.js')}}"></script>
  {{-- BOOTSTRAP FILE INPUT JS --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

    <script>
    $(document).ready(function(){
        'use strict';

        var datepicker_awal=$('.datepicker-awal').datepicker({
              startDate: "now",
              format: "yyyy-mm-dd",
              clearBtn: true,
              language: "id",
              autoclose: true,
              todayHighlight: true
          });

        $('.datepicker-awal').change(function() {
          var start_date_end = $(this).val();

          $('.datepicker-akhir').datepicker({
              startDate: start_date_end,
              format: "yyyy-mm-dd",
              clearBtn: true,
              language: "id",
              autoclose: true
          });
        });

        $('#button-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Event Sudah Terisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                // showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    let formData = $('#form-save').serialize();

                    $.ajax({
                        url         : "{{route('save-event')}}",
                        type        : "POST",
                        data        : formData,
                        success     : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Event Berhasil Disimpan!",
                                        type: "success"
                                    }, function(){
                                        window.location.reload();
                                    });
                                }, 1000);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Event Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Event Batal Disimpan :)", "error");
                }
            });
        });

        $('#list-event').on('click', '.button-delete', function(){
            let id = $(this).data('id');
            //let id = 4;
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Data Event Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url : `/master/role/${id}`,
                        url   : `{{route('delete-event', '')}}/${id}`,
                        type  : "DELETE",
                        data  : {
                            "_token": "{{ csrf_token() }}"
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Event Berhasil Dihapus!",
                                    type: "success"
                                }, function(){
                                    location.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Event Gagal Dihapus", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Event Batal Dihapus :)", "error");
                }
            });
        });
    });
    </script>
@endpush
