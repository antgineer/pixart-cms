@extends('layouts.cms')

@section('title', 'Dashboard EO')

@section('sidebar')
    @include('sidebar.eo') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  {{-- <link href="{{asset('assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" /> --}}
  <link href="{{asset('pages/css/pages.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <link rel="stylesheet" href="{{asset('assets/ism/css/my-slider.css')}}"/>
  <style>
  .windows h1 {
    font-size: 50px !important;
  }
  .ism-slider { margin-left: auto; margin-right: auto; }
  .table-user-information > tbody > tr {
      border-top: 1px solid rgb(221, 221, 221);
  }
  .table-user-information > tbody > tr:first-child {
      border-top: 0;
  }
  .table-user-information > tbody > tr > td {
      border-top: 0;
  }
  .feedback-card {
    height: 85px;

  }
  .feedbacks {
    border: 1px black solid;
    border-radius: 8px;
    padding: 2px
  }
  </style>

@endpush

@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-info bordered" role="alert">
          <p class="pull-left">Hai <strong>{{ session('fullname') }}</strong>, Selamat Datang di Dashboard Event Organizer</p>
              <button class="close" data-dismiss="alert"></button>
          <div class="clearfix"></div>
      </div>
    </div>
      <div class="col-md-6 col-lg-6 col-sm-6">
        <div class="card share full-width" style="padding-bottom: 0px;">
          <div class="card-header clearfix">
            <h5 class="text-center text-uppercase"><b>Latest Event</b></h5>
          </div>
          <div class="card-description" style="padding-bottom: 17px;">
            <div class="ism-slider" data-radios="false" id="my-slider" style="height:486px">
            <ol>
                @if($data && $data[0] && $data[0]['photo'])
                    @foreach($data[0]['photo'] as $foto)
                    <li>
                        <img src="{{ route('download', $foto['photo_name']) }}" class="img-responsive">
                    </li>
                    @endforeach
                @else
                    <li>
                        <img src="{{asset('assets/img/gallery/no-pic.png')}}" class="img-responsive">
                    </li>
                @endif
            </ol>
          </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-6">
        <div class="card share full-width" style="padding-bottom: 15px;">
          <div class="card-header clearfix">
            <h5 class="text-center text-uppercase"><b>Event Details</b></h5>
          </div>
          <div class="card-description" style="padding-bottom: 0px;">
            <div class="panel panel-info" style="margin-bottom: 0px;">
              <div class="panel-body">
                <div class="col-md-12 col-lg-12 ">
                    @php
                    if($data && $data[0]){
                        $namaEvent = $data[0]['nama_event'] ?: null;
                        $lokasiEvent = $data[0]['address'] ?: null;
                        $tanggalMulai = $data[0]['event_date'] ? date("d-m-Y", strtotime($data[0]['event_date'])) : null;
                        $tanggalAkhir = $data[0]['expired_date'] ? date("d-m-Y", strtotime($data[0]['expired_date'])) : null;
                        $jumlahPengunjung = $data[0]['jumlah_pengunjung'] ? number_format($data[0]['jumlah_pengunjung'], 0, ',', '.') : null;
                        $keteranganEvent = $data[0]['note'] ?: null;
                        $statusEvent = $data[0]['status'] ? ($data[0]['status'] == '1') ? 'Sedang Berjalan' : 'Telah Berakhir' : null;
                    }
                    @endphp
                    <table class='table table-user-information'>
                        <tbody>
                        <tr>
                            <td>Nama Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $namaEvent or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Lokasi Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $lokasiEvent or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $tanggalMulai or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Akhir</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $tanggalAkhir or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Jumlah Pengunjung</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $jumlahPengunjung or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Keterangan Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $keteranganEvent or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Status Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $statusEvent or '-' }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="card share full-width" style="padding-bottom: 15px;">
          <div class="card-header clearfix">
            <h5 class="text-center text-uppercase">Previous Event</h5>
          </div>
          <div class="card-description" style="padding-bottom: 0px;">
            <div class="panel panel-info" style="margin-bottom: 0px;">
              <div class="panel-body">
                <div class="col-md-12 col-lg-12 ">
                    @php
                    if($data && $data[1]){
                        $namaEvent2 = $data[1]['nama_event'] ?: null;
                        $lokasiEvent2 = $data[1]['address'] ?: null;
                        $tanggalMulai2 = $data[1]['event_date'] ? date("d-m-Y", strtotime($data[1]['event_date'])) : null;
                        $tanggalAkhir2 = $data[1]['expired_date'] ? date("d-m-Y", strtotime($data[1]['expired_date'])) : null;
                        $jumlahPengunjung2 = $data[1]['jumlah_pengunjung'] ? number_format($data[1]['jumlah_pengunjung'], 0, ',', '.') : null;
                        $keteranganEvent2 = $data[1]['note'] ?: null;
                        $statusEvent2 = $data[1]['status'] ? ($data[1]['status'] == '1') ? 'Sedang Berjalan' : 'Sudah Selesai' : null;
                    }
                    @endphp
                    <table class='table table-user-information'>
                        <tbody>
                        <tr>
                            <td>Nama Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $namaEvent2 or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Lokasi Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $lokasiEvent2 or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $tanggalMulai2 or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Akhir</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $tanggalAkhir2 or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Jumlah Pengunjung</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $jumlahPengunjung2 or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Keterangan Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $keteranganEvent2 or '-' }}</td>
                        </tr>
                        <tr>
                            <td>Status Event</td>
                            <td class="col-sm-1">:</td>
                            <td>{{ $statusEvent2 or '-' }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
  </div>
@endsection

@push('script')
  <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
  {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
  <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/ism/js/ism-2.2.min.js')}}"></script>
  <script>
    $(document).ready(function(){

    });
  </script>
@endpush
