<!doctype html>
<html>

<head>
	@includeif('includes.head')
	@section('title', 'Page Login')
</head>

<body class="fixed-header ">
  <div class="login-wrapper ">
    <!-- START Login Background Pic Wrapper-->
    <div class="bg-pic">
      <!-- START Background Pic-->
      <img src="{{asset('assets/img/gallery/foto3.jpg')}}" data-src="{{asset('assets/img/gallery/foto3.jpg')}}" alt="" class="lazy">
      <!-- END Background Pic-->
      <!-- START Background Caption-->
      <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <img src="{{asset('assets/img/logopixartwhite.png')}}"/>
        <h2 class="semi-bold text-white">
        Your Advertising Solution</h2>
        <p class="small">
          images Displayed are solely for representation purposes only, All work copyright of respective owner, otherwise © 2017 Pixart Digital
        </p>
      </div>
      <!-- END Background Caption-->
    </div>
    <!-- END Login Background Pic Wrapper-->
    <!-- START Login Right Container-->
    <div class="login-container bg-white">
      <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
        <img src="{{asset('assets/img/logopixartdark.png')}}" data-src="{{asset('assets/img/logopixartdark.png')}}" alt="logopixart" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" width="300">
        <p class="p-t-35"><h1>Sign into your account</h1></p>
        <!-- START Login Form -->
        <form id="form-login" class="p-t-15" role="form" method="post" action="{{route('login')}}">
          {{ csrf_field() }}
          <!-- START Form Control-->
          <div class="form-group form-group-default">
            <label>Username</label>
            <div class="controls">
              <input type="username" id="username" name="username" placeholder="Username" class="form-control" value="{{ old('username') }}" required>
            </div>
          </div>
          <!-- END Form Control-->
          <!-- START Form Control-->
          <div class="form-group form-group-default">
            <label>Password</label>
            <div class="controls">
              <input type="password" id="password" class="form-control" name="password" placeholder="Credentials" required>
            </div>
          </div>
          <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
        </form>
        <!--END Login Form-->
      </div>
    </div>
    <!-- END Login Right Container-->
  </div>

  <!-- BEGIN VENDOR JS -->
  <script src="{{asset('assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/boostrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-bez/jquery.bez.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-select2/select2.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/classie/classie.js')}}"></script>
  <script src="{{asset('assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript')}}"></script>
  <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
  <!-- END VENDOR JS -->
  <script src="{{asset('pages/js/pages.min.js')}}"></script>
  <script>
  $(function()
  {
    $('#form-login').validate()
  })
  </script>
</body>
</html>
