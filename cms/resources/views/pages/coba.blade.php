@extends('layouts.cms')

@section('title', 'Details Contract')

@push('style')
  <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/sliderengine/css/amazingslider-1.css')}}">
@endpush

@section('sidebar')
  @php $role = session('role_name') @endphp
  @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection
class="amazingslider-nav-featuredarrow-1"
@section('content')
<div class="container-fluid container-fixed-lg bg-info">
  <div class="row">
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:900px;margin:0px auto 98px;">
      <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
        <ul class="amazingslider-slides" style="display:none;">
            <li><img src="{{asset('assets/img/gallery/billboard1.png')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/billboard2.png')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/billboard3.jpg')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/foto1.jpg')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/foto2.jpg')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/foto3.jpg')}}" alt="man" title="man" /></li>
        </ul>
        <ul class="amazingslider-thumbnails" style="display:none;">
            <li><img src="{{asset('assets/img/gallery/billboard1.png')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/billboard2.png')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/billboard3.jpg')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/foto1.jpg')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/foto2.jpg')}}" alt="man" title="man" /></li>
            <li><img src="{{asset('assets/img/gallery/foto3.jpg')}}" alt="man" title="man" /></li>
        </ul>
      </div>
    </div>
  </div>
</div>
@endsection


@push('script')
  <script src="{{asset('assets/plugins/sliderengine/js/amazingslider.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/sliderengine/js/froogaloop2.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/sliderengine/js/initslider-1.js')}}" type="text/javascript"></script>
@endpush
