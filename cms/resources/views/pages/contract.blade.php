@extends('layouts.contract')

@section('title', 'Contract')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
    <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker3.min.css')}}">

    <link href="{{asset('assets/css/default.css')}}"rel="stylesheet" type="text/css" media="screen" />
    <style>
        .w3l {
            background-color:#2b82ad;
        }
        .w3l h1 {
            color:#ffffff;
            font-size:70px;
            font-weight:700;
            text-align:center;
            padding-top:50px;
        }

        .actived{
            background-color: blue;
            text-color: white
        }

        .w3l p{
            color:#ffffff;
            font-size:20px;
            font-weight:300;
            text-align:center;
            padding-top:50px;
        }
        #gallery{
            width: 100% !important;
        }
        .panel-heading:hover{
            cursor: pointer;
        }
        .panel-body{
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        .panel-body ul {
            list-style: none;
        }

        .panel-body ul a {
            text-decoration: none;
            color: black;
        }

        .panel-body ul a:hover {
            text-decoration: underline;
            cursor: pointer;
        }

        .panel-default {
            margin-bottom: 0px;
        }

        .form-group-default{
            margin-left: 10px;
            margin-right: 10px;
        }

        .pull-right{
            margin-right: 10px;
        }

        .thumbnail a>img, .thumbnail>img {
            min-height:235px;
            max-height: 236px;
            width: 100%;
        }

       .item {
            position:relative;
            padding-top:20px;
            display:inline-block;
            border: none !important;
        }
    .notify-badge{
        position: absolute;
        right:15px;
        top:10px;
        text-align: center;
        border-radius: 30px 30px 30px 30px;
        color:white;
        padding:5px 10px;
        font-size:12px;


    }
    .thumbnail{display:block;padding:4px;margin-bottom:20px;line-height:1.42857143;border-radius:4px;-webkit-transition:border .2s ease-in-out;-o-transition:border .2s ease-in-out;transition:border .2s ease-in-out}
    .badge {
        width: 100%;
        border: 1px #bdc3c7 solid;
        border-radius:15px !important;
        text-align:left !important;
        line-height:20px;
        font-size: 15px !important;
        background: #ecf0f1;
    }
    .info-badge {
        width: 40%;
        border : 1px #BDC3C7 dotted;
        border-radius:25px !important;
        text-align: center !important;
        line-height: 30px;
        font-size:13px !important;
        background-color:#BDC3C7;
        color:#2C3E50;
    }


    .text li {
        font-size:16px;
    }

    .text li a {
        text-decoration: none;
        color: black
    }

    .haempat {
        font-family: 'Montserrat';
        text-transform: uppercase;
    }
    </style>
@endpush

@section('content')
{{--<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="container">
            <div class="well well-sm list-inline">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="list-inline">
                            <li class="hint-text">Sort by: </li>
                            <li><span class="badge badge-info"><a href="{{route('contract')}}" class="text-white active p-r-5 p-l-5 active">All</a></span></li>
                            <li><a href="{{route('contract')}}?status=contractexpired" class="text-master active p-r-5 p-l-5">Expired Contract</a></li>
                            <li><a href="{{route('contract')}}?status=willexpired" class="text-master active p-r-5 p-l-5 active">Will Expire</a></li>
                            <li><a href="{{route('contract')}}?status=detailexpired" class="text-master active p-r-5 p-l-5 active">Expired Installment</a></li>
                            <li><a href="{{route('contract')}}?status=newupdated" class="text-master active p-r-5 p-l-5 active">Updated Today</a></li>
                            <li><a href="{{route('contract')}}?status=occupied" class="text-master active p-r-5 p-l-5 active">Occupied Billboard</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="text-right">
                            <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#modalSlideUpSmall">More Filters</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="products" class="row list-group">
                @if($data)
                    @foreach($data as $key => $contract)
                    @php
                        $foto = ($contract['detail_contract']) ? route('download', $contract['detail_contract'][0]['photo'][0]['photo_name']) : asset('assets/img/gallery/no-pic.png');
                    @endphp

                    <div class="item  col-xs-4 col-lg-4">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="{{$foto}}" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    {{$contract['billboard']['kode_billboard']}}</h4>
                                <p class="group inner list-group-item-text">
                                    {{$contract['billboard']['address']}}</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-9">
                                        <p class="lead">Rp.
                                            {{number_format($contract['billboard']['harga'], 0, ',', '.')}}</p>
                                    </div>
                                    <div class="col-xs-12 col-md-3">
                                        <a class="btn btn-primary" href="{{route('contract-details', $contract['id'])}}">Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="col-xs-12">
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span> </a>
                                </li>
                                @php
                                    $limit = 3;
                                    $pages = ceil($total_data/$limit);
                                    $queryPage = ($query) ? "&$query" : null;
                                @endphp

                                @for($a = 1; $a <= $pages; $a++)
                                @php
                                    $pageLimitQuery = "?page=$a";
                                @endphp

                            <li class="page-item {{ ($a == $current_page) ? 'active' : '' }}"><a class="page-link" href="{{route('contract').$pageLimitQuery.$queryPage}}">{{$a}}</a></li>
                            @endfor
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span class="sr-only">Next</span> </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            @else
                <div class="col-sm-12">
                  <div class="alert alert-info" role="alert">
                    <h4 class="alert-heading">Tidak Ada Data</h4>
                  </div>
                </div>

        </div>
    </div>
</div>--}}
<div class="content sm-gutter">
    <div class="container-fluid padding-25 ">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#filterPanelOne">
                    <h4 class="text-center haempat">
                        Filters
                    </h4>
                </div>
                {{--  <div id="filterPanelOne" class="panel-collapse panel-collapse collapse in">
                    <div class="panel-body">
                        <ul>
                            <li><a href="{{route('contract')}}">All</a></li>
                            <li><a href="{{route('contract')}}?status=newupdated">Updated Today</a></li>
                            <li><a href="{{route('contract')}}?status=occupied">Occupied Billboard</a></li>
                            <li><a href="{{route('contract')}}?status=contractexpired">Expired Contract</a></li>
                            <li><a href="{{route('contract')}}?status=willexpired">Will Expire</a></li>
                            <li><a href="{{route('contract')}}?status=detailexpired">Expired Installment</a></li>
                        </ul>
                    </div>
                </div>  --}}
            </div>
            <form id="form-project" method="GET" action="{{route('contract')}}" role="form" autocomplet"off">
                <input type="hidden" class="form-control" name="status" value="{{$status_search}}"> {{-- JANGAN DIHAPUS ! --}}
                @if(session('role_name') == 'customer')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion">
                            <span>Nama Vendor</span>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-group-default">
                            <label>E.x: Kusuma Jaya</label>
                            <input type="text" value="{{old('vendor')}}" class="form-control" name="vendor" >
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class=" text-center">
                            <button class=" btn btn-success btn-lg" type="submit"><span style="font-size:17px">Search<span></button>
                        </div>
                    </div>
                </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion">
                            <span>Tema</span>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-group-default">
                            <label>Tema</label>
                            <input type="text" value="{{old('tema')}}" class="form-control" name="tema">
                        </div>
                    </div>
                </div>
                @if(session('role_name') == 'vendor')
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class=" text-center">
                            <button class=" btn btn-success btn-lg" type="submit"><span style="font-size:17px">Search<span></button>
                        </div>
                    </div>
                </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion">
                            <span>Brand</span>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-group-default">
                            <label>Brand</label>
                            <input type="text" value="{{old('brand')}}" class="form-control" name="brand">
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#filterPanelTwo">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#filterPanelTwo">
                            <span>Lokasi</span>
                        </h4>
                        <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelTwo">
                            <i class="glyphicon glyphicon-chevron-down pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelTwo"></i>
                        </span>
                    </div>
                    <div id="filterPanelTwo" class="panel-collapse panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group form-group-default">
                                <label>Address</label>
                                <input type="text" value="{{old('address')}}" class="form-control" name="address">
                            </div>
                            <div class="form-group form-group-default">
                                <label>Provinsi</label>
                                <select class="full-width form-control" data-init-plugin="select2" name="provinsi" id="id_provinsi" >
                                    <option value="">Pilih Provinsi</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Kabupaten</label>
                                <select class="full-width form-control" data-init-plugin="select2" name="kabupaten" id="id_kabupaten" >
                                    <option value="">Pilih Kabupaten</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Kecamatan</label>
                                <select class="full-width form-control" data-init-plugin="select2" name="kecamatan" id="id_kecamatan" >
                                    <option value="">Pilih Kecamatan</option>
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Region</label>
                                <select class="full-width form-control" data-init-plugin="select2" name="region" id="id_region" >
                                    <option value="">Pilih Region</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#filterPanelThree">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#filterPanelThree">
                            <span>Ukuran</span>
                        </h4>
                        <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelThree">
                            <i class="glyphicon glyphicon-chevron-down pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelThree"></i>
                        </span>
                    </div>
                    <div id="filterPanelThree" class="panel-collapse panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group form-group-default input-group">
                                <label>Panjang</label>
                                <input type="text" value="{{old('panjang')}}" name="panjang" class="form-control usd" >
                                <span class="input-group-addon">Meter</span>
                            </div>
                            <div class="form-group form-group-default input-group">
                                <label>Lebar</label>
                                <input type="text" value="{{old('lebar')}}" name="lebar" class="form-control usd" >
                                <span class="input-group-addon">Meter</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFormat">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFormat">
                            <span>Format</span>
                        </h4>
                        <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFormat">
                            <i class="glyphicon glyphicon-chevron-down pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFormat"></i>
                        </span>
                    </div>
                    <div id="filterPanelFormat" class="panel-collapse panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group form-group-default">
                                <label>Format</label>
                                <select class="full-width form-control" data-init-plugin="select2" name="format" id="format" >
                                    <option value="">Pilih Format</option>
                                    <option value="v">Vertical</option>
                                    <option value="h">Horizontal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFour">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFour">
                            <span>Tanggal</span>
                        </h4>
                        <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="accordion" href="#filterPanelFour">
                            <i class="glyphicon glyphicon-chevron-down pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanelFour"></i>
                        </span>
                    </div>
                    <div id="filterPanelFour" class="panel-collapse panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="form-group form-group-default input-group date">
                                <label>Tanggal Mulai</label>
                                {{-- <input type="text" class="form-control" placeholder="Pick a date" name="contract_date" id="datepicker-component2"> --}}

                                <input id="datepicker" value="{{old('start_date')}}" class="datepicker form-control" type="text" name="start_date">

                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <div class="form-group form-group-default input-group">
                                <label>Tanggal Akhir</label>
                                {{-- <input type="text" class="form-control" name="end" placeholder="Pick a date" id="datepicker-component2"> --}}

                                <input id="datepicker" value="{{old('end_date')}}" class="datepicker form-control" type="text" name="end_date">

                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion">
                            <span>Kode Billboard</span>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-group-default">
                            <label>E.x: AB123</label>
                            <input type="text" value="{{old('kode_billboard')}}" class="form-control" name="kode_billboard" >
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="col-md-9">
        <ul class="list-inline text-right text">
            <li><b>Sort By : </b></li>
            <li class="p-r-5 p-l-5"><a href="{{route('contract')}}">All</a></li></span>
            <li class="p-r-5 p-l-5"><a href="{{route('contract')}}?status=newupdated">Updated Today</a></li>
            <li class="p-r-5 p-l-5"><a href="{{route('contract')}}?status=occupied">Occupied Billboard</a></li>
            <li class="p-r-5 p-l-5"><a href="{{route('contract')}}?status=contractexpired">Expired Contract</a></li>
            <li class="p-r-5 p-l-5"><a href="{{route('contract')}}?status=willexpired">Will Expire</a></li>
            <li class="p-r-5 p-l-5"><a href="{{route('contract')}}?status=detailexpired">Expired Installment</a></li>
        </ul>
        <br>
            {{--var_dump(Request::input('vendor'))--}}
            <div class="panel-heading badge">
                <div class="panel-title">
                    <div class="col-md-8">
                        <span style="font-size:20px">Pencarian Anda Saat Ini :</span>
                            <h4>
                                <strong>
                                    <!-- {{Request::getQueryString() === null ? 'ALL' : ''}}
                                    {{Request::getQueryString() === 'status=occupied' ? 'Occupied Billboard' : ''}}
                                    {{Request::getQueryString() === 'status=contractexpired' ? 'Expired Contract' : ''}}
                                    {{Request::getQueryString() === 'status=willexpired' ? 'Will Expire' : ''}}
                                    {{Request::getQueryString() === 'status=detailexpired' ? 'Expired Installment' : ''}}
                                    {{Request::getQueryString() === 'status=newupdated' ? 'Updated Today' : ''}} -->
                                    <span class="badge info-badge" style="width:10% !important">{{Request::input('status') === null ? 'ALL' : ''}}</span>
                                    <span class="badge info-badge" style="width:20% !important">{{Request::input('status')=== 'newupdated' ? 'Updated Today': ''}}</span>
                                    <span class="badge info-badge" style="width:22% !important">{{Request::input('status')=== 'occupied' ? 'Occupied Billboard': ''}}</span>
                                    <span class="badge info-badge" style="width:22% !important">{{Request::input('status')=== 'contractexpired' ? 'Expired Contract': ''}}</span>
                                    <span class="badge info-badge" style="width:16% !important">{{Request::input('status')=== 'willexpired' ? 'Will Expire': ''}}</span>
                                    <span class="badge info-badge" style="width:25% !important">{{Request::input('status')=== 'detailexpired' ? 'Expired Installment': ''}}</span>
                                    <span class="badge info-badge">{{Request::input('null')}}</span>
                                    <span class="badge info-badge">{{Request::input('vendor') !== null ? 'Vendor : ' . Request::input('vendor') : '' }}</span>
                                    <span class="badge info-badge">{{Request::input('kode_billboard') !== null ? 'Kode Billboard : ' . Request::input('kode_billboard'): '' }}</span>
                                    <span class="badge info-badge">{{Request::input('tema') !== null ? 'Tema : ' . Request::input('tema'): '' }}</span>
                                    <span class="badge info-badge">{{Request::input('brand') !==null ? 'Brand : '. Request::input('brand'): '' }}</span>
                                    <span class="badge info-badge">{{Request::input('address') !==null ? 'Address : '. Request::input('address'): '' }}</span>
                                    <span id="provinsi_zzzz" class="badge info-badge">{{Request::input('provinsi') !==null ? 'Provinsi: ' : ''}}</span>
                                    <span id="kabupatens" class="badge info-badge">{{Request::input('kabupaten') !==null ? 'Kabupaten: ' : ''}}</span>
                                    <span id="kecamatans" class="badge info-badge">{{Request::input('kecamatan') !==null ? 'Kecamatan: ' : ''}}</span>
                                    {{--Request::input('kabupaten')--}}
                                    {{--Request::input('kecamatan')--}}
                                    <span class="badge info-badge" style="color:black !important;width:24%">{{Request::input('panjang') !==null ? 'Panjang : ' . Request::input('panjang') .' Meter ': ''}}</span>
                                    <span class="badge info-badge" style="color:black !important;width:24%">{{Request::input('lebar') !==null ? 'Lebar : ' . Request::input('lebar') . ' Meter ' : ''}}</span>
                                    <span class="badge info-badge" style="width:22% !important">{{Request::input('format')=== 'v' ? 'Format : Vertical': ''}}</span>
                                    <span class="badge info-badge" style="width:24% !important">{{Request::input('format')=== 'h' ? 'Format : Horizontal': ''}}</span>
                                    <span class="badge info-badge" style="color:black !important;width:30% !important">{{Request::input('start_date') !==null ? 'Tanggal Mulai : ' . Request::input('start_date'): ''}}</span>
                                    <span class="badge info-badge" style="color:black !important;width:30% !important">{{Request::input('end_date') !==null ? 'Tanggal Mulai : ' . Request::input('end_date'): ''}}</span>

                                </strong>
                            </h4>
                    </div>
                    <div class="col-md-4" style="text-align:right !important; padding-top:20px">
                        <a href="{{ route('contract.excel').'?'.Request::getQueryString() }}" class="btn btn-info btn-animated from-top fa fa-print"><span><b>Print</b></span></a>
                    </div>
                </div>
            </div>

        <div id="products" class="row list-group">
            {{--var_dump('$billboard')--}}
                @if($data)
                    @foreach($data as $key => $contract)
                    @php
                        if($contract['detail_contract']){
                            if($contract['detail_contract'][0]['photo']){
                                $foto = route('download', $contract['detail_contract'][0]['photo'][0]['photo_name']);
                            }
                            else{
                                $foto = asset('assets/img/gallery/no-pic.png');
                            }
                        }
                        else{
                            if($contract['billboard']['photo']){
                                $foto = route('download', $contract['billboard']['photo'][0]['photo_name']);
                            }
                            else{
                                $foto = asset('assets/img/gallery/no-pic.png');
                            }
                        }
                    @endphp

                    <div class="item  col-xs-3 col-lg-3">
                        <div class="thumbnail">
                            <div class="thumbnail item">
                            @php
                                if($contract['contract_status'] == 'Now Updated') $classBadge = 'bg-success';
                                elseif($contract['contract_status'] == 'Active') $classBadge = 'bg-primary';
                                elseif($contract['contract_status'] == 'Expired Contract') $classBadge = 'bg-danger';
                                elseif($contract['contract_status'] == 'Will Expired') $classBadge = 'bg-warning';
                                else $classBadge = 'bg-danger';
                            @endphp
                                <span class="notify-badge {{$classBadge}}">{{$contract['contract_status']}}</span>
                                <img class="group list-group-image" src="{{$foto}}" alt="" />
                            </div>
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    {{$contract['billboard']['kode_billboard']}}</h4>
                                <p class="group inner list-group-item-text">
                                    {{str_limit($contract['billboard']['address'],20)}}</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <p class="lead">Rp.
                                            {{number_format($contract['billboard']['harga'], 0, ',', '.')}}</p>
                                    </div>
                                    <div class="col-xs-12 col-md-12">
                                        <a class="btn btn-primary pull-right" href="{{route('contract-details', $contract['id'])}}">Detail</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="col-xs-12">
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span> </a>
                                </li>
                                @php
                                    $limit = 12;
                                    $pages = ceil($total_data/$limit);
                                    $queryPage = ($query) ? "&$query" : null;
                                @endphp

                                @for($a = 1; $a <= $pages; $a++)
                                @php
                                    $pageLimitQuery = "?page=$a";
                                @endphp

                            <li class="page-item {{ ($a == $current_page) ? 'active' : '' }}"><a class="page-link" href="{{route('contract').$pageLimitQuery.$queryPage}}">{{$a}}</a></li>
                                @endfor
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span class="sr-only">Next</span> </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            @else
        <div class="col-md-12 w3l">
            <div class="row">
                <div class="col-md-8">
                    <h1>Not Found</h1>
                    <p>We're sorry the item that you search didn't match please check again</p>
                </div>
                <div class="col-md-4">
                    <div class="image">
                        <img class="img-responsive" src="{{asset('assets/images/smile.png')}}">
                    </div>
                </div>
            </div>
        </div>
            @endif
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade slide-up disable-scroll sm-gutters" id="modalSlideUpSmall" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><strong>Advance Filters</strong></h4>
                </div>
                <div class="modal-body text-center m-t-20">
                    <form id="form-project" method="GET" action="{{route('contract')}}" role="form" autocomplet"off">
                        <input type="hidden" class="form-control" name="status" value="{{$status_search}}"> {{-- JANGAN DIHAPUS ! --}}
                        <div class="form-group-attached">
                            <div class="row clearfix">
                                @if(session('role_name') == 'customer')
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                      <label>Nama Vendor</label>
                                      <input type="text" value="{{old('vendor')}}" class="form-control" name="vendor" >
                                  </div>
                                </div>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                      <label> Kode Billboard</label>
                                      <input type="text" value="{{old('kode_billboard')}}" class="form-control" name="kode_billboard" >
                                    </div>
                                </div>
                                <div class="{{session('role_name') == 'customer' ? 'col-md-12' : 'col-md-6'}}">
                                    <div class="form-group form-group-default input-group">
                                      <label>Address</label>
                                      <input type="text" value="{{old('address')}}" class="form-control" name="address">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default input-group">
                                      <label>Tema</label>
                                      <input type="text" value="{{old('tema')}}" class="form-control" name="tema">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default input-group">
                                      <label>Brand</label>
                                      <input type="text" value="{{old('brand')}}" class="form-control" name="brand">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                      <label>Provinsi</label>
                                      <select class="full-width form-control" data-init-plugin="select2" name="provinsi" id="id_provinsi" >
                                          <option value="">Pilih Provinsi</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                      <label>Kabupaten</label>
                                      <select class="full-width form-control" data-init-plugin="select2" name="kabupaten" id="id_kabupaten" >
                                          <option value="">Pilih Kabupaten</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                      <label>Kecamatan</label>
                                      <select class="full-width form-control" data-init-plugin="select2" name="kecamatan" id="id_kecamatan" >
                                          <option value="">Pilih Kecamatan</option>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default input-group">
                                      <label>Panjang</label>
                                      <input type="text" value="{{old('panjang')}}" name="panjang" class="form-control usd" >
                                      <span class="input-group-addon">Meter</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default input-group">
                                      <label>Lebar</label>
                                      <input type="text" value="{{old('lebar')}}" name="lebar" class="form-control usd" >
                                      <span class="input-group-addon">Meter</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix input-daterange" id="datepicker-range">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default input-group date col-sm-10">
                                      <label>Tanggal Mulai</label>
                                      {{-- <input type="text" class="form-control" placeholder="Pick a date" name="contract_date" id="datepicker-component2"> --}}

                                      <input id="datepicker" value="{{old('start_date')}}" class="datepicker form-control" type="text" name="start_date">

                                      <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default input-group col-sm-10">
                                        <label>Tanggal Akhir</label>
                                        {{-- <input type="text" class="form-control" name="end" placeholder="Pick a date" id="datepicker-component2"> --}}

                                        <input id="datepicker" value="{{old('end_date')}}" class="datepicker form-control" type="text" name="end_date">

                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <button class="pull-right btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->
@endsection

@push('script')
  <script src="{{asset('assets/plugins/new-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('assets/plugins/new-datepicker/locales/bootstrap-datepicker.id.min.js')}}"></script>
    <script>
    $(document).ready(function(){
        'use strict';
        //combobox provinsi
        function kab_slt(prov=null){
            if(prov==null){
                var prov = $("#id_provinsi").val();
            }
            $.get(`{{route('getKabupatenByProvinsi', '')}}/`+prov, function(data){
                //$('#select2-chosen-3').text(data.data[0].nama_kabupaten);
                $('#id_kabupaten').html("");
                $('#id_kabupaten').append(`<option value="">Pilih Kabupaten</option>`);
                var i=0;
                $.each(data.data, function(key, val){
                    @if(!empty(Request::input('kabupaten')))
                        var req_kab=parseInt("{{Request::input('kabupaten')}}");
                    @else
                        var req_kab=null;
                    @endif
                        if(req_kab!=null){
                            if(req_kab==data.data[i].id){
                                var selected="selected";
                                $("#kabupatens").html('Kabupaten: '+data.data[i].nama_kabupaten);
                            }
                            else{
                                var selected="";
                            }
                            $('#id_kabupaten').append(`<option value="${val.id}" `+selected+`>${val.nama_kabupaten}</option>`);

                        }
                        else{
                            $('#id_kabupaten').append(`<option value="${val.id}" >${val.nama_kabupaten}</option>`);
                        }
                        i++;

                    });
                    $('#id_kabupaten').select2();
                    //$('#id_kabupaten').append(`<option value="${val.id}">${val.nama_kabupaten}</option>`);
                });
        }
        function kec_slt(kab=null){
            if(kab==null){
                var kab = $("#id_kabupaten").val();
            }

            $.get(`{{route('getKecamatanByKabupaten', '')}}/`+kab, function(data){
                //$('#select2-chosen-4').text(data.data[0].nama_kecamatan);
                 var i=0;
                $('#id_kecamatan').html("");
                $('#id_kecamatan').append(`<option value="">Pilih Kecamatan</option>`);
                $.each(data.data, function(key, val){
                    @if(!empty(Request::input('kecamatan')))
                        var req_kec="{{Request::input('kecamatan')}}";
                    @else
                        var req_kec=null;
                    @endif
                    if(req_kec!=null){
                        if(req_kec==data.data[i].id){
                                var selected="selected";
                                $("#kecamatans").html('Kecamatan: '+data.data[i].nama_kecamatan);
                            }
                            else{
                                var selected="";
                            }
                        $('#id_kecamatan').append(`<option value="${val.id}" `+selected+`>${val.nama_kecamatan}</option>`);
                    }
                    else{
                        $('#id_kecamatan').append(`<option value="${val.id}">${val.nama_kecamatan}</option>`);
                    }
                    i++;
                });
                $('#id_kecamatan').select2();

            });
        }

        $.get(`{{route('getAllProvinsi')}}`, function(data){
            var i=0;
            $.each(data.data, function(key, val){
                @if(!empty(Request::input('provinsi')))
                    var req_prov="{{Request::input('provinsi')}}";
                @else
                    var req_prov=null;
                @endif
                if(req_prov!=null){
                    if(req_prov==data.data[i].id){
                        var selected="selected";
                        $("#provinsi_zzzz").html('Provinsi: ' + data.data[i].nama_provinsi);
                    }
                    else{
                        var selected="";
                    }
                    $('#id_provinsi').append(`<option value="${val.id}" `+selected+`>${val.nama_provinsi}</option>`);
                }
                else{
                    $('#id_provinsi').append(`<option value="${val.id}" >${val.nama_provinsi}</option>`);
                }
                i++;
            });
            $('#id_provinsi').select2();
        });

        $('#id_kabupaten').select2('val', '');
        $('#id_kecamatan').select2('val', '');
        $('#id_kabupaten').empty();
        $('#id_kecamatan').empty();
        $('#id_kecamatan').append(`<option value="">Pilih Kecamatan</option>`);
        @if(!empty(Request::input('provinsi')))
            kab_slt({{Request::input('provinsi')}});
        @endif

        $('#id_provinsi').change(function(){
            kab_slt();
        });

        $('#id_kecamatan').select2('val', '');
        $('#id_kecamatan').empty();
        @if(!empty(Request::input('kabupaten')))
            kec_slt({{Request::input('kabupaten')}});
        @endif
        $('#id_kabupaten').change(function(){
            kec_slt();
        });

        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            clearBtn: true,
            language: "id",
            autoclose: true,
            todayHighlight: true
        });
    });
    </script>
@endpush
