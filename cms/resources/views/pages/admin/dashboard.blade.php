@extends('layouts.cms')

@section('title', 'Dashboard Admin')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  {{-- <link href="{{asset('assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" /> --}}
  <link href="{{asset('pages/css/pages.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <style>
  .windows h1 {
    font-size: 50px !important;
  }
  </style>
@endpush

@section('content')
  <div class="row">

    <div class="col-sm-12">
        <div class="alert alert-info bordered" role="alert">
            <p class="pull-left">Hai <strong>{{ session('fullname') }}</strong>, Selamat Datang di Dashboard Admin</p>
                <button class="close" data-dismiss="alert"></button>
            <div class="clearfix"></div>
        </div>
    </div>

    {{-- Awal Total Billboard --}}
    <div class="col-xs-12 col-md-4 col-lg-4 m-b-10">
      <div class="widget-9 panel no-border bg-complete no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    TOTAL BILLBOARD
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$total_billboard}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-globe fa-5x text-white"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('master-billboard')}}"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- Akhir Total Billboard --}}

    {{-- Awal Total Vendor --}}
    <div class="col-xs-12 col-md-4 col-lg-4 m-b-10">
      <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    Total Vendor
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$total_vendor}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-bookmark fa-5x text-white" style="padding-right: 5px;"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('master-vendor')}}"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- Akhir Total Vendor --}}

    {{-- Awal Total Customer --}}
    <div class="col-xs-12 col-md-4 col-lg-4 m-b-10">
      <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    Total Customer
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$total_customer}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-users fa-5x text-white"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('master-customer')}}"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- Akhir Total Customer --}}

    <div class="col-lg-12 hidden-xlg m-b-10">
     <!-- START MAP -->
     <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
       <div class="panel-body" style="padding-left: 0px;
       padding-right: 0px; padding-bottom: 0px; padding-top:1px">
         <div id="map" style="width: 100%; height: 400px;"></div>
       </div>
     </div>
   <!-- END MAP -->
    </div>

    <div class="col-lg-12 hidden-xlg m-b-10">
      <!-- START Waiting To Be Verified-->
        <div class="widget-11-2 panel no-border panel-condensed no-margin widget-loader-circle">
          <div class="panel-heading top-right">
            <div class="panel-controls">
              <ul>
                <li>
                  <a data-toggle="refresh" class="portlet-refresh text-black" href="#">
                    <i class="portlet-icon portlet-icon-refresh"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="padding-25">
            <div class="pull-left">
              <h2 class="text-success no-margin">Waiting To Be Verified</h2>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-11-2-table">
            <table class="table table-hover" id="tableWithSearch">

            </table>
             <div class="pull-right" style="margin-right:10px">
              <a href="{{route('verify-billboard')}}" class="btn btn-md btn-success fs-15">Show More</a>
            </div>
          </div>
        </div>
      <!-- END Waiting To Be Verified -->
    </div>
  </div>

  {{-- START MODAL DECLINE --}}
  <div class="modal fade" id="modal-decline">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Decline Billboard</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="GET" class="form-horizontal" role="form" id="form-decline">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Note</label>
                                <div class="col-sm-9" style="padding-top: 7px;">
                                  <input type="hidden" id="id_verify">
                                  <textarea class="form-control" name="note" id="note" required style="height: 70px; "></textarea>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button class="btn btn-success btn-animated from-top fa fa fa-check" type="button" id="btn-decline-submit">
                  <span>Submit</span>
                </button>
                <button type="button" class="btn btn-info btn-animated from-top fa fa fa-times" data-dismiss="modal">
                  <span>Close</span>
                </button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL DECLINE --}}
@endsection

@push('script')
  <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
  {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
  <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function(){
        'use strict';

        let id_vendor = "{{ session('id') }}";

        /* Map Object */
        var mapObj = new GMaps({
            el: '#map',
            lat: -1.553482,
            lng: 116.870117,
            zoom: 5
        });

        // $.get(`/master/billboard/all/latest/verified`, function(data){
        $.get(`{{route('getLatestBillboard')}}`, function(data){
            $.each(data.data, function(key, value){
                //if(key == 0) mapObj.setCenter(value.latitude, value.longitude);

                mapObj.addMarker({
                    lat: value.latitude,
                    lng: value.longitude,
                    title: value.kode,
                    infoWindow: {
                        content: `Kode Billboard : ${value.kode}<br>
                                  Nama Vendor : ${value.vendor}</br>
                                  Alamat : ${value.address}</p>
                                  <a href="{{route('detail-billboard','')}}/${value.id}" target="blank">
                                    <i class="fa fs-16 fa-paper-plane text-info m-r-10"></i>
                                    <span class="text-info">Show more</span>
                                  </a>`
                    }
                });
            });
        });

        var table = $('#tableWithSearch').DataTable({
          "bInfo" : false,
            "ajax":{
                type : "GET",
                url : "{{route('all-billboard')}}?verify=unverified&limit=5",
                //url : "/master/billboard/all?verify=unverified&limit=5",
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                  title : "Kode Billboard",
                  data : "kode"
                },
                {
                    title: "Nama Vendor",
                    data: "vendor"
                },
                {
                    title: "Alamat",
                    data: "address"
                },
                {
                    title: "Foto",
                    data: null,
                    render: function (data) {
                        let photo = data.photo;
                        let foto = '';

                        if(photo.length > 0){
                            $.each(photo, function(key, value){
                                let link = `/file/${value.photo_name}`;

                                if(key == 0){
                                    foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}">
                                        <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                            <span>Lihat</span>
                                        </button>
                                    </a>`;
                                } else {
                                    foto += `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}"></a>`;
                                }
                            });
                        } else {
                            foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="{{asset('assets/img/gallery/no-pic.png')}}">
                                <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                    <span>Lihat</span>
                                </button>
                            </a>`;
                        }
                        return foto.replace();
                    },
                    searchable: false,
                    orderable: false
                },
                {
                    title: "Actions",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        actions = `<a href="{{route('detail-billboard', '')}}/${data.id}" class="btn btn-success btn-complete btn-animated from-top fa fa-info" id="btn-detail" target="_blank">
                            <span class="fa fa-info"></span>
                        </a>
                        <button class="btn btn-success btn-animated from-top fa fa-check btn-accept" type="button" data-id="${data.id}">
                            <span class="fa fa-check"></span>
                        </button>
                        <button class="btn btn-danger btn-animated from-top fa fa-times btn-decline" type="button" data-toggle="modal" data-target="#modal-decline" data-id="${data.id}">
                            <span class="fa fa-times"></span>
                        </button>`;
                        return actions.replace();
                    },
                }
            ],
            searching: false,
            paging: false
        });

        //verif billboard
        $('#tableWithSearch').on('click', '.btn-accept', function(){
            let id = $(this).data('id');
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Billboard Sudah Diverifikasi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00a65a",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        //url : `/billboard/verify/${id}`,
                        url   : `{{route('billboard-verify', '')}}/${id}`,
                        type  : "PUT",
                        data  : {
                            "_token": "{{ csrf_token() }}",
                            "type" : "verify",
                            "note" : null
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Terverifikasi!",
                                    type: "success"
                                }, function(){
                                    window.location.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Billboard Gagal Diverifikasi", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Billboard Batal Diverifikasi :)", "error");
                }
            });
        });

        $('#tableWithSearch').on('click', '.btn-decline', function(){
            let id = $(this).data('id');
            $('#id_verify').val(id);
        });

        //decline billboard
        $('#btn-decline-submit').click(function(){
            let id = $('#id_verify').val();
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Note Sudah Terisi Dengan Jelas",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00a65a",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url : `/billboard/verify/${id}`,
                        url   : `{{route('billboard-verify', '')}}/${id}`,
                        type : "PUT",
                        data : {
                            "_token": "{{ csrf_token() }}",
                            "type" : "decline",
                            "note" : $('#note').val()
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Billboard Berhasil Didecline!",
                                    type: "success"
                                }, function(){
                                    window.location.reload();
                                });
                            }
                            $('#modal-decline').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Billboard Gagal Didecline", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Billboard Batal Didecline", "error");
                }
            });
        });
    });
  </script>
@endpush
