@extends('layouts.cms')

@section('title', 'Verify Billboard')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="row">
              <div class="panel-title" style="padding: 10px 10px;">Verify Billboard</div>
                  {{--  <div class="pull-right">
                    <div class="col-xs-12">
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                    </div>
                  </div>  --}}
                <div class="clearfix"></div>
            </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">
              {{--  <thead>
                <tr>
                  <th>No</th>
                  <th>Vendor</th>
                  <th>Address</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="v-align-middle semi-bold col-md-1">
                    <p>1</p>
                  </td>
                  <td class="v-align-middle col-md-3" style="padding-left: 10px;">
                    <p><strong>PT.Maxima Citra Prima<strong></p>
                  </td>
                  <td class="v-align-middle col-md-3" style="padding-left: 10px;">
                    <p>
                      <strong>
                      Jl Raya Parung Lebak Wangi Sebrang Pom Bensin
                      </strong>
                    </p>
                  </td>
                  <td class="col-md-3 text-center">
                    <a href="/detail-verify-billboard" class="btn btn-warning btn-complete btn-animated from-top pg pg-search" id="btn-detail">
                      <span>Details</span>
                    </a>
                    <button class="btn btn-complete btn-animated from-top fa fa-check" type="button" onclick="accept()">
                      <span>Accept</span>
                    </button>
                    <button class="btn btn-danger btn-animated from-top fa fa-times" type="button" data-toggle="modal" data-target="#modal-decline" id="btn-decline">
                      <span>Decline</span>
                    </button>
                  </td>
                </tr>
              </tbody>  --}}
            </table>
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

  {{-- START MODAL DECLINE --}}
  <div class="modal fade" id="modal-decline">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Decline Billboard</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="GET" class="form-horizontal" role="form" id="form-decline">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Note</label>
                                <div class="col-sm-9" style="padding-top: 7px;">
                                  <input type="hidden" id="id_verify">
                                  <textarea class="form-control" name="note" id="note" required style="height: 70px; "></textarea>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button class="btn btn-success btn-animated from-top fa fa fa-check" type="button" id="btn-decline-submit">
                  <span>Submit</span>
                </button>
                <button type="button" class="btn btn-info btn-animated from-top fa fa fa-times" data-dismiss="modal">
                  <span>Close</span>
                </button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL DECLINE --}}

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>

    <script>
      $(document).ready(function(){
        'use strict';

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type  : "GET",
                url   : "{{route('all-billboard')}}?verify=unverified",
                // url : "/master/billboard/all/unverified",
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                  title : "Kode Billboard",
                  data : "kode"
                },
                {
                    title: "Nama Vendor",
                    data: "vendor"
                },
                {
                    title: "Alamat",
                    data: "address"
                },
                {
                    title: "Foto",
                    data: null,
                    render: function (data) {
                        let photo = data.photo;
                        let foto = '';

                        if(photo.length > 0){
                            $.each(photo, function(key, value){
                                let link = `/file/${value.photo_name}`;

                                if(key == 0){
                                    foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}">
                                        <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                            <span>Lihat</span>
                                        </button>
                                    </a>`;
                                } else {
                                    foto += `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}"></a>`;
                                }
                            });
                        } else {
                            foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="{{asset('assets/img/gallery/no-pic.png')}}">
                                <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                    <span>Lihat</span>
                                </button>
                            </a>`;
                        }
                        return foto.replace();
                    },
                    searchable: false,
                    orderable: false
                },
                {
                    title: "Actions",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        // <a href="/master/detail-billboard/${data.id}" class="btn btn-tag btn-tag-dark btn-animated from-top pg pg-search btn-detail">
                        //     <span>Details</span>
                        // </a>
                        actions = `<a href="{{route('detail-billboard', '')}}/${data.id}" class="btn btn-success btn-complete btn-animated from-top fa fa-info" id="btn-detail">
                            <span class="fa fa-info"></span>
                        </a>

                        <button class="btn btn-success btn-animated from-top fa fa-check btn-accept" type="button" data-id="${data.id}">
                            <span class="fa fa-check"></span>
                        </button>
                        <button class="btn btn-danger btn-animated from-top fa fa-times btn-decline" type="button" data-toggle="modal" data-target="#modal-decline" data-id="${data.id}">
                            <span class="fa fa-times"></span>
                        </button>`;
                        return actions.replace();
                    },
                }
            ]
        });

        //verif billboard
        $('#tableWithSearch').on('click', '.btn-accept', function(){
            let id = $(this).data('id');
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Billboard Sudah Diverifikasi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00a65a",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url   : `/billboard/verify/${id}`,
                        url   : `{{route('billboard-verify', '')}}/${id}`,
                        type  : "PUT",
                        data  : {
                            "_token": "{{ csrf_token() }}",
                            "type" : "verify",
                            "note" : null
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Terverifikasi!",
                                    type: "success"
                                }, function(){
                                    table.ajax.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Billboard Gagal Diverifikasi", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Billboard Batal Diverifikasi :)", "error");
                }
            });
        });

        $('#tableWithSearch').on('click', '.btn-decline', function(){
            let id = $(this).data('id');
            $('#id_verify').val(id);
        });

        //decline billboard
        $('#btn-decline-submit').click(function(){
            let id = $('#id_verify').val();
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Note Sudah Terisi Dengan Jelas",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00a65a",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url : `/billboard/verify/${id}`,
                        url : `{{route('billboard-verify', '')}}/${id}`,
                        type  : "PUT",
                        data  : {
                            "_token": "{{ csrf_token() }}",
                            "type" : "decline",
                            "note" : $('#note').val()
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Billboard Berhasil Didecline!",
                                    type: "success"
                                }, function(){
                                    table.ajax.reload();
                                });
                            }
                            $('#modal-decline').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Billboard Gagal Didecline", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Billboard Batal Didecline", "error");
                }
            });
        });
    });
    </script>
@endpush
