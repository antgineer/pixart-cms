@extends('layouts.cms')

@section('title', 'Detail Verify Billboard')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
<link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
<link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg bg-white">
        <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
            <li>
              <a href="{{route('verify-billboard')}}">Verify Billboard</a>
            </li>
            <li><a href="#" class="active">Detail Verify Billboard</a>
            </li>
          </ul>
        <!-- END BREADCRUMB -->

        <div class="row">
          <div class="col-sm-5">
            <!-- START MAP -->
            <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
              <div class="panel-heading" style="padding-left: 0px; padding-right: 0px;">
                <div class="row">
                  <div class="col-md-12">
                    <p style="font-size: 20px; margin-bottom: 3px;">Billboard Location</p>
                  </div>
                </div>
              </div>
              <div class="panel-body" style="padding-left: 0px;
              padding-right: 0px; padding-bottom: 0px;">
                <div id="map" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
            <!-- END MAP -->

            <!-- START Carousel Image -->
            <div class="panel panel-default">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">
                    <img class="img-responsive" src="{{asset('assets/img/gallery/foto1.jpg')}}" alt="Foto 1"  style="width: 100%; height: 300px;">
                  </div>

                  <div class="item">
                    <img class="img-responsive" src="{{asset('assets/img/gallery/coba.jpg')}}" alt="Foto 2"  style="width: 100%; height: 300px;">
                  </div>

                  <div class="item">
                    <img class="img-responsive" src="{{asset('assets/img/gallery/foto2.jpg')}}" alt="Foto 3"  style="width: 100%; height: 300px;">
                  </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <!-- END Carousel Image -->
          </div> {{-- END COL-SM-5 --}}

          <div class="col-sm-7">
            <!-- START FORM INPUT -->
            <div class="panel panel-transparent">
              <div class="panel-body">
                <form id="form-project" role="form" autocomplete="off">
                  <p style="font-size: 20px;">Basic Information</p>
                  <div class="form-group-attached">
                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Kode Billboard</label>
                          <input type="text" class="form-control" name="kode_billboard" required style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Nama Vendor</label>
                          <select class="full-width" data-init-plugin="select2" name="nama_vendor" id="nama_vendor" required>
                            <option value="V01">Vendor 01</option>
                            <option value="V02">Vendor 02</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Provinsi</label>
                          <select class="full-width" data-init-plugin="select2" name="prov" id="prov" required>
                            <option value="JB">Jawa Barat</option>
                            <option value="JKT">DKI Jakarta</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Kabupaten</label>
                          <select class="full-width" data-init-plugin="select2" name="kab" id="kab" required>
                            <option value="BD">Bandung</option>
                            <option value="SMD">Sumedang</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default required">
                          <label>Kecamatan</label>
                          <select class="full-width" data-init-plugin="select2" name="kec" id="kec" required>
                            <option value="CG">Ciwaruga</option>
                            <option value="CH">Cihamplas</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Alamat</label>
                          <textarea class="form-control" id="address" style="height: 70px;" required></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Latitude</label>
                          <input type="text" class="form-control" name="latitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Longtitude</label>
                          <input type="text" class="form-control" name="longtitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>

                  </div>

                  <p class="m-t-10" style="font-size: 20px;">Advanced Information</p>
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Panjang</label>
                          <input type="text" class="form-control" name="panjang" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Lebar</label>
                          <input type="text" class="form-control" name="lebar" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Lighting</label>
                          <input id="start-date" type="text" class="form-control date" name="lighting" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Side</label>
                          <input id="end-date" type="text" class="form-control date" name="side" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Arah Pandang</label>
                          <input type="text" class="form-control" name="arah_pandang" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Format</label>
                          <select class="full-width" data-init-plugin="select2" name="format" id="format">
                            <option value="v">Vertical</option>
                            <option value="h">Horizontal</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Bahan</label>
                          <input type="text" class="form-control" name="bahan" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Harga</label>
                          <input type="text" class="form-control" name="harga" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- END FORM INPUT -->
          </div>
        </div> {{-- END ROW --}}

      </div>
  <!-- END CONTAINER FLUID -->
@endsection

@push('script')

  {{-- FILE UPLOAD --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>
  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>
  <script>
  /* Map Object */
  var mapObj = new GMaps({
      el: '#map',
      lat: 48.857,
     lng: 2.295
   });

  /* Map Object Add Marker and infoWindow */
  var m = mapObj.addMarker({
      lat: 48.8583701,
      lng: 2.2944813,
      title: 'Eiffel Tower',
      infoWindow: {
        content: '<h5>Eiffel Tower</h5><div>Paris, France</div>',
        maxWidth: 100
      }
  });
  </script>
@endpush
