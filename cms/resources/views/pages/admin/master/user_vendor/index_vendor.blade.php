@extends('layouts.cms')

@section('title', 'Master - User Vendor')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
    <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
    <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="panel-title" style="padding: 10px 10px;">Master - User Vendor</div>
              <div class="row">
                <div class="pull-left">
                  <div class="col-xs-12">
                    <button class="btn btn-success btn-animated from-top pg pg-plus" id="btn-add" type="button" data-toggle="modal" href='#modal-add'>
                      <span>Add</span>
                    </button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
          </div> {{-- END PANEL BODY --}}

        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

  {{-- START MODAL ADD --}}
  <div class="modal fade" id="modal-add">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Add User Vendor</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-add">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Full Name</label>
                                <div class="col-sm-9">
                                  <input type="text" name="fullname_add" id="fullname_add" class="form-control" placeholder="PT. Eau Minerale de montagne" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                  <input type="text" name="username_add" id="username_add" class="form-control" placeholder="Minerale" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Induk</label>
                                <div class="col-sm-9">
                                  <select class="full-width" data-init-plugin="select2" name="parent_add" id="parent_add">

                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                  <input type="password" name="password_add" id="password_add" class="form-control" placeholder="******" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                  <input type="email" name="email_add" id="email_add" class="form-control" placeholder="admin.minerale@email.com" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                  <input type="text" name="phone_add" id="phone_add" class="form-control" placeholder="+62-21445564" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" name="address_add" id="address_add" required style="height: 70px;"></textarea>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="photo" class="col-sm-3 control-label">Photo</label>
                                <div class="col-sm-9">
                                  <label>Select Photos</label>
                                  <input id="files" name="input7[]" type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg"]' style="padding-top: 5px; height: 40px;">
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-success" id="btn-save">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL ADD --}}

  {{-- START MODAL EDIT --}}
  <div class="modal fade" id="modal-edit">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Edit User Vendor</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Full Name</label>
                                <div class="col-sm-9">
                                  <input type="hidden" name="id_edit" id="id_edit" class="form-control" required>
                                  <input type="text" name="fullname_edit" id="fullname_edit" class="form-control" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                  <input type="text" name="username_edit" id="username_edit" class="form-control" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Induk</label>
                                <div class="col-sm-9">
                                  <select class="full-width" data-init-plugin="select2" name="parent_edit" id="parent_edit">

                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                  <input type="email" name="email_edit" id="email_edit" class="form-control" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                  <input type="text" name="phone_edit" id="phone_edit" class="form-control" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" name="address_edit" id="address_edit" required style="height: 70px;"></textarea>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="photo" class="col-sm-3 control-label">Photo</label>
                                <div class="col-sm-9">
                                  <label>Select Photos</label>
                                  <input id="files_edit" name="input7[]" type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg", "jpeg"]' style="padding-top: 5px; height: 40px;">
                                  <hr style="margin-bottom: 0px; margin-top: 10px;">
                                  <output id="list">
                                    <h4 style="margin-top: 0px; margin-bottom: 0px; padding-left: 40px;">Current Photo</h4>
                                    <span>
                                        <div class="file-preview-frame krajee-default kv-preview-thumb" style="padding-right: 10px; padding-left: 10px; margin-left: 0px;">
                                          <div class="kv-file-content">
                                            <img id="photo_edit" class="thumb file-preview-image kv-preview-data" src="http://via.placeholder.com/200x165" width="200px" height="165px">
                                          </div>
                                        </div>
                                    </span>
                                  </output>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-success" id="button-update">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL EDIT --}}

  {{-- START MODAL DETAIL --}}
  <div class="modal fade" id="modal-detail">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Detail User</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Full Name</label>
                                <div class="col-sm-9">
                                  <input type="text" id="fullname_detail" class="form-control" disabled style="color:black"/>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                  <input type="text" id="username_detail" class="form-control" disabled style="color:black">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                  <input type="email" id="email_detail" class="form-control" disabled style="color:black">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="phone" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                  <input type="text" id="phone_detail" class="form-control" disabled style="color:black">
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control"id="address_detail" disabled style="height: 70px;color:black">Jl.Kelapa Molek V Blok z2 No.1</textarea>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="photo" class="col-sm-3 control-label">Photo</label>
                                <div class="col-sm-9">
                                  <img id="photo_detail" src="http://via.placeholder.com/200x200" width="200px" height="200px">
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL DETAIL --}}
@endsection

@push('script')
    {{-- START FORM VALIDATOR --}}
    <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form_layouts.js')}}" type="text/javascript"></script>
    {{-- END FORM VALIDATOR --}}
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/locales/id.js')}}"></script>
    <script>
    $('document').ready(function(){
        'use strict';

        //combobox user parent
        // $.get(`/master/user/all`, function(data){
        $.get(`{{route('getAllUser')}}`, function(data){
            $.each(data.data, function(key, val){
                $('#parent_add, #parent_edit').append(`<option value="${val.id}">${val.fullname}</option>`);
            });
        });

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type  : "GET",
                url   : "{{route('getAllUser')}}/vendor",
                // url : "/master/user/all/vendor",
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                    title: "Nama Lengkap",
                    data: "fullname",
                },
                {
                    title: "Username",
                    data: "username",
                },
                {
                    title: "Email",
                    data: "email",
                },
                {
                    title: "Phone",
                    data: "phone",
                },
                {
                    title: "Address",
                    data: "address",
                },
                {
                    title: "Induk",
                    data: "parent",
                    defaultContent: '-'
                },
                {
                    title: "Action",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        actions = `
                        <button class=" btn btn-success btn-complete btn-animated from-top fa fa-info button-detail" type="button" data-toggle="modal" data-target="#modal-detail" data-id="${data.id}">
                            <span class="fa fa-info"></span>
                        </button>

                        <button class=" btn btn-warning  btn-animated from-top fa fa-pencil button-edit " type="button" data-toggle="modal" data-target="#modal-edit" data-id="${data.id}">
                            <span class="fa fa-pencil"></span>
                        </button>
                        <button class="btn btn-danger btn-complete btn-animated from-top fa fa-trash button-delete" type="button" data-id="${data.id}">
                            <span class="fa fa-trash"></span>
                        </button>
                        `;
                        return actions.replace();
                    },
                },
            ]
        });

        $('#btn-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data User Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-add').serializeArray();
                    var file = $('#files')[0].files[0];

                    data.append('file', file);

                    $.each(formData, function(key, value){
                        let keyname = value.name;
                        let name = keyname.replace('_add', '');
                        data.append(name, value.value);
                    });

                    $.ajax({
                        // url : "/master/user/vendor",
                        url   : "{{route('save-user', '')}}/vendor",
                        type  : "POST",
                        data  : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data User Berhasil Disimpan!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-add').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data User Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data User Batal Disimpan :)", "error");
                }
            });
        });

        $('#tableWithSearch').on('click', '.button-detail', function(){
            let id = $(this).data('id');
            $('.form-control').val('');

            $.get(`{{route('getUserByID', '')}}/${id}`, function(data){
                let user = data.data;
                let profile = user.profile;

                let photo = `{{route('download', '')}}/${user.photo}`;
                console.log(photo);
                $('#id_detail').val(user.id);
                $('#fullname_detail').val(profile.fullname);
                $('#username_detail').val(user.username);
                $('#email_detail').val(profile.email);
                $('#address_detail').val(profile.address);
                $('#phone_detail').val(profile.phone);
                $('#photo_detail').attr('src', photo);
            });
        });

        //edit user
        $('#tableWithSearch').on('click', '.button-edit', function(){
            let id = $(this).data('id');
            $('.form-control').val('');
            $('#select2-chosen-2').text('');

            // $.get(`/master/user/${id}`, function(data){
            $.get(`{{route('getUserByID', '')}}/${id}`, function(data){
                let user = data.data;
                let profile = user.profile;
                let photo = `{{route('download', '')}}/${user.photo}`;

                $('#id_edit').val(user.id);
                $('#fullname_edit').val(profile.fullname);
                $('#username_edit').val(user.username);
                $('#email_edit').val(profile.email);
                $('#address_edit').val(profile.address);
                $('#phone_edit').val(profile.phone);
                $('#photo_edit').attr('src', photo);

                let selectedUser = $('#parent_edit').find(`option[value=${user.parent}]`);
                $('#select2-chosen-2').text(selectedUser[0].innerHTML);
                selectedUser.prop("selected", true);
            });
        });

        //update user
        $('#button-update').click(function(){
            let id = $('#id_edit').val();

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data User Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-edit').serializeArray();
                    var file = $('#files_edit')[0].files[0];

                    data.append('file', file);

                    $.each(formData, function(key, value){
                        let keyname = value.name;
                        let name = keyname.replace('_edit', '');
                        data.append(name, value.value);
                    });

                    data.append('_method', 'PUT');

                    $.ajax({
                        // url   : `/master/user/${id}`,
                        url   : `{{route('update-user', '')}}/${id}`,
                        type  : "POST",
                        data  : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data User Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data User Gagal Diubah", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data User Batal Diubah :)", "error");
                }
            });
        });

        //delete user
        $('#tableWithSearch').on('click', '.button-delete', function(){
            let id = $(this).data('id');
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Data User Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url : `/master/user/${id}`,
                        url   : `{{route('delete-user', '')}}/${id}`,
                        type  : "DELETE",
                        data  : {
                            "_token": "{{ csrf_token() }}"
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data User Berhasil Dihapus!",
                                    type: "success"
                                }, function(){
                                    location.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data User Gagal Dihapus", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data User Batal Dihapus :)", "error");
                }
            });
        });

    });
    </script>
@endpush
