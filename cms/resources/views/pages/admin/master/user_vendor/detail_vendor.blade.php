@extends('layouts.cms')

@section('title','Master - Detail Vendor')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
<link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
<link href="{{asset('assets/plugins/remove-image/remove-image.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="container-fluid container-fixed-lg bg-white">
<!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li>
            {{-- <a href="/vendor-billboard">Billboard</a> --}}
            <a href="{{route('master-vendor')}}">Master Vendor</a>
        </li>
        <li>
            <a href="#" class="active">Detail Vendor</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-sm-5">
            <!-- START MAP -->
            <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
                <div class="panel-heading" style="padding-left: 0px; padding-right: 0px;">
                </div>
                <div class="panel-body" style="padding-left: 0px;padding-right: 0px; padding-bottom: 0px;">
                    <div id="map" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
            <!-- END MAP -->
        </div>
        <div class="col-sm-7">
            <div class="panel panel-transparent">
                <div class="panel-body">
                    <form id="form-project" role="form" autocomplete="off">
                        <div style="padding-top:24px"></div>
                        <div class="form-group-attached">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Nama Perusahaan</label>
                                        <input type="text" class="form-control" value="PT Maxima Citra Prima" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Nomor Telepon</label>
                                        <input type="text" class="form-control" value="021-224455" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" class="form-control" value="maximacitra@info.com" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Username</label>
                                        <input type="text" class="form-control" value="maximacitra" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Induk</label>
                                        <input type="text" class="form-control" value="IndukContoh" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Alamat Kantor</label>
                                        <textarea class="form-control" id="address" style="height: 70px; color: black;" disabled>JL.Kelapa Molek 5 Blok z2 no1 , Kelapa Gading</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
        </div> {{-- END PANEL BODY --}}
    </div>
</div>
@endsection





@push('script')
<script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
{{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
<script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

{{-- BOOTSTRAP FILE INPUT JS --}}
<script src="{{ asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}"></script>

<script src="{{asset('assets/plugins/lightbox/js/lightbox.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script src="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>

<script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>

<script>
$(document).ready(function(){
    let id_vendor = "{{ session('id') }}";

    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });

    var table = $('#tableWithSearch').DataTable({
        "ajax":{
            type : "GET",
            url : `/billboard/${id_vendor}/all`,
        },
        "columns": [
            {
                title : '#',
                data : "index"
            },
            {
              title : "Kode Billboard",
              data : "kode"
            },
            {
                title: "Nama Vendor",
                data: "vendor"
            },
            {
                title: "Alamat",
                data: "address"
            },
            {
                title: "Status",
                data: "verify",
                render: function (data) {
                    let actions = '';
                    if(data == '1') actions = '<span class="badge badge-inverse">Verified</span>';
                    else if(data == '0') actions = '<span class="badge badge-important">Unverified</span>';
                    else actions = '<span class="badge badge-important">Declined</span>';

                    return actions.replace();
                },
            },
            {
                title: "Foto",
                data: null,
                render: function (data) {
                    let photo = data.photo;
                    let foto = '';

                    if(photo.length > 0){
                        $.each(photo, function(key, value){
                            let link = `/file/${value.photo_name}`;

                            if(key == 0){
                                foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}">
                                    <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                        <span>Lihat</span>
                                    </button>
                                </a>`;
                            } else {
                                foto += `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="${link}"></a>`;
                            }
                        });
                    } else {
                        foto = `<a id="foto" data-lightbox="image-${data.id}" data-title="Foto Billboard" href="{{asset('assets/img/gallery/no-pic.png')}}">
                            <button class="btn btn-info btn-complete btn-animated from-top fa fa-eye" type="button">
                                <span>Lihat</span>
                            </button>
                        </a>`;
                    }
                    return foto.replace();
                },
                searchable: false,
                orderable: false
            },
            {
                title: "Actions",
                data: null,
                render: function (data) {
                    var actions = '';
                    actions = `<a href="{{route('vendordetailbillboard','')}}/${value.id}" class="btn btn-success btn-complete btn-animated from-top fa fa-info" id="btn-detail">
                        <span class="fa fa-info"></span>
                    </a>
                    <a href="{{route('vendoreditbillboard','')}}/${value.id}" class="btn btn-warning btn-complete btn-animated from-top fa fa-pencil" id="btn-edit">
                        <span class="fa fa-pencil"></span>
                    </a>
                    <button class="btn btn-danger btn-complete btn-animated from-top pg pg-trash_line button-delete" type="button" data-id="${data.id}">
                        <span class="pg pg-trash_line"></span>
                    </button>
                    `;
                    return actions.replace();
                },
            }
        ]
    });

    //delete user
    $('#tableWithSearch').on('click', '.button-delete', function(){
        let id = $(this).data('id');
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Billboard Akan Dihapus",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false
            },
            function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : `/master/billboard/${id}`,
                    type : "DELETE",
                    data : {
                        "_token": "{{ csrf_token() }}"
                    },
                    success : function(data, status){
                        if(status=="success"){
                            swal({
                                title: "Sukses",
                                text: "Data Billboard Berhasil Dihapus!",
                                type: "success"
                            }, function(){
                                window.location.reload();
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal", "Data Billboard Gagal Dihapus", "error");
                    }
                });
            } else {
                swal("Dibatalkan", "Data Billboard Batal Dihapus :)", "error");
            }
        });
    });
});
</script>

<!-- Google Maps JS API -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
{{-- GMaps Library --}}
<script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>
<script>
/* Map Object */
var mapObj = new GMaps({
    el: '#map',
    lat: -6.159869,
    lng: 106.851019,
});

var m = mapObj.addMarker({
    lat: -6.159869,
    lng: 106.851019,
});
</script>
@endpush
