@extends('layouts.cms')

@section('title', 'Master - Kabupaten')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="panel-title" style="padding: 10px 10px;">Master - Kabupaten</div>
              <div class="row">
                <div class="pull-left">
                  <div class="col-xs-12">
                    <button class="btn btn-success btn-animated from-top pg pg-plus" id="btn-add" type="button" data-toggle="modal" href='#modal-add'>
                      <span>Add</span>
                    </button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

  {{-- START MODAL ADD --}}
  <div class="modal fade" id="modal-add">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Add Kabupaten/Kota</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-add">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="prov" class="col-sm-3 control-label">Provinsi</label>
                                <div class="col-sm-9">
                                  <select class="full-width" data-init-plugin="select2" name="prov" id="prov">
                                    <option value="">Pilih Provinsi</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="kab" class="col-sm-3 control-label">Kabupaten/Kota</label>
                                <div class="col-sm-9">
                                  <input type="text" name="kab" id="kab" placeholder="Kabupaten/Kota" class="form-control" required>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-success" id="btn-save">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL ADD --}}

  {{-- START MODAL EDIT --}}
  <div class="modal fade" id="modal-edit">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Edit Kabupaten/Kota</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="prov" class="col-sm-3 control-label">Provinsi</label>
                                <div class="col-sm-9">
                                  <input type="hidden" name="id_edit" id="id_edit" class="form-control" required>
                                  <select class="full-width" data-init-plugin="select2" name="prov_edit" id="prov_edit">
                                    <option value="">Pilih Provinsi</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="kab" class="col-sm-3 control-label">Kabupaten/Kota</label>
                                <div class="col-sm-9">
                                  <input type="text" name="kab_edit" id="kab_edit" class="form-control" value="Kabupaten Pekalongan" required>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-success" id="button-update">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL EDIT --}}
@endsection

@push('script')
    {{-- START FORM VALIDATOR --}}
    <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form_layouts.js')}}" type="text/javascript"></script>
    {{-- END FORM VALIDATOR --}}
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script>
      $('document').ready(function(){
        'use strict';

        //combobox provinsi
        // $.get(`/master/provinsi/all`, function(data){
        $.get(`{{route('getAllProvinsi')}}`, function(data){
            $.each(data.data, function(key, val){
                $('#prov, #prov_edit').append(`<option value="${val.id}">${val.nama_provinsi}</option>`);
            });
        });

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type  : "GET",
                url   : "{{route('getAllKabupaten')}}",
                // url : "/master/kabupaten/all",
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                    title: "Nama Kabupaten",
                    data: "nama_kabupaten"
                },
                {
                    title: "Action",
                    data: null,
                    render: function (data) {
                        var actions = '';
                        actions = `<button class="  btn btn-warning btn-complete btn-animated from-top fa fa-pencil button-edit" type="button" data-toggle="modal" data-target="#modal-edit" data-id="${data.id}">
                            <span class="fa fa-pencil"></span>
                        </button>
                        <button class="btn btn-danger btn-complete btn-animated from-top pg pg-trash_line button-delete" type="button" data-id="${data.id}">
                            <span class="pg pg-trash_line"></span>
                        </button>`;
                        return actions.replace();
                    },
                },
            ]
        });

        $('#btn-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Kabupaten/Kota Sudah Terisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url : "/master/kabupaten",
                        url   : "{{route('save-kabupaten')}}",
                        type  : "POST",
                        data  : {
                            "_token": "{{ csrf_token() }}",
                            "nama_kabupaten" : $("#kab").val(),
                            "id_provinsi" : $("#prov").val()
                        },
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kabupaten/Kota Berhasil Disimpan!",
                                        type: "success"
                                        },
                                        function(){
                                            table.ajax.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-add').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kabupaten/Kota Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kabupaten/Kota Batal Disimpan :)", "error");
                }
            });
        });

        //edit kabupaten
        $('#tableWithSearch').on('click', '.button-edit', function(){
            let id = $(this).data('id');
            $('.form-control').val('');
            $('#select2-chosen-2').text('');

            // $.get(`/master/kabupaten/show/${id}`, function(data){
            $.get(`{{route('getByIDKabupaten', '')}}/${id}`, function(data){
                let kabupaten = data.data;

                $('#id_edit').val(kabupaten.id);
                $('#kab_edit').val(kabupaten.nama_kabupaten);

                let selectedKab = $('#prov_edit').find(`option[value=${kabupaten.id_provinsi}]`);
                $('#select2-chosen-2').text(selectedKab[0].innerHTML);
                selectedKab.prop("selected", true);
            });
        });

        //update user
        $('#button-update').click(function(){
            let id = $('#id_edit').val();

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Kabupaten/Kota Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url   : `/master/kabupaten/${id}`,
                        url   : `{{route('update-kabupaten', '')}}/${id}`,
                        type  : "PUT",
                        data  : {
                            "_token": "{{ csrf_token() }}",
                            "id_provinsi" : $("#prov_edit").val(),
                            "nama_kabupaten" : $("#kab_edit").val()
                        },
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kabupaten/Kota Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            table.ajax.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kabupaten/Kota Gagal Diubah", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kabupaten/Kota Batal Diubah :)", "error");
                }
            });
        });

        //delete user
        $('#tableWithSearch').on('click', '.button-delete', function(){
            let id = $(this).data('id');
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Data Kabupaten/Kota Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        // url : `/master/kabupaten/${id}`,
                        url   : `{{route('delete-kabupaten', '')}}/${id}`,
                        type  : "DELETE",
                        data  : {
                            "_token": "{{ csrf_token() }}"
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Kabupaten/Kota Berhasil Dihapus!",
                                    type: "success"
                                }, function(){
                                    table.ajax.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Kabupaten/Kota Gagal Dihapus", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kabupaten/Kota Batal Dihapus :)", "error");
                }
            });
        });
    });
    </script>
@endpush
