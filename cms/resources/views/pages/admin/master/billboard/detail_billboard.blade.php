@extends('layouts.cms')

@section('title', 'Master - Detail Billboard')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg bg-white">
        <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
            <li>
              <a href="{{route('master-billboard')}}">Billboard</a>
            </li>
            <li><a href="#" class="active">Detail Billboard</a>
            </li>
            <div class="pull-right">
              <div class="col-xs-6" style="padding-left: 0px;">
              <a href="{{route('billboardReportPreviewOne', $id)}}" class="btn btn-info btn-animated from-top fa fa-print" id="btn-print" style="background-color : #95a5a6; border-color : #95a5a6">
              <span style="color: white">Cetak</span>
            </a>
              </div>
            </div>
          </ul>
        <!-- END BREADCRUMB -->

        <div class="row">
          <div class="col-sm-5">
            <!-- START MAP -->
            <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
              <div class="panel-heading" style="padding-left: 0px; padding-right: 0px;">
                <div class="row">
                  <div class="col-md-12">
                    <p style="font-size: 20px; margin-bottom: 3px;">Billboard Location</p>
                  </div>
                </div>
              </div>
              <div class="panel-body" style="padding-left: 0px;
              padding-right: 0px; padding-bottom: 0px;">
                <div id="map" style="width: 100%; height: 300px;"></div>
              </div>
            </div>
            <!-- END MAP -->

            <!-- START Carousel Image -->
            <div class="panel panel-default">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                @foreach($photo as $key => $foto)
                    <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active' : '' }}"></li>
                @endforeach
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                @foreach($photo as $key => $foto)
                    <div class="item {{ ($key == 0) ? 'active' : '' }}">
                        <img class="img-responsive" style="width: 100%; height: 300px;" src="{{ route('download', $foto['photo_name']) }}" alt="Foto.$key">
                    </div>
                @endforeach
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <!-- END Carousel Image -->
          </div> {{-- END COL-SM-5 --}}

          <div class="col-sm-7">
            <!-- START FORM INPUT -->
            <div class="panel panel-transparent">
              <div class="panel-body">
                <form id="form-project" role="form" autocomplete="off">
                  <p style="font-size: 20px;">Basic Information</p>
                  <div class="form-group-attached">
                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Kode Billboard</label>
                          <input type="text" class="form-control" value="{{$kode_billboard}}" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Nama Vendor</label>
                          <input type="text" class="form-control" value="{{$user_fullname}}" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group form-group-default">
                          <label>Provinsi</label>
                          <input type="text" class="form-control" value="{{$provinsi['nama_provinsi']}}" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default">
                          <label>Kabupaten</label>
                          <input type="text" class="form-control" value="{{$kabupaten['nama_kabupaten']}}" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group form-group-default">
                          <label>Kecamatan</label>
                          <input type="text" class="form-control" value="{{$kecamatan['nama_kecamatan']}}" name="kode_billboard" disabled style="padding-top: 5px; height: 40px; color: black;">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Alamat</label>
                          <textarea class="form-control" id="address" style="height: 70px; color: black;" disabled>{{$address}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default required">
                          <label>Latitude</label>
                          <input type="text" class="form-control" value="{{$latitude}}" name="latitude" style="padding-top: 5px; height: 40px;" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Longtitude</label>
                          <input type="text" class="form-control" value="{{$longitude}}" name="longtitude" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                    </div>

                  </div>

                  <p class="m-t-10" style="font-size: 20px;">Advanced Information</p>
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Panjang</label>
                          <input type="text" class="form-control" value="{{$panjang}}" name="panjang" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Lebar</label>
                          <input type="text" class="form-control" value="{{$lebar}}" name="lebar" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Lighting</label>
                          <input id="start-date" type="text" value="{{$lighting}}" class="form-control date" name="lighting" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Side</label>
                          <input id="side" type="text" class="form-control date" value="{{$side}} Sisi" name="side" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      {{-- <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Arah Pandang</label>
                          <input type="text" class="form-control" value="{{$arah_pandang}}" name="arah_pandang" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div> --}}
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Bahan</label>
                          <input type="text" class="form-control" value="{{$bahan}}" name="bahan" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default">
                          <label>Format</label>
                          <input type="text" class="form-control" value="{{ ($format == 'v') ? 'Vertical' : 'Horizontal' }}" name="bahan" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Harga</label>
                          <input type="text" class="form-control" value="{{$harga}}" id="harga" name="harga" style="padding-top: 5px; height: 40px; color: black;" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Keterangan</label>
                          <textarea class="form-control" id="note" name="note" style="height: 70px; color: black;" disabled>{{$note}}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- END FORM INPUT -->
          </div>
        </div> {{-- END ROW --}}

      </div>
  <!-- END CONTAINER FLUID -->
@endsection

@push('script')

  {{-- FILE UPLOAD --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>
  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>

  {{-- Cleave JS --}}
  <script src="{{ asset('assets/plugins/cleave.js/cleave.min.js') }}"></script>
  <script>
  /* Map Object */
  var mapObj = new GMaps({
      el: '#map',
      lat: {{ $latitude }},
      lng: {{ $longitude }},
   });

  var m = mapObj.addMarker({
      lat: {{ $latitude }},
      lng: {{ $longitude }},
      /*title: 'Eiffel Tower',
      infoWindow: {
        content: '<h5>Eiffel Tower</h5><div>Paris, France</div>',
        maxWidth: 100
      }*/
  });

  //cleave.js number format
  var harga = new Cleave('#harga', {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand',
    numeralDecimalMark: ',',
    delimiter: '.'
  });
  </script>
@endpush
