@extends('layouts.cms')

@section('title', 'Report')

@section('sidebar')
    @include('sidebar.admin') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
<link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="row">
              <div class="panel-title" style="padding: 10px 10px;">Report</div>
                  <div class="pull-right">
                    <div class="col-sm-12">
                      {{--  <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">  --}}
                      {{-- <div class="form-group">
                        <label for="prov" class="col-sm-4 control-label">Filter</label>
                        <div class="col-sm-8"> --}}
                            {{-- <select class="full-width" data-init-plugin="select2" name="prov" id="prov">
                              <option value="">Show All</option>
                            </select> --}}
                        {{-- </div>
                      </div> --}}
                    </div>
                  </div>
                <div class="clearfix"></div>
            </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Vendor</th>
                  <th>Address</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="v-align-middle semi-bold col-md-1">
                    <p>1</p>
                  </td>
                  <td class="v-align-middle col-md-3" style="padding-left: 10px;">
                    <p><strong>PT.Maxima Citra Prima<strong></p>
                  </td>
                  <td class="v-align-middle col-md-3" style="padding-left: 10px;">
                    <p>
                      <strong>
                      Jl Raya Parung Lebak Wangi Sebrang Pom Bensin
                      </strong>
                    </p>
                  </td>
                  <td class="col-md-3 text-center">
                    <a href="/#" class="btn btn-tag btn-tag-dark btn-animated from-top pg pg-search" id="btn-detail">
                      <span>Details</span>
                    </a>
                    <button class="btn btn-success btn-animated from-top fa fa-check" type="button" onclick="accept()">
                      <span>Accept</span>
                    </button>
                    <button class="btn btn-danger btn-animated from-top fa fa-times" type="button" data-toggle="modal" data-target="#modal-decline" id="btn-decline">
                      <span>Decline</span>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

  {{-- START MODAL DECLINE --}}
  {{--  <div class="modal fade" id="modal-decline">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Decline Billboard</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="GET" class="form-horizontal" role="form" id="form-decline">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Note</label>
                                <div class="col-sm-9" style="padding-top: 7px;">
                                  <textarea class="form-control" name="note" id="note" required style="height: 70px; "></textarea>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button class="btn btn-success btn-animated from-top fa fa fa-check" type="button" onclick="decline()">
                  <span>Submit</span>
                </button>
                <button type="button" class="btn btn-info btn-animated from-top fa fa fa-times" data-dismiss="modal">
                  <span>Close</span>
                </button>
              </div>
          </div>
      </div>
  </div>  --}}
  {{-- END MODAL DECLINE --}}

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

    <script>
      function accept() {
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#1cbf0d",
          confirmButtonText: "Yes, accept it!",
          cancelButtonText: "No, cancel it!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            swal("Accpeted!", "Vendor has been Accpeted.", "success");
          } else {
            swal("Cancelled", "Vendor has been Cancelled :)", "error");
          }
        });
      }

      function decline() {
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
          } else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      }
    </script>
@endpush
