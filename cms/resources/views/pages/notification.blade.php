@extends('layouts.cms')

@section('title','Notification')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
<link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="timeline-content">
    <h2><strong>Your Notifications<stron></h2>
    <hr>
    <div class="container-fluid">
        <div class="row" id="notif-body">
            @if($data)
                @php 
                    $groupDate = null;
                @endphp

                @foreach($data as $notif)
                    @if($notif['created_at'] != $groupDate || $groupDate == null)
                        <h6><strong class="notif-date">{{ $notif['created_at'] }}</strong></h6>
                        <hr style="border-color:black;">

                        @php 
                            $groupDate = $notif['created_at'];
                        @endphp
                    @endif
                        <div class="alert {{$notif['class']}}">
                            <div class="container-fluid">
                                <a href="{{$notif['url']}}?ref=notification&notif={{$notif['id']}}" class="pull-left">{{$notif['message']}}</a>
                                <div class="pull-right">
                                    <span>{{ $notif['created_at'] }}</span>
                                </div>
                            </div>
                        </div>
                @endforeach

                @if(count($data) >= 10)
                    <div class="col-sm-12 text-center">
                        <button type="button" id="load-more-notif" data-page="2" class="btn btn-primary">Load More</button>
                    </div>
                @endif
            @else
                <div class="alert alert-default">
                    <div class="container-fluid">
                        <div class="pull-left">Tidak ada notifikasi</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
$(document).ready(function(){
    'use strict';

    $('#notif-body').on('click', '#load-more-notif', function(){
    //$('#load-more-notif').click(function(){
        let page = parseInt($(this).data('page'));
        let parent = $(this).parent();
        
        parent.remove();

        $.get(route('notification'), {page: page}, result => {
            if(result.length > 0){
                let elem = '';

                let datesElem = $('.notif-date');
                let dateIndex = datesElem.length - 1;
                //console.log(datesElem[dateIndex].innerText);

                let groupDate = datesElem[dateIndex].innerText;

                $.each(result, (key, val) => {
                    if(val.created_at != groupDate){
                        elem += `<h6><strong class="notif-date">${val.created_at}</strong></h6>
                        <hr style="border-color:black;">`;

                        groupDate = val.created_at;
                    }

                    elem += `<div class="alert ${val.class}">
                        <div class="container-fluid">
                            <a href="${val.url}?ref=notification&notif=${val.id}" class="pull-left">${val.message}</a>
                            <div class="pull-right">
                                <span>${val.created_at}</span>
                            </div>
                        </div>
                    </div>`;
                });

                if(result.length >= 10){
                    elem += `<div class="col-sm-12 text-center">
                        <button type="button" id="load-more-notif" data-page="${page + 1}" class="btn btn-primary">Load More</button>
                    </div>`;
                }

                $('#notif-body').append(elem);
            }
        });
    });
});
</script>
@endpush