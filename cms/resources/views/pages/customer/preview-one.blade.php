@extends('layouts.cms')

@section('title', 'Report')

@section('sidebar')
    @include('sidebar.customer') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
<link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">

@endpush

@section('content')

  {{--  <a class="btn btn-success btn-cons" href="/report/billboard/contract/excel/{{$id}}">Excel</a>  --}}
  
  <br><br>


        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                    <a class="btn btn-success btn-cons" href="/customer-previewone/{{$id}}/pdf">PDF</a>
                      {{--  <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/invoice/squarespace2x.png')}}" data-src="{{asset('assets/img/invoice/squarespace.png')}}" src="{{asset('assets/img/invoice/squarespace2x.png')}}">  --}}
                      <address class="m-t-10">
                                      <br>
                                      <br>
                                  </address>
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Report</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="table-responsive">
                    <table class="table m-t-50">
                      <tbody>
                        <tr>
                          <td class="">
                            <strong>Kode Billboard</strong>
                          </td>
                          <td>:</td>
                          <td>{{$billboard['kode_billboard']}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Contract date</strong>
                          </td>
                          <td>:</td>
                          <td>{{$contract_date}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Expired date</strong>
                          </td>
                          <td>:</td>
                          <td>{{$expired_date}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Vendor Name</strong>
                          </td>
                          <td>:</td>
                          <td>{{$vendor['profile']['fullname']}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Customer Name</strong>
                          </td>
                          <td>:</td>
                          <td>{{$customer['profile']['fullname']}}</td>
                        </tr>
                        @foreach($detail_contract as $dc)
                        @if($dc['status']==1)
                        <tr>
                          <td class="">
                            <strong>Brand</strong>
                          </td>
                          <td>:</td>
                          <td>{{$dc['brand']}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Advertise Theme</strong>
                          </td>
                          <td>:</td>
                          <td>{{$dc['tema_iklan']}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Created Date</strong>
                          </td>
                          <td>:</td>
                          <td>{{$dc['created_date']}}</td>
                        </tr>
                        <tr>
                          <td class="">
                            <strong>Expired Date</strong>
                          </td>
                          <td>:</td>
                          <td>{{$dc['expired_date']}}</td>
                        </tr>
                      </tbody>
                    </table>
                    @foreach($dc['photo'] as $photo)
                    <div class="gallery col-md-12" style="margin:25px auto 0 auto">
                    <figure class="col-md-3">
                    <img src="{{url('file/'.$photo['photo_name'])}}" alt="" class='img-responsive'/>
                    </figure>
                    </div>
                    @endforeach
                    @endif
                    @endforeach
                    {{--  <div class="gallery col-md-12" style="margin:25px auto 0 auto">
                    <figure class="col-md-4">
                      <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                    </figure>
                    <figure class="col-md-4">
                      <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                    </figure>
                    <figure class="col-md-4">
                      <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                    </figure>
                  </div>  --}}
                  </div>

                </div>
              </div>
            </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->


@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
@endpush
