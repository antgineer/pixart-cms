@extends('layouts.contract')

@section('title', 'Contract Customer')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
    {{--  <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">

    <link href="{{asset('assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/boostrapv3/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/bootstrap-select2/select2.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/jquery-metrojs/MetroJs.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/codrops-dialogFx/dialog.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/codrops-dialogFx/dialog-sandra.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/owl-carousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('assets/plugins/jquery-nouislider/jquery.nouislider.css')}}" rel="stylesheet" type="text/css" media="screen" />  --}}
    <style>
        #gallery{
            width: 100% !important;
        }
    </style>
@endpush

@section('content')
<div class="content ">
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <a href="#demo" class="btn btn-info" style="margin-left:50px;margin-top:20px" data-toggle="collapse">Advance Filters</a>
        <div id="demo" class="collapse" style="margin-left:50px">
            <div class="panel panel-transparent">
                <div class="panel-body">
                    <form id="form-project" role="form" autocomplete="off">
                        <p>Advance Filters</p>
                        <div class="form-group-attached">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Nama Vendor</label>
                                        <select class="full-width form-control" data-init-plugin="select2" name="id_user" id="vendor" required>
                                            <option value="">Pilih Vendor</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label> Kode Billboard</label>
                                        <input type="text" class="form-control" name="firstName" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label> Tema</label>
                                        <input type="text" class="form-control" name="firstName" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Provinsi</label>
                                        <select class="full-width form-control" data-init-plugin="select2" name="id_user" id="vendor" required>
                                            <option value="">Pilih Provinsi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Kabupaten</label>
                                        <select class="full-width form-control" data-init-plugin="select2" name="id_user" id="vendor" required>
                                            <option value="">Pilih Kabupaten</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Kecamatan</label>
                                        <select class="full-width form-control" data-init-plugin="select2" name="id_user" id="vendor" required>
                                            <option value="">Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group-attached">
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default input-group">
                                        <label>Panjang</label>
                                        <input type="text" class="form-control usd" required>
                                        <span class="input-group-addon">Meter</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default input-group">
                                        <label>Lebar</label>
                                        <input type="text" class="form-control usd" required>
                                        <span class="input-group-addon">Meter</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group form-group-default">
                                    <label>Brand</label>
                                    <input type="text" class="form-control" name="url">
                                </div>
                            </div>
                            <div class="row input-daterange" id="datepicker-range">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default input-group date col-sm-10">
                                        <label>Tanggal Mulai</label>
                                        <input type="text" class="form-control" placeholder="Pick a date" name="contract_date" id="datepicker-component2">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                <div class="form-group form-group-default input-group col-sm-10">
                                    <label>Tanggal Akhir</label>
                                    <input type="text" class="form-control" name="end" placeholder="Pick a date" id="datepicker-component2">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <button class="btn btn-success" type="submit">Submit</button>
                    </form>
                </div>
          </div>
        </div>
        <div class="gallery" id="gallery">
            <div class="gallery-filters p-t-20 p-b-10">
                <ul class="list-inline text-right">
                    <li class="hint-text">Sort by: </li>
                    <li><a href="{{route('contract')}}?status=contractexpired" class="text-master hint-text p-r-5 p-l-5">Expired Contract</a></li>
                    <li><a href="{{route('contract')}}?status=willexpired" class="text-master hint-text p-r-5 p-l-5">Will Expire</a></li>
                    <li><a href="{{route('contract')}}?status=detailexpired" class="text-master hint-text p-r-5 p-l-5">Expired Installment</a></li>
                    <li><a href="{{route('contract')}}?status=newupdated" class="text-master hint-text p-r-5 p-l-5">Updated Today</a></li>
                    <li><a href="{{route('contract')}}?status=occupied" class="text-master hint-text p-r-5 p-l-5">Occupied Billboard</a></li>
                </ul>
            </div>
            <div class="container">
                @foreach($data as $key => $contract)
                <div class="gallery-item" data-width="1" data-height="1">
                    <a href=""><!--Masukin link disini -->
                        @php
                            $foto = ($contract['detail_contract']) ? route('download', $contract['detail_contract'][0]['photo'][0]['photo_name']) : asset('assets/img/gallery/no-pic.png');
                        @endphp
                        <img src="{{$foto}}" alt="" class="image-responsive-height">
                        <div class="overlayer bottom-left full-width">
                            <div class="overlayer-wrapper item-info ">
                                <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                                    <div class="">
                                        <p class="pull-left bold text-white fs-20 p-t-10">Contract Billboard</p>
                                        <h5 class="pull-right semi-bold text-white font-montserrat bold">{{$contract['billboard']['kode_billboard']}}</h5>
                                        <div class="clearfix"></div>
                                        <br>
                                    </div>
                                    <div class="m-t-10">
                                        <div class="inline m-l-10">
                                            <p class="no-margin text-white fs-12">
                                                {{$contract['billboard']['address']}}
                                            </p>
                                            <p>
                                                Expired Date : {{$contract['expired_date']}}
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-xs-12 text-center">
            <nav aria-label="...">
                <ul class="pagination">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span> </a>
                    </li>
                    @php
                        $limit = 2;
                        $pages = ceil($total_data/$limit);
                    @endphp

                    @for($a = 1; $a <= $pages; $a++)
                    @php
                        $pageLimitQuery = "?page=$a";
                        $query = ($query) ? "&$query" : null;
                    @endphp

                    <li class="page-item {{ ($a == $current_page) ? 'active' : '' }}"><a class="page-link" href="{{route('contract').$pageLimitQuery.$query}}">{{$a}}</a></li>
                    @endfor
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span class="sr-only">Next</span> </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

@endsection
