@extends('layouts.cms')

@section('title', 'Dashboard Customer')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link href="{{asset('assets/plugins/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <style>
  .windows h1 {
    font-size: 50px !important;
  }
  </style>
@endpush

@section('content')
<div class="row">

  <div class="col-sm-12">
    <div class="alert alert-info bordered" role="alert">
        <p class="pull-left">Hai <strong>{{ session('fullname') }}</strong>, Selamat Datang di Dashboard {{ucfirst(session('role_name'))}}</p>
            <button class="close" data-dismiss="alert"></button>
        <div class="clearfix"></div>
    </div>
  </div>

  <!-- Start Widget Section -->
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 m-b-10">
      <div class="widget-9 panel no-border bg-complete no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    Active Billboard
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$contract_active}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-globe fa-5x text-white"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('contract')}}?status=occupied"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 m-b-10">
      <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    Latest Installment
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$contract_new_updated}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-thumbs-o-up fa-5x text-white"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('contract')}}?status=newupdated"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 m-b-10">
      <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    Expired Installment
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$detail_contract_expired}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-wrench fa-5x"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('contract')}}?status=detailexpired"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 m-b-10">
      <div class="widget-9 panel no-border bg-warning no-margin widget-loader-bar">
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="col-xs-height col-top">
              <div class="panel-heading  top-left top-right">
                <div class="panel-title text-black">
                  <span class="font-montserrat fs-11 all-caps">
                    About to Expire
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="row-xs-height">
                <div class="col-xs-height col-top">
                  <div class="p-l-20 p-t-15">
                    <h1 class="no-margin p-b-5 text-white">{{$contract_will_expired}}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pull-right" style="padding-right: 20px;">
                <div class="row-md-height">
                  <div class="col-md-height col-top">
                    <div class="p-l-20 p-t-15">
                      <i class="fa fa-calendar-times-o fa-5x text-white"></i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="padding-10 pull-right">
                <p class="small no-margin" style="padding-right: 10px;">
                  <a href="{{route('contract')}}?status=willexpired"><i class="fa fs-16 fa-paper-plane text-white m-r-10"></i>
                  <span class="text-white">Show more</span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- End Widget Section -->

  <div class="col-lg-12">
  <!-- START MAP -->
    <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
      <div class="panel-body" style="padding-left: 0px; padding-right: 0px; padding-bottom: 0px; padding-top:1px">
        <div id="map" style="width: 100%; height: 455px;"></div>
      </div>
    </div>
  <!-- END MAP -->
  </div>

  {{-- AWAL HIGHCHART - TOP 10 VENDOR --}}
  <div class="col-sm-6" style="padding-bottom: 8px;">
    <div id="topten" style="height: 400px"></div>
  </div>
  {{-- AKHIR HIGHCHART - TOP 10 VENDOR --}}

  {{-- AWAL HIGHCHART - BILLBOARD WILL EXPIRED --}}
  <div class="col-sm-6" style="padding-bottom: 8px;">
    <div id="billexp" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
  </div>
  {{-- AKHIR HIGHCHART - BILLBOARD WILL EXPIRED --}}

  {{-- AWAL HIGHCHART - KONTRAK AKTIF PERPROVINSI --}}
  <div class="col-sm-12" style="padding-bottom: 8px;">
    <div class="panel-group">
      <div class="panel panel-primary" style="border-color : #ffffff">
        <div class="panel-heading text-center" style="padding-top: 5px; padding-bottom: 5px;"><span><h4>Kontrak Billboard Aktif per Provinsi</h4></span></div>
        <div class="panel-body" id="contract" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
      </div>
    </div>
  </div>
  {{-- <div class="col-sm-12" style="padding-bottom: 8px;">
    <span class="text-center">Kontrak Billboard Aktif per Provinsi</span>
    <div id="contract" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
  </div> --}}
  {{-- AKHIR HIGHCHART - KONTRAK AKTIF PERPROVINSI --}}
</div>
@endsection

@push('script')
  <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>
  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>
  {{-- Highcharts --}}
  <script src="{{asset('assets/plugins/highchart/highcharts.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/highchart/highcharts-3d.js')}}" type="text/javascript"></script>
  {{-- <script src="{{asset('assets/plugins/highchart/exporting.js')}}" type="text/javascript"></script> --}}
  <script src="{{asset('assets/plugins/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jqvmap/maps/jquery.vmap.indonesia.js')}}" type="text/javascript"></script>

  <script>
  $(document).ready(function(){
    let id_customer = "{{ session('id') }}";

    /* Map Object */
    var mapObj = new GMaps({
        el: '#map',
        lat: -1.553482,
        lng: 116.870117,
        zoom: 5
    });

    $.get(`{{route('customeroccupiedstatus',array(
        'role'=>'customer',
        'id'=>session('id'),
        'status'=>'occupied'
        ))}}`,
      function(data){
        $.each(data.data, function(key, value){
            mapObj.addMarker({
                lat: value.latitude,
                lng: value.longitude,
                title: value.kode,
                infoWindow: {
                    content: `Kode Billboard : ${value.kode}<br>
                                Alamat : ${value.address}<br>
                                Sisa Hari: <span class="label label-success">${value.sisa_hari} Hari</span><br>
                                <a href="{{route('contract-details','')}}/${value.id_contract}" target="blank">
                                    <i class="fa fs-16 fa-paper-plane text-info m-r-10"></i>
                                    <span class="text-info">Show more</span>
                                </a>`
                }
            });
        });
    });

    $.get(route('statistic.top.user', {
        role : 'customer',
        id   : "{{ session('id') }}"
    }), function(result){
        let data = result.data;
        let chartData = [];
        let chart;

        if(data.length > 0){
            $.each(data, (key, val) => {
                chart = [
                    val.vendor.profile.fullname,
                    val.total_kontrak
                ];

                chartData.push(chart);
            });
        }
        else{
            chart = ['Anda belum memiliki kontrak', 1];
            chartData.push(chart);
        }

        Highcharts.chart('topten', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Top 10 Vendor'
            },
            /*tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },*/
            tooltip: {
                formatter: function(){
                    if(data.length > 0){
                        return `Total Billboard : ${this.y}`;
                    }

                    return `Total Billboard : 0`;
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Total Billboard',
                data: chartData
            }],
            credits: {
            enabled: false
            }
        });
    });

    function showMapProvinsi(data){
        // list provinsi di INDONESIA
        var enabledRegions = [
        'aceh', 'sumaterautara', 'sumaterabarat', 'riau', 'jambi', 'sumateraselatan', 'bengkulu', 'lampung', 'kepulauanbangkabelitung', 'kepulauanriau', 'dkijakarta', 'jawabarat', 'jawatengah', 'banten', 'jawatimur', 'diyogyakarta',
        'bali', 'nusatenggarabarat', 'nusatenggaratimur', 'kalimantanbarat', 'kalimantantengah', 'kalimantanselatan', 'kalimantantimur', 'kalimantanutara', 'sulawesiutara',
        'sulawesitengah', 'sulawesiselatan', 'sulawesitenggara', 'gorontalo', 'sulawesibarat', 'maluku', 'malukuutara', 'papua', 'papuabarat'
        ];

        $('#contract').vectorMap({
            map: 'indonesia_id',
            enableZoom: false,
            showTooltip: true,
            borderColor: "#000000",
            borderWidth: 2,
            hoverColor: "#FFF590",
            values: data,
            /*colors: {
                aceh: '#90ED7D', sumaterautara: '#FFBC75', sumaterabarat: '#6e1fd4', riau: '#915151', jambi: '#97c14d', sumateraselatan: '#2ECC71', bengkulu: '#ce5454', lampung: '#980606', kepulauanbangkabelitung: '#ff6666', kepulauanriau: '#9117a1', dkijakarta: '#90ED7D', jawabarat: '#e12525', jawatengah: '#48C9B0', banten: '#7D3C98', jawatimur: '#6897bb', diyogyakarta: '#D35400', bali: '#FFBC75', nusatenggarabarat: '#97c14d',
                nusatenggaratimur: '#6e1fd4', kalimantanbarat: '#6897bb', kalimantantengah: '#97c14d', kalimantanselatan: '#FFBC75', kalimantantimur: '#e12525', kalimantanutara: '#7D3C98', sulawesiutara: '#FFBC75', sulawesitengah: '#6897bb', sulawesiselatan: '#915151',
                sulawesitenggara: '#F62459', gorontalo: '#48C9B0', sulawesibarat: '#87D37C', maluku: '#90ED7D', malukuutara: '#9B59B6', papua: '#915151', papuabarat: '#34495E'

            },*/
            onRegionClick: function(event, code, region){
                event.preventDefault();
            },
            onLabelShow: function(event, label, code){
                let total =  data.hasOwnProperty(code) ? data[code] : '0';
                label.append(` : ${total} kontrak aktif`);
            }
        });
    }

    //contract per provinsi
    (function(){
        let dataProv = {};

        $.get(route('statistic.contract.provinsi', {
            role : 'customer',
            id : "{{ session('id') }}"
        }), function(result){
            $.each(result, (key, val) => {
                dataProv[val.kode_provinsi] = val.total_kontrak;
            });

            showMapProvinsi(dataProv);
        });
    })();

    //contract will expired
    (function(){
        $.get(route('statistic.contract.willexpired', {
            role : 'customer',
            id   : "{{ session('id') }}"
        }), function(result){
            Highcharts.chart('billexp', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Billboard About to Expire'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Billboard About To Expired'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Billboard About To Expired: <b>{point.y} contract(s)</b>'
                },
                series: [{
                    name: 'Bulan',
                    data: result,
                    dataLabels: {
                        enabled: true,
                        //rotation: 90,
                        color: '#FFFFFF',
                        align: 'center',
                        format: '{point.y}', // one decimal
                        y: 30, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }],
                credits: {
                enabled: false
                }
            });
        });
    })();

  });

  </script>
@endpush
