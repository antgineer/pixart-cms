@extends('layouts.cms')

@section('title', 'Occupied')

@section('sidebar')
    @include('sidebar.customer') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
<link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="row">
              <div class="panel-title" style="padding: 10px 10px;">Occupied</div>
                  <div class="pull-right">
                    <div class="col-xs-6">
                      <a href="{{route('reportcontractpdf',array('status'=> 'occupied'))}}" class="btn btn-complete btn-animated from-top fa fa-print" id="btn-cetak"><span>Cetak</span>
                      </a>
                    </div>
                  </div>
                <div class="clearfix"></div>
            </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->
@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script>
    $('document').ready(function(){
        'use strict';

        //let id_customer = "{{ session('id') }}";

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type : "GET",
                //url : `/customer-occupied/customer/${id_customer}/occupied`,
                url : `{{route('customeroccupiedstatus',array(
                  'role' => 'customer',
                  'id'  => session('id'),
                  'status' => 'occupied'
                ))}}`,
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                  title : "Kode Billboard",
                  data : "kode"
                },
                {
                  title : "Lokasi",
                  data : "address"
                },
                {
                    title: "Tanggal Mulai",
                    data: "contract_date"
                },
                {
                    title: "Tanggal Akhir",
                    data: "expired_date"
                },
                {
                  title : "Harga",
                  data : "harga"
                },
            ]
        });
    });
  </script>
@endpush
