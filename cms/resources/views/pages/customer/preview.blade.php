@extends('layouts.cms')

@section('title', 'Report')

@section('sidebar')
    @include('sidebar.customer') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="row">
              <div class="panel-title" style="padding: 10px 10px;">Report</div>
                  <div class="pull-right">
                    {{--<a class="btn btn-success btn-cons" href="{{route('customerpdf',array('verify' => 'all'))}}">PDF</a>--}}
                    <a class="btn btn-success btn-cons" href="{{route('reportcontractexcel',array('verify' => 'all'))}}">Excel</a>
                  </div>
                <div class="clearfix"></div>
            </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            @include('includes.customer-report-preview')
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

    <script>
      function accept() {
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#1cbf0d",
          confirmButtonText: "Yes, accept it!",
          cancelButtonText: "No, cancel it!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            swal("Accpeted!", "Vendor has been Accpeted.", "success");
          } else {
            swal("Cancelled", "Vendor has been Cancelled :)", "error");
          }
        });
      }

      function decline() {
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
          } else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
      }
    </script>
@endpush
