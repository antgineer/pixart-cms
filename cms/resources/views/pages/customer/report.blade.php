@extends('layouts.cms')

@section('title', 'Report')

@section('sidebar')
    @include('sidebar.customer') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('script')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="panel-title" style="padding: 10px 10px;">Report</div>
            <div class="row">
                  <div class="col-xs-6" style="padding-left: 15px;">
                    <a href="{{route('customer-preview',array('verify'=>'all'))}}" class="btn btn-complete btn-animated from-top fa fa-print" id="btn-cetak">
                      <span class="fa fa-print"></span>
                    </a>
                  </div>
                <div class="clearfix"></div>
            </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch">

            </table>
          </div> {{-- END PANEL BODY --}}
        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

    <!-- Awal Modal -->
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
      <div class="modal-dialog ">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <h5>Pilih Tanggal Report</h5>
            </div>
            <div class="modal-body">
              <form role="form">
                <div class="form-group-attached">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Tanggal Awal</label>
                        <input type="text" class="form-control" placeholder="12-Januari-2000">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group form-group-default">
                        <label>Tanggal Akhir</label>
                        <input type="text" class="form-control" placeholder="12-Januari-2002">
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="row">
                <div class="col-sm-4 m-t-10 sm-m-t-10">
                  <a href="/customer-preview" class="btn btn-primary btn-block m-t-5">Pilih</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- Akhir Modal -->
@endsection

@push('script')
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

    <script>
    $('document').ready(function(){
        'use strict';

        let id_customer = "{{ session('id') }}";

        var table = $('#tableWithSearch').DataTable({
            "ajax":{
                type : "GET",
                url : `/customer-occupied/customer/${id_customer}`,
            },
            "columns": [
                {
                    title : '#',
                    data : "index"
                },
                {
                  title : "Kode Billboard",
                  data : "kode"
                },
                {
                  title : "Lokasi",
                  data : "address"
                },
                {
                    title: "Tanggal Mulai",
                    data: "contract_date"
                },
                {
                    title: "Tanggal Akhir",
                    data: "expired_date"
                },
                {
                    title: "Status",
                    data: null,
                    render: function(data){
                        let action = '';
                        action = (data.sisa_hari > 0 ) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Expired</span>';
                        return action.replace();
                    }
                },
                {
                    title: "Action",
                    data: null,
                    render: function(data){
                        let action = '';
                        action = `<a href="{{route('customer-report','')}}/${data.id_contract}" class="btn btn-complete btn-animated from-top fa fa-print" id="btn-cetak"><span class="fa fa-print"></span>`;
                        return action.replace();
                    }
                }
            ]
        });
    });
    </script>
@endpush
