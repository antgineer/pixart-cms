@extends('layouts.cms')

@section('title','Search By Map')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@section('content')

  <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <!-- START MAP -->
          <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 15px;">
              <div class="panel-body" style="padding-left: 0px;
              padding-right: 0px; padding-bottom: 0px; padding-top:1px">
                  <div id="map" style="width: 100%; height: 650px;"></div>
              </div>
          </div>
      <!-- END MAP -->
      </div>
  </div>
            
@endsection

@push('script')
    <!-- Google Maps JS API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
    {{-- GMaps Library --}}
    <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function (){
            'use strict'

            /* Map Object Reseerved */
            var mapObj = new GMaps({
                el: '#map',
                lat: -6.205202,
                lng: 106.846466,
                zoom: 8
            });

            GMaps.on('click', mapObj.map, function(event) {
                mapObj.removeMarkers(mapObj.markers);
                mapObj.removePolygons();

                let center = {
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng()
                }

                mapObj.drawCircle({
                    //map: map,
                    center: center,
                    radius: 50000, //satuan meter
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.5,
                    strokeWeight: 1,
                    fillColor: "#0000FF",
                    fillOpacity: 0.1
                });

                let data = {
                    latitude: event.latLng.lat(),
                    longitude: event.latLng.lng()
                }

                $.get(`{{route('contract-search-map')}}`, data, function(result){
                    if(result.data.length > 0){
                        $.each(result.data, function(key, val){
                            //let status = (value.remaining == 'Expired' || value.remaining == null) ? '<span class="label label-info">Available</span>' : '<span class="label label-warning">Occupied</span>';

                            mapObj.addMarker({
                                lat: val.billboard.latitude,
                                lng: val.billboard.longitude,
                                title: val.billboard.kode_billboard,
                                infoWindow: {
                                    content: `Kode Billboard  : ${val.billboard.kode_billboard}<br>
                                                Alamat        : ${val.billboard.address}<br>
                                                Status        : ${val.contract_status}<br>
                                                <a href="{{route('contract-details','')}}/${val.id}" target="blank">
                                                <i class="fa fs-16 fa-paper-plane text-info m-r-10"></i>
                                                <span class="text-info">Show more</span>
                                                </a>`
                                }
                            });
                        });
                    }
                });
            });
        });
    </script>
@endpush
