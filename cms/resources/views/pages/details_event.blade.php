@extends('layouts.cms')

@section('title', 'Details Event')

@push('style')
  <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker3.min.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{asset('css/next-prev.css')}}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/remove-image/remove-image.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('css/ticket.css')}}" rel="stylesheet" type="text/css" />
  <style>
      img.img-responsive {
      min-width: 118px;
      min-height: 118px;
    }

    .thumbnail.active {
      border-color: blue;
    }

    .btn-paginate{
        background-color: #4CAF50 !important;
        border-color: #4CAF50 !important;
        margin-top: 60px;
    }
  </style>
@endpush

@section('sidebar')
  @include('sidebar.eo') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@section('content')
  <!-- START BREADCRUMB -->
    <ul class="breadcrumb" style="padding-bottom: 0px;">
      <li>
        <a href="{{route('event')}}" style="margin-left: 0px; padding-left: 0px; padding-right: 0px;">Event</a>
      </li>
      <li><a href="#" class="active" style="padding-left: 0px;">Details Event</a></li>
      <div class="clearfix"></div>
    </ul>
  <!-- END BREADCRUMB -->

  {{-- @php
      $expDate = strtotime($expired_date);
      $nowDate = strtotime(date('Y-m-d'));
  @endphp --}}

  {{-- START BUTTON TOP --}}
  <div class="row" style="padding-top: 5px;">
    <div class="pull-left">
        <div class="col-xs-12" style="padding-left: 5px;">
          @if(session('role_name') == 'eo')
              @if($ticket)
              <a class="btn btn-success replyFeedback" id="btn-replyFeedback" data-target="#replyFeedback" data-toggle="modal" href="#replyFeedback">
                  <span>Reply Feedback</span>
              </a>
              @endif
          @else
              @if(!$ticket)
              <a class="btn btn-complete openFeedback" id="btn-openFeedback" data-target="#openFeedback" data-toggle="modal" href="#openFeedback">
                  <span>Feedback</span>
              </a>
              @else
              <a class="btn btn-success replyFeedback" id="btn-replyFeedback" data-target="#replyFeedback" data-toggle="modal" href="#replyFeedback">
                  <span>Reply Feedback</span>
              </a>
              @endif
          @endif
          <a class="btn btn-success" href="{{route('report.event.single', $id)}}">View Report</a>
        </div>
    </div>

    {{-- START STATUS FEEDBACK --}}
    <div class="pull-right">
      <p style="margin-right: 5px;"><strong>Status Feedback :
        @if(!$ticket)
         </strong><span class="badge badge-success">Closed</span></p>
        @elseif($ticket[0]['status']=="1")
         </strong><span class="badge badge-danger">Open</span></p>
        @else
        </strong><span class="badge badge-warning">Replied</span></p>
        @endif
    </div>
    {{-- END STATUS FEEDBACK --}}
    <div class="clearfix"></div><br>
  </div>
  {{-- END BUTTON TOP --}}

  <div class="row">
    {{-- START DETAIL EVENT --}}
    <div id="detail-event" class="panel-group col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
        <div class="row">
          <div class="col-xs-6 col-md-6 col-lg-12 m-b-10">
              <div class="card share full-width" style="padding-bottom: 15px;">
                <div class="card-header clearfix">
                  <div class="row" style="margin-right: 5px; margin-left: 5px;">
                    <span style="padding-top: 5px; font-size: 20px;">Detail Event</span>
                    <button id="edit-detail-event" class="pull-right btn btn-primary btn-animated from-top fa fa-pencil btn-edit" type="button" title="Edit Event" data-toggle="modal" href="#modal-edit-event" data-id="{{$id}}">
                        <span class="fa fa-pencil"></span>
                    </button>
                  </div>
                </div>
                <div class="card-description" style="padding-bottom: 0px;">
                  <div class="panel panel-info" style="margin-bottom: 0px;">
                    <div class="panel-body" style="padding-bottom: 10px; padding-top: 10px;">
                      <table class='table table-user-information'>
                        <tbody>
                          <tr>
                            <td>Nama EO</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{$user["profile"]["fullname"]}}</strong></td>
                          </tr>
                          <tr>
                            <td>Nama Event</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{$nama_event}}</strong></td>
                          </tr>
                          <tr>
                            <td>Lokasi Event</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{$address}}</strong></td>
                          </tr>
                          <tr>
                            <td>Tanggal Mulai Event</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{date("d-m-Y", strtotime($event_date))}}</strong></td>
                          </tr>
                          <tr>
                            <td>Tanggal Selesai Event</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{date("d-m-Y", strtotime($expired_date))}}</strong></td>
                          </tr>
                          <tr>
                            <td>Jumlah Pengunjung</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{$jumlah_pengunjung}}</strong></td>
                          </tr>
                          <tr>
                            <td>Keterangan</td>
                            <td class="col-sm-1">:</td>
                            <td><strong>{{$note}}</strong></td>
                          </tr>
                          <tr>
                            <td>Status Event</td>
                            <td class="col-sm-1">:</td>
                            <td><span class="label {{$status == '1' ? 'label-success' : 'label-danger'}}">{{$status == '1' ? 'Sedang Berjalan' : 'Telah Berakhir'}}</span></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
    {{-- END DETAIL EVENT --}}

    {{-- START FOTO EVENT --}}
    <div id="detail-event" class="panel-group col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
        <div class="row">
          <div class="col-xs-6 col-md-6 col-lg-12 m-b-10">
              <div class="card share full-width" style="padding-bottom: 15px;">
                <div class="card-header clearfix" style="padding-top: 20px; padding-bottom: 20px;">
                  <div class="row" style="margin-right: 5px; margin-left: 5px;">
                    <span style="padding-top: 5px; font-size: 20px;">Foto Event</span>
                  </div>
                </div>
                <div class="card-description" style="padding-bottom: 0px;">
                  <div class="panel panel-info" style="margin-bottom: 0px;">
                    <div class="panel-body" style="padding-bottom: 15px; padding-top: 15px; padding-left: 0px; padding-right: 0px;">
                      <div id="myCarouselFoto" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              @foreach($photo as $key => $foto)
                                  <li data-target="#myCarouselFoto" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active' : '' }}"></li>
                              @endforeach
                            </ol>

                        {{-- FOTO --}}
                        <div class="col-sm-12">
                          <div class="card share full-width" style="margin-bottom: 0px; padding-bottom: 0px;">
                            {{-- <div class="card-header clearfix">
                              <h5 class="text-center text-uppercase"><b>Foto Event</b></h5>
                            </div> --}}
                            <div id="myCarouselFoto" class="carousel slide" data-ride="carousel">
                                  <!-- Indicators -->
                                  <ol class="carousel-indicators">
                                    @foreach($photo as $key => $foto)
                                        <li data-target="#myCarouselFoto" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active' : '' }}"></li>
                                    @endforeach
                                  </ol>

                                  <!-- Wrapper for slides -->
                                  <div class="carousel-inner">
                                    @if($photo)
                                        @foreach($photo as $key => $foto)
                                        <div class="item {{ ($key == 0) ? 'active' : '' }}">
                                            <img class="img-responsive" style="height: 492px; width: 1000px;" src="{{ route('download', $foto['photo_name']) }}" alt="Foto Event">
                                        </div>
                                        @endforeach
                                    @else
                                        <div class="item active">
                                            <img class="img-responsive" style="height: 492px; width: 700px;" src="{{ asset('assets/img/gallery/no-pic.png') }}" alt="Foto Event">
                                        </div>
                                    @endif
                                  </div>

                                  <!-- Left and right controls -->
                                  <a class="left carousel-control" href="#myCarouselFoto" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="right carousel-control" href="#myCarouselFoto" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                            </div>
                          </div>
                        </div>
                        {{-- END FOTO --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
    {{-- END FOTO EVENT --}}

  {{--start modal edit event--}}
  <div class="modal fade" id="modal-edit-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Ubah Event</h2><br>
            </div>
            <div class="modal-body" style="padding-bottom: 0px;">
              <form id="form-edit" role="form" autocomplete="off" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group-attached">
                <input type="hidden" name="id_edit" id="id_edit_event" value="{{$id}}">
                @if( session('role_name') == 'sampoerna' )
                  <div class="row clearfix">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Nama Event</label>
                        <input type="text" class="form-control" id="nama_event_edit" name="nama_event_edit" style="padding-top: 5px; height: 40px;">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Lokasi Event</label>
                        <input type="text" class="form-control" id="address_edit" name="address_edit" style="padding-top: 5px; height: 40px;">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Mulai Event</label>
                            <input id="datepicker" class="datepicker-awal-edit form-control" type="text" name="event_date_edit" style="padding-top: 5px; height: 40px;">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Selesai Event</label>
                            <input id="datepicker" class="datepicker-akhir-edit form-control" type="text" name="expired_date_edit" style="padding-top: 5px; height: 40px;">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Status Event</label>
                        <select class="full-width" data-init-plugin="select2" name="status_edit" id="status_edit">
                          <option value="">Pilih Status</option>
                          <option value="1">Sedang Berjalan</option>
                          <option value="0">Telah Berakhir</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  @endif

                  @if( session('role_name') == 'eo' )
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Jumlah Pengunjung</label>
                        <input type="text" class="form-control" id="jumlah_pengunjung_edit" name="jumlah_pengunjung_edit" style="padding-top: 5px; height: 40px;">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Keterangan</label>
                        <textarea class="form-control" id="note_edit" name="note_edit" style="padding-top: 5px; height: 70px; color: black;"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Photos</label>
                        <input name="foto_edit[]" id="foto_edit" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg", "jpeg"]' style="padding-top: 5px; height: 40px;">
                        <hr style="margin-bottom: 0px; margin-top: 10px;">
                        <div id="detail-event-box-photo">

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Videos</label>
                        <input name="video_edit" id="video_edit" type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["3gp", "mp4", "avi", "mpg", "mkv", "mov", "webm"]' style="padding-top: 5px; height: 40px;">
                        <hr style="margin-bottom: 0px; margin-top: 10px;">
                        <div id="detail-event-box-video">

                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  {{--  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group form-group-default">
                        <label>Videos</label>
                        <input name="video_edit" id="video_edit" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["mp4", "avi", "mpg", "mkv", "mov", "webm"]' style="padding-top: 5px; height: 40px;">
                        <hr style="margin-bottom: 0px; margin-top: 10px;">
                        <output id="list">
                          <h4 style="margin-top: 0px; margin-bottom: 0px;">Current Videos</h4>
                                <span class="file-preview-wrapper">
                                    <div class="file-preview-frame krajee-default kv-preview-thumb" style="padding-right: 10px; padding-left: 10px; margin-left: 0px;">
                                      <div class="kv-file-content">
                                        <video class="thumb_vid kv-preview-data file-preview-video" controls style="padding-left: 13px;">
                                          <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" type="video/mp4">
                                        </video>
                                        <span class="remove_video_preview"></span>
                                      </div>
                                    </div>
                                </span>
                        </output>
                      </div>
                    </div>
                  </div>  --}}
                </div>
              </form>
            </div><br>
            <div class="modal-footer" style="padding-right: 20px;">
              <button type="button" class="btn btn-success" id="button-update">Update</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
  </div>
  {{--end modal edit event--}}

  {{-- START MODAL OPEN FEEDBACK --}}
  <div class="modal fade" id="openFeedback">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Open Feedback</h2>
              </div>
              <form action="{{ route('open.feedback.event', $id) }}" method="POST">
              <div class="modal-body" style="padding-bottom: 0px;">
                  <div class="form-horizontal" role="form" id="form-decline">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label>Keluhan</label>
                                <input type="hidden" id="id_pemasangan">
                                <textarea class="form-control" name="keluhan" id="keluhan" required style="height: 300px; "></textarea>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button class="btn btn-success btn-animated from-top fa fa fa-check" type="submit" id="btn-decline-submit">
                  <span>Submit</span>
                </button>
                <button type="button" class="btn btn-default btn-animated from-top fa fa fa-times" data-dismiss="modal">
                  <span>Cancel</span>
                </button>
              </div>
              </form>
          </div>
      </div>
  </div>
  {{-- END MODAL OPEN TICKET --}}

    @if($ticket)
    {{-- START MODAL REPLY TICKET --}}
    <div class="modal fade" id="replyFeedback">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Reply Feedback</h2>
                </div>
                <div class="modal-body" style="padding-bottom: 10px;">
                    <form action="{{ route('reply.feedback.event', $ticket[0]['id']) }}" method="POST" class="form-horizontal" role="form" id="form-decline">
                        {{ csrf_field() }}
                        <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" style="padding-bottom: 0px;">
                                    <label>Keluhan</label>
                                    <input type="hidden" id="id_pemasangan">
                                    <p align="justify" style="font-size: 15px;">{{ $ticket[0]['title'] }}</p>
                                </div>
                                <div class="panel-group">
                                    <div class="panel panel-primary" style="margin-top: 10px; width: 530px; right: -10; right: 20px; border-color: #48b0f7;">
                                        <div class="panel-heading" style="background-color: #48b0f7; padding-top: 10px; padding-bottom: 0px;">
                                        <div class="row">
                                            <p class="col-md-4 col-sm-3 col-xs-3" style="font-size: 20px; padding-top: 5px;">Reply Feedback</p>
                                            <div class="btn-group pull-right">
                                            @if(session('role_name') == 'sampoerna')
                                                <a style="margin-bottom: 10px; margin-right: 5px;" href="{{route('close.feedback.event', $ticket[0]['id'])}}" class="pull-right btn btn-info">
                                                <span>Close Feedback</span>
                                                </a>
                                            @endif
                                            </div>
                                        </div>
                                        </div>
                                        <div class="panel-body" style="padding-top: 20px;">
                                            <ul class="chat">
                                                @foreach($ticket[0]['ticket_content'] as $reply)
                                                    @if($reply['id_user']==$user['id'])

                                                    <li class="left clearfix">
                                                    <span class="chat-img pull-left">
                                                        <img width="50" height="50" src="{{route('download', $reply['user']['photo'])}}" alt="User Avatar" class="img-circle">
                                                    </span>
                                                    <div class="chat-body clearfix">
                                                        <div class="row" style="height: 20px; margin-left: 0px;">
                                                            <strong class="primary-font">{{ $reply['user']['profile']['fullname'] }}</strong>
                                                        </div>
                                                        <p>
                                                            {{ $reply['content']}}
                                                        </p>
                                                    </div>
                                                    </li>
                                                    @else
                                                    <li class="right clearfix">
                                                    <span class="chat-img pull-right">
                                                        <img width="50" height="50" src="{{route('download', $reply['user']['photo'])}}" alt="User Avatar" class="img-circle">
                                                    </span>
                                                    <div class="chat-body clearfix" style="text-align:right">
                                                        <div class="row" style="height: 20px; margin-right: 0px;">
                                                        <strong class="primary-font">{{ $reply['user']['profile']['fullname'] }}</strong>
                                                        </div>
                                                        <p>
                                                            {{ $reply['content']}}
                                                        </p>
                                                    </div>
                                                    </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            <textarea class="form-control" name="reply_keluhan" id="reply_keluhan" required style="height: 100px;" placeholder="Reply here..."></textarea>
                                            <button class="btn btn-success btn-animated from-top fa fa fa-check" type="submit" id="btn-decline-submit" style="margin-top: 10px;">
                                                <span>Send</span>
                                            </button>
                                        </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- END MODAL REPLY TICKET --}}
    @endif
@endsection


@push('script')
<script src="{{asset('assets/plugins/new-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/new-datepicker/locales/bootstrap-datepicker.id.min.js')}}"></script>

{{-- FILE UPLOAD --}}
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/locales/id.js')}}"></script>
<script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>
<script>
  $(document).ready(function(){
      'use strict';

      var datepicker_awal=$('.datepicker-awal').datepicker({
            format: "yyyy-mm-dd",
            clearBtn: true,
            language: "id",
            autoclose: true,
            todayHighlight: true
        });

      $('.datepicker-awal').change(function() {
        var start_date_end = $(this).val();

        $('.datepicker-akhir').datepicker({
            startDate: start_date_end,
            format: "yyyy-mm-dd",
            clearBtn: true,
            language: "id",
            autoclose: true
        });
      });

      var datepicker_awal_edit=$('.datepicker-awal-edit').datepicker({
            format: "yyyy-mm-dd",
            clearBtn: true,
            language: "id",
            autoclose: true,
            todayHighlight: true
        });

      $('.datepicker-awal-edit').change(function() {
        var start_date_end_edit = $(this).val();

        $('.datepicker-akhir-edit').datepicker({
            startDate: start_date_end_edit,
            format: "yyyy-mm-dd",
            clearBtn: true,
            language: "id",
            autoclose: true
        });
      });

      $('#edit-detail-event').click(function(){
          let id = $(this).data('id');
          //let id = 13;
          $('.form-control').val('');
          $('#detail-event-box-photo').empty();
          $('#detail-event-box-video').empty();

          $.get(route('detail-event-single', id), function(data) {
              let event = data.data;

              //reformat tanggal
              let date1 = event.event_date.split(' ');
              let date2 = event.expired_date.split(' ');
              let eventDate = date1[0];
              let expiredDate = date2[0];
              // let eventDate = `${date3[2]}/${date3[1]}/${date3[0]}`;
              // let expiredDate = `${date4[2]}/${date4[1]}/${date4[0]}`;
              $('input[name="event_date_edit"]').val(eventDate);
              $('input[name="expired_date_edit"]').val(expiredDate);

              $('#id_edit').val(event.id);
              $('#nama_event_edit').val(event.nama_event);
              $('#address_edit').val(event.address);
              $('#note_edit').val(event.note);
              $('#jumlah_pengunjung_edit').val(event.jumlah_pengunjung);
              $('#status_edit').select2('val', event.status);

              //show foto
              let elem;
              if(event.photo.length > 0){
                  elem = $(`<output id="list">
                      <h4 style="margin-top: 0px; margin-bottom: 0px;">Current Photos</h4>
                  </output>`);

                  //let list = ``;
                  $.each(event.photo, (key, foto) => {
                      let image = route('download', foto.photo_name);

                      let list = `<span class="file-preview-wrapper">
                          <div class="file-preview-frame krajee-default kv-preview-thumb" style="padding-right: 10px; padding-left: 10px; margin-left: 0px;">
                          <div class="kv-file-content">
                              <img class="thumb file-preview-image kv-preview-data" src="${image}">
                              <span class="remove_img_preview" data-id="${foto.id}"></span>
                          </div>
                          </div>
                      </span>`;

                      elem.append(list);
                  });
              }
              else{
                  elem = `<div>
                      <h4>Billboard belum memiliki foto</h4>
                  </div>`;
              }
              $('#detail-event-box-photo').append(elem);

              // show video
              let elemvid;
              if(event.video){
                  elemvid = $(`<output id="list">
                      <h4 style="margin-top: 0px; margin-bottom: 0px;">Current Videos</h4>
                  </output>`);


                  $.each(event.video, (key, video) => {
                      let image = route('download', video.video_name);

                      let list = `<span class="file-preview-wrapper">
                          <div class="file-preview-frame krajee-default kv-preview-thumb" style="padding-right: 10px; padding-left: 10px; margin-left: 0px;">
                            <div class="kv-file-content">
                              <video class="thumb_vid kv-preview-data file-preview-video" controls style="padding-left: 13px;">
                                <source src="${video}">
                              </video>
                              <span class="remove_video_preview" data-id="${video.id}"></span>
                            </div>
                          </div>
                      </span>`;

                      elemvid.append(list);
                  });
              }
              else{
                  elemvid = `<div>
                      <h4>Event ini belum memiliki video</h4>
                  </div>`;
              }
              $('#detail-event-box-video').append(elemvid);
          });
      });

      $('#button-update').click(function(){
          let id = $('#id_edit_event').val();

          swal({
              title: "Apakah Anda Yakin ?",
              text: "Pastikan Data Detail Event Sudah Diisi Dengan Benar",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#00C853",
              confirmButtonText: "Ya, Yakin !",
              cancelButtonText: "Tidak, Batalkan !",
              closeOnConfirm: false,
              closeOnCancel: false,
              showLoaderOnConfirm: true
              },
              function(isConfirm){
              if (isConfirm) {
                  let role_name = `{{ session('role_name') }}`;
                  let data = new FormData();
                  let formData = $('#form-edit').serializeArray();
                  let files = null;
                  let video = null;

                  if( role_name == 'eo' ){
                      files = $('#foto_edit')[0].files;
                      video = $('#video_edit')[0].files[0];
                  }

                  let url = (role_name == 'sampoerna') ? route('update-event', id) : route('update-event-eo', id);

                  $.each(formData, function(key, value){
                      let keyname = value.name;
                      let name = keyname.replace('_edit', '');
                      data.append(name, value.value);
                  });

                  if( role_name == 'eo' ){
                      $.each(files, function(key, value){
                          data.append('file[]', value);
                      });

                      data.append('video', video);
                  }

                  $.ajax({
                      url         : url,
                      type        : "POST",
                      data        : data,
                      cache 		  : false,
                      contentType : false,
                      processData : false,
                      success : function(data, status){
                          if(status=="success"){
                              setTimeout(function(){
                                  swal({
                                      title: "Sukses",
                                      text: "Data Detail Event Berhasil Diubah!",
                                      type: "success"
                                      },
                                      function(){
                                          location.reload();
                                      });
                                  }, 1000);
                          }
                          $('#form-edit').modal('hide');
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                          setTimeout(function(){
                              swal("Gagal", "Data Detail Event Gagal Diubah", "error");
                          }, 1000);
                      }
                  });
              } else {
                  swal("Dibatalkan", "Data Detail Event Batal Diubah :)", "error");
              }
          });
      });

      $('#detail-event-box-photo').on('click', ".remove_img_preview", function(){
            let id = $(this).data('id');
            let boxPhoto = $(this).parents('.file-preview-wrapper');

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Foto Akan Dihapus Permanen !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url  : route('file-delete-photo', id),
                            type : "DELETE",
                            data : {
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(data, status){
                                boxPhoto.remove();

                                if(status=="success"){
                                    setTimeout(function(){
                                        swal({
                                            title: "Sukses",
                                            text: "Foto Billboard Berhasil Dihapus!",
                                            type: "success"
                                        });
                                    }, 1000);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                setTimeout(function(){
                                    swal("Gagal", "Data Billboard Gagal Diubah", "error");
                                }, 1000);
                            }
                        });
                    }
                }
            );
        });

  });
</script>
@endpush
