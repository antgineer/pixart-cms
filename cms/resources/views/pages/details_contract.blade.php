@extends('layouts.cms')

@section('title', 'Details Contract')

@push('style')
  <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/new-datepicker/css/bootstrap-datepicker3.min.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{asset('css/next-prev.css')}}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/remove-image/remove-image.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('css/ticket.css')}}" rel="stylesheet" type="text/css" />
  <style>
      img.img-responsive {
      min-width: 118px;
      min-height: 118px;
    }

    .thumbnail.active {
      border-color: blue;
    }

    .btn-paginate{
        background-color: #4CAF50 !important;
        border-color: #4CAF50 !important;
        margin-top: 60px;
    }
  </style>
@endpush

@section('sidebar')
  @php $role = session('role_name') @endphp
  @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@section('content')
  <!-- START BREADCRUMB -->
    <ul class="breadcrumb" style="padding-bottom: 0px;">
      <li>
        <a href="{{route('contract')}}" style="margin-left: 0px; padding-left: 0px; padding-right: 0px;">Contract</a>
      </li>
      <li><a href="#" class="active" style="padding-left: 0px;">Details Contract</a></li>
      <div class="clearfix"></div>
    </ul>
  <!-- END BREADCRUMB -->

    @php
        $expDate = strtotime($expired_date);
        $nowDate = strtotime(date('Y-m-d'));
    @endphp

  {{-- START BUTTON TOP --}}
  <div class="row" style="padding-top: 5px;">
    <div class="pull-left">
        <div class="col-xs-12" style="padding-left: 5px;">
          @if(session('role_name') == 'vendor')
              @if($expDate >= $nowDate)
                  <a class="btn btn-primary btn-animated from-top pg pg-plus" id="btn-add" data-target="#add-details" data-toggle="modal" href="#add-details">
                      <span>Add Installation</span>
                  </a>
              @endif

              @if(!empty($detail_contract[0]['ticket']))
              <a class="btn btn-success replyFeedback" id="btn-replyFeedback" data-target="#replyFeedback" data-toggle="modal" href="#replyFeedback">
                  <span>Reply Feedback</span>
              </a>
              @endif
          @else
              @if(empty($detail_contract[0]['ticket']))
              <a class="btn btn-complete openFeedback" id="btn-openFeedback" data-target="#openFeedback" data-toggle="modal" href="#openFeedback">
                  <span>Feedback</span>
              </a>
              @else
              <a class="btn btn-success replyFeedback" id="btn-replyFeedback" data-target="#replyFeedback" data-toggle="modal" href="#replyFeedback">
                  <span>Reply Feedback</span>
              </a>
              @endif
          @endif
          <a class="btn btn-success" href="{{ route('contract.preview.one', $id) }}">View Report</a>
        </div>
    </div>

    {{-- START STATUS FEEDBACK --}}
    <div class="pull-right">
      <p style="margin-right: 5px;"><strong>Status Feedback :
        @if(empty($detail_contract[0]['ticket']))
         </strong><span class="badge badge-success">Closed</span></p>
        @elseif($detail_contract[0]['ticket'][0]['status']=="1")
         </strong><span class="badge badge-danger">Open</span></p>
        @else
        </strong><span class="badge badge-warning">Replied</span></p>
        @endif
    </div>
    {{-- END STATUS FEEDBACK --}}
    <div class="clearfix"></div><br>
  </div>
  {{-- END BUTTON TOP --}}

  <div class="row">
    {{-- START PREVIEW IMAGE --}}
    @if($detail_contract != null)
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <div class="panel panel-default">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
          @foreach($detail_contract[0]['photo'] as $key => $photo)
            <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active' : '' }}"></li>
          @endforeach
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
          @if($detail_contract[0]['photo'])
              @foreach($detail_contract[0]['photo'] as $key => $photo)
              <div class="item {{ ($key == 0) ? 'active' : '' }}" style="height: 230px;">
                  <img src="{{ route('download', $photo['photo_name']) }}" alt="Foto Detail Pemasangan Billboard">
              </div>
              @endforeach
          @else
              <div class="item active" style="height: 230px;">
                  <img src="{{ asset('assets/img/gallery/no-pic.png') }}" alt="Foto Detail Pemasangan Billboard">
              </div>
          @endif
          </div>
        </div>
      </div>
    </div>
    {{-- END PREVIEW IMAGE --}}

    {{-- START DETAIL INSTALLATION --}}
    <div class="panel-group col-xs-12 col-sm-6 col-md-6 col-lg-4" style="padding-top: 0px; padding-bottom: 0px;">
      <div class="row">
        <div class="col-xs-6 col-md-6 col-lg-12 m-b-10" style="margin-bottom: 15px;">
          <div class="panel panel-primary" style="border-color : #48b0f7">
            <div class="panel-heading" style="background-color: #48b0f7;">
              <div class="row">
                <span style="padding-top: 5px; font-size: 20px;">Details Installation</span>

                <div class="pull-right edit-delete-box">
                  @if(session('role_name') == 'vendor' && $expDate >= $nowDate && $detail_contract[0]['status'] == 1)
                    <a class="btn btn-warning btn-animated from-top fa fa-pencil" id="btn-edit" data-target="#edit-details" data-toggle="modal" data-id="{{$detail_contract[0]['id']}}" href="#edit-details">
                      <span><b>Edit</b></span>
                    </a>
                    {{-- @elseif($detail_contract[0]['status']==1)
                      <div class="btn btn-success">Active</div> --}}
                  @endif
                </div>
              <div class="clearfix"></div>
              </div>
            </div>
            <div class="panel-body" style="padding-bottom: 10px; padding-top: 10px;">
              <p style="font-size: 15px;">Tema Kontrak : <strong id="detail-tema"> {{ $detail_contract[0]['tema_iklan'] }} </strong></p>
              <p style="font-size: 15px;">Brand Kontrak : <strong id="detail-brand"> {{ $detail_contract[0]['brand'] }} </strong></p>
              <p style="font-size: 15px;">Tanggal Pemasangan Visual :<strong id="detail-tglpasang"> {{ $detail_contract[0]['created_date'] }} </strong></p>
              <p style="font-size: 15px;">Tanggal Expired Visual : <strong id="detail-tglexpired"> {{ $detail_contract[0]['expired_date'] }} </strong></p>
              {{-- Masih Bug --}}
              <p style="font-size: 15px;">Status Installation : <strong id="status-installation"> {!! $detail_contract[0]['status'] == '1' ? '<span data-stat="aktif" class="label label-complete text-white" style="background-color : #10cfbd; border-color : #10cfbd">Aktif</span>' : '<span class="label label-danger">Tidak Aktif</span>' !!} </strong></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- END DETAIL INSTALLATION --}}

    {{-- START DETAIL CONTRACT --}}
    <div class="panel-group col-xs-12 col-sm-6 col-md-6 col-lg-3" style="padding-top: 0px; padding-bottom: 0px;">
      <div class="row">
        <div class="col-xs-6 col-md-6 col-lg-12 m-b-10" style="margin-bottom: 15px;">
          <div class="panel panel-primary" style="border-color : #48b0f7">
            <div class="panel-heading" style="background-color: #48b0f7;">
              <div class="row">
                <span style="padding-top: 5px; font-size: 20px;">Details Contract</span>
                @if(session('role_name') == 'vendor')
                  <div class="pull-right">
                    @if($expDate >= $nowDate)
                    <a class="btn btn-warning btn-animated from-top fa fa-pencil" id="btn-edit-contract" data-target="#modal-edit-contract" data-toggle="modal" data-id="{{$id}}" href="#edit-contract" style="margin-right: 0px;">
                      <span class="text-white"><b>Edit</b></span>
                    </a>
                    @endif
                  </div>
                @endif
              </div>
            </div>
            <div class="panel-body" style="padding-bottom: 20px; padding-top: 10px;">
              <p style="font-size: 15px;">Kode Billboard : <strong>{{ $billboard['kode_billboard'] }}</strong></p>
              <p style="font-size: 15px;">Nama Customer : <strong>{{ ($customer != null) ? $customer['profile']['fullname'] : $customer_name }}</strong></p>
              <p style="font-size: 15px;">Tgl Awal Kontrak : <strong>{{ $contract_date }}</strong></p>
              <p style="font-size: 15px;">Tgl Akhir Kontrak : <strong>{{ $expired_date }}</strong></p><br>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- END DETAIL CONTRACT --}}
  </div>

  {{-- START THUMBNAIL --}}
  <div class="row detail-contract-box">
    {{-- AWAL TOMBOL PREVIOUS --}}
      <div class="col-sm-2 col-md-1">
      <ul class="pager">
        <li class="previous paginate-left">
            <a href="#" id="paginate-newer" class="btn btn-paginate paginate text-white disabled">Previous</a>
        </li>
      </ul>
    </div>
    {{-- AKKHIR TOMBOL PREVIOUS --}}

    {{-- AWAL LIST THUMBNAIL INSTALLATION --}}
      @php
          $sliceStart = 5;
          $slicePage = 0;
      @endphp

      {{--  Jangan suka hapus data-attribute sembarangan!,
            Dipake di JS datanya, bs malfungsi nanti. --}}
      <div class="installation" data-active-page="0">
      @foreach($detail_contract as $key => $detail)
          @php
              if($key == $sliceStart){
                  $sliceStart+=5;
                  $slicePage++;
              }
          @endphp
          <div class="col-sm-2 col-md-2 thumbnail-box">
              <div class="thumbnail {{$key == 0 ? 'active' : ''}}" data-thumbnail="{{$detail['id']}}" data-slice="{{$slicePage}}">
              <a href="#" data-id="{{$detail['id']}}" class="paginate-image">
              @php
                  $foto = $detail['photo'] ? route('download', $detail['photo'][0]['photo_name']) : asset('assets/img/gallery/no-pic.png');
              @endphp
                  <img src="{{$foto}}" alt="Lights" style="width:100%" class="img-responsive">
                  <div class="caption">
                  <p style="margin-bottom: 0px;">Tgl Pemasangan : <br> <b>{{$detail['created_date']}}</b></p>
                  </div>
              </a>
              </div>
          </div>
      @endforeach
      </div>
    {{-- AKHIR LIST THUMBNAIL INSTALLATION --}}

    {{-- AWAL TOMBOL NEXT --}}
    <div class="col-sm-1 col-md-1">
      <ul class="pager">
        <li class="next paginate-right">
            @php $classNext =  $detail_contract[0]['previous_id'] == null ? 'disabled' : ''; @endphp
            <a href="#" id="paginate-older" class="btn btn-paginate text-white paginate {{$classNext}}" data-id="{{ $detail_contract[0]['previous_id'] }}" style="width: 80px;">Next</a>
        </li>
      </ul>
    </div>
    {{-- AKHIR TOMBOL NEXT --}}

  </div>
  {{-- END THUMBNAIL --}}

{{-- ========================================================================================= --}}

    @else
        @if(session('role_name') == 'vendor')
        <div class="col-sm-12">
            <div class="alert alert-info" role="alert">
              <h4 class="alert-heading">Belum Ada Pemasangan</h4>
              <p>Jika Anda mau menambahkan detail kontrak, silahkan klik tombol <b>"Add"</b> yang ada diatas kemudian isi data secara lengkap dan benar.</p>
              <hr>
              <h4 class="alert-heading">No Installation</h4>
              <p class="mb-0">If you want to add contract details, please click the <b> "Add" </b> button above then complete and correct data.</p>
            </div>
        </div>
        @else
          <div class="col-sm-12">
              <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Belum Ada Pemasangan</h4>
                <p>Belum ada installasi dari vendor</p>
              </div>
          </div>
        @endif
    @endif

    <div class="row">
        {{-- START INFO BILLBOARD --}}
        <div class="panel-group col-xs-12 col-sm-5 col-md-5 col-lg-5" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
            <div class="row">
            <div class="col-xs-6 col-md-6 col-lg-12 m-b-10">
                <div class="panel panel-primary" style="border-color : #48b0f7">
                <div class="panel-heading" style="background-color: #48b0f7;">
                    <div class="row">
                    <span style="padding-top: 5px; font-size: 20px;">Info Billboard</span>
                    </div>
                </div>
                <div class="panel-body" style="padding-bottom: 10px; padding-top: 10px;">
                    <p style="font-size: 15px;">Kode Billboard : <strong>{{ $billboard['kode_billboard'] }}</strong></p>
                    <p style="font-size: 15px;">Nama Vendor : <strong>{{ $vendor['profile']['fullname'] }}</strong></p>
                    <p style="font-size: 15px;">Provinsi : <strong> {{ $billboard['provinsi']['nama_provinsi'] }}</strong></p>
                    <p style="font-size: 15px;">Kabupaten : <strong> {{ $billboard['kabupaten']['nama_kabupaten'] }}</strong></p>
                    <p style="font-size: 15px;">Kecamatan : <strong> {{ $billboard['kecamatan']['nama_kecamatan'] }}</strong></p>
                    <p style="font-size: 15px;">Alamat : <strong> {{ $billboard['address'] }}</strong></p>
                    <p style="font-size: 15px;">Lighting : <strong> {{ $billboard['lighting'] }}</strong></p>
                    <p style="font-size: 15px;">Format : <strong> {{ $billboard['format'] == 'v' ? 'Vertical' : 'Horizontal' }}</strong></p>
                    <p style="font-size: 15px;">Panjang x Lebar : <strong> {{ $billboard['panjang'] }}M x {{ $billboard['lebar'] }}M</strong></p>
                    <p style="font-size: 15px;">Bahan Billboard : <strong> {{ $billboard['bahan'] }}</strong></p>
                    <p style="font-size: 15px;">Harga Kontrak : <strong> Rp. {{ number_format($billboard['harga'], 0, ',', '.') }}</strong></p>
                </div>
                </div>
            </div>
            </div>
        </div>
        {{-- END INFO BILLBOARD --}}

        {{-- START MAP --}}
        <div class="col-lg-7 hidden-xlg m-b-10" style="margin-bottom: 0px;">
            <div class="panel panel-transparent" style="border-bottom-width: 0px; margin-bottom: 0px;">
            <div class="panel-body" style="padding-left: 0px;
            padding-right: 0px; padding-bottom: 0px; padding-top:1px">
                <div id="map" style="width: 100%; height: 400px;"></div>
            </div>
            </div>
        </div>
        {{-- END MAP --}}
    </div>

{{-- ========================================================================================= --}}

  {{--start modal edit contract--}}
    <div class="modal fade" id="modal-edit-contract">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Ubah Kontrak</h2><br>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                <form action="{{url('/#')}}" id="form-edit" method="POST" role="form" autocomplete="off">
                <input type="hidden" name="id_edit" id="id_edit" value="{{$id}}" class="form-control" required>
                <input type="hidden" class="form-control" value="{{$id_billboard}}" id="id_billboard_edit" name="id_billboard_edit" required>
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                  <div class="form-group-attached">
                    <div class="row clearfix">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Nama Customer</label>
                          <select class="full-width" data-init-plugin="select2" name="id_customer_edit" id="id_customer_edit" required>
                            <option value="">Pilih Customer</option>
                            <option value="other">Customer Lain-lain</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Nama Customer Lain</label>
                          <input type="text" class="form-control" id="customer_name_edit" name="customer_name_edit" required style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Mulai</label>
                            <input id="datepicker" type="text" class="form-control datepicker-awal" placeholder="Pick a date" name="contract_date_edit" id="datepicker-component2">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group form-group-default input-group col-sm-10">
                            <label>Tanggal Akhir</label>
                            <input id="datepicker" type="text" class="form-control datepicker-akhir" placeholder="Pick a date" name="expired_date_edit" id="datepicker-component2">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div><br>
              <div class="modal-footer" style="padding-right: 20px;">
                <button type="button" class="btn btn-success" id="button-update-contract">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
    </div>
  {{--end modal edit contract--}}

  {{--start modal add detail pemasangan--}}
    <div class="modal fade" id="add-details">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Tambah Detail Pemasangan</h2><br>
              </div>
              <div class="modal-body" style="padding-bottom: 20px;">
                <form action="{{url('/#')}}" id="form-add-detail" method="POST" role="form" autocomplete="off">
                  {{ csrf_field() }}
                  <input type="hidden" value="{{ $id }}" class="form-control text-black" name="id_contract" id="id_contract">
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Tema</label>
                          <input type="text" class="form-control text-black" name="tema_iklan" id="tema_iklan" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default required">
                          <label>Brand</label>
                          <input type="text" class="form-control text-black" name="brand" id="brand" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div id="datepicker-component" class="form-group form-group-default input-group date col-sm-10">
                            <label>Tanggal Expired</label>
                            <input type="text" class="form-control" placeholder="Pick a date" name="expired_date" id="expired_date" style="padding-top: 5px; height: 40px;">

                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- START UPLOADER -->
                        <div class="panel panel-default" style="margin-bottom: 0px;">
                          <div class="form-group form-group-default required" style="border-bottom-width: 0px; margin-bottom: 0px;">
                            <label>Select Photos</label>
                            <input id="file-add" name="file-add[]" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg", "jpeg"]' style="padding-top: 5px; height: 40px;">
                          </div>
                        </div>
                        <!-- END UPLOADER -->
                      </div>
                    </div>
                  </div>
                </form><br>
                <div class="modal-footer" style="padding-right: 0px;">
                  <button type="button" class="btn btn-success" id="button-save">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  {{--end modal add detail pemasangan--}}

  {{--start modal edit detail contract--}}
    <div class="modal fade" id="edit-details">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Ubah Detail Pemasangan</h2><br>
              </div>
              <div class="modal-body" style="padding-bottom: 20px;">
                <form action="{{url('/#')}}" id="form-edit-detail" method="POST" role="form" autocomplete="off">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                  <input type="hidden" class="form-control text-black" name="id_edit" id="id_edit">
                  <input type="hidden" class="form-control text-black" name="id_contract_edit" id="id_contract_edit">
                  <div class="form-group-attached">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Tema</label>
                          <input type="text" class="form-control text-black" name="tema_iklan_edit" id="tema_iklan_edit" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default">
                          <label>Brand</label>
                          <input type="text" class="form-control text-black" name="brand_edit" id="brand_edit" style="padding-top: 5px; height: 40px;">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group form-group-default input-group date col-sm-10">
                            <label>Tanggal Expired</label>
                            <input id="datepicker" type="text" class="form-control datepicker-expired" placeholder="Pick a date" name="expired_date_edit" style="padding-top: 5px; height: 40px;">

                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <!-- START UPLOADER -->
                        <div class="panel panel-default" style="margin-bottom: 0px;">
                          <div class="form-group form-group-default">
                            <label>Select Photos</label>
                            <input id="file_edit" name="file_edit[]" multiple type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg", "jpeg"]' style="padding-top: 5px; height: 40px;">

                            <hr style="margin-bottom: 0px; margin-top: 10px;">
                            <div id="detail-contract-box-photo">

                            </div>

                          </div>
                        </div>
                        <!-- END UPLOADER -->
                      </div>
                    </div>
                  </div>
                </form><br>
                <div class="modal-footer" style="padding-right: 0px;">
                  <button type="button" class="btn btn-success" id="button-update">Update</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  {{--end modal edit detail contract--}}

  {{-- START MODAL OPEN FEEDBACK --}}
  <div class="modal fade" id="openFeedback">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Open Feedback</h2>
              </div>
              @if(!empty($detail_contract[0]['id']))
              <form action="{{url('tiket/keluhan/'.$detail_contract[0]['id'])}}" method="post">
              @else
              <form action="#" method="post">
              @endif
              <div class="modal-body" style="padding-bottom: 0px;">
                  <div class="form-horizontal" role="form" id="form-decline">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label>Keluhan</label>
                                <input type="hidden" id="id_pemasangan">
                                <textarea class="form-control" name="keluhan" id="keluhan" required style="height: 300px; "></textarea>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button class="btn btn-success btn-animated from-top fa fa fa-check" type="submit" id="btn-decline-submit">
                  <span>Submit</span>
                </button>
                <button type="button" class="btn btn-default btn-animated from-top fa fa fa-times" data-dismiss="modal">
                  <span>Cancel</span>
                </button>
              </div>
              </form>
          </div>
      </div>
  </div>
  {{-- END MODAL OPEN TICKET --}}
  @if(!empty($detail_contract[0]['ticket']))
  {{-- START MODAL REPLY TICKET --}}
  <div class="modal fade" id="replyFeedback">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Reply Feedback</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 10px;">
                  <form action="{{url('tiket/balas-keluhan/'.$detail_contract[0]['ticket'][0]['id'])}}" method="post" class="form-horizontal" role="form" id="form-decline">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group" style="padding-bottom: 0px;">
                                <label>Keluhan</label>
                                <input type="hidden" id="id_pemasangan">
                                <p align="justify" style="font-size: 15px;">{{ $detail_contract[0]['ticket'][0]['title'] }}</p>
                              </div>
                              <div class="panel-group">
                                <div class="panel panel-primary" style="margin-top: 10px; width: 530px; right: -10; right: 20px; border-color: #48b0f7;">
                                    <div class="panel-heading" style="background-color: #48b0f7; padding-top: 10px; padding-bottom: 0px;">
                                      <div class="row">
                                        <p class="col-md-4 col-sm-3 col-xs-3" style="font-size: 20px; padding-top: 5px;">Reply Feedback</p>
                                        <div class="btn-group pull-right">
                                          @if(session('role_name') == 'customer' || session('role_name') == 'sampoerna')
                                            <a style="margin-bottom: 10px; margin-right: 5px;" href="{{ route('close.feedback',['id_ticket'=>$detail_contract[0]['ticket'][0]['id']]) }}" class="pull-right btn btn-info">
                                              <span>Close Feedback</span>
                                            </a>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                    <div class="panel-body" style="padding-top: 20px;">
                                        <ul class="chat">
                                          @foreach($detail_contract[0]['ticket'][0]['ticket_content'] as $reply)
                                            @if($reply['id_user']==$vendor['id'])

                                            <li class="left clearfix">
                                              <span class="chat-img pull-left">
                                                <img width="50" height="50" src="{{route('download', $reply['user']['photo'])}}" alt="User Avatar" class="img-circle">
                                              </span>
                                              <div class="chat-body clearfix">
                                                  <div class="row" style="height: 20px; margin-left: 0px;">
                                                      <strong class="primary-font">{{ $reply['user']['profile']['fullname'] }}</strong>
                                                  </div>
                                                  <p>
                                                    {{ $reply['content']}}
                                                  </p>
                                              </div>
                                            </li>
                                            @else
                                            <li class="right clearfix">
                                              <span class="chat-img pull-right">
                                                <img width="50" height="50" src="{{route('download', $reply['user']['photo'])}}" alt="User Avatar" class="img-circle">
                                              </span>
                                              <div class="chat-body clearfix" style="text-align:right">
                                                  <div class="row" style="height: 20px; margin-right: 0px;">
                                                  <strong class="primary-font">{{ $reply['user']['profile']['fullname'] }}</strong>
                                                  </div>
                                                  <p>
                                                    {{ $reply['content']}}
                                                  </p>
                                              </div>
                                            </li>
                                            @endif
                                          @endforeach
                                        </ul>
                                        <input type="hidden" id="id_pemasangan">
                                        <textarea class="form-control" name="reply_keluhan" id="reply_keluhan" required style="height: 100px;" placeholder="Reply here..."></textarea>
                                        <button class="btn btn-success btn-animated from-top fa fa fa-check" type="submit" id="btn-decline-submit" style="margin-top: 10px;">
                                          <span>Send</span>
                                        </button>
                                    </div>
                                    {{-- <div class="panel-footer">
                                      <input type="hidden" id="id_pemasangan">
                                      <textarea class="form-control" name="reply_keluhan" id="reply_keluhan" required style="height: 100px;"></textarea>
                                      <button class="btn btn-success btn-animated from-top fa fa fa-check" type="button" id="btn-decline-submit" style="margin-top: 10px;">
                                        <span>Send</span>
                                      </button>
                                    </div> --}}
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL REPLY TICKET --}}
  @endif
@endsection


@push('script')
    <script>
        //paling atas biar duluan di eksekusi
        function paginate(start = 0){
            var currentPage = start;
            var numPerPage = 5;		//inisialisasi baris per page

            $('.installation').data('active-page', currentPage);
            $('.thumbnail-box').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        }
        paginate();
    </script>

  <!-- Google Maps JS API -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
  {{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>

  <script src="{{asset('assets/plugins/new-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('assets/plugins/new-datepicker/locales/bootstrap-datepicker.id.min.js')}}"></script>

  {{-- FILE UPLOAD --}}
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/locales/id.js')}}"></script>
  <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

  <script>
    function status_instalasi(){
      if($('#status-installation>span').data("stat")=="aktif"){
        $('.replyFeedback').show();
        $('.openFeedback').show();
      }
      else{
        $('.replyFeedback').hide();
        $('.openFeedback').hide();
      }
    }

    $('.datepicker-expired').datepicker({
        format: "yyyy-mm-dd",
        clearBtn: true,
        language: "id",
        // autoclose: true
    });

    var datepicker_awal=$('.datepicker-awal').datepicker({
          // startDate: "now",
          format: "yyyy-mm-dd",
          clearBtn: true,
          language: "id",
          autoclose: true,
          todayHighlight: true
      });

    $('.datepicker-awal').change(function() {
      var start_date_end = $(this).val();

      $('.datepicker-akhir').datepicker({
          startDate: start_date_end,
          format: "yyyy-mm-dd",
          clearBtn: true,
          language: "id",
          autoclose: true
      });
    });

    $(document).ready(function(){
        var mapObj = new GMaps({
            el: '#map',
            lat: "{{ $billboard['latitude'] }}",
            lng: "{{ $billboard['longitude'] }}",
        });

        var m = mapObj.addMarker({
            lat: "{{ $billboard['latitude'] }}",
            lng: "{{ $billboard['longitude'] }}",
            infoWindow: {
                        content: `Kode Billboard        : <strong>{{$billboard['kode_billboard']}}</strong><br>
                                  Nama Vendor           : <strong>{{$vendor['username']}}</strong><br>
                                  Alamat                : <strong>{{$billboard['address']}}</strong><br>
                                  Tanggal Awal Kontrak  : <strong>{{$contract_date}}</strong><br>
                                  Tanggal Akhir Kontrak : <strong>{{$expired_date}}</strong><br>
                                 `
                    }
        });

        //combobox user parent
        $.get(route('getAllUser', 'customer'), function(data){
            $.each(data.data, function(key, val){
                $('#id_customer_edit').append(`<option value="${val.id}">${val.fullname}</option>`);
            });
        });

        $('#btn-edit-contract').click(function(){
            let id = $(this).data('id');

            $.get(route('contractShowByID', id), function(data){
                let contract = data.data;

                if(contract.id_customer != null){
                    $('#id_customer_edit').select2('val', contract.id_customer);
                }
                else{
                    $('#id_customer_edit').select2('val', 'other');
                }

                if(contract.customer_name != null){
                    $('#customer_name_edit').val(contract.customer_name);
                }

                //reformat tanggal
                let date1 = contract.contract_date.split('-');
                let date2 = contract.expired_date.split('-');
                let contractDate = `${date1['1']}/${date1['2']}/${date1['0']}`;
                let expiredDate = `${date2['1']}/${date2['2']}/${date2['0']}`;

                $('input[name="contract_date_edit"]').val(contractDate);
                $('input[name="expired_date_edit"]').val(expiredDate);
            });
        });

        $('#button-update-contract').click(function(){
            let id = $('#id_edit').val();

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Kontrak Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-edit').serializeArray();
                    var files = $('#file_edit')[0].files;

                    $.each(formData, function(key, value){
                        let keyname = value.name;
                        let name = keyname.replace('_edit', '');
                        data.append(name, value.value);
                    });

                    $.each(files, function(key, value){
                        data.append('file[]', value);
                    });

                    $.ajax({
                        url : route('update-contract', id),
                        type : "POST",
                        data : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kontrak Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kontrak Gagal Diubah", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kontrak Batal Diubah :)", "error");
                }
            });
        });

        $('#button-save').click(function(){
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Detail Kontrak Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-add-detail').serializeArray();
                    var files = $('#file-add')[0].files;

                    $.each(formData, function(key, value){
                        data.append(value.name, value.value);
                    });

                    $.each(files, function(key, value){
                        data.append('file[]', value);
                    });

                    $.ajax({
                        url : route('save-contract-detail'),
                        type : "POST",
                        data : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Kontrak Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Kontrak Gagal Disimpan", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Kontrak Batal Disimpan :)", "error");
                }
            });
        });

        $('.detail-contract-box').on('click', '.paginate, .paginate-image', function(e){
            e.preventDefault();
            let id = $(this).data('id');

            $.get(route('contractDetailShowByID', id), function(result){
                $('.carousel-indicators, .carousel-inner').empty();
                //$('.paginate-left, .paginate-right').empty();
                $('.edit-delete-box').empty();

                let data = result.data;

                $('#detail-tema').text(data.tema_iklan);
                $('#detail-brand').text(data.brand);
                $('#detail-tglpasang').text(data.created_date);
                $('#detail-tglexpired').text(data.expired_date);
                if (data.status == "0") {
                  var status = `<span class="label label-danger">Tidak Aktif</span>`;
                } else {
                  var status = `<span data-stat="aktif" class="label label-success">Aktif</span>`;
                }

                $('#status-installation').html(status);
                status_instalasi();

                if(data.next_id != null){
                    $('#paginate-newer').data('id', data.next_id);
                    $('#paginate-newer').removeClass('disabled');
                }
                else{
                    $('#paginate-newer').addClass('disabled');

                  @if(session('role_name') == 'vendor' && $expDate >= $nowDate)
                    if(data.status == 1){
                        let editDeleteElem = `<a class="btn btn-warning btn-animated from-top fa fa-pencil" id="btn-edit" data-target="#edit-details" data-toggle="modal" data-id="${data.id}" href="#edit-details">
                            <span><b>Edit</b></span>
                        </a>`;

                        $('.edit-delete-box').append(editDeleteElem);
                    }
                  @endif
                }

                if(data.previous_id != null){
                    $('#paginate-older').data('id', data.previous_id);
                    $('#paginate-older').removeClass('disabled');
                }
                else{
                    $('#paginate-older').addClass('disabled');
                }

                if(data.photo.length > 0){
                    $.each(data.photo, function(key, val){
                        let active = (key == 0) ? 'active' : '';

                        let radioElem = `<li data-target="#myCarousel" data-slide-to="${key}" class="${active}"></li>`;
                        let fotoElem = `<div class="item ${active}" style="height: 230px;">
                            <img src="${route('download', val.photo_name)}" alt="Foto Detail Pemasangan Billboard">
                        </div>`;

                        $('.carousel-inner').append(fotoElem);
                        $('.carousel-indicators').append(radioElem);
                    });
                }
                else{
                    let radioElem = `<li data-target="#myCarousel" data-slide-to="0" class="active"></li>`;
                    let fotoElem = `<div class="item active" style="height: 230px;">
                        <img src="{{asset('assets/img/gallery/no-pic.png')}}" alt="Foto Detail Pemasangan Billboard">
                    </div>`;

                    $('.carousel-inner').append(fotoElem);
                    $('.carousel-indicators').append(radioElem);
                }

                //set active border
                $('.installation').find('.thumbnail').removeClass('active');
                $('.installation').find(`[data-thumbnail=${data.id}]`).addClass('active');

                //repaginate
                let activePage = $('.installation').data('active-page');
                let sliceData = $('.installation').find('.thumbnail.active').data('slice');
                if(activePage != sliceData){
                    paginate(sliceData);
                }
            });
        });

        $('.edit-delete-box').on('click', '#btn-edit', function(){
            let id = $(this).data('id');
            $('.form-control').val('');
            $('#detail-contract-box-photo').empty();

            $.get(route('contractDetailShowByID', id), function(result){
                let data = result.data;

                let date2 = data.expired_date.split('-');
                let expiredDate = `${date2['1']}/${date2['2']}/${date2['0']}`;

                $('#id_edit').val(data.id);
                $('#tema_iklan_edit').val(data.tema_iklan);
                $('#brand_edit').val(data.brand);
                $('.datepicker-expired').val(expiredDate);
                $('#id_contract_edit').val(data.id_contract);

                //show foto
                let elem;
                if(data.photo.length > 0){
                    elem = $(`<output id="list">
                        <h4 style="margin-top: 0px; margin-bottom: 0px;">Current Photos</h4>
                    </output>`);

                    //let list = ``;
                    $.each(data.photo, (key, foto) => {
                        let image = route('download', foto.photo_name);

                        let list = `<span class="file-preview-wrapper">
                            <div class="file-preview-frame krajee-default kv-preview-thumb" style="padding-right: 10px; padding-left: 10px; margin-left: 0px;">
                            <div class="kv-file-content">
                                <img class="thumb file-preview-image kv-preview-data" src="${image}">
                                <span class="remove_img_preview" data-id="${foto.id}"></span>
                            </div>
                            </div>
                        </span>`;

                        elem.append(list);
                    });
                }
                else{
                    elem = `<div>
                        <h4>Billboard belum memiliki foto</h4>
                    </div>`;
                }

                $('#detail-contract-box-photo').append(elem);
            });
        });

        $('#detail-contract-box-photo').on('click', ".remove_img_preview", function(){
            let id = $(this).data('id');
            let boxPhoto = $(this).parents('.file-preview-wrapper');

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Foto Akan Dihapus Permanen !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            url  : route('file-delete-photo', id),
                            type : "DELETE",
                            data : {
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(data, status){
                                boxPhoto.remove();

                                if(status=="success"){
                                    setTimeout(function(){
                                        swal({
                                            title: "Sukses",
                                            text: "Foto Billboard Berhasil Dihapus!",
                                            type: "success"
                                        });
                                    }, 1000);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                setTimeout(function(){
                                    swal("Gagal", "Data Billboard Gagal Diubah", "error");
                                }, 1000);
                            }
                        });
                    }
                }
            );
        });

        $('#button-update').click(function(){
            let id = $('#id_edit').val();

            swal({
                title: "Apakah Anda Yakin ?",
                text: "Pastikan Data Detail Pemasangan Sudah Diisi Dengan Benar",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    let data = new FormData();
                    let formData = $('#form-edit-detail').serializeArray();
                    let files = $('#file_edit')[0].files;

                    $.each(formData, function(key, value){
                        let keyname = value.name;
                        let name = keyname.replace('_edit', '');
                        data.append(name, value.value);
                    });

                    $.each(files, function(key, value){
                        data.append('file[]', value);
                    });

                    $.ajax({
                        url : route('update-contract-detail', id),
                        type : "POST",
                        data : data,
                        cache 		: false,
                        contentType : false,
                        processData : false,
                        success : function(data, status){
                            if(status=="success"){
                                setTimeout(function(){
                                    swal({
                                        title: "Sukses",
                                        text: "Data Detail Pemasangan Berhasil Diubah!",
                                        type: "success"
                                        },
                                        function(){
                                            location.reload();
                                        });
                                    }, 1000);
                            }
                            $('#modal-edit').modal('hide');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            setTimeout(function(){
                                swal("Gagal", "Data Detail Pemasangan Gagal Diubah", "error");
                            }, 1000);
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Detail Pemasangan Batal Diubah :)", "error");
                }
            });
        });

        $('.edit-delete-box').on('click', '#btn-delete', function(e){
            e.preventDefault();
            let id = $(this).data('id');
            swal({
                title: "Apakah Anda Yakin ?",
                text: "Data Detail Kontrak Akan Dihapus",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Yakin !",
                cancelButtonText: "Tidak, Batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
                },
                function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url : route('delete-contract-detail', id),
                        type : "DELETE",
                        data : {
                            "_token": "{{ csrf_token() }}"
                        },
                        success : function(data, status){
                            if(status=="success"){
                                swal({
                                    title: "Sukses",
                                    text: "Data Detail Kontrak Berhasil Dihapus!",
                                    type: "success"
                                }, function(){
                                    location.reload();
                                });
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal", "Data Detail Kontrak Gagal Dihapus", "error");
                        }
                    });
                } else {
                    swal("Dibatalkan", "Data Detail Kontrak Batal Dihapus :)", "error");
                }
            });
        });
        status_instalasi();
    });
  </script>

    {{--  datepickernya ganti aja is samain ky yg bikin kontrak, notifikasi ga jalan disini  --}}
  <script src="{{asset('assets/js/form_elements.js')}}" type="text/javascript"></script>
@endpush
