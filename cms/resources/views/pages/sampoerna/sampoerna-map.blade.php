@extends('layouts.cms')

@section('title','Search By Map')

@section('sidebar')
    {{--  @php $role = session('role_name') @endphp  --}}
    {{--@include('sidebar.'.$role)--}} {{--sesuikan sidebar dengan kebutuhan--}}
    @include('sidebar.sampoerna'){{-- sementara doang--}}
@endsection
@push('style')
  <link href="{{asset('assets/plugins/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <style>
  .windows h1 {
    font-size: 50px !important;
  }
  </style>
@endpush
@section('content')

  <div class="col-sm-12" style="padding-bottom: 8px;">
    <div class="panel-group">
      <div class="panel panel-primary" style="border-color : #ffffff">
        <div id="vmap" style="width:100%;height:1000px"></div>
      </div>
    </div>
  </div>
  
  
            
@endsection

@push('script')
    <!-- Google Maps JS API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9AKT_vX0AAVz-i1PS-sJMW_RyFnx-6K0"></script>
    {{-- GMaps Library --}}
    {{--  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>  --}}
    <script type="text/javascript" src="{{asset('assets/plugins/dropzone/dropzone.min.js')}}"></script>
{{-- GMaps Library --}}
  <script src="{{asset('assets/js/gmaps.js')}}" type="text/javascript"></script>
  {{-- Highcharts --}}
  <script src="{{asset('assets/plugins/highchart/Highcharts.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/highchart/Highcharts-3d.js')}}" type="text/javascript"></script>
  {{-- <script src="{{asset('assets/plugins/highchart/exporting.js')}}" type="text/javascript"></script> --}}
  <script src="{{asset('assets/plugins/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/plugins/jqvmap/maps/jquery.vmap.sampoerna.js')}}" type="text/javascript"></script>
    <script>
      jQuery(document).ready(function () {
        jQuery('#vmap').vectorMap({
          map: 'indonesia_id',
          enableZoom: true,
          showTooltip: true,
          selectedColor: null,
          colors: {
                aceh: '#e25353',
                sumaterautara: '#e25353', 
                sumaterabarat: '#05a861',
                riau: '#05a861',
                kepulauanriau: '#05a861',
                jambi: '#603601',
                sumateraselatan: '#603601',
                kepulauanbangkabelitung: '#603601',
                lampung: '#0c0c0c',
                bengkulu: '#0c0c0c',
                kalimantanbarat: '#f77d0c',
                kalimantantimur: '#f77d0c',
                kalimantantengah: '#f77d0c',
                kalimantantenggara: '#f77d0c',
                kalimantanutara: '#f77d0c',
                kalimantanselatan: '#f77d0c',
                sulawesitengah: '#9803a5',
                gorontalo: '#9803a5',
                sulawesiutara: '#9803a5',
                sulawesibarat: '#9803a5',
                sulawesitenggara: '#9803a5',
                sulawesiselatan: '#9803a5',
                malukuutara: '#777777',
                maluku: '#777777',
                papuabarat: '#777777',
                papua: '#777777',
                bali: '#1192d8',
                nusatenggarabarat: '#1192d8',
                nusatenggaratimur: '#1192d8',
                jawatimur: '#a3061e',
                banten: '#dba718',
                dkijakarta: '#dba718',
                jawabarat: '#b26e8e',
                jawatengah: '#0c2d7f',
                diyogyakarta: '#0c2d7f',
                jawatimurs: '#0c9e1d',
                
                


            },
          onRegionClick: function(event, code, region){
            event.preventDefault();
          }
        });
      });
    </script>
@endpush
