@extends('layouts.cms')

@section('title', 'Master - Wilayah')

@section('sidebar')
    @include('sidebar.sampoerna') {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link href="{{asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <!-- START ROW -->
  <div class="row">
      <div class="col-md-12 col-xs-12"> {{-- START COL --}}
        <!-- START PANEL -->
        <div class="panel">
          <div class="panel-heading"> {{-- START PANEL HEADING --}}
            <div class="panel-title" style="padding: 10px 10px;">Master - Wilayah</div>
              <div class="row">
                <div class="pull-left">
                  <div class="col-xs-12">
                    <button class="btn btn-success btn-animated from-top pg pg-plus" id="btn-add" type="button" data-toggle="modal" href='#modal-add'>
                      <span>Add</span>
                    </button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
          </div> {{-- END PANEL HEADING --}}

          <div class="panel-body"> {{-- START PANEL BODY --}}
            <table class="table table-hover" id="tableWithSearch" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Region</th>
                  <th>Zone</th>
                  <th>Area</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="v-align-middle semi-bold col-md-1">
                    <p>1</p>
                  </td>
                  <td class="v-align-middle" style="padding-left: 10px;">
                    <p><strong>Java 1<strong></p>
                  </td>
                  <td class="v-align-middle" style="padding-left: 10px;">
                    <p><strong>Jawa Barat<strong></p>
                  </td>
                  <td class="v-align-middle" style="padding-left: 10px;">
                    <p><strong>Bandung 1, Bandung 2, Bandung 3, Sukabumi, Garut, Cirebon<strong></p>
                  </td>
                  <td class="col-md-3 text-center">
                    <button class=" btn btn-complete btn-animated from-top pg pg-search" type="button" data-toggle="modal" data-target="#modal-detail" id="btn-detail">
                      <span>Details</span>
                    </button>
                    <button class=" btn btn-primary btn-animated from-top pg pg-form" type="button" data-toggle="modal" data-target="#modal-edit" id="btn-edit">
                      <span>Edit</span>
                    </button>
                    <button class=" btn btn-danger btn-animated from-top pg pg-trash_line" type="button" id="button-delete" onclick="hapus()">
                      <span>Delete</span>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div> {{-- END PANEL BODY --}}

        </div>
        <!-- END PANEL -->
      </div> {{-- END COL --}}
    </div>
  <!-- END ROW -->

  {{-- START MODAL ADD --}}
  <div class="modal fade" id="modal-add">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Add Wilayah</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-add">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="region" class="col-sm-3 control-label">Region</label>
                                <div class="col-sm-9">
                                  <input type="text" name="region_add" id="region_add" class="form-control" placeholder="Java 1" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="zone" class="col-sm-3 control-label">Zone</label>
                                <div class="col-sm-9">
                                  <select class="full-width" data-init-plugin="select2" name="zone_add" id="zone_add" required>
                                    <option value="">Chose Zone</option>
                                    <option value="jateng">Jawa Tengah</option>
                                    <option value="jabar">Jawa Barat</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="area" class="col-sm-3 control-label">Area</label>
                                <div class="col-sm-9">
                                  <select class="full-width" name="area_add" id="area_add" data-init-plugin="select2" multiple required>
                                    <option value="cirebon">Cirebon</option>
                                    <option value="garut">Garut</option>
                                    <option value="other">Other Area</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group" id="other_area_add">
                                <label for="other_area" class="col-sm-3 control-label">Other Area</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" name="other_area_add" id="other_area_add" required style="height: 70px;" placeholder="Bandung 1, Bandung 2, Bandung 3"></textarea>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-success" id="btn-save">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL ADD --}}

  {{-- START MODAL EDIT --}}
  <div class="modal fade" id="modal-edit">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Edit Wilayah</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-add">
                  {{ csrf_field() }}
                  <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                              <label for="region" class="col-sm-3 control-label">Region</label>
                              <div class="col-sm-9">
                                <input type="text" name="region_edit" id="region_edit" class="form-control" placeholder="Java 1" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="zone" class="col-sm-3 control-label">Zone</label>
                              <div class="col-sm-9">
                                <select class="full-width" data-init-plugin="select2" name="zone_edit" id="zone_edit" required>
                                  <option value="">Chose Zone</option>
                                  <option value="jateng">Jawa Tengah</option>
                                  <option value="jabar">Jawa Barat</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="area" class="col-sm-3 control-label">Area</label>
                              <div class="col-sm-9">
                                <select class="full-width" name="area_edit" id="area_edit" data-init-plugin="select2" multiple required>
                                  <option value="cirebon">Cirebon</option>
                                  <option value="garut">Garut</option>
                                  <option value="other">Other Area</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group" id="other_area_edit">
                              <label for="other_area" class="col-sm-3 control-label">Other Area</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" name="other_area_edit" id="other_area_edit" required style="height: 70px;" placeholder="Bandung 1, Bandung 2, Bandung 3"></textarea>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-success" id="button-update">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL EDIT --}}

  {{-- START MODAL DETAIL --}}
  <div class="modal fade" id="modal-detail">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 class="modal-title">Detail Wilayah</h2>
              </div>
              <div class="modal-body" style="padding-bottom: 0px;">
                  <form action="{{url('/#')}}" method="POST" class="form-horizontal" role="form" id="form-edit">
                    {{ csrf_field() }}
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                <label for="region" class="col-sm-3 control-label">Region</label>
                                <div class="col-sm-9">
                                  <input type="text" id="region_detail" class="form-control" placeholder="Java 1" disabled style="color:black"/>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="zone" class="col-sm-3 control-label">Zone</label>
                                <div class="col-sm-9">
                                  <input type="text" id="zone_detail" class="form-control" placeholder="Jawa Barat" disabled style="color:black"/>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="area" class="col-sm-3 control-label">Area</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control"id="area_detail" disabled style="height: 70px;color:black">Bandung 1, Bandung 2, Bandung 3, Sukabumi, Garut, Cirebon</textarea>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="modal-footer" style="padding-right: 35px;">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>
  {{-- END MODAL DETAIL --}}
@endsection

@push('script')
    {{-- START FORM VALIDATOR --}}
    <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form_layouts.js')}}" type="text/javascript"></script>
    {{-- END FORM VALIDATOR --}}

    <script src="{{asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    {{--  <script src="{{asset('assets/js/datatables.js')}}" type="text/javascript"></script>  --}}
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/locales/id.js')}}"></script>

    <script>
      $('document').ready(function(){
          'use strict';

          // Fungsi untuk show and hidden jika memilih Other Area [ADD WILAYAH]
          $("#area_add").change(function(){
              if($(this).val() == "other")
              {
                  $("#other_area_add").show();
              } else {
                  $("#other_area_add").hide();
              }
          });
          $("#other_area_add").hide();

          // Fungsi untuk show and hidden jika memilih Other Area [EDIT WILAYAH]
          $("#area_edit").change(function(){
              if($(this).val() == "other")
              {
                  $("#other_area_edit").show();
              } else {
                  $("#other_area_edit").hide();
              }
          });
          $("#other_area_edit").hide();
      });
    </script>
@endpush
