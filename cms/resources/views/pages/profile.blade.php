@extends('layouts.cms')

@section('title', 'Profile')

@section('sidebar')
    @php $role = session('role_name') @endphp
    @include('sidebar.'.$role) {{-- sesuikan sidebar dengan kebutuhan --}}
@endsection

@push('style')
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/alerts/sweet-alert.css')}}">
  <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}">
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/plugins/bootstrap-fileinput/css/fileinput-profile.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="container-fluid container-fixed-lg bg-white">
<!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li>
            <a href="#" class="active">Profile</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-sm-5 text-center">
            <img class="img-circle" src="{{ ($photo != null) ? route('download', $photo) : asset('assets/img/profiles/2x.jpg') }}" style="margin-top:35px;border:black 2px solid" width="250" height="250" />
        </div>
        <div class="col-sm-7">
            <div class="panel panel-transparent">
                <div class="panel-body">
                    <form id="form-work" role="form" autocomplete="off">
                        <div style="padding-top:24px"></div>
                        <div class="form-group-attached">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" value="{{$profile['fullname']}}" id="fname" name="full_name"  disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group form-group-default">
                                        <label>Username</label>
                                        <input type="text" class="form-control" value="{{$username}}" id="username" name="username" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" value="{{$profile['email']}}" id="email" name="email" class="form-control" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-group-default">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" value="{{$profile['phone']}}" id="phone" name="phone" disabled style="padding-top: 5px; height: 40px; color: black;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Alamat Kantor</label>
                                        <textarea class="form-control" id="address" style="height: 70px; color: black;" disabled>{{$profile['address']}}</textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="padding-top:20px">
                              <div class="pull-right">
                                <button class="btn btn-success btn-primary btn-animated from-top fa fa-key" type="button" data-toggle="modal" data-target="#modal-change" id="btn-change" style="margin-right: 5px;">
                                  <span>Change Password</span>
                                </button>
                                <button class="btn btn-success btn-complete btn-animated from-top pg pg-form" type="button" data-toggle="modal" data-target="#modal-edit" id="btn-edit" style="margin-right: 5px;">
                                  <span>Edit</span>
                                </button>
                              </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- START MODAL EDIT --}}
  <div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">YOUR PROFILE</h2>
            </div>
            <div class="modal-body" style="padding-bottom: 0px;">
                <form action="{{url('/profile/store')}}" method="POST" class="form-horizontal" role="form" id="form-edit">
                  {{ csrf_field() }}
                  <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                              <label for="name" class="col-sm-3 control-label">Full Name</label>
                              <div class="col-sm-9">
                                <input type="hidden" name="id_edit" id="id_edit" class="form-control" required>
                                <input type="text" name="fullname_edit" id="fullname_edit" class="form-control" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="name" class="col-sm-3 control-label">Username</label>
                              <div class="col-sm-9">
                                <input type="text" name="username_edit" id="username_edit" class="form-control" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="email" class="col-sm-3 control-label">Email</label>
                              <div class="col-sm-9">
                                <input type="email" name="email_edit" id="email_edit" class="form-control" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="phone" class="col-sm-3 control-label">Phone</label>
                              <div class="col-sm-9">
                                <input type="text" name="phone_edit" id="phone_edit" class="form-control" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="address" class="col-sm-3 control-label">Address</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" name="address_edit" id="address_edit" style="height: 70px;"></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="password" class="col-sm-3 control-label">Photo</label>
                              <div class="col-sm-9">
                                {{-- <input type="file" name="file_edit" id="file_edit" class="form-control"> --}}

                                {{-- <label>Select Photos</label>
                                <input id="photo-profile" name="input7[]" type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg"]' style="padding-top: 5px; height: 40px;"> --}}

                                <label>Select Photos</label>
                                <input name="file_edit" id="file_edit" type="file" class="file file-loading" data-show-upload="false" data-show-caption="false" data-allowed-file-extensions='["png", "jpg"]' style="padding-top: 5px; height: 40px;">

                                {{-- <div id="kv-avatar-errors-1" class="center-block" style="width:400px; display:none;"></div>
                                <div class="kv-avatar text-center">
                                    <div class="file-loading">
                                        <input id="photo-profile" name="photo-profile[]" type="file" multiple required>
                                    </div>
                                </div> --}}

                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer" style="padding-right: 35px;">
              <button type="button" class="btn btn-success" id="button-update">Update</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
  </div>
  {{-- END MODAL EDIT --}}

  {{-- START MODAL CHANGE PASSWORD --}}
  <div class="modal fade" id="modal-change">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">CHANGE PASSWORD</h2>
            </div>
            <div class="modal-body" style="padding-bottom: 0px;">
                <form action="{{url('/profile/store')}}" method="POST" class="form-horizontal" role="form" id="form-update-password">
                  {{ csrf_field() }}
                  <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                              <label for="current_password" class="col-sm-4 control-label">Current Password</label>
                              <div class="col-sm-8">
                                <input type="password" name="current_password" id="current_password" class="form-control" placeholder="Old Password" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="new_password" class="col-sm-4 control-label">New Password</label>
                              <div class="col-sm-8">
                                <input type="password" name="new_password" id="new_password" placeholder="New Password" class="form-control">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="confirm_password" class="col-sm-4 control-label">Confirm Password</label>
                              <div class="col-sm-8">
                                <input type="password" name="new_password_confirmation" id="new_password_confirmation" placeholder="New Password" class="form-control">
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer" style="padding-right: 35px;">
              <button type="button" class="btn btn-success" id="btn-update-password">Update</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
  </div>
  {{-- END MODAL CHANGE PASSWORD --}}
@endsection

@push('script')
    {{-- START FORM VALIDATOR --}}
    <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/form_layouts.js')}}" type="text/javascript"></script>
    {{-- END FORM VALIDATOR --}}

    <script type="text/javascript" src="{{asset('assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
    <script src="{{asset('assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/alerts/sweet-alert.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/bootstrap-fileinput/js/locales/id.js')}}"></script>
    <script>
      $(document).ready(function() {
          // lightbox.option({
          //     'resizeDuration': 200,
          //     'wrapAround': true
          // });

          $('#btn-edit').click(function(){
              let id = {{ $id }}

              $.get(`{{route('update-user','')}}/${id}`, function(data){
                  let user = data.data;
                  let profile = user.profile;

                  $('#id_edit').val(user.id);
                  $('#fullname_edit').val(profile.fullname);
                  $('#username_edit').val(user.username);
                  $('#email_edit').val(profile.email);
                  $('#address_edit').val(profile.address);
                  $('#phone_edit').val(profile.phone);
              });
          });

          $('#btn-update-password').click(function(){
              swal({
                  title: "Apakah Anda Yakin ?",
                  text: "Pastikan Data Password Sudah Terisi Dengan Benar",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#00C853",
                  confirmButtonText: "Ya, Yakin !",
                  cancelButtonText: "Tidak, Batalkan !",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm){
                  if (isConfirm) {
                      $.ajax({
                          url : `{{route('changepassword')}}`,
                          type : "PUT",
                          data : {
                              "_token": "{{ csrf_token() }}",
                              "current_password" : $("#current_password").val(),
                              "new_password" : $("#new_password").val(),
                              "new_password_confirmation" : $("#new_password_confirmation").val()
                          },
                          success : function(data, status){
                              if(status=="success"){
                                  setTimeout(function(){
                                      swal({
                                          title: "Sukses",
                                          text: "Data Password Berhasil Diubah!",
                                          type: "success"
                                          },
                                          function(){
                                              //table.ajax.reload();
                                          });
                                      }, 1000);
                              }
                              $('#modal-change').modal('hide');
                          },
                          error: function (xhr, ajaxOptions, thrownError) {
                              setTimeout(function(){
                                  swal("Gagal", "Data Password Gagal Diubah", "error");
                              }, 1000);
                          }
                      });
                  } else {
                      swal("Dibatalkan", "Password Batal Diubah :)", "error");
                  }
              });
          });

          //update user
          $('#button-update').click(function(){
              let id = $('#id_edit').val();

              swal({
                  title: "Apakah Anda Yakin ?",
                  text: "Pastikan Data Profile Sudah Terisi Dengan Benar",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#00C853",
                  confirmButtonText: "Ya, Yakin !",
                  cancelButtonText: "Tidak, Batalkan !",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm){
                  if (isConfirm) {
                      let data = new FormData();
                      let formData = $('#form-edit').serializeArray();
                      // return console.log($('#photo-profile'));
                      var files = $('#file_edit')[0].files[0];
                      // return console.log(files);
                      // $.each(files, function(key, value){
                      //   data.append('file[]', value);
                      // });
                      data.append('file', files);
                      $.each(formData, function(key, value){
                          let keyname = value.name;
                          let name = keyname.replace('_edit', '');
                          data.append(name, value.value);
                      });
                      
                      // $.each(formData, function(key, value){
                      //     data.append(value.name, value.value);
                      // });

                      data.append('_method', 'PUT');

                      $.ajax({
                          url : `{{route('update-user', '')}}/${id}`,
                          type : "POST",
                          data : data,
                          cache 		: false,
                          contentType : false,
                          processData : false,
                          success : function(data, status){
                              if(status=="success"){
                                  setTimeout(function(){
                                      swal({
                                          title: "Sukses",
                                          text: "Data Profile Berhasil Diupdate!",
                                          type: "success"
                                          }, function(){
                                              window.location.reload();
                                          });
                                      }, 1000);
                              }
                              $('#modal-edit').modal('hide');
                          },
                          error: function (xhr, ajaxOptions, thrownError) {
                              setTimeout(function(){
                                  swal("Gagal", "Data Profile Gagal Diupdate", "error");
                              }, 1000);
                          }
                      });
                  } else {
                      swal("Dibatalkan", "Data Profile Batal Diupdate :)", "error");
                  }
              });
          });
      });
    </script>
@endpush
