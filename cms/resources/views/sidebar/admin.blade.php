<nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="{{asset('assets/img/logopixartwhite.png')}}" alt="logo" class="brand" data-src="{{asset('assets/img/logopixartwhite.png')}}" data-src-retina="{{asset('assets/img/logopixartwhite.png')}}" width="120" height="30">
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{route('dashboard-admin')}}" class="detailed">
              <span class="title">Dashboard</span>
            </a>
            <span class="icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li class="">
            <a href="{{route('profile')}}" class="detailed">
              <span class="title">Profile</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
          </li>
          <li class="">
            <a href="javascript:;"><span class="title">Master</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="fa fa-book"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{route('master-paket')}}" class="detailed">
                  <span class="title">Paket</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-bolt"></i></span>
              </li>
              <li>
                <a href="javascript:;"><span class="title">Users</span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                <ul class="sub-menu">
                  <li class="">
                    <a href="{{route('master-admin')}}">Admin</a>
                    <span class="icon-thumbnail"><i class="fa fa-user-secret "></i></span>
                  </li>
                  <li class="">
                    <a href="{{route('master-vendor')}}">Vendor</a>
                    <span class="icon-thumbnail"><i class="fa fa-bookmark"></i></span>
                  </li>
                  <li class="">
                    <a href="{{route('master-customer')}}">Customer</a>
                    <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
                  </li>
                </ul>
              </li>
              <li>
                <a href="javascript:;"><span class="title">Wilayah</span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="fa fa-globe"></i></span>
                <ul class="sub-menu">
                  <li class="">
                    <a href="{{route('master-provinsi')}}" class="detailed">
                      <span class="title">Provinsi</span>
                    </a>
                    <span class="icon-thumbnail" style="font-size: 10px;">Prov</span>
                  </li>
                  <li class="">
                    <a href="{{route('master-kabupaten')}}" class="detailed">
                      <span class="title">Kabupaten</span>
                    </a>
                    <span class="icon-thumbnail">Kab</span>
                  </li>
                  <li class="">
                    <a href="{{route('master-kecamatan')}}" class="detailed">
                      <span class="title">Kecamatan</span>
                    </a>
                    <span class="icon-thumbnail">Kec</i></span>
                  </li>
                </ul>
              </li>
              <li class="">
                <a href="{{route('master-billboard')}}" class="detailed">
                  <span class="title">Billboard</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-television"></i></span>
              </li>
            </ul>
          </li>
          <li style="height: 47px;">
            <a href="{{route('verify-billboard')}}" class="detailed">
              <span class="title">Verify Billboard</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-md fa-google-wallet"></i></span>
          </li>
          <li class="">
            <a href="{{route('logout')}}" class="detailed">
              <span class="title">Logout</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-power-off"></i></span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->

</nav>
