
<nav class="page-sidebar" data-pages="sidebar">

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="{{asset('assets/img/logopixartwhite.png')}}" alt="logo" class="brand" data-src="{{asset('assets/img/logopixartwhite.png')}}" data-src-retina="{{asset('assets/img/logopixartwhite.png')}}" width="120" height="30">
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{route('dashboard-vendor')}}" class="detailed">
              <span class="title">Dashboard</span>
            </a>
            <span class="icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li class="">
            <a href="{{route('profile')}}" class="detailed">
              <span class="title">Profile</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
          </li>
          <li class="">
            <a href="javascript:;"><span class="title">Billboard</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="fa fa-television"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{route('vendor-billboard')}}" class="detailed">
                  <span class="title">Master</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-plus"></i></span>
              </li>
              <li class="">
                <a href="{{route('vendor-unoccupied')}}" class="detailed">
                  <span class="title">Unoccupied</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-calendar-o"></i></span>
              </li>
              <li class="">
                <a href="{{route('vendor-willexpire')}}" class="detailed">
                  <span class="title">Will Expire</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-location-arrow"></i></span>
              </li>
              <li class="">
                <a href="{{route('vendor-occupied')}}" class="detailed">
                  <span class="title">Occupied</span>
                </a>
                <span class="icon-thumbnail"><i class="fa fa-clipboard"></i></span>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="{{route('contract')}}" class="detailed">
              <span class="title">Contract</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-book"></i></span>
          </li>
          <li class="">
            <a href="{{route('logout')}}" class="detailed">
              <span class="title">Logout</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-power-off"></i></span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->

</nav>
