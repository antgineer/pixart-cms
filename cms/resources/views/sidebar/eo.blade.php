<nav class="page-sidebar" data-pages="sidebar">

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="{{asset('assets/img/logopixartwhite.png')}}" alt="logo" class="brand" data-src="{{asset('assets/img/logopixartwhite.png')}}" data-src-retina="{{asset('assets/img/logopixartwhite_2x.png')}}" width="120" height="30">
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{route('dashboard-eo')}}" class="detailed">
              <span class="title">Dashboard</span>
            </a>
            <span class="icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li class="">
            <a href="{{route('profile')}}" class="detailed">
              <span class="title">Profile</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
          </li>
          <li class="">
            <a href="{{route('event')}}" class="detailed">
                <span class="title">Event</span>
            </a>
            <span class="icon-thumbnail"><i class=" fa fa-edge"></i></span>
          </li>
          <li class="">
            <a href="{{route('logout')}}" class="detailed">
              <span class="title">Logout</span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-power-off"></i></span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->

</nav>
