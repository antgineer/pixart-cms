<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
</head>
<body>
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START PANEL -->
            <div class="panel-body">
            <div class="invoice padding-50 sm-padding-10">
                <div>
                <div class="pull-left">
                     <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{public_path('assets/img/invoice/squarespace2x.png')}}" data-src="{{asset('assets/img/invoice/squarespace.png')}}" src="{{public_path('assets/img/invoice/squarespace2x.png')}}">
                    <address class="m-t-10">
                        <strong style="font-size:20px"> {{$vendor_fullname}}</strong>
                        <strong></strong>
                        <br>.
                        <br>
                    </address>
                </div>
                <div class="pull-right sm-m-t-20">
                    <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
                </div>
                <br>
                <br>
                <div class="table-responsive">
                <table class="table m-t-50">
                    <thead>
                    <tr>
                        <th class=""> Description</th>
                        <th class="text-center">Detail</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="">
                        <p class="text-black">Kode Billboard</p>
                        </td>
                        <td class="text-center">{{$billboard['kode_billboard']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="">
                        <p class="text-black">Alamat</p>
                        </td>
                        <td class="text-center">{{$billboard['address']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-center"><strong>Provinsi</strong></td>
                        <td class="text-center"><strong>Kabupaten</strong></td>
                        <td colspan="2"><strong>Kecamatan</strong></td>
                    </tr>
                    <tr>
                        <td class="text-center">{{$billboard['provinsi']['nama_provinsi']}}</td>
                        <td class="text-center">{{$billboard['kabupaten']['nama_kabupaten']}}</td>
                        <td colspan="2">{{$billboard['kecamatan']['nama_kecamatan']}}</td>
                    </tr>
                    <tr>
                        <td class="text-center"><strong>Panjang</strong></td>
                        <td class="text-center"><strong>Lebar</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-center">{{$billboard['panjang']}}</td>
                        <td class="text-center">{{$billboard['lebar']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="">
                        <p class="text-black">Lighting</p>
                        </td>
                        <td class="text-center">{{$billboard['lighting']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="">
                        <p class="text-black">Side</p>
                        </td>
                        <td class="text-center">{{$billboard['side']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="">
                        <p class="text-black">Format</p>
                        </td>
                        <td class="text-center">{{$billboard['format']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="">
                        <p class="text-black">Bahan</p>
                        </td>
                        <td class="text-center">{{$billboard['bahan']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="">
                        <p class="text-black">Harga</p>
                        </td>
                        <td class="text-center">{{$billboard['harga']}}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                {{--  <div class="gallery col-md-12" style="margin:25px auto 0 auto">
                <figure class="col-md-4">
                    <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                </figure>
                <figure class="col-md-4">
                    <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                </figure>
                <figure class="col-md-4">
                    <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                </figure>
                </div>  --}}
                </div>

            </div>
            </div>
        <!-- END PANEL -->
        </div>
        <!-- END CONTAINER FLUID -->
</body>
</html>
