<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{public_path('assets/img/logopixartdark.png')}}" data-src="{{public_path('assets/img/logopixartdark.png')}}" src="{{ public_path('assets/img/logopixartdark.png')}}">
                      
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Report</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="table table-striped">
                  
                  <table class="table m-t-50">
                  <tbody>
                    <tr>
                      <td class="">
                        <strong>Kode Billboard</strong>
                      </td>
                      <td>:</td>
                      <td>{{$billboard['kode_billboard']}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Contract date</strong>
                      </td>
                      <td>:</td>
                      <td>{{$contract_date}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Expired date</strong>
                      </td>
                      <td>:</td>
                      <td>{{$expired_date}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Vendor Name</strong>
                      </td>
                      <td>:</td>
                      <td>{{$vendor['profile']['fullname']}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Customer Name</strong>
                      </td>
                      <td>:</td>
                      <td>{{$customer['profile']['fullname']}}</td>
                    </tr>
                    @foreach($detail_contract as $dc)
                    @if($dc['status']==1)
                    <tr>
                      <td class="">
                        <strong>Brand</strong>
                      </td>
                      <td>:</td>
                      <td>{{$dc['brand']}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Advertise Theme</strong>
                      </td>
                      <td>:</td>
                      <td>{{$dc['tema_iklan']}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Created Date</strong>
                      </td>
                      <td>:</td>
                      <td>{{$dc['created_date']}}</td>
                    </tr>
                    <tr>
                      <td class="">
                        <strong>Expired Date</strong>
                      </td>
                      <td>:</td>
                      <td>{{$dc['expired_date']}}</td>
                    </tr>
                  </tbody>
                </table>
                @foreach($dc['photo'] as $photo)
                <div class="gallery col-md-12" style="margin:25px auto 0 auto">
                <figure class="col-md-3">
                <!-- <img src="{{url('file/'.$photo['photo_name'])}}" alt="" class='img-responsive'/> -->
                </figure>
                </div>
                @endforeach
                @endif
                @endforeach
                  </div>

                </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->

</body>
</html>
    
    