    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{asset('assets/img/invoice/squarespace2x.png')}}" data-src="{{asset('assets/img/invoice/squarespace.png')}}" src="{{asset('assets/img/invoice/squarespace2x.png')}}">
                      <address class="m-t-10">
                                     <strong style="font-size:20px"> PT.Maxima Citra Prima </strong>
                                     <strong>Jl.Boulevard Raya no.23</strong>
                                      <br>(021) 412-7753.
                                      <br>
                                  </address>
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Report</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class=""> Description</th>
                          <th></th>
                          <th></th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="">
                            <p class="text-black">Kode Billboard</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">SS112233</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Alamat</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Jl.Kelapa Molek 5 Blok Z2 No1</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Provinsi</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">DKI Jakarta</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Kabupaten</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">KOTA ADM. JAKARTA UTARA</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Kecamatan</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Kelapa Gading</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Panjang</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">200</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Lebar</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">122</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Lighting</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Front Light</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Side</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">2</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Arah Pandang</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Utara</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Format</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Vertical</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Bahan</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Baja Ringan</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Harga</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Rp 200000000</td>
                        </tr>
                      </tbody>
                    </table>
                    {{--  <div class="gallery col-md-12" style="margin:25px auto 0 auto">
                    <figure class="col-md-4">
                      <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                    </figure>
                    <figure class="col-md-4">
                      <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                    </figure>
                    <figure class="col-md-4">
                      <img src="{{asset('assets/img/profiles/2x.jpg')}}" alt=""  width="400" height="280"/>
                    </figure>
                  </div>  --}}
                  </div>

                </div>
              </div>
            </div>
            <!-- END PANEL -->
          </div>
          <!-- END CONTAINER FLUID -->
    