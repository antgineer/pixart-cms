<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <style>
  .cuba-cuba{
    page-break-after: always;
  }
  </style>
</head>
<body>
<!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
    @foreach($data as $dta)
    <!-- START PANEL -->
    <div class="row">
      <div class="col-sm-12">
            <div>
            <div class="pull-left">
              
            <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{public_path('assets/img/logopixartdark.png')}}" data-src="{{public_path('assets/img/logopixartdark.png')}}" src="{{ public_path('assets/img/logopixartdark.png')}}">
              
            </div>
            <div class="pull-right sm-m-t-20">
                  <h2 class="font-montserrat all-caps hint-text">Report</h2>
                </div>
                <div class="clearfix"></div>
            <table class="table">
              <tbody>
                <tr>
                  <td><strong>Billboard Code</strong></td>
                  <td>:</td>
                  <td>{{$dta['kode_billboard']}}</td>
                </tr>
                <tr>
                  <td><strong>Address</strong></td>
                  <td>:</td>
                  <td>{{$dta['address']}}</td>
                </tr>
                <tr>
                  <td><strong>Province</strong></td>
                  <td>:</td>
                  <td>{{$dta['provinsi']['nama_provinsi']}}</td>
                </tr>
                <tr>
                  <td><strong>City</strong></td>
                  <td>:</td>
                  <td>{{$dta['kabupaten']['nama_kabupaten']}}</td>
                </tr>
                <tr>
                  <td><strong>District</strong></td>
                  <td>:</td>
                  <td>{{$dta['kecamatan']['nama_kecamatan']}}</td>
                </tr>
                <tr>
                  <td><strong>Length</strong></td>
                  <td>:</td>
                  <td>{{$dta['panjang']}} meters</td>
                </tr>
                <tr>
                  <td><strong>Width</strong></td>
                  <td>:</td>
                  <td>{{$dta['lebar']}} meters</td>
                </tr>
                <tr>
                  <td><strong>Lighting</strong></td>
                  <td>:</td>
                  <td>{{$dta['lighting']}}</td>
                </tr>
                <tr>
                  <td><strong>Side</strong></td>
                  <td>:</td>
                  <td>{{$dta['side']}} sides</td>
                </tr>
                <tr>
                  <td><strong>Format</strong></td>
                  <td>:</td>
                  <td>{{$dta['format']=='h'?'Horizontal':'Vertical'}}</td>
                </tr>
                <tr>
                  <td><strong>Material</strong></td>
                  <td>:</td>
                  <td>{{$dta['bahan']}}</td>
                </tr>
                <tr>
                  <td><strong>Price</strong></td>
                  <td>:</td>
                  <td>Rp {{$dta['harga']}}</td>
                </tr>
              </tbody>
            </table>
            
            </div>
          </div>
        </div>
    <div class="cuba-cuba"></div>
    <!-- END PANEL -->
@endforeach
          </div>
          <!-- END CONTAINER FLUID -->

</body>
</html>
    
    