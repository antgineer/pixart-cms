<table class="table table-hover" id="tableWithSearch">

</table>

<script>
  $(document).ready(function(){
    var table = $('#tableWithSearch').DataTable({
        "ajax":{
            type : "GET",
            url : "/master/billboard/all/verified",
        },
        "columns": [
            {
                title : 'No',
                data : "index"
            },
            {
              title : "Kode Billboard",
              data : "kode"
            },
            {
                title: "Nama Vendor",
                data: "vendor"
            },
            {
                title: "Provinsi",
                data: "provinsi"
            },
            {
                title: "Kabupaten/Kota",
                data: "kabupaten"
            },
            {
                title: "Kecamatan",
                data: "kecamatan"
            },
            {
                title: "Alamat",
                data: "address"
            },
            {
                title: "Panjang",
                data: "panjang"
            },
            {
                title: "Lebar",
                data: "lebar"
            },
            {
                title: "Format",
                data: "format"
            },
            {
                title: "Bahan",
                data: "bahan"
            },
            {
                title: "Harga",
                data: "harga"
            }
        ]
    });
  });
</script>
