<table class="table table-hover" id="tableWithSearch">
    <thead>
    <tr>
        <th>No</th>
        <th>Kode Billboard</th>
        <th>Address</th>
        <th>Tanggal Mulai</th>
        <th>Valid Thru</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $key=>$row)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $row['kode_billboard'] }}</td>
            {{--  <td>{{ $row['user_fullname'] }}</td>  --}}
            <td>{{ $row['address'] }}</td>
            <td>{{ $row['contract_date'] }}</td>
            <td>{{ $row['expired_date'] }}</td>
            <td>Rp.{{ number_format($row['harga'], '0', ',', '.') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>