<table class="table table-striped">
    <thead>
    <tr>
        <th>No</th>
        <th>Kode Billboard</th>
        {{--  <th>Nama Vendor</th>  --}}
        <th>Provinsi</th>
        <th>Kabupaten</th>
        <th>Kecamatan</th>
        <th>Alamat</th>
        <th>Ukuran</th>
        <th>Lighting</th>
        <th>Format</th>
        <th>Bahan</th>
        <th>Harga</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key=>$row)
    <tr>
        <td>{{ ++$key }}</td>
        <td>{{ $row['kode_billboard'] }}</td>
        {{--  <td>{{ $row['user_fullname'] }}</td>  --}}
        <td>{{ $row['provinsi']['nama_provinsi'] }}</td>
        <td>{{ $row['kabupaten']['nama_kabupaten'] }}</td>
        <td>{{ $row['kecamatan']['nama_kecamatan'] }}</td>
        <td>{{ $row['address'] }}</td>
        <td>{{ $row['panjang'] . ' X ' . $row['lebar'] }}</td>
        <td>{{ $row['lighting'] }}</td>
        <td>{{ $row['format'] == 'h' ? 'Horizontal' : 'Vertikal' }}</td>
        <td>{{ $row['bahan'] }}</td>
        <td>Rp.{{ number_format($row['harga'], '0', ',', '.') }}</td>
    </tr>
    @endforeach
    </tbody>
</table>

<div class="page-break"></div>