{{--  ini kok header ada 2 gini sih !  --}}
{{--  <div class="header ">
    <!-- START MOBILE CONTROLS -->
    <div class="container-fluid relative">
    <!-- LEFT SIDE -->
    <div class="pull-left full-height visible-sm visible-xs">
        <!-- START ACTION BAR -->
        <div class="header-inner">
        <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
            <span class="icon-set menu-hambuger"></span>
        </a>
        </div>
        <!-- END ACTION BAR -->
    </div>
    <div class="pull-center hidden-md hidden-lg">
        <div class="header-inner">
        <div class="brand inline">
            <img src="{{asset('assets/img/logopixartdark.png')}}" alt="logo" data-src="{{asset('assets/img/logopixartdark.png')}}" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" width="78" height="22">
        </div>
        </div>
    </div>
    <!-- RIGHT SIDE -->
    <div class="pull-right full-height visible-sm visible-xs">
        <!-- START ACTION BAR -->
        <div class="header-inner">
        <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
            <span class="icon-set menu-hambuger-plus"></span>
        </a>
        </div>
        <!-- END ACTION BAR -->
    </div>
    </div>
    <!-- END MOBILE CONTROLS -->
    <div class=" pull-left sm-table hidden-xs hidden-sm">
    <div class="header-inner">
        <div class="brand inline">
        <img src="{{asset('assets/img/logopixartdark.png')}}" alt="logo" data-src="{{asset('assets/img/logopixartdark.png')}}" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" width="78" height="22">
        </div>
        <!-- START NOTIFICATION LIST -->
        <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
        </ul>
        <!-- END NOTIFICATIONS LIST -->
    </div>
    </div>
    <div class=" pull-right">
    <div class="header-inner">
    </div>
    </div>
    <div class=" pull-right">
    <!-- START User Info-->
    <div class="visible-lg visible-md m-t-10">
        <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
        <span class="semi-bold">Hello,</span> <span class="text-master">{{ session('fullname') }} </span>
        </div>
        <div class="dropdown pull-right">
        <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="thumbnail-wrapper d32 circular inline m-t-5">
            <img src="{{ (session('photo') != null) ? route('download', session('photo')) : asset('assets/img/profiles/2x.jpg') }}" alt="" data-src="{{ (session('photo') != null) ? route('download', session('photo')) : asset('assets/img/profiles/2x.jpg') }}" data-src-retina="{{ (session('photo') != null) ? route('download', session('photo')) : asset('assets/img/profiles/2x.jpg') }}" width="32" height="32">
            </span>
        </button>
        <ul class="dropdown-menu profile-dropdown" role="menu">
            <li class="bg-master-lighter">
            <a href="{{route('logout')}}" class="clearfix">
                <span class="pull-left">Logout</span>
                <span class="pull-right"><i class="pg-power"></i></span>
            </a>
            </li>
        </ul>
        </div>
    </div>
    <!-- END User Info-->
    </div>
</div>  --}}

<div class="header ">
    <!-- START MOBILE CONTROLS -->
    <div class="container-fluid relative">
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
        <!-- START ACTION BAR -->
        <div class="header-inner">
            <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
            <span class="icon-set menu-hambuger"></span>
            </a>
        </div>
        <!-- END ACTION BAR -->
        </div>
        <div class="pull-center hidden-md hidden-lg">
        <div class="header-inner">
            <div class="brand inline">
            <img src="{{asset('assets/img/logopixartdark.png')}}" alt="logo" data-src="{{asset('assets/img/logopixartdark.png')}}" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" width="120" height="30">
            </div>
        </div>
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
        <!-- START ACTION BAR -->
        <div class="header-inner">
            <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
            <span class="icon-set menu-hambuger-plus"></span>
            </a>
        </div>
        <!-- END ACTION BAR -->
        </div>
    </div>
    <!-- END MOBILE CONTROLS -->
    <div class=" pull-left sm-table hidden-xs hidden-sm">
        <div class="header-inner">
        <div class="brand inline">
            <img src="{{asset('assets/img/logopixartdark.png')}}" alt="logo" data-src="{{asset('assets/img/logopixartdark.png')}}" data-src-retina="{{asset('assets/img/logopixartdark.png')}}" width="120" height="22">
        </div>
        <!-- START NOTIFICATION LIST -->
        <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
            <li class="p-r-15 inline">
            <div class="dropdown">
                <a href="javascript:;" id="notification-center" class="icon-set globe-fill" data-toggle="dropdown">
                @if($notification['total_unread'] > 0)
                    <span class="bubble-notif"><span>{{$notification['total_unread']}}</span></span>
                @endif
                </a>
                <!-- START Notification Dropdown -->
                <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                <!-- START Notification -->
                <div class="notification-panel">
                    <!-- START Notification Body-->
                    <div id="notification-body" class="notification-body scrollable"></div>
                    <!-- END Notification Body-->
                    <!-- START Notification Footer-->
                    <div class="notification-footer text-center">
                    <a href="{{route('notification.all')}}" class="">Read all notifications</a>
                    <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                        <i class="pg-refresh_new"></i>
                    </a>
                    </div>
                    <!-- START Notification Footer-->
                </div>
                <!-- END Notification -->
                </div>
                <!-- END Notification Dropdown -->
            </div>
            </li>
        </ul>
        <!-- END NOTIFICATIONS LIST -->

        </div>
    </div>
    <div class=" pull-right">
        <div class="header-inner">
        </div>
    </div>
    <div class=" pull-right">
        <!-- START User Info-->
        <div class="visible-lg visible-md m-t-10">
        <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            <span class="semi-bold">Hello,</span> <span class="text-master">{{ session('fullname') }}</span>
        </div>
        <div class="dropdown pull-right">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="thumbnail-wrapper d32 circular inline m-t-5">
            <img src="{{ (session('photo') != null) ? route('download', session('photo')) : asset('assets/img/profiles/2x.jpg') }}" alt="" data-src="{{ (session('photo') != null) ? route('download', session('photo')) : asset('assets/img/profiles/2x.jpg') }}" data-src-retina="{{ (session('photo') != null) ? route('download', session('photo')) : asset('assets/img/profiles/2x.jpg') }}" width="32" height="32">
        </span>
            </button>
            <ul class="dropdown-menu profile-dropdown" role="menu">
            <li class="bg-master-lighter">
                <a href="{{route('logout')}}" class="clearfix">
                <span class="pull-left">Logout</span>
                <span class="pull-right"><i class="pg-power"></i></span>
                </a>
            </li>
            </ul>
        </div>
        </div>
        <!-- END User Info-->
    </div>
</div>
