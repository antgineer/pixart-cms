<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
<!-- START PANEL -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                <img width="235" height="47" alt="company logo" class="invoice-logo" data-src-retina="assets/img/invoice/squarespace2x.png" data-src="assets/img/invoice/squarespace.png" src="assets/img/invoice/squarespace2x.png"/>
                <address class="m-t-10">
                    <span style="font-weight:bold;font-size:25px;color:black">PT Maxima Citra Prima</span>
                    <br><span style="font-weight:bold;font-size:15px;color:black">Jalan Abdullah No.01, RT.001/RW.001, Kayumanis, Bogor Utara</span>
                    <br><span style="font-weight:bold;font-size:14px;color:black">(021) 111222333.</span>
                    <br>
                </address>
                <br>
                <br>
                <div>
                        <p class="small hint-text">Kode Billboard</p>
                        <h2 class="font-montserrat all-caps hint-text">SS11233</h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- START MAP -->
                    <div class="panel panel-transparent" style="border-bottom-width: 0px;margin-bottom: 15px;">
                        
                        <div class="panel-body" style="padding-left: 0px;padding-right: 0px; padding-bottom: 0px;">
                            <div id="map" style="width: 100%; height: 300px;"></div>
                        </div>
                    </div>
                    <!-- END MAP -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <table class="table">
                      <thead>
                        <tr>
                          <th class=""> Description</th>
                          <th></th>
                          <th></th>
                          <th class="text-center">Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="">
                            <p class="text-black">Alamat</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Jl.Kelapa Molek 5 Blok Z2 No1</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Provinsi</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">DKI Jakarta</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Kabupaten</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">KOTA ADM. JAKARTA UTARA</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Kecamatan</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Kelapa Gading</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Panjang</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">200</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Lebar</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">122</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Lighting</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Front Light</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Side</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">2</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Arah Pandang</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Utara</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Format</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Vertical</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Bahan</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Baja Ringan</td>
                        </tr>
                        <tr>
                          <td class="">
                            <p class="text-black">Harga</p>
                          </td>
                          <td></td>
                          <td></td>
                          <td class="text-center">Rp 200000000</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                <img src="http://via.placeholder.com/730x250" class="img-thumbnail" alt="Cinque Terre">
                <img src="http://via.placeholder.com/350x250" class="img-thumbnail" alt="Cinque Terre"/>
                <img src="http://via.placeholder.com/340x250" class="img-thumbnail" alt="Cinque Terre"/>
                <img src="http://via.placeholder.com/730x250" class="img-thumbnail" alt="Cinque Terre"> 
                <img src="http://via.placeholder.com/400x150" class="img-thumbnail" alt="Cinque Terre"/>
                <img src="http://via.placeholder.com/290x150" class="img-thumbnail" alt="Cinque Terre"/>
                               
                </div>
            </div>
        </div>
    </div>
<!-- END PANEL  -->
</div>