<!DOCTYPE html>
<html>
  <head>
    @routes
    <script src="{{asset('assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
    @includeif('includes.head')

    @stack('style')
  </head>
  <body class="fixed-header dashboard">
    <!-- LEFT SIDEBAR -->
		@yield('sidebar')
		<!-- END LEFT SIDEBAR -->

    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      <!-- START HEADER -->
      @includeif('includes.header')
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter" style="padding-top: 50px;">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
            @yield('content')
          </div>
        </div>
        @includeif('includes.footer')
      </div>

    @stack('script')
    @includeif('includes.script')
  </body>
</html>
