<!DOCTYPE html>
<html>
  <head>
    @routes
    {{-- <script src="{{asset('assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    @includeif('includes.head')

    @stack('style')
  </head>
  <body class="fixed-header ">
    <!-- BEGIN SIDEBPANEL-->
        @yield('sidebar')
    <!-- END SIDEBPANEL-->
    <!-- START PAGE-CONTAINER -->

    <div class="page-container ">
      <!-- START HEADER -->
      @includeif('includes.header')
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        
          @yield('content')  
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        @includeif('includes.footer')
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    
    @stack('script')
    @includeif('includes.script-contract')
  </body>
</html>
